const stripeDetails = require('../.lib/stripe-details')
const stripe = require('stripe')(stripeDetails.secretKey, {
  apiVersion: '2019-08-14'
})
stripe.setTelemetryEnabled(false)

//
// Refactor: also used in HTTPS GET route. Pull out.
//

const moment = require('moment')

// Converts a Stripe (Unix) timestamp string to a JavaScript date object.
// (JS expects the timestamp in miliseconds so we pad three decimal places).
function timestampToDate(timestamp) {
  return new Date(parseInt(`${timestamp}000`))
}

function lowercaseFirstLetter (s) {
  if (typeof s !== 'string') return ''
  return s.charAt(0).toLowerCase() + s.slice(1)
}

function parseTimestamp(timestamp) {
  const timestampAsDate = timestampToDate(timestamp)
  const dateAsMoment = moment(timestampAsDate)
  return {
    relative: dateAsMoment.fromNow(),
    absolute: lowercaseFirstLetter(dateAsMoment.calendar())
  }
}


module.exports = (webSocket, request) => {
  webSocket.on('message', async data => {

    const command = JSON.parse(data)

    console.log(`   🤗    ❨Patronage❩ ${command.action} ${command.itemId} ${command.action === 'update' ? `(to €${command.amount}/month)` : ''}`)

    let result

    switch (command.action) {

      case 'update':
        // Update the patronage amount, starting with the next payment (not prorated).
        try {
          result = await stripe.subscriptions.update(
            command.subscriptionId,
            {
              prorate: false,
              items: [
                {
                  id: command.itemId,
                  plan: command.planId,
                  quantity: command.amount
               }
              ]
            }
          )
        } catch (error) {
          webSocket.send(JSON.stringify({for: command.action, error}))
        }
        webSocket.send(JSON.stringify({for: command.action, amount: result.quantity}))
      break;

      case 'pause':
        // Pausing a subscription is not a native concept within the Stripe API. Instead, we implement
        // one of the recommended strategies for obtaining that outcome by applying a 100% discount coupon.
        try {
          result = await stripe.subscriptions.update(
            command.subscriptionId,
            {
              prorate: false,
              coupon: stripeDetails.pausePatronageCouponId
            }
          )
        } catch (error) {
          webSocket.send(JSON.stringify({for: command.action, error}))
        }
        webSocket.send(JSON.stringify({for: command.action, coupon: result.coupon}))
      break;

      case 'resume':
        // A paused subscription (see above) is resumed by deleting the 100% subscription discount.
        try {
          result = await stripe.subscriptions.deleteDiscount(command.subscriptionId)
        } catch (error) {
          webSocket.send(JSON.stringify({for: command.action, error}))
        }
        webSocket.send(JSON.stringify({for: command.action}))
      break;

      case 'cancel':
        // Cancel the subscription.
        try {
          result = await stripe.subscriptions.del(command.subscriptionId)
        } catch (error) {
          webSocket.send(JSON.stringify({for: command.action, error}))
        }
        webSocket.send(JSON.stringify({
          for: command.action,
          cancelledAt: parseTimestamp(result.canceled_at)
        }))
      break;

      default:
        const error = new Error(`Unknown command received: ${command.action}`)
        console.log(error)
        webSocket.send(JSON.stringify({for: command.action, error }))
    }
  })
}
