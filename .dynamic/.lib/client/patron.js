const $ = document.querySelector.bind(document)

let socket = null

window.addEventListener('load', _ => {

  const Modal = Swal

  connect()

  //
  // Connectivity.
  //

  function connect () {
    socket = new WebSocket(`wss://${window.location.hostname}/patron`)

    // Handle connectivity.
    socket.onopen = _ => {
      enableActions()
      $('#offline-message').hidden = true
    }

    socket.onclose = _ => {
      enableActions(false)
      $('#offline-message').hidden = false

      // Try to reconnect.
      const reconnectionAttemptIntervalId = setInterval(_ => {
        if (socket.readyState === WebSocket.CLOSED) {
          connect()
        } else {
          clearInterval(reconnectionAttemptIntervalId)
        }
      }, 3000)
    }

    socket.onerror = error => {
      console.log('Connection error', error)
    }

    // Display received messages.
    socket.onmessage = message => {
      response = JSON.parse(message.data)

      if (response.error) {
        Modal.fire('Server error', response.error, 'error')
        return
      }

      switch (response.for) {

        case 'update':
          $('#update-button').value = 'Update my patronage'
          $('#update-button').disabled = false
          $('#patronageAmount').value = response.amount
          $('#patronageAmountDisplay').innerHTML = response.amount
          Modal.fire(
            'Patronage updated',
            `Your patronage has been updated to €${response.amount}/month, starting with your next payment. Thank you for supporting us.`,
            'success'
          )
        break

        case 'pause':
          $('#pause-your-patronage').hidden = true
          $('#pause-button').value = 'Pause my patronage'
          $('#pause-button').disabled = false
          $('#resume-your-patronage').hidden = false
          $('#patronage-status').innerHTML = 'paused'
          $('#next-payment-date').hidden = true
          Modal.fire(
            'Patronage paused',
            `Your patronage has been paused effective immediately. You can resume it again at any time. Thank you for supporting us.`,
            'success'
          )
        break

        case 'resume':
          $('#resume-your-patronage').hidden = true
          $('#resume-button').value = 'Resume my patronage'
          $('#resume-button').disabled = false
          $('#pause-your-patronage').hidden = false
          $('#patronage-status').innerHTML = 'active'
          $('#next-payment-date').hidden = false
          Modal.fire(
            'Patronage resumed',
            `Your patronage has been resumed effective immediately. You can pause it again at any time. Thank you for supporting us.`,
            'success'
          )
        break

        case 'cancel':
          $('#cancel-button').value = 'Cancel my patronage'
          $('#cancel-button').disabled = false
          $('#active-patronage').hidden = true
          $('#cancelled-patronage').hidden = false
          $('#cancelled-at-relative').innerHTML = response.cancelledAt.relative
          $('#cancelled-at-absolute').innerHTML = response.cancelledAt.absolute
          Modal.fire(
            'Patronage canceled',
            `Your patronage has been canceled effective immediately. If you want to become a patron again in the future, please visit our <a href='/fund-us'>Fund Us</a> page. Thank you again for supporting us.`,
            'success'
          )
        break
      }
    }

  }

  function enableActions (enabled = true) {
    ['update', 'pause', 'resume', 'cancel'].forEach(action => $(`#${action}-fields`).disabled = !enabled)
  }

  //
  // Interface.
  //

  async function confirmCancellation () {
    // return window.confirm('Are you sure you want to cancel your patronage?')
    return (await Modal.fire({
      title: 'Are you sure you want to cancel your patronage?',
      text: "",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    })).value
  }

  function handleCommand (commandName) {
    $(`#${commandName}`).addEventListener('submit', async event => {
      event.preventDefault()

      const command = {
        action: commandName,
        subscriptionId,
        planId,
        itemId
      }

      // Sanity check: socket must be open.
      // (Note: our validation should have caught this by this stage and it’s a bug if it hasn’t.)
      if (socket.readyState !== WebSocket.OPEN) {
        Modal.fire('Not connected', `Sorry, we cannot ${command.action} your patronage right now as we cannot connect to the server.`, 'error')
        return
      }

      if (commandName === 'update') {
        command.amount = $('#patronageAmount').value
        $('#update-button').value = 'Updating…'
        $('#update-button').disabled = true
      }

      if (commandName === 'pause') {
        $('#pause-button').value = 'Pausing…'
        $('#pause-button').disabled = true
      }

      if (commandName === 'resume') {
        $('#resume-button').value = 'Resuming…'
        $('#resume-button').disabled = true
      }

      let proceed = true
      if (commandName === 'cancel') proceed = await confirmCancellation()

      if (!proceed) return

      if (commandName === 'cancel') {
        $('#cancel-button').value = 'Cancelling…'
      }

      try {
        socket.send(JSON.stringify(command))
      } catch (error) {
        console.log('Cannot send message', error)
        Modal.fire('Cannot send message', error, 'error')
      }
    })
  }

  handleCommand('update')
  handleCommand('pause')
  handleCommand('resume')
  handleCommand('cancel')
})
