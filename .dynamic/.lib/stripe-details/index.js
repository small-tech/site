////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Returns Stripe details (secret key and the coupon ID for pausing a patronage; i.e., a 100% off coupon)
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const os = require('os')
const path = require('path')
const fs = require('fs')

const isRunningInProduction = (process.env.NODE_ENV === 'production')
const specificallyAskedToUseLiveStripeKey = (process.env.stripe !== undefined && process.env.stripe === 'live')

let secretKey = null
let pausePatronageCouponId = null

const logKeyType = (keyType) => console.log(`   🐖    ❨Patronage❩ WebSocket: using ${keyType} Stripe API key.`)

if (isRunningInProduction || specificallyAskedToUseLiveStripeKey) {
  // Use live key.
  logKeyType('LIVE')
  secretKey = fs.readFileSync(path.join(os.homedir(), '.small-tech.org', 'stripe-live-api.key'), 'utf-8').trim()
  pausePatronageCouponId = 'RIuGywiD'
} else {
  // Use test key.
  logKeyType('test')
  secretKey = 'sk_test_GT9DfvDjhljTdIe3rKyEHtyU00RVQ0FCNP'
  pausePatronageCouponId = 'VKQ30S0b'
}

const stripeDetails = {
  secretKey,
  pausePatronageCouponId
}

// console.log('Stripe details', stripeDetails)

module.exports = stripeDetails
