const path = require('path')
const fs = require('fs')
const moment = require('moment')

const stripeDetails = require('../../.lib/stripe-details')
const stripe = require('stripe')(stripeDetails.secretKey, {
  apiVersion: '2019-08-14'
})
stripe.setTelemetryEnabled(false)

// Ensure that included template materials are always referenced from the
// path of this file to ensure the app does not break regardless of which directory
// the server is started from.
const fromSiteRoot = pathToResolve => path.join(__dirname, '../../../', pathToResolve)
const fromClientFolder = pathToResolve => path.join(__dirname, '../../.lib/client/', pathToResolve)

const smallTechnologySiteTemplate = fs.readFileSync(fromSiteRoot('.generated/template/index.html'), 'utf-8')
const htmlTemplate = fs.readFileSync(fromClientFolder('patron.html'))
const errorTemplate = fs.readFileSync(fromClientFolder('patron-error.html'))

// Template data; do not remove.
// (Unused here but will be substituted in the template.)
const clientSideJavaScript = fs.readFileSync(fromClientFolder('patron.js'))
const sweetAlert2 = fs.readFileSync(fromClientFolder('sweetalert2.min.js'))
const styles = fs.readFileSync(fromClientFolder('patron.css'))

//
// Refactor: also used in WebSocket route. Pull out.
//

// Converts a Stripe (Unix) timestamp string to a JavaScript date object.
// (JS expects the timestamp in miliseconds so we pad three decimal places).
function timestampToDate(timestamp) {
  return new Date(parseInt(`${timestamp}000`))
}

function lowercaseFirstLetter (s) {
  if (typeof s !== 'string') return ''
  return s.charAt(0).toLowerCase() + s.slice(1)
}

function parseTimestamp(timestamp) {
  const timestampAsDate = timestampToDate(timestamp)
  const dateAsMoment = moment(timestampAsDate)
  return {
    relative: dateAsMoment.fromNow(),
    absolute: lowercaseFirstLetter(dateAsMoment.calendar())
  }
}

module.exports = async (request, response, next) => {

  function render (template) {
    return eval('`' + template + '`')
  }

  function patronageNotFoundError (error) {
    console.log(error)

    const main = eval('`' + errorTemplate + '`')
    const html = eval('`' + smallTechnologySiteTemplate + '`')
    response.type('html').end(html)
  }

  const sessionId = request.params.id

  // TODO: Document this edge case in the Site.js docs.
  if (sessionId === '.websocket') {
    // This is a web socket request, leave it be.
    next()
    return
  }

  const isSubscriptionId = sessionId.startsWith('sub_')
  const isSessionId = sessionId.startsWith('cs_live_') || sessionId.startsWith('cs_test_')
  const isInvalidId = !isSubscriptionId && !isSessionId
  if (isInvalidId) {
    return patronageNotFoundError('\n   ❌    ❨Patronage❩ Error: ID with invalid syntax passed to patronage page.\n')
  }

  let subscription  = null
  let patronName = null

  try {
    if (sessionId.startsWith('sub_')) {
      // This is a subscription. More than likely , the person did not bookmark the URL and we
      // sent them this so they can manage their subscription.
      const subscriptionId = sessionId
      subscription = await stripe.subscriptions.retrieve(subscriptionId, {
        expand: ['default_payment_method']
      })
    } else {
      // Get the subscription id from the session.
      const session = await stripe.checkout.sessions.retrieve(sessionId, {
        expand: ['subscription', 'subscription.default_payment_method']
      })
      subscription = session.subscription
    }
    patronName = subscription.default_payment_method.billing_details.name
  } catch (error) {
    return patronageNotFoundError(`\n   ❌    ❨Patronage❩ ${error.type}: ${error.message}\n`)
  }

  const patronFirstName = patronName.slice(0, patronName.indexOf(' '))

  const subscriptionId = subscription.id
  const shortLink = `https://small-tech.org/patron/${subscriptionId}`
  const subscriptionStatus = subscription.status

  const itemId = subscription.items.data[0].id
  const planId = subscription.plan.id

  const patronSince = parseTimestamp(subscription.billing_cycle_anchor)
  const currentPeriodStart = parseTimestamp(subscription.current_period_start)
  const currentPeriodEnd = parseTimestamp(subscription.current_period_end)

  let cancelledAt = {relative: null, absolute: null}
  if (subscription.canceled_at !== null) {
    cancelledAt = parseTimestamp(subscription.canceled_at)
  }

  // Template data; do not remove.
  // (Unused here but will be substituted in the template.)
  const patronageAmount = subscription.quantity
  const isActive = subscriptionStatus === 'active'
  const isPaused = subscription.discount !== null
  const patronageStatus = isActive ? (isPaused ? 'paused' : 'active') : subscriptionStatus

  // Newly-created?
  let newPatronMessage = ''
  // TODO: Base the display of this message on whether it is 1+ day after the subscription was created or not.
  //   if (request.query.action === 'created') {
  //   newPatronMessage = `
  //     <h2>Thank you for becoming a patron!</h2>

  //     <p><strong>Please bookmark this page now.</strong> You can return to this page at any time to update your patronage settings or to cancel your patronage. Please do not lose this address and do not share it with anyone else.</p>

  //     <p>(It’s not the end of the world if you do lose it – just shoot us an email at <a href='mailto:support@small-tech.org'>support@small-tech.org</a> and we can help you out.)</p>
  //   `
  // }

  // Template data; do not remove.
  // (Unused here but will be substituted in the template.)
  const main = render(htmlTemplate)

  const html = render(smallTechnologySiteTemplate)
  response.type('html').end(html)
}
