---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
categories: ["Updates"] # Updates or Labs
author: "" # Aral Balkan or Laura Kalbag (or another writer if article etc)
image: "" # file name relative to this page
postURL: "https://" # URL for the post
sourceURL: "https://ar.al/" # URL for the site the post is on
sourceName: "Aral’s blog" # Name of the site the post is on
---