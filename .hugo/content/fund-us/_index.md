---
title: "Fund Us"
date: 2019-06-12T09:28:43+01:00
menu:
 main:
  weight: 9
meta_title: "Fund Small Technology Foundation"
description: "We’re a tiny team of two funded by individuals like you. Your patronage goes towards keeping a roof over our heads as we build a better future for the Internet."
---

# Fund Us

__If you’re happy we exist, please help us continue to exist.__

We’re [a tiny team of two](/about) funded by individuals like you. Your patronage helps us work on [building the Small Web](/research-and-development).

{{< fund-us-form >}}

### Current patronage

When we last checked on November 16, 2024, we had 42 patrons, supporting us with €403/month (on average, ~€9.60 per patron).

This currently helps us cover our web hosting costs.

### Where your donation goes

We’re [a tiny team of two people](/about#the-foundation). Our work at Small Technology Foundation pays our rent and buys dog food.

### Past funding

As of 2024, we’ve been going – first as [Ind.ie](https://ind.ie) in the UK and now as Small Technology Foundation in Ireland – for the past ten years.

In 2014, we were initially funded by a round of crowdfunding.

Since then, Aral has sold three family homes to keep us going (his family have no more homes to sell).

As of 2021, Laura started contracting – [first with Stately](https://laurakalbag.com/working-with-stately/) and, currently, with [Penpot](https://penpot.app), to keep us afloat financially while Aral works on building [Kitten](https://kitten.small-web.org), [Domain](https://codeberg.org/domain/app), and [Place](https://codeberg.org/place/app) as the three components of the Small Web.

### Our sustainability (not growth) plan

We are building [small-web.org](https://small-web.org) as the first of what we hope will become many interoperable Small Web hosts running [Domain](https://codeberg.org/domain/app). Our plan is to sustain the foundation by charging a little more than what we pay our upstream providers.

Eventually, we hope that institutions like the EU will see that our work benefits the commons and fund us from the commons. But we’re not holding our breaths for that just yet.

Our goal is not to scale Small Technology Foundation itself.

In fact, we are specifically designing our tools so that if they’re successful, we don’t scale alongside them but that we foster a community of small sustainable organisations around the world.

That said, it would be wonderful if one day we could afford to get someone to handle the administrative work so both of us can focus all our time on research and development. One day we’d also like to be able to work with other people from the free and open source community and be able to pay them for their time.

Finally, [we speak at a large number of events](/events) to advocate for small technology. These include platforms such as the Nobel Peace Center, The Royal Society of Arts, British Parliament, and European Parliament as well as industry conferences and events. Our work also gets covered in the media. Your donations give us the freedom to carry out this advocacy work.

[We don’t take venture capital](https://ind.ie/ethical-design) and we don’t have the resources to undertake the lengthy application processes for government and EU funding so we need your individual support to help subsidise our work until we can sustain ourselves.

### Other ways to support us

#### Pay us to speak at your events

We get asked to speak at a large number of events. Sadly, many of these inquiries do not pay an honorarium. Paying us for speaking engagements helps us pay the rent. (Honorariums we receive go directly to Small Technology Foundation, not to us personally.)

We have a state-of-the-art studio setup at our home office as well as a 1Gbit fibre connection so we can present virtually. You can find [recordings of past talks](/videos) on our [Videos page](/videos).

#### Buy Laura’s book on accessibility

Laura invests royalties from her book [Accessibility For Everyone](https://accessibilityforeveryone.site) in Small Technology Foundation.

#### Help us apply for funding

If you know of any funding opportunities and can help us apply for them (we don’t have the resources to undertake lengthy bureaucratic funding application processes), we’d be hugely grateful.

_(We’ve never received a public grant even though all our work is free and open and for the common good. It would be nice to change that!)_

#### Invest in us

We are a not-for-profit based in Ireland that is limited by guarantee. Apart from equity investment (which we cannot accept, since we don’t have share capital), we are open to investment ideas that will maintain our independence and allow us to protect the integrity of our social mission (e.g., share of revenue).

If you want to invest in the ongoing, sustainable success of Small Technology Foundation, instead of our sale in an exit (which cannot happen), please <a href='/contact-us'>get in touch with us</a>.

#### Tell your friends

Tell your friends about us. Help us spread the word.

_Your support has kept us going and gotten us to where we are today. It hasn’t been an easy journey but we would not have been able to get here without being able to continuously iterate on the problem for the past eight years._
