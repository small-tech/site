---
title: "Thank you for your one-time donation!"
date: 2019-06-12T09:28:43+01:00
meta_title: "Fund Small Technology Foundation"
description: "We’re a tiny team of two funded by individuals like you. Your patronage goes towards keeping a roof over our heads as we build a better future for the Internet."
---

# Thank you for your one-time donation!

We really appreciate your support.
