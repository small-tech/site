
/**
  Patronage form.

  Enables one-time and recurring donations by card via Stripe and
  one-time donations in Ethereum (with has moved to proof of stake and
  does not destroy the environment like Bitcoin or other proof-of-work
  currencies.)

  Copyright ⓒ 2019-present Aral Balkan, Small Technology Foundation
*/

// Globals

// Convenience method for iterating NodeList instances like Arrays.
NodeList.prototype.forEach = Array.prototype.forEach

// Shorthand references for DOM lookup.
const $ = document.querySelector.bind(document)
const $$ = document.querySelectorAll.bind(document)

// Stripe Payment
class Payment {
  static get UNKNOWN_HOST_ERROR () { return `unknown host (not specified as test host or live host): ${this.host}` }
  static get liveHost () { return 'small-tech.org' }
  static get testKey () { return 'pk_test_mLQRpGuO7qq3XMfSgwmt4n8U00FSZOIY1h' }
  static get liveKey () { return 'pk_live_CYYwSVoh2kC4XcTPCudVIocg005StHQ47e' }


  constructor () {
    this.host = window.location.hostname
  }

  // Instance properties.

  get stripe () {
    if (this._stripe === undefined) {
      this._stripe = Stripe(this.publicKey)
    }
    return this._stripe
  }

  get isTestHost () { return !this.isLiveHost }
  get isLiveHost () { return this.host === Payment.liveHost }

  get publicKey () {
    if (this.isTestHost) return Payment.testKey
    if (this.isLiveHost) return Payment.liveKey
    throw new Error(Payment.UNKNOWN_HOST_ERROR)
  }

  get items () {
    if (this.isTestHost) {
       return this.payment.isPatronage ?
        [{plan: 'plan_FSsO2vwva5oEOP', quantity: this.payment.amount}] :
        [{sku: 'sku_FVm0elVvrMW0sX', quantity: this.payment.amount}]
    } else if (this.isLiveHost) {
      return this.payment.isPatronage ?
        [{plan: 'plan_FeugOFuPMFBfHv', quantity: this.payment.amount}] :
        [{sku: 'sku_FeueGkHgSH1h09', quantity: this.payment.amount}]
    } else {
      throw new Error(Payment.UNKNOWN_HOST_ERROR)
    }
  }

  get successUrl () {
    return this.payment.isPatronage ?
      `https://${this.host}/patron/{CHECKOUT_SESSION_ID}` :
      `https://${this.host}/fund-us/thank-you`
  }

  get cancelUrl () { return `https://${this.host}/fund-us/` }

  get stripePaymentDetails () {
    return {
      items: this.items,
      successUrl: this.successUrl,
      cancelUrl: this.cancelUrl
    }
  }

  async redirectToCheckout (payment) {
    this.payment = payment
    return await this.stripe.redirectToCheckout(this.stripePaymentDetails)
  }
}


class PatronageForm {
  static get CUSTOM_DONATION_AMOUNT () { return -1 }

  constructor () {
    window.addEventListener('load', this.setInitialInterfaceState.bind(this))
    this.payment = new Payment()
    this.currentlySelectedPaymentType = 'card'
  }

  async setInitialInterfaceState () {
    // Store references to the interface elements.
    this.interface = {
      patronageForm: $('#patronageForm'),
      ethereum: $('#ethereum'),
      cardPayment: $('#cardPayment'),
      submitButton: $('#submitButton'),
      customDonationAmountTextInput: $('#customDonationAmount'),
      customDonationAmountRadioButton: $('#tier8'),
      serverError: $('#serverError'),
      errorMessage: $('#errorMessage'),
      testMessage: $('#testMessage'),

      donationTypeRadioButtons: $$('input[name="donationType"]'),
      donationAmountRadioButtons: $$('input[name="donationAmount"]'),
      paymentTypeRadioButtons: $$('input[name="paymentType"]'),

      selectedDonationTypeButton: _ => document.querySelector('input[name="donationType"]:checked'),
      selectedDonationAmountButton: _ => document.querySelector('input[name="donationAmount"]:checked'),
      selectedPaymentTypeButton: _ => document.querySelector('input[name="paymentType"]:checked')
    }

    // If we’re running in test mode, display the test message.
    if (this.payment.isTestHost) {
      this.interface.testMessage.classList.remove('displayNone');
    }

    //
    // Handle all interface events in a single state update method.
    //

    // Ensure that passing a reference to the method keeps its instance reference (”this”).
    const updateFormState = this.updateFormState.bind(this)

    this.interface.donationTypeRadioButtons.forEach(element => {
      element.addEventListener('change', updateFormState)
    })

    this.interface.donationAmountRadioButtons.forEach(element => {
      element.addEventListener('change', updateFormState)
    })

    this.interface.paymentTypeRadioButtons.forEach(element => {
      element.addEventListener('change', updateFormState)
    })

    // Ensure that selecting either the button or the text input focuses the custom
    // amount control and that focus is not lost when person clicks on already-selected control.
    this.interface.customDonationAmountRadioButton.addEventListener('click', updateFormState)
    this.interface.customDonationAmountTextInput.addEventListener('click', updateFormState)
    this.interface.customDonationAmountTextInput.addEventListener('focus', updateFormState)
    this.interface.customDonationAmountTextInput.addEventListener('input', updateFormState)

    //
    // Handle form submit requests.
    //

    const handleSubmitRequest = event => {
      event.preventDefault()
      if (this.currentlySelectedPaymentType === 'ethereum') {
        // Ethereum payments do not require a form submit.
        return
      }
      if (this.formIsValid) {
        this.redirectToCheckout()
      }
    }

    patronageForm.addEventListener('submit', event => { handleSubmitRequest(event) })
    document.addEventListener('keypress', event => { if (event.keyCode == 13) { handleSubmitRequest(event) } })

    // Update the form state for the first time.
    this.updateFormState()
  }

  // Updates form state in response to interface events.
  updateFormState () {
    const customDonationAmountTextInput = this.interface.customDonationAmountTextInput

    // If the donation amount text input has focus, ensure that the corresponding
    // radio button is also selected.
    if (customDonationAmountTextInput === document.activeElement) {
      this.interface.customDonationAmountRadioButton.checked = true
    }

    // Handle the state of the custom donation amount control.
    if (this.interface.selectedDonationAmountButton().id === 'tier8') {
      // Custom donation is active
      customDonationAmountTextInput.style.opacity = 1
      customDonationAmountTextInput.focus()
      let customDonationAmountString = customDonationAmountTextInput.value
      customDonationAmountString = customDonationAmountString.slice(0,6)      // Limit to six digits.
      let customDonationAmountInteger = parseInt(customDonationAmountString)  // Ensure it is a valid integer.
      customDonationAmountTextInput.value = isNaN(customDonationAmountInteger) ? '' : customDonationAmountInteger
    } else {
      // Clear the other donation amount entry if it is not valid and
      // it isn’t selected.
      if (!this.customDonationAmountIsValid) {
        this.interface.customDonationAmountTextInput.value = ''
      }
      // De-emphasize the text input when the control is not in focus.
      this.interface.customDonationAmountTextInput.style.opacity = 0.5
    }

    // Handle payment type tab selection.
    if (this.currentlySelectedPaymentType === 'card' && this.interface.selectedPaymentTypeButton().id === 'paymentTypeEthereum') {
      // Ethereum payment.
      this.interface.ethereum.classList.remove('hidden')
      this.interface.cardPayment.classList.add('hidden')
      this.currentlySelectedPaymentType = 'ethereum'
    } else if (this.currentlySelectedPaymentType === 'ethereum' && this.interface.selectedPaymentTypeButton().id === 'paymentTypeCard') {
      // Card payment.
      this.interface.ethereum.classList.add('hidden')
      this.interface.cardPayment.classList.remove('hidden')
      this.currentlySelectedPaymentType = 'card'
    }

    // Handle the label of the submit button based on the type of donation.
    // Note: we do this after the payment type checks as they can change the state of the form
    // ===== and affect the result.
    this.interface.submitButton.innerHTML = this.isPatronage ? 'Become a patron' : 'Donate'

    // Enable/disable the submit button based on whether the form is valid.
    this.interface.submitButton.disabled = !this.formIsValid
  }


  async redirectToCheckout () {
    const paymentResult = await this.payment.redirectToCheckout({
      isPatronage: this.isPatronage,
      amount: this.donationAmount
    })

    if (paymentResult.error) {
      showError(result.error.message)
    }
  }


  get formIsValid () {
    try { this.donationType } catch (error) { return false }
    try { this.donationAmount } catch (error) { return false }
    return true
  }


  get isPatronage () {
    return this.donationType === 'monthly'
  }


  get donationType () {
    const selectedDonationTypeButton = this.interface.selectedDonationTypeButton()
    if (selectedDonationTypeButton !== null) {
      return selectedDonationTypeButton.value
    } else {
      throw new Error('donation type is invalid')
    }
  }


  // Returns the donation amount from the form.
  get donationAmount () {
    const selectedDonationAmountButton = this.interface.selectedDonationAmountButton()

    if (selectedDonationAmountButton !== null) {
      const amount = Number(selectedDonationAmountButton.value)
      return (amount === PatronageForm.CUSTOM_DONATION_AMOUNT) ? this.customDonationAmount : amount
    } else {
      throw new Error('donation amount is invalid')
    }
  }


  get customDonationAmount () {
    const amount = Number(this.interface.customDonationAmountTextInput.value)
    if (amount > 0) {
      return amount
    } else {
      throw new Error('invalid custom donation amount')
    }
  }


  get customDonationAmountIsValid () {
    return Number(this.interface.customDonationAmountTextInput.value) > 0
  }


  showError (error) {
    var serverErrorDetails = document.getElementById('serverError')
    serverErrorDetails.innerHTML = response.body.error
    serverErrorDetails.classList.remove('displayNone')
    var errorMessage = document.getElementById('errorMessage')
    errorMessage.classList.remove('displayNone')
  }
}

new PatronageForm()
