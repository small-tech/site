---
title: "Small Is Beautiful #27"
date: 2023-02-16T17:00:00+00:00
speaker: "Aral"
description: "End-to-end encrypted Kitten Chat"
---

{{<video poster="./poster.jpg" vimeo="https://player.vimeo.com/progressive_redirect/playback/799597015/rendition/1080p/file.mp4?loc=external&signature=77550654f53ecbc203211f93710a9c96d8d510ae0d62824c5b441ed25dde2701#t=25" captions-en="./auto_generated_captions_edited.vtt" >}}

Want to watch it offline? [Download the video.](https://player.vimeo.com/progressive_redirect/download/799597015/rendition/1080p/small_is_beautiful_end-to-end_encrypted_kitten_chat_-_feb_16,_2023%20%281080p%29.mp4?loc=external&signature=5aa64bb92cee5c59b97c02741e5970420ee42cf386c50147a8ff29537e654966)

## End-to-end encrypted Kitten Chat

**Broadcast on February 17, 2023.**

In this hour-and-a-half long live stream recording, Aral shows you how [WebSockets](https://codeberg.org/kitten/app#kitten-chat), [project-specific secrets](https://codeberg.org/kitten/app#project-specific-secret), and [authenticated routes](https://codeberg.org/kitten/app#authenticated-routes) work in [Kitten](https://codeberg.org/kitten/app) and migrate [a centralised WebSocket chat application](https://codeberg.org/kitten/app#persistent-kitten-chat) to [an end-to-end-encrypted peer-to-peer Small Web chat application](https://codeberg.org/kitten/app#end-to-end-encrypted-kitten-chat) in Kitten.

The example also makes use of the native support for [htmx](https://htmx.org) and [Alpine.js](https://alpinejs.dev) in Kitten.

### Links to other things mentioned or shown during the stream:

- [Kitten](https://codeberg.org/kitten/app)
- [Domain](https://codeberg.org/domain/app)
- [Black Box Terminal](https://gitlab.gnome.org/raggesilver/blackbox)
- [Lipstick on a Pig](https://codeberg.org/small-tech/lipstick)
- [Helix Editor](https://helix-editor.com)
- [lf](https://github.com/gokcehan/lf)
- [WebSocket Weasel](https://github.com/mhgolkar/Weasel)
- [RESTED](https://github.com/RESTEDClient/RESTED)
- [SkipTo Landmarks & Headings](https://github.com/skipto-landmarks-headings/browser-extension)
- [Open Switcher Control](https://openswitcher.org/)

_Streamed using our own [Owncast](https://owncast.online) instance. (Hint: you can install Owncast using [Site.js](https://sitejs.org).)_

If you like this livestream, please help support out work at Small Technology Foundation with a [patronage or donation](/fund-us), or share our videos with your friends!

### Transcript

[Auto-generated transcript from the captions.](./auto_generated_transcript_edited.txt)

{{< fund-us >}}
