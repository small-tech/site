---
title: "Small Is Beautiful #23"
date: 2022-10-20T17:00:00+00:00
speaker: "Aral"
description: "What is the Small Web and why do we need it?"
---

{{< video 
  poster="./poster.jpg" 
  vimeo="https://player.vimeo.com/progressive_redirect/playback/762676594/rendition/1080p/file.mp4?loc=external&signature=6f327f42324b157a97add4fd4c532c69e0cdd29b206ff93f79f4cdae570ed922#t=27" 
>}}

## What is the Small Web and why do we need it?

**Broadcast on October 20th, 2022.**

I talk about what [Small Web](https://small-web.org) is and demonstrate the current state of development on [Kitten](https://codeberg.org/kitten/app) and [Domain](https://codeberg.org/domain/app).

_Streamed using our own [Owncast](https://owncast.online) instance. (Hint: you can install Owncast using [Site.js](https://sitejs.org).)_

If you like this livestream, please help support out work at Small Technology Foundation with a [patronage or donation](/fund-us), or share our videos with your friends!

### Transcript

To follow.

{{< fund-us >}}
