---
title: "Small Is Beautiful #11"
date: 2021-06-18T19:46:35+01:00
speaker: "Aral and Laura"
description: "Can the Small Web be as easy (or easier) to use than the Big Web? (Spoiler: Yes.)"
---

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/564266462" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>

## Can the Small Web be as easy (or easier) to use than the Big Web? (Spoiler: Yes.)

Livestreamed on Thursday, June 18th, 2021. [Read the transcript below](#transcript).

It takes under a minute to sign up for Mark’s Facebook and Jack’s Twitter. Can we make it as simple to sign up for your own place on the Small Web?

Yes!

In this episode, Laura and Aral talked about the design challenges of doing so as well as demonstrating [Domain](https://github.com/small-tech/domain) (previously Basil), a work-in-progress platform that enables just that.

_This was the first time we streamed Small is Beautiful using our own [Owncast](https://owncast.online) instance. (Hint: you can install Owncast using [Site.js](https://sitejs.org).)_

### Links from this week’s livestream

- [Owncast - Selfhosted Livestreaming](https://owncast.online)
- [Site.js](https://sitejs.org)
- [Aral Balkan’s blog and S’updates](https://ar.al)
- [ATEM Mini](https://www.blackmagicdesign.com/products/atemmini)
- [Public Suffix List](https://publicsuffix.org)
- [DNSimple](https://dnsimple.com)
- [Mastodon](https://joinmastodon.org)
- [PeerTube](https://joinpeertube.org)
- [Write.as](https://write.as)
- [Nextcloud](https://nextcloud.com)
- [cloud-init](https://cloud-init.io)
- [Small is Beautiful live stream reminders](https://small-tech.org/events/small-is-beautiful/#get-live-stream-reminders)
- [Fund Small Technology Foundation](https://small-tech.org/fund-us/)
- [Better Blocker](https://better.fyi)

If you like this livestream, please help support out work at Small Technology Foundation with a [patronage or donation](/fund-us), or share our videos with your friends!

### Transcript

**Aral**: Hello, hello and welcome to the first Small is Beautiful on Owncast. Hi, Laura.

**Laura**: Hello Aral.

**Aral**: How are you?

**Laura**: I'm good. I'm Laura Kalbag, that's Aral Balkan, and as ever, he is upstairs, I am downstairs and together we are Small Technology Foundation, a teeny tiny, not-for-profit organisation based here in Ireland.

**Aral**: And this is a monthly stream that we call Small is Beautiful. And this is the first Small is Beautiful that we are actually streaming on our own Owncast server. Previously we were on… were streaming via Vimeo and it's really cool that we're able to move over to Owncast, which… I guess should we do an introduction to Owncast. Laura, would you like to do that or should I?

**Laura**: You go for it.

**Aral**: OK, so for those of you that might not know, Owncast, let me actually I can show you. So let me just go…

**Laura**: I’m going to voice of God and say by previous guest, Gabe Kangas, who is on a couple of episodes ago…

**Aral**: Yes, indeed, so if you go to owncast.online, just one, on owncast.online, you'll find their website and it's basically your own Twitch, your own live streaming server. And it's very, very easy to set up, in fact, will show you how to do that today. But yeah. So Gabe is was very kind and actually gave us five t-shirts to give away as well.

**Aral**: So I'm just going to put the studio, tudio URL up there if you want to join us in the studio. So if you hit that URL, you'll join us in the studio that we're in right now. The first five people to join and maybe ask a question, etc. will get an Owncast t-shirt. And actually, while I'm at it, Laura, let me show you what the t shirts look like.

**Laura**: I was going to say, you can also see that you've posted them on Mastodon and then it's been reposted to Twitter, if you want to see what these groovy t-shirt designs look like.

**Aral**: Yeah, and I can't seem to find the URL, so I'll just go to my Mastodon and let me show you from there. So let me just go to myself here,. And show you my screen and let's see where are those t-shirts?

**Aral**: OK, so this is [crosstalk] what's Mastodon Laura? Do you want to tell?

**Laura**: You just show how how often you post… Mastodon is I guess if Owncast is a Twitch alternative, Mastodon is a Twitter alternative. But you can have your own instances. There are instances out there that have a lot of people on, but Aral and I both have our own instances, our websites, and yet we can still use them to communicate with all the people. And those are what the t-shirts look like. And I think, as Aral said they're available in both straight cut and fitted cut. So that would be conventionally known as “men’s cuts” or “women's cuts”, or “straight cuts/unisex cuts” and…

**Aral**: And as I learned today, thanks to you. All right. So very cool. That is… that's Owncast. And oh, we already have two people in the studio. Laura, if you wouldn't mind maybe chatting and…

**Laura**: Yeah yeah, leave me to it, Aral. You don’t need to tell me how!

**Aral**: Cool. All right, awesome.

**Laura**: But actually, just before we get to that, I was going to say now usually we invite guests into the studio with the studio URL and we give you the option to ask questions, either with microphone and a camera if you've got them, or just via text in the private chat. But today gonna try a new thing and Aral, if you could find the banner for me, you can also, either post to Mastodon or to Twitter with the hashtag #SmallIsBeautiful, and I'll try to pick it up on there as well. So I'm attempting to do a bit more multitasking today, more than I usually do, which is already a fair bit. So we'll see how well it goes. But I just thought it would give people an opportunity if they can't get into the studio or if you’re just more comfortable in that other environment. So if you want to… go for it.

**Aral**: Awesome. So while Laura is chatting to the folks in the studio and we might be joining some of them, we might be having them… some of them on the stream live as well. If they want to be on the stream, of course. I am going to show you what we've been working on recently at Small Technology Foundation. So you might know through some of my S’updates, which are on my blog, ar.al, my more frequent developer updates, which haven't been very frequent recently, actually, that… oops, just zoomed in on Laura. [Laura waves] Hi, Laura. There we go. That's me…

**Aral**: …that we are trying to build the Small Web. So what's a Small Web? Basically take the Big Web and turn it upside down. So the Big Web is about having servers that scale to two, two hundred, two thousand, twenty thousand, two million, twenty million, two billion people, right? It's about… And those servers, of course, are owned by huge Big Tech corporations like Facebook, Google, etc..

**Aral**: What if we tilted that on its head? Basically turned it on its head, and instead, what if the Web was made out of sites, locations that were just just for one person and one person alone? What if websites were single tenant? And what if we owned and controlled them? So that's the Small Web. Now, this has huge challenges, mostly because the Web has been built for the Big Web. Right? It's been built in corporate interests, maybe not at the very beginning, but very quickly afterwards with the influence of venture capital, the Silicon Valley model, etc..

**Aral**: So the constructs we have are all about having servers with multiple users and scaling and growth and all of these success criteria that are really the success criteria of capitalism and maybe what you would call surveillance capitalism, Silicon Valley model of doing things. So what would it take to build what we want, which is that I have my own Place on the Web. Laura has her own Place you have your own Place. Well, it has to be very simple for these places to be set up and used.

**Aral**: In fact, it has to be as simple as signing up, say, for a Twitter account or signing up for a Facebook account, as simple as if not simpler. If we can't do that, no one is going to use them, even if they exist. And by no one, I don't mean the people who are enthusiasts who will, of course, use them and love them and and maybe even work on them. But everyday people who use technology as an everyday thing. And that's not because these people are dumb or stupid. It's because these people are brain surgeons and they have brain surgery in the morning.

**Aral**: So they don't have time to mess with something that doesn't work. If it doesn't work well, if it doesn't do what they want it to do because there are alternatives that do maybe those alternatives don't respect our human rights, but they work. So we need to work. We need to work really well. So I've been working on Domain, which used to be called Basil. It's a work in progress. But let me show you how easy it is with that to set up your own small website. Now, this is still a work in progress, is still very early. It's not released.

**Aral**: You can find the source code. And if you're a developer, you can go in and you can play with it. But let me show you what it's like, the experience of setting up a site. So initially we'll just set up a plain site. So say you're a developer and you just want to set up a site. How easy would it be if if you were using Domain? So let me show you. I'm going to just bring up my terminal. I'm going to run a local copy of Domain here. OK, so this is something that you would… this is a development version of it.

**Aral**: You would, of course, have this in… I'm just sorry I open up my web browser here as well and show you what it looks like when it's running. I'm going to go to localhost:3000, which is my development version of it running, and this is what it looks like. So I'm just going to make it a little smaller so you can see it. So if this was running at, say, small-web.org, which is where this is going to be running, initially, you could say, oh, I want my own Site.js, my own Owncast, my own Place, which is something that we're working on at, I don't know, maybe aral.small-web.org.

**Aral**: And maybe at small-web.org you would be paying something like 10€ a month for it, which would go to our not-for-profit and towards making this sustainable for us, within the system of capitalism that we that we live in. So this functionality is not there yet… now what is? So I'm going to go into my admin panel here. And this is the dev version, so it's loading a little slower than the actual version would, and I'm just going to sign it with my password.

**Aral**: Doesn't matter if you have this password, it's just my little password that I'm using right now. And this is my administration panel. So you can see here that I've set a lot of things up for Domain, but payment is showing as there's a problem with the payment systems because it's because I haven't actually finished implementing the Stripe payments module for it yet. You will be able to set up a Stripe payment module or you can use tokens and we'll talk about that later. Or maybe you just don't have a payment provider. And that's perfectly fine. If its set to “none” and I go to the main page of it, you can see that it says “this is a private instance” and it means that you can only create sites if you have access to the administrator. So this is something you might use for your family maybe or for a hyper local… sort of for your neighbourhood to set up Small Web Places for them. But let's go to our admin panel again, and I'm going to now sign in here. So this is the functionality that currently works right now.

**Aral**: OK? And I'm going to go to Places and I'm going to create a new web Place. So I'm just going to create a web Place that has Site.js. OK, and let's call this small-is-beautiful or just call it small-is-beautiful, and I'm going to create server. So I'm just clicking “create server” here. And what happens is… I'm going to make this a little smaller so you can see it… It's now setting up small-is-beautiful.small-web.org with dashes in between. And this is how it would be if, for setting up anything really. But we're just setting up Site.js, which is a very developer-centric thing for creating your own sites. But this is early days, right? So it's doing all the things it needs to to set up small-is-beautiful.small-web.org and giving you progress as it goes. And now it's like, OK, let's see if the server is actually ready or if it's still doing a few final things?

**Aral**: And once it gets a response from the server, it knows it's actually actually there. What should happen any second now? But who knows? I mean, this is a live demo, so it could take a little longer, a little shorter amount of time. Let's see. We're waiting for… usually it's faster than this, I have to say. But I think Hetzner, Hetzner is the VPS provider we're using initially. There we go. It's there. They were having an issue with some of their services. So if I go there now, this is just saying “welcome to your Small Web site powered by Site.js.” Now, again, this is for developers right now, but how can I change this if I have Site.js now, if you don't know what Site.js is, by the way, let's just quickly go to sitejs.org. It is a Small Web construction set. It is how you can create your own websites and web apps very easily for, websites and apps for one person. So if you have Site.js, as I do install, it's very easy to install it. You just copy and paste the line from the Site.js website. In fact, if I didn't have it installed, how easy it is to install it, I would just scroll down over here. And I would go to… where is that? “Install Site.js” over here, I click that, I go into my Terminal, I paste that. Again, this is for developers, right? So it's downloading Site.js.

**Aral**: And then again, that's taking a bit longer. Maybe my connection here isn't that great right now, right. Yeah, I'm on the right network. OK, so it's installed. So let's just make a little demo site. So I'm going to go into “demo.” I'm going to say “touch Hello, Small Is Beautiful!”, and I'll write that into an index.html File. Now I'm just going to sync that, OK, to site @ small-is-beautiful.small-web.org  and into the public folder there. OK, we want to keep connecting. Yes. Because the first time it connected. OK, now if we go over to small-is-beautiful.small-web.org, you can do this too and you refresh. Should see… what happened there? Let's see… let’s see what's up.

**Aral**: Did I just actually touch? Nope, no, “touch” does not do that, ladies and gentlemen, “Echo” will. So let me also remove that file. OK, there we go. Now, that's much more like a website. Oh, God. I just managed to screw up the most basic demo. It's amazing. All right, let's go back to that. So if I sync that now if I say site --sync to… And I do the same thing. OK, it's not making it up… site --sync-to-aral@small-web… sorry, site @ small-is-beautiful.small-web.org into the public folder. See that the sync is starting now, if you go over there, it says “Hello, Small Is Beautiful!” Now, of course, this is just a very basic example, right? Let's go into some examples that come with Site.js.

**Aral**: OK, so this is the source code for Site.js and there’s an examples folder in there. There are some examples in there. One of them is simple chat. So what if I want to have a simple chat server? OK, let me just sync that to small-is-beautiful.small-web.org. Now, if I refresh there, I have a simple chat app running there. OK, this is cool. This is actually online. You can actually go there and I'll say “hello everyone”. If there is anyone watching… and this is a very, very simple chat app in that it doesn't even do persistance. So if I go to small-is-beautiful.small-web.org in a different tab, I'll say also Aral and I'll say “hello Aral”. You can see the first chat isn't there, although Vinci said “hey” in there. So I didn't see that because Vinci said that earlier. Now any new chats that come in, Olav is saying “hi” there now are coming in and he says, “Wicked!”, that's really cool. OK, so this is great. But Laura is “hard core multitasking”. That's great. So this is very simple. So people who join in are not actually seeing previous messages. That's not great for chat, is it? So let's do a persistent chat.

**Aral**: And thankfully, Site.js comes to the persistent chat example as well. So you might be wondering or not, well, how would we deploy the persistent chat? So I just go into the persistent chat folder and this is where the source code is. And I'll just say site --sync-to, the same thing, just syncing to it. All right, let's refresh. And now we have a persistent chat application running there. That's pretty cool. Sorry, the previous chats are gone. But if you now… if I now say “hello” here, anyone who joins the persistent chat… OK, so I'm going to refresh this one as well… will see what's there. So, “Hey, Vinci, thanks for joining us :)” Yay! “Send”. OK, so… Élise, thank you. Nice. Mike says “hello”.

**Aral**: Very cool. Very cool. Also folks, let me know how the stream is going. Are you having difficulty with it? Is it fine? Is the quality good? It’s first time we're running…

**Laura**: I’m coming in here as the voice of God…

**Aral**: Nice…

**Laura**: So far we've had pretty good feedback on how the Owncast live stream is running.

**Aral**: Excellent. And there's no reason why it shouldn't. We've got it set up right now just doing a passthrough. So our studio’s in Streamyard and we're just doing a pass through stream. It's not doing any transcoding. So actually if I go on the owncast.small-web… not small-tech, small-web.org, that's our server where our server’s installed. If I go there and I go to the admin there, let me just sign in here.

**Aral**: Sorry. Give me one second. Admin and my password. I wonder if my password is still in… nope… it's not ok. Sorry I'm just going to get my password. It is not a web form so I actually need to go into 1Password to get it. Give me one second, let me reveal that, OK, copy, that's the correct password. So if I go into the admin in one password, our case, I'm going to show you what that's like. Let's go over here. So this is the admin. We can see that we've been streaming for 20 minutes today. This is Owncast’s admin. And you can also see the logs. If you go over here, you can see that everything's going swimmingly. Our server setup is very, very simple. Just the defaults, over here. We've just I've hidden the chat because I don't know how much of an impact that would have. So I'm just being very, very conservative right now. Storage, we're not even using external storage to something like Digital Ocean spaces or AWS, etc. …So this is just running on a very small VPS. In fact, if you're wondering what kind of VPS it's running on. Oh, sorry, I didn't even make myself full screen there. So you probably didn't see all that. So let me just…

**Laura**: They just got to look at me at the same time! [laughs]

**Aral**: So here you can see that. What did I show you earlier? Our server setup is the the default setup. Our video configuration is very simple. We just have a pass through stream, so we're not doing any transcoding whatsoever. But that actually means if I go to hardware over here currently, you can see our usage on a very simple VPS is 26% CPU, 6% memory, 4% disk. So it's doing swimmingly well in terms of resource utilisation.

**Aral**: And if you're wondering. What kind of VPS it's running on? Exactly the same kind that you just saw me deploy Site.js to, OK, no other tweaks. In fact, one of the things that you can install with Domain, what I just showed you is the fact that we're working on is Owncast. So I'm going to show you that… it's the last thing and then we'll take questions and maybe we'll have a conversation, etc.. Is that OK? Laura?

**Laura**: Yep, that's OK by me.

**Aral**: All right, so let me just do that, so let's go back here. So again, if I go back here and let me just go back into the admin, and sign into the admin here. OK? So, all right. I'm going to this time… let me go into Places, not into set up. I can select “Owncast (with Site.js)” and let's call this, I don't know, let's call it a smallcast, smallcast.small-web.org. If I go there now .small-web.org, it doesn't exist yet. We haven't made it yet. So let me close these simple chats. People are still chatting in the simple chat, that's cool dum-duh-da-da-dah. Oh, Adam, no transcoding. So I'm not streaming directly from the ATEM. We're not streaming directly from the ATEM to Owncast. I'm using the ATEM is for me and my set up here and I'm using it as a USB camera. We're going through Streamyard, which allows us to talk to different people and have Laura and me on at the same time…

**Laura**: Aral, would you read the question?

**Aral**: to Streamyard. Sorry, the question is…

**Laura**: The statement…

**Aral**: Sorry, statement is “Adam: no transcoding, so ATEM output which has hardware encoder for streaming … I think is pretty dynamic.” So we're just not going directly from the ATEM. We could, and it works for that as well. I've tried that and it works really well with the pass through, so that's very cool.

**Aral**: Anyway, so let's go back to Domain. We're going to create smallcast.small-web.org, OK? And it's going to be Owncast via Site.js. So I'm just gonna say “create server” like I did before and there we are… So it's doing the same thing, right? There's going to be nothing too different happening here apart from if you look at it, it is installing Owncast. So Site.js is actually installing Owncast. I recently added that feature to it. And if you go on Owncast’s website as well, you'll see that you can… that's one of the ways that you can install Owncast. It's doing the same thing, but it's now installing Owncast alongside itself. So what Site.js is giving it is automatic LetsEncrypt certificates. It's setting it up a daemon to run, as a daemon. So it's basically setting up a production installation of Owncast for you. And the idea is that we do that in under a minute. So, again, we're waiting for the response from the server right now. And the moment we get the response from that server, we'll be able to go over there and use our Owncast installation. So let's see. Like I said today, it does seem to be taking a little bit longer than before.

**Aral**: But again, it should be pretty much under a minute. And this is all very, very early. There's a lot that can be optimised, especially on the Site.js side of things and the what will come after Site.js as well. So we're just waiting smallcast.small-web.org, “waiting for response from server.” This is definitely taking much longer than it usually does… to the point where I wonder if something has gone wrong. Maybe? I don't think so…Let me check, the beauty of it is we can check. Over here, let's see, those are just… everything seems to be fine. Let's go back here. OK, well, it didn't… doesn't seem to be liking that… let’s see… small-web.org

**Laura**: Vinci’s making suggestions in the chat, if you want to have a look, Aral.

**Aral**: In which chat?

**Laura**: In the private chat in the studio.

**Aral**: OK, so let's go to the private chat that, uh, it could be a DNS issue. I wonder if I actually did forget to clear that from our DNS server when I was testing? So instead, let's just very quickly, let's try this again. I'm just going to try this one more time, and if there's an issue, maybe it's just the demo gods do not like me… today… or we might actually let me just go to Hetzner. So Hetzner is where the VPS stuff is being handled right now, and I'm going to try and let's see, hopefully. Very responsive. There we go. Let's log into “cloud.” I think I might also know what might have happened… maybe… let's see. Because this is all on development right now, we don't have production quotas either. No, that should have been fine, smallcast seems to have been created correctly. OK, let’s go to small-web.org. Nope. OK, let's just try one last time. And a create Owncast we’ll call this, let's call it owncast-small-is-beautiful to make sure that I haven't got some DNS issues or something there. All right. I have been testing with these things, so it's quite possible that there is an issue there. So it is now going through the same process and we're going to see… and Hetzner actually did say it had some issues earlier, but the site seems to have been created properly. So let's just see what this does. If it keeps us waiting, then we'll just move on. But basically, this is how I set up our Owncast and that's what's running… that's that's how you're watching this stream right now. It was set up using Domain. So let's see, it's gone to the point where it's saying “waiting for response from server” again. So let's see if that's just going to keep us waiting. And if… and if it is, then maybe I've introduced the bug or something in the last day. Nope, nope. Here we go. So something happened previously. “Visit your Place.” So it must have been DNS. It's always DNS. So there's Owncast running over here.

**Aral**: One of the first things you should do if you set up Owncast is go to the admin and enter the default password and then change that default password from… I think it's server setup. It's the same as your stream key. I’ll just set it to my Basil dev password here. It's not that important. I just don't want anyone to log in right now and stream porn or anything, because that kind of thing happens on the Interwebs. So if I sign in now with that password, this is just Owncast running over here.

**Aral**: So that is how simple it is. To set this stuff up with Domain. Now, I do want to take you through a couple of things. First of all, this is for… Domain is for people who will actually run Small Web domains, which are kind of like on ramps onto the Small Web. So this is not what someone who wants to run Owncast, for example, or run their own Place on the on the Small Web will run themselves. This is like someone like me, like Small Web Small Technology Foundation… we’ll be running a Small Web domain, like at small-web.org, which is what we're going to do…

**Laura**: We have… Aral, we actually have a question that totally encaptures that…

**Aral**: Let's go for it. Let's take questions. Right. I’ve spoken enough…

**Laura**: …from Federico. No, I'm not saying… I'm not stopping you completely, I’m just saying, for now, perfect question.

**Aral**: Yeah, go for it.

**Laura**: So this is from Federico, who is in the chat here in the studio. “So which of these tools are to be used by the brain surgeon types of user? Which one is for the tech savvy? Here you are creating small sites under small-web.org, but if I want to offer the service to a local community, I guess I need an expert managing a dedicated VPS with Site.js?” And I thought, great question. I mean, I know the answer to it, but I think that you'll answer it well.

**Aral**: Well, no, that is a great question. Thank you. So as I was saying, basically this piece of software that you saw is for… is meant to be run by people with technical knowledge who either want to run a commercial service. And that's not commercial as in “we want to scale as large as possible and become billion dollar unicorns”. No, that's never going to happen with this. If you want to run a sustainable organisation enabling people to get on the Small Web, right? Like us, for example, we're not for profit. So we're going to run our own at small-web.org. And you need technical knowledge in order to set this up. So which is actually what I want to show, which is this setup over here. Sorry, just up resizing the wrong browser. OK, so there we go. So this setup is what I wanted to show you. So this is for organisations, pretty much so. This is US technology Small Technology Foundation. You can set up apps that you want to make available to people, to everyday people to install. Right now, Place is what we're actually really focusing on for the future. But you can install things right now very early things like Owncast with Site.js. And I'll talk a bit about what the difference is there in approach. But you can actually set up your apps here. So I can say if it's Owncast, you see it's got a different description, it's got its logo… Site.js is a different one. And this cloud-init is how the server is set up. This is a basically a standard way of doing things, that a lot of VPS providers support. So you have to set up your apps. If you are a commercial service or if you're not a private instance, then you also have to get on the Public Suffix list.

**Aral**: And the Public Suffix list is this list that's currently maintained by Mozilla, where basically if your domain is on there, for all intents and purposes, it behaves like a top level domain, which is really important because you don't want, for example, cookies to be set on the master domain that then apply to all of the subdomains, et cetera, if different people are registering those. If you are if you have a private instance, that's not as important. You have to get a DNSimple account right now and attach it to your instance. The the idea is in the future. Let me show you that. So you have your DNS setup over here, details. It tells you how to get it and what to do, etc. and then you enter your details here and it automatically links to it. But this is for organisations who want to run this service. Again, you have to set it up with a VPS provider. The initial one that we're supporting is Hetzner, because their API is amazing. Their speed at setting up is amazing.

**Aral**: But the goal is in the future that we will have multiples of these, and this will hopefully abstract that out so that we're not tying it into any single one provider. And then, of course, you can set your payments. So I said there is “none” for private organisations, Stripe, if you want to run a commercial instance of this. So I showed you that if you're running a commercial instance then this is what it looks like, and this is the bit, to answer the question, where the brain surgeon would use it right to just get their own Place, for example, or to run their own Twitch, their own Owncast if they wanted to.

**Aral**: And the other thing that's not implemented right now is tokens. So this is if what if you don't want to run a commercial service? But maybe you're a municipality, we worked with the city of Ghent several years back to prototype something that was the precursor of this. And unfortunately, we ran out of funds, a conservative government came into power, et cetera, et cetera, and they cut our budget. But what if a municipality, what if the city of Ghent, for example, created tokens, coupons for their all of their citizens because they saw that as a fundamental human right, something in the public good?

**Aral**: And so you can just enter a token instead of the payment, and you get your own Small Web Place. So this is something that we're really excited about for the future and where we want to go. We kind of see this as a bridge, really. We have to exist and be sustainable within the system of capitalism that we find ourselves in, and under, because we also don't want to take money from Big Tech, from Silicon Valley or from organisations that are one degree removed from Silicon Valley, who get all their money from Big Tech. Not going to mention anyone specifically, you know, half a billion dollar organisations, not-for-profits that own for-profits, et cetera.

**Aral**: But yeah. So that's that's kind of what Domain is for. I said we have different apps in there and this is what I'm going to end on. So we have different apps that you can set up right now. And the goal really is, in the future, for Place to be the app or the thing that you actually set up. So right now we're supporting Owncast and Site.js because this is one of the fundamental things about the Small Web as well. Currently we have the Big Web and then we have, for example, federated services where… which is great, it's awesome… things like Mastodon are federated. But what I'm seeing is that we are basically recreating the services that exist on the Big Web in the same way that they exist on the Big Web but with federation, so more people can actually own and control these places. But we're… if we see YouTube, which allows you to store your videos and share your videos etc, then we're building PeerTube, which is excellent. If we see Twitter, we're building Mastodon. That's excellent because Mastodon's much better than Twitter, because there are thousands of Mastodons being run by different people with millions of people using the system. And it's not locked into Twitter. But we see YouTube, we build PeerTube. We see Twitter, we build Mastodon. We see Medium, we build, I don't know, Write.as. And this is all great until you start thinking about in a Small Web fashion. So if we talk about a single instance, web applications and sites, does that mean that I'm going to set up one small website for my YouTube alternative, one for my Twitch alternative, one for my, I don't know, Medium alternative? And let's say that this is initially like all 10€ a month, even that's going to add up. Right? And not to mention that's not actually very ergonomic. That's not what the Small Web is about. When you have a single instance web application, what you can start to do is build the elements of what make YouTube and Twitch and Medium useful and private messaging useful into a single app. So what if you installed Place, for example, and with Place, yeah you could have your own stream if you wanted to, a livestream. Yeah. You can talk privately to your friends if you wanted to from your Place you could make public posts from your Place. So we've kind of inverted it again. We've inverted the Big Web way of doing things. When we are building single tenant applications, we've completely turned it on its head. And now instead of focusing the complexity on how do we support millions of people with our system, it's how do we support the things that one person wants to do really well on their own Place?

**Aral**: So that's what Place is going to be going forward. So that's me done… hopefully…

**Laura**: Right, I'm going to jump in then. So one point to make: we are having some issues with buffering and things like that in some locations, it seems. Gabe has said that he's reporting issues from the US and says maybe next stream we should increase the latency buffer a little bit but Mikalai in the chat…

**Aral**: It’s on medium now I think…

**Laura**: Mikalai in the chat has reported that sometimes a reload helps. So if that, and I'm not even sure that's an…

**Aral**: Ok…

**Laura**: …like integrated like proper solution. But it helps some people, it’s worth giving it a go…

**Aral**: For those of you having buffering issues, how is your connection right now? Do you have a high bandwidth connection or is it a slower connection by chance? I'm just wondering which end the issue is on because we're not transcoding. Because we're just passing through this very high bandwidth stream, 1080P at 30 frames per second. I wonder if that's the problem, and that may be adding a lower resolution transcode would be better… you're smirking. Why, what happened?

**Laura**: I'm sorry. I'm not smirking at you. I'm smirking at something that somebody said in the chat… [laughs]

**Aral**: yeah, no, no. What is it? Share it with the class.

**Laura**: No, it was it was either a dirty joke or my dirty mind. So I'm not going to…

**Aral**: I guess I’ll never know…

**Laura**: …say.

**Aral**: OK, fair enough.

**Laura**: That is an in-joke just for people in the studio.

**Aral**: Do we have any other questions?

**Laura**: Yes, we do. Right. Sorry.

**Aral**: Do we have anyone who wants to join us on the stream or is that…?

**Laura**: We do, but I'm going to ask another question first if that is all right by you?

**Aral**: Sure.

**Laura**: Because it’s just making sure that we're going back to what you were just talking about. And so this is… and the name I'm going to butcher horribly, but I promised I would give it a go… Mzumquadrat. So I hope I got that right.

**Aral**: Can’t help you there…

**Laura**: “do I understand it correctly? Site.js is a provisioning tool for hardware/software, right? Kind of what TerraForm and Ansible would do for large fleets of servers?”

**Aral**: Kind of. In a way, that's what Domain is. Site.js is essentially at its core, a nodeJS server with all of the with all the batteries included. So it automatically provisions https certificates for you. It can automatically create your systemD service and make sure that your daemon supports restarts, etc. So you can use it during development. So it's, you can use it during development to develop your sites, and you can also use it for production. Domain, right now, actually everything Domain is deploying is based on Site.js, see the Site.js or Owncast installed through Site.js. The simplicity of Site.js enables us to make that process very simple to set up. But Place, which is going to be, which is the thing that we're working on next, is actually a fork of Site.js that I've ported to ECMAScript 6. And it's taken out a lot of the bloat that was in Site.js because Site.js really kind of supports the Small Web vision, but within the context of the current web, Place is going to be very much focused on the context of the Small Web. Specifically. So I know this is all confusing because I'm throwing words out there that some of which we've just kind of invented over the past weeks and months and years.

**Aral**: But I think it'll become much clearer and kind of much more focused in the following weeks as Domain shapes up and as we start hosting it on small-web.org And as we can actually focus our attention on building Place out as well. But yeah, so that's what Domain is more like, to answer your question.

**Laura**: Thank you.

**Aral**: You’re welcome.

**Laura**: That was a good question. Well, I just want to drop in and say that we have a full studio right now, and I've just said this in the chat, if people have questions I want to ask, please ask them in the chat or let me know if you want to ask a question and if you are able to watch the stream on the Small Tech site right now, on owncast.small-web.org or on the embedded version on the Small Tech site, if you could drop out of the studio and do that, if you don't have a question, because then we'll have room to let people who might have questions into the studio.

**Laura**: And so we do have a question from Mikalai who is able to come on stream.

**Aral**: Ok, excellent.

**Laura**: So Aral, if you would be willing to add Mikalai in…

**Aral**: Sure where… Mikalai, right?

**Laura**: Right at the bottom! Scroll down.

**Aral**: Alright. There we go. Hi, Mikalai. I love your hat. Hello. Hi. Welcome.

**Mikalai**: Hello. So my question is, do you hear me correctly?

**Laura**: Yeah, I can hear you good.

**Mikalai**: Cool. So my question is, what will it take to turn my… any web application on a website, existing one to work on the Site.js… Small Web? Because there's already a ton of everything that already exists.

**Aral**: Right.

**Mikalai**: So sometimes I find myself, for example, there's like applications with GNU license, for example, for Own or Nextcloud. You know, all of those people…

**Aral**: Yeah. Yeah.

**Mikalai**: Actually there's thousands of common good that already existed…

**Aral**: Yeah.

**Mikalai**: So…

**Aral**: Well, here's here's the thing and it's a great question. Thank you. Because it's very important that we make that clear. So Domain or Small Web Domain is not a general web host. It's not a Digital Ocean. It's not about you running your own Digital Ocean. It's very, very focused on running single tenant web applications. So Nextcloud is not a single tenant web application either, right? You've got lots of users, etc.. The reason for that is the moment you go from an application supporting just one person to even writing an application, and you must know this as well, writing an application that supports even two people, or can scale to a thousand or ten thousand, that is a whole different world of complexity, right? There are. There are. And that's where you get into the Big Web. Right? The Big Web way of doing things. You add an external, you add a database to an external process, you add a messaging server, etc. this that you start decoupling bits of the application because it needs to scale. Right?

**Aral**: All the things that you start making sure that you can shard your da-, all of these things. Even if you're not doing that, even if it's just for you, all that complexity is in there, because if your app needs to support a thousand people or ten thousand people… What we're saying is we're cutting that out. We're never going to support more than one person. So the kind of apps that this is being designed for are built for one person. And maybe app isn't even the right term because it's kind of, if you think about it that way, it becomes more like an operating system in a way that you're installing, which has certain features. Like think of the original iPhone. You couldn't create apps for it right? At the very beginning. I mean, there were web apps, I think, and that wasn't even at the very first one. But at the launch, I remember because I was there… they when Steve Jobs showed it, he basically showed all the things they could do that were built in. The apps were built in. But the apps were the things you wanted to do. Right? I think we're missing this. You know, I think we're missing this thing where you can have your own Place on the Web and you can make public posts and you can talk privately end-to-end encrypted with your friends.

**Aral**: And if you want to, you can stream your live stream through that, not to a million people, but to the people who care, to your community maybe. You know, I mean, we're doing it through the system right now. So as many people are watching right now with this. So I think it's fit for purpose even at this point. And with Owncast, that's what's so cool about Owncast, Owncast has kind of proven that, right? So if we take away all this complexity, we have the ability to focus on creating this kind of beautiful experience that lets people do what they want to do on their own place without having to have 10 different things installed. You know? And that's also a whole bunch of complexity.

**Aral**: We're pretty technically knowledgeable, I guess, and I don't run my own Mastodon. I did initially. I set it up, but then I couldn't just manage to keep that going and do the coding I'm doing, and take the dog out for walks, and… so you multiply that by every app, that's not sustainable, I don't think. Now don't get me wrong, I love the fact that these alternatives exist. I love because they're showing us different ways of doing things. They're creating open source code, like you said, that we can benefit from. Yeah? So, I mean, I learned so much from my Owncast, for example, like Gabe, how he shares what he does as well. So I had the same fear of of live streaming. I was like, that must be some dark magic because I never looked into it. Right? And you look at something like this and you go, this must be dark magic. How do they do it? It's not. It's with HSL. It's breaking up a video file into ten second video files and then writing them as quickly as you can and then making sure you're at least 10 or 20 seconds ahead of anyone who's watching.

**Aral**: That's how HSL works. So, you know, you look at something like Owncast and and Gabe doing this, and he's trying to demystify it and he's succeeding. You know, he's showing you that… And Owncast is just for one person as well, which is what I love. It has no concept of users right now, and that's why it's compatible. That's why I could easily install it with Site.js. And I think that's what we're focusing on, at least. It's really great that Nextcloud exists. It's really great that the fediverse exists. But this is something different. It's a different approach and it's going to be exciting to see how that you know, how that spans out. Does that answer your question at all?

**Mikalai**: Well, um… basically…

**Laura**: In the chat, Forest was also going to elaborate on that. But you go first, Mikalai.

**Mikalai**: Like, probably the more technical answer is like, OK, put it down in the documentation and like some example or something like that because…

**Aral**: Of course. Yeah, yeah, yeah. It's so early right now.

**Mikalai**: Yeah. Because in a way it may seem like that some of the applications are complex, but basically you say, here's my target, here's what we want to see, and this is how you go into that other code. And you separate what pieces are and those other pieces you just cut out and that's it. But that also leads me to what you were talking about, the application of being just simple and stuff into this concept that… so I want to stream. So I want just to have that process.

**Aral**: Mm hmm.

**Mikalai**: An in having that process is this whole thing like, you know, the serverless, the the Docker thing, basically I want a process and maybe I'll put it on my own hosted somewhere Place…

**Aral**: Yeah.

**Mikalai**: And maybe I Place it on one of my home devices.

**Aral**: Exactly. Yeah. Yeah.

**Mikalai**: So…

**Aral**: So you can so you can do that with, with what we're building here as well. So you can run for example right now, today you can run Site.js on a Raspberry Pi, you can even run it on a Pi Zero. I haven't tested the latest build but I did test earlier builds. Yeah, the idea is not to in any way lock you into anything. Everything is free and open that we do. And this is one way of getting it set up because again, a Raspberry Pi is good for us, but you still need a little bit of technical knowledge or at least a little bit of like willingness to tinker, which is great. And so we need to support that. And I think in the future we might even chip out a little tiny computer. I'm playing with those. A little tiny box that you just connect your router and it does all this, you know. But this is the way that I can see us kind of getting started as quickly as we can. You know, so yeah, yeah, and you say…

**Laura**: Three things, three things from me, quickly first, I'm sorry to everyone if you suddenly had some issues with the live stream, even Streamyard started reporting that we had some issues with the streaming going on, but it seemed to sort itself out. So I'm hoping that you're able to watch it now. If you can't, we will have the video with transcript and captions available later, for this, so you won't miss out completely. And second thing I have…

**Aral**: Yeah, I did… there was a CPU spike of about sixty one percent. I'm just seeing that right now.

**Laura**: Yeah. And I'll just want a couple of minutes before we finish because I got a little baby thing that I want to talk about and…

**Aral**: Yes!

**Laura**: And finally sorry, Forest… Forest was elaborating on what Mikalai was saying, so I thought it would be interesting to bring him into the chat just to ask questions that he wanted to talk about as well.

**Aral**: OK, and has everyone have you taken note of everyone who's won their t shirt?

**Laura**: Oh, yes, I have. Aral, what do we do? Are we going to send them a code or we will we need their emails or?

**Aral**: So if you can just get everyone's, I guess email would be the simplest thing.

**Laura**: Well have to I'll have to go back and hunt them down. Some of them will have left the studio, but I can manage it, we’ll work it out.

**Aral**: OK, all right. Well, we'll work it out. We have five to give away in total.

**Laura**: We've… and all five have been claimed. So we will get there.

**Aral**: OK, excellent. So these are Owncast t-shirts thanks to Gabe. All right. So…

**Laura**: Thank you Gabe!

**Aral**: Yeah thank you Gabe, and thank you for Owncast. So here's how we'll do it then we'll bring Forest on so we can elaborate and then we'll go back to you Laura so that you can talk about what you wanted to, and then we'll wrap it up under the hour on the hour. All right. I'm bringing Forest in and Forest. Hello.

**Forest**: Hello!

**Aral**: Forest, you’re portrait mode.

**Forest**: Yeah

**Aral**: …always the rebel. How are you?

**Forest**: Well. I think, uh, kind of hearing and echo of myself.

**Aral**: Oh, yeah, I can hear an echo…

**Forest**: I have a new microphone now, so it's a little different. I'll just roll with it… So I wanted to ask about what Mikalai I was asking about. Essentially, I mean, I can go read the code, build it, put the packages, domains, Site.js like even if I did have a single web application… for example, I have a password manager that I use that could be single tenant. How do I create and deploy that? Like, he makes something for Terraform and Ansible. Wait, I can't tell this… is my audio…?

**Aral**: Um Forest, we're actually getting the echo as well, so you to us, [mimics echoing] you sound like you sound like you sound like… we're kind of getting back.

**Forest**: Sorry.

**Aral**: Oh, no worries. I think I got your question. It's what is the format for that for the deployment scripts, et cetera, for the VPS? Was that it? [Forest nods] Yeah, so basically… it's just a cloud-init, so if you go and look it up, that's I think Canonical came up with it.

**Forest**: I know what cloud-init is…

**Aral**: If I'm not mistaken, this cloud is just cloud-init. That's all it is. And that's again on purpose to keep it as simple as possible is interoperable as possible so that hopefully in the future we can support multiple VPS providers so that going forward we don't lock into anything, really. I mean, I see this sort of thing. It should really be a commodity. And that's all… that's easier said than done because, of course, different providers have different APIs, different performance on setting up a server, etc.

**Aral**: But these are all details we can solve in the future. The goal is initially to have like a proof of concept up that works that we can actually start iterating on. And that's… you know we're not even there yet. It's very early. It's prerelease right now. Consider it Alpha like, I mean, it's not a feature complete in any way, but hopefully it will be very soon. So. Yeah, and I hope that answers your question. [Forest nods]

**Aral**: Awesome. All right. Forest, well, thank you so much. I'm going to remove you from the stream just so Laura has enough time to talk about this last thing that she wants to talk about. But thank you so much for joining us. Take care, man.

**Aral**: Go Laura.

**Laura**: This is, as I said, just a little baby thing, but quite a few episodes ago I suggested that maybe we give people some advance notice on guests and the topics of these live streams, the Small is Beautiful live streams, ahead of time and not on social media because we've had that whole discussion about trying to get away from social media. And so I’ve finally done it. If you want to now sign up for notifications of the live streams, they will only go out once a month by email. You can sign up on our site at small-tech.org. You can do so by the /small-is-beautiful page, I think also via the Events section as well. Or if you don't want email and you don't like social media, there's also an option to get those same notifications via RSS. So I finally set that up and I hope that's useful for people. I've already noticed that a few people have somehow found that, in the few hours since I put it live. So that is very cool. So now we have those email notifications for all, and of course, as ever, there is no tracking in those emails. There is no tracking pixels. There is no tracking links or any of that kind of nonsense. We will just use your email address for that one purpose, that one purpose only. And that's it.

**Aral**: And so that is the URL… [banner for small-tech.org/small-is-beautiful]

**Laura**: Oh thank you, Aral.

**Aral**: I think… There we go. Just frantically typing that in into a banner. All right, cool. Well, I think that nicely brings us to the conclusion of this first Small is Beautiful that we've broadcast via Owncast.

**Aral**: We always forget this, but we are a not-for-profit, a tiny two person independent, not-for-profit. We don't take VC, we don't take, you know, sponsorship from Big Tech or surveillance capitalists. So we exist partially because of your individual donations and your patronages. Thank you so much to those of you who are patrons and have donated in the past as well, you make it possible for us to go on. We also exist on the money that comes in from sales of Better Blocker, our tracker blocker that we work on, as well as other things like professional speaking fees, etc. You can find out, you know, the whole history of how we've been funded on the URL that you see there /fund-us and also you can fund us there.

**Aral**: So I just wanted to mention that because we keep forgetting mentioning that and we really should more often… it doesn't help that we both suck with money, but hey,

**Laura**: Agh, speak for yourself! [laughs]

**Aral**: OK, I suck with money anyway. So I guess that brings us to the end of this, this Small is Beautiful.

**Laura**: Yeah…

**Aral**: Doesn't it?

**Laura**: So…

**Aral**: Want do the outro?

**Laura**: Of course I was… I was I was rolling into it like the professional I am.

**Aral**: Hahaha, All right.

**Laura**: As ever, I have been Laura Kalbag.

**Aral**: I'm just going to so [window zooms in and out of Laura, Aral has vanished] [laughs] I just removed myself…

**Laura**: Aral has muted himself.

**Aral**: Did I? Did I mute myself?

**Laura**: You muted yourself and removed yourself simultaneously.

**Aral**: Am I back? I’m back, right?

**Laura**: Yeah, you’re back. You’re back.

**Aral**: This is because I'm like clicking around this tiny Streamyard interface.

**Laura**: You need to stop doing so much clicking.

**Aral**: Anyway, Streamyard… if anyone from Streamyard is watching, for goodness sake, let us use a streamdeck, OK, like do key bindings so that I can use my streamdeck instead of pecking at this interface that I have. All right. So let's try this again, Laura. Let's do it. Let's do it right. You go. Go ahead. Take two. Boom.

**Aral**: [wearily] Well, I have been a very tired Laura Kalbag.

**Aral**: [laughs] And I am Aral Balkan and thank you so much for joining us this month on Small is Beautiful. Take care. Bye bye.

{{< fund-us >}}