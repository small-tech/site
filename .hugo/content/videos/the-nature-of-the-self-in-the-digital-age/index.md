---
title: "The Nature of The Self in the Digital Age"
date: 2016-02-27T17:08:57Z
speaker: "Aral"
description: "When Aral was on BBC World Service: World Have Your Say, speaking about Apple, FBI & Encryption, the BBC made this short video. February 2016."
---

{{< video 
	poster="poster-the-nature-of-the-self-in-the-digital-age.jpg" 
	vimeo="//player.vimeo.com/external/157130642.hd.mp4?s=af7729dc6861ba4855a4975a6c4f5ddd72a2f240&profile_id=113" 
>}}

When Aral was on BBC World Service: World Have Your Say, speaking about Apple, FBI & Encryption, the BBC made this short video from a clip. February 2016.
