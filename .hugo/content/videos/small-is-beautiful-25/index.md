---
title: "Small Is Beautiful #25"
date: 2022-12-15T17:00:00+00:00
speaker: "Aral and Laura"
description: "Identity, privacy, communication and personhood on the Small Web"
---

{{<video poster="./poster.jpg" vimeo="https://player.vimeo.com/progressive_redirect/playback/781770468/rendition/1080p/file.mp4?loc=external&signature=198ebc4db39d89dae98feadfc6f5d2669355626b452b8b42dbf2a493ed140360#t=25" >}}

Want to watch it offline? [Download the video.](https://player.vimeo.com/progressive_redirect/download/781770468/rendition/1080p/small_web_&_kitten:_identity,_authentication,_communication_%E2%80%93_small_is_beautiful_%2325_%E2%80%93_dec_15,_2022%20%281080p%29.mp4?loc=external&signature=15e482399e4df15da5e2f34c4857e73ba3b98383d21f03753c9c439d01e07e4b)

## Identity, privacy, communication and personhood on the Small Web

**Broadcast on December 15th, 2022.**

A first look at identity and what it means for privacy, communication, 
and personhood on the Small Web with the recent addition of public-key 
authentication to [Kitten](https://codeberg.org/kitten/app).

### Links mentioned during the stream:

- [Kitten](https://codeberg.org/kitten/app)
- [Domain](https://codeberg.org/domain/app)

_Streamed using our own [Owncast](https://owncast.online) instance. (Hint: you can install Owncast using [Site.js](https://sitejs.org).)_

If you like this livestream, please help support out work at Small Technology Foundation with a [patronage or donation](/fund-us), or share our videos with your friends!

### Transcript

We’re behind on transcripts at the moment, sorry.

{{< fund-us >}}
