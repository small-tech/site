---
title: "Live: Small Tech > Big Tech"
date: 2020-07-16T12:00:00+01:00
speaker: "Aral Balkan"
description: "Aral Balkan spoke at the Interactive Futures Exhibition 2020."
---

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/438876109#t=10m53s" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>

## Details

This talk was live streamed from this page on Thursday, 16 July, at 15:00 German time, 14:00 Irish time.

{{< fund-us >}}
