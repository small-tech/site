---
title: "Small Is Beautiful #21"
date: 2022-08-18T17:00:00+00:00
speaker: "Aral and Laura"
description: "Lipstick on a Pig"
---

{{< video 
  poster="./poster.jpg" 
  vimeo="https://player.vimeo.com/progressive_redirect/playback/740819171/rendition/1080p/file.mp4?loc=external&signature=d372d109c941bf992852e56a94e94300207a6f8a15e9608ce35b337599250fcc" 

>}}

## Lipstick on a Pig

**Broadcast on August 18th, 2022.**

We discussed [Lipstick on a Pig](https://codeberg.org/small-tech/lipstick) and what it can teach us about [the most important lesson in design](https://ar.al/2022/08/17/lipstick-on-a-pig/). 

### Topics mentioned

- [Fedora Silverblue](https://silverblue.fedoraproject.org/)
- [Black Box terminal](https://gitlab.gnome.org/raggesilver/blackbox#black-box)
- [Helix Editor](https://helix-editor.com)
- [Bat](https://github.com/sharkdp/bat#readme)
- [Delta](https://github.com/dandavison/delta#readme)
- [Kitten](https://codeberg.org/nodekit/app/src/branch/kitten#kitten)

_Streamed using our own [Owncast](https://owncast.online) instance. (Hint: you can install Owncast using [Site.js](https://sitejs.org).)_

If you like this livestream, please help support out work at Small Technology Foundation with a [patronage or donation](/fund-us), or share our videos with your friends!

### Transcript

To follow.

{{< fund-us >}}
