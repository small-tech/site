---
title: "Small Technology"
date: 2019-08-23T16:42:57+01:00
speaker: "Laura and Aral"
description: "Laura and Aral speaking at ThinkAbout! in Cologne, Germany. May 2019."
---

{{< video 
	poster="poster-small-technology.jpg" 
	vimeo="//player.vimeo.com/external/355368347.hd.mp4?s=3e60da74579b368cd489b5493065f354db762d1a&profile_id=175"  
>}}

Laura and Aral speaking at ThinkAbout! in Cologne, Germany. May 2019.
