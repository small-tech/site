---
title: "Sabotaging Our Future Selves"
date: 2019-08-22T17:43:25+01:00
speaker: "Laura"
description: "Laura speaking at Webstock in Wellington, New Zealand. February 2018."
---

{{< video 
	poster="poster-sabotaging-our-future-selves.jpg" 
	vimeo="//player.vimeo.com/external/355368109.hd.mp4?s=8cea185b52fc8f6f6751446278cbc27757cfee90&profile_id=175"  
>}}

Laura speaking at Webstock in Wellington, New Zealand. February 2018.
