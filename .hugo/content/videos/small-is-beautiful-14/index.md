---
title: "Small Is Beautiful #14"
date: 2021-12-16T17:00:00+00:00
speaker: "Aral and Laura"
description: "Out with the old, in with the new!"
---

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/657555228" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>

## Out with the old, in with the new!

Laura and Aral [said goodbye to an old friend](https://better.fyi/) while also celebrating [the inclusion of Catts as the default task switcher in elementary OS](https://github.com/small-tech/catts#catts-is-now-part-of-elementary-os) and the launch of [Comet](https://comet.small-web.org/), Aral’s little Git commit message editor for elementary OS.

Aral also demonstrated the latest improvements to [Watson](https://github.com/small-tech/watson) – his best-practices application template for elementary OS.

Finally, we took a look at how what Aral thought was a three-month tangent in desktop development actually come full-circle back to our work on the [Small Web](https://small-web.org) with decentalised app distribution via the Small Web.

_Streamed using our own [Owncast](https://owncast.online) instance. (Hint: you can install Owncast using [Site.js](https://sitejs.org).)_

### Links from this month’s livestream

- To follow.

If you like this livestream, please help support out work at Small Technology Foundation with a [patronage or donation](/fund-us), or share our videos with your friends!

### Transcript

To follow.

{{< fund-us >}}
