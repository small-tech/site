---
title: "Small Is Beautiful #13"
date: 2021-11-18T17:00:00+01:00
speaker: "Aral and Laura"
description: "A rotten Apple."
---

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/656090806" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>

## A rotten Apple

We’re finally back with a Small is Beautiful livestream. We forgot the livestream in October because we were in the middle of moving house 🙈

Today’s livestream is just Aral and Laura, catching you up on what we’ve been working on. As Apple shoots itself in the foot on privacy by doubling down on implementing client-side scanning, we’ll take a look at the three projects Aral has been working on for elementary OS.

_Streamed using our own [Owncast](https://owncast.online) instance. (Hint: you can install Owncast using [Site.js](https://sitejs.org).)_

### Links from this month’s livestream

- To follow.

If you like this livestream, please help support out work at Small Technology Foundation with a [patronage or donation](/fund-us), or share our videos with your friends!

### Transcript

To follow.

{{< fund-us >}}
