---
title: "Small Is Beautiful #22"
date: 2022-09-15T17:00:00+00:00
speaker: "Aral, Laura, Tyler, Michael"
description: "Kitten, DWeb Camp, and Braid"
---

{{< video 
  poster="./poster.jpg" 
  vimeo="https://player.vimeo.com/progressive_redirect/playback/750091408/rendition/1080p/file.mp4?loc=external&signature=da723a39ea8edd5171ea09d082ec596c16034d894c0257f566555cfa2d72bbba#t=26" 
>}}

## Kitten, DWeb Camp, and Braid

**Broadcast on September 15th, 2022.**

We discussed [Kitten](https://codeberg.org/kitten/app), [DWeb Camp](https://dwebcamp.org/), and [Braid](https://www.braid.org/) with special guests [Tyler Childs](https://tychi.me/) and [Michael Toomim](https://invisible.college/@toomim).

_Streamed using our own [Owncast](https://owncast.online) instance. (Hint: you can install Owncast using [Site.js](https://sitejs.org).)_

If you like this livestream, please help support out work at Small Technology Foundation with a [patronage or donation](/fund-us), or share our videos with your friends!

### Transcript

To follow.

{{< fund-us >}}
