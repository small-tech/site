---
title: "Small Is Beautiful #20"
date: 2022-07-21T17:00:00+00:00
speaker: "Aral and Laura"
description: "Kitten"
---

{{< video 
  poster="./poster.jpg" 
  vimeo="https://player.vimeo.com/progressive_redirect/playback/740699547/rendition/1080p/file.mp4?loc=external&signature=4a19a175ea2613b45fe29273ee9d3d3fb2077dde366728c48953ddaa4204772b" 

>}}

## Kitten

**Broadcast on July 21st, 2022.**

We talk about moving from GitHub to Codeberg (and why we’re also moving away from our own hosted GitLab instance), why switching from Fastmail to HEY was a mistake, and how Nodekit is becoming Kitten.

### Topics covered

- [Codeberg](https://coderberg.org)
- [Open Switcher](https://openswitcher.org) (and Aral’s rant on inclusiveness and [SourceHut](https://sourcehut.org/))
- Why [HEY](https://hey.com) was a mistake.
- [Fastmail](https://www.fastmail.com/)
- Moving [Nodekit](https://codeberg.org/nodekit/app) (with [Svelte](https://svelte.dev/)) to [Kitten](https://codeberg.org/nodekit/app/src/branch/kitten#kitten) (with native HTML, CSS, and JavaScript).
- [htm](https://github.com/developit/htm)
- [vhtml](https://github.com/developit/vhtml)
- [htmx](https://htmx.org/)
- [hyperscript](https://hyperscript.org/)
- Aral’s rant on how “enterprise” screws everything up.
- [Penpot](https://penpot.app) (online illustration tool)

_Streamed using our own [Owncast](https://owncast.online) instance. (Hint: you can install Owncast using [Site.js](https://sitejs.org).)_

If you like this livestream, please help support out work at Small Technology Foundation with a [patronage or donation](/fund-us), or share our videos with your friends!

### Transcript

To follow.

{{< fund-us >}}
