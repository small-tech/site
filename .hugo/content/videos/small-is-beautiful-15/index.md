---
title: "Small Is Beautiful #15"
date: 2022-01-20T17:00:00+00:00
speaker: "Aral and Laura"
description: "web3 is bullshit (long live web0) and wtf is NodeKit?"
---

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/668568565" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>

## web3 is bullshit (long live web0) and wtf is NodeKit?

This month, we extend a friendly middle finger to web3 with the [web0 manifesto](https://web0.small-web.org), talk about Laura’s presentation for Ikea on ethical design, and introduce you to [NodeKit](https://github.com/small-web/nodekit), the successor of [Site.js](https://sitejs.org).

_Streamed using our own [Owncast](https://owncast.online) instance. (Hint: you can install Owncast using [Site.js](https://sitejs.org).)_

### Links from this month’s livestream

- [web0 manifesto](https://web0.small-web.org)
- [NodeKit](https://github.com/small-web/nodekit)
- [Site.js](https://sitejs.org)
- [Smashing Conference Meet(s) for Good: design ethics (Laura is speaking)](https://smashingconf.com/meets-for-good/)
- [Svelte](https://svelte.dev)
- [esbuild](https://esbuild.github.io/)
- [SvelteKit](https://kit.svelte.dev/)

If you like this livestream, please help support out work at Small Technology Foundation with a [patronage or donation](/fund-us), or share our videos with your friends!

### Transcript

To follow.

{{< fund-us >}}
