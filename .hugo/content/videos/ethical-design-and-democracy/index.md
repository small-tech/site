---
title: "Ethical Design and Democracy"
date: 2016-01-30T17:08:57Z
speaker: "Aral"
description: "Aral at ‘Power To The People’ at De Balie in Amsterdam. January 2016."
---

{{< video 
	poster="poster-ethical-design-and-democracy.jpg" 
	vimeo="//player.vimeo.com/external/157133166.hd.mp4?s=c4a208516346b1a2c2530c66909e32711d1a1aa0&profile_id=113" 
>}}

Aral speaking at Power To The People at [De Balie](http://www.debalie.nl) in January 2016.
