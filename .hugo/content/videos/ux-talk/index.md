---
title: "Superheroes & Villains in Design"
date: 2013-05-30T17:08:57Z
speaker: "Aral"
description: "Aral at Thinking Digital in Newcastle, UK. May 2013."
---

{{< video 
	poster="poster-ux-talk.jpg" 
	vimeo="//player.vimeo.com/external/133430959.hd.mp4?s=8a96e7ede72482a65add5610be0271eb&profile_id=119" 
>}}

Aral speaking about experience design at Thinking Digital in May 2013.
