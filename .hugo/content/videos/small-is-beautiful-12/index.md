---
title: "Small Is Beautiful #12"
date: 2021-08-19T17:00:00+01:00
speaker: "Aral and Laura"
description: "One bad Apple."
---

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/667298975" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>

## One bad Apple

Small is Beautiful took a break last month but we are back and raring to go!

What Apple is doing with its plans to scan content on your own devices and notify third parties is an attempt to redefine what it means to violate your privacy.

_Streamed using our own [Owncast](https://owncast.online) instance. (Hint: you can install Owncast using [Site.js](https://sitejs.org).)_

### Links from this month’s livestream

- To follow.

If you like this livestream, please help support out work at Small Technology Foundation with a [patronage or donation](/fund-us), or share our videos with your friends!

### Transcript

To follow.

{{< fund-us >}}
