---
title: "Small Is Beautiful #9"
date: 2021-04-15T18:15:57+01:00
speaker: "Aral and Laura"
description: "National Rail Fail: lessons to be learned on privacy and accessibility."
---

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/534527822" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>

## National Rail Fail: lessons to be learned on privacy and accessibility.

Livestreamed on Thursday, April 15, 2021. [Read the transcript below](#transcript).

We discussed the National Rail greyscale “mourning” accessibility issues, how Better Blocker (not so) mysteriously blocked the greyscale changes and the privacy implications of it all. With great questions from guests in the studio, we talked about avoiding tracking on site creator platforms, 3NWeb, solar-powered websites, privacy in accessibility audits and the dreaded AccessiBe overlay. And Aral introduced a way to easily [install Owncast using Site.js](https://sitejs.org).

### Links from this week’s livestream

- [Better Blocker](https://better.fyi)
- [How to fight back against Google FLoC by Plausible Analytics](https://plausible.io/blog/google-floc)
- [Site.js](https://sitejs.org)
- [Margo de Weerdt’s website](https://www.margodeweerdt.com) and [her fabulous 2021 calendar](https://www.margodeweerdt.com/product-page/calendar-2021)
- [Owncast - Selfhosted Livestreaming](https://owncast.online)
- [Small Technology Foundation’s own Owncast instance](https://owncast.small-tech.org)
- [Aral Balkan’s website (where he streams his S’updates)](https://ar.al)
- [3NWeb](https://3nweb.org)
- [Star Labs Linux Laptops](https://starlabs.systems)
- [LOW←TECH MAGAZINE](https://solar.lowtechmagazine.com)
- [Solar Protocol](http://solarprotocol.net)
- [A Clickbait Site’s Trackers Cost the USA $4M in Data Monthly, Entrepreneur Says](https://observer.com/2016/10/aral-balkan-dokutech/)

### Transcript

**Aral**: Hello, I'm Aral Balkan

**Laura**: and I am Laura Kalbag, and together we are Small Technology Foundation, a teeny tiny, not for profit based here in Cork in Ireland. And unlike last week, we've gone back to the original set up. I am back downstairs and he is upstairs.

**Aral**: And this is a monthly live stream that we call Small Is Beautiful about all things Small Technology. This week, if you happen to go on the National Rail website, you didn't see it like this in colour. What you probably saw was the site in black and white. Now, why was the site in black and white? Well, if you don't live in the UK, you may not be aware, but there was this very rich, very racist guy and he died.

**Aral**: So the nation is in mourning. And so because of this, National Rail thought let's make the site black and white, or at least this is what we heard. When I went there, I saw the site in colour, oddly. And it turns out that that was because I had Better, the tracker blocker that Laura and I make, on my computer when I went there, and it actually was on my phone so I could see it in colour. So why? Why was that? Well, if you turn Better off, then the site went back to black and white.

**Aral**: And this is because they were using a Google tracking script in order to add the greyscale to their website. So that's what happened today. And that's going to be our major topic because it was a huge accessibility issue, wasn't it, Laura?

**Laura**: Yeah, it was. That's what happened on Monday, we should be specific. And they actually did, because of such a huge uproar on Twitter, and I imagine all the platforms as well, they ended up changing it back within a couple of hours. After first initially defending it a little bit, in fact, tweeting it because they were proud of it to begin with, which I mean, it's a  strange cultural thing. I know I'm the British one here, and the way that people are about royalty… it was in case you didn't guess, Aral was saying, it was because Prince Philip, the Queen's husband, died.

**Laura**: And that's why they decided to mourn his loss by making the train booking website greyscale. It's bizarre. And so there was a huge uproar because this affects accessibility. Now, let me show you so that I can explain why. Let me share my screen.

**Laura**: So, oh, yes. This was the… is my screen showing ok, Aral?

**Aral**: I can't seem to see it, no.

**Laura**: OK, let me just check. I've got to select the options first. There you go.

**Aral**: That's looking better.

**Laura**: So, yes, “hi, the website has been set to grey while we are in the mourning period of Prince Philip.” This mourning period… I mean, it depends on what they're talking about, but it could be a week or so. Obviously, their mourning period only lasted a few hours. So this is the National Rail website in greyscale. Now this website is one that a lot of people use for booking their train travel across the whole country, the whole of the U.K. And so you can see here in greyscale it’s pretty, it's tricky to use. This is quite a complex interface… If you look at it in colour… Now, in colour, one of the things that really is distinctive on the National Rail website is that they highlight the cheapest fares in a bright yellow colour. The problem is that when you make this into greyscale, it is pretty much indistinctive from any other element on the page. So one of the conventions that people would have really relied upon, trying to find the cheapest fare, would be nearly impossible to navigate to without reading the text, which is crammed in amongst a lot of other text.

**Laura**: When I looked at a lot of the tweets on Monday, a lot of people were talking about colour contrast issues and I thought I would look into that a little bit more. So I think if you joined Aral and I last week when we were talking… what did we talk about last week, Aral? I’m so tired.

**Aral**: I believe it was the…

**Laura**: Oh yeah, the component, the Nano donate component…

**Aral**: The Nano donation component. That's it.

**Laura**: So we talked then a little bit about colour contrast accessibility. So I'm not going to go into it too much today. And you can go back and listen to that video, watch that video if you're interested. But one of the things that people were talking about was the problem, fundamentally, the accessibility of the site was broken when they put it into greyscale because of the colour contrast. So I thought I'd have a little look into this. So I'm just in Firefox at the moment and there's this really handy little colour contrast checker extension. I've got quite a few on my computer, but this one’s pretty useful and I just use that to check the site when it's in greyscale.

**Laura**: So I've actually mimicked what they did because the site is no longer in greyscale. And you can see here there's an interface that has… what it does is it actually tells you when the contrast isn't acceptable for accessibility against the WCAG…

**Aral**: Would it be possible to make that text a bit bigger?

**Laura**: I can try…

**Aral**: Might help folks on smaller screens.

**Laura**: Yeah, I've tried to make it big already, but I can't scroll this interface when it's zoomed in. But that's accessibility issue, ironically. And so the… this contrast checker, it just shows you, in order, when there are elements that have colour contrast issues. So that's when the text is not distinguishable enough from the background colour. Now it uses the WCAG guidelines which are the Web Content Accessibility Guidelines to determine what enough contrast is.

**Laura**: And that's a debate and an argument for another time anyway. But from what… I just use this little extension and from checking it, it told me that about 10 elements on the page did not have sufficient contrast and the rest of them apparently do. So what I did was I then went and looked on the colour version of the site, I ran the same check again. Only to find that there's actually still 10 elements that don't have sufficient contrast, so despite what a lot of people have said, the colour contrast, the difference, the greyscale, didn't actually have a difference to the colour contrast, but it did make it harder to use.

**Laura**: And that's why I think it's really important to be able to understand different issues that we can have with colour and accessibility. And I think that this was partly that it's a very overcrowded page. And so people use the colour to navigate it, and to find distinctive elements on the page, rather than it specifically being colour contrast issue. Although, of course, people reportedly, anecdotally say that they do actually struggle more with interfaces when there’s greyscale as well. If you're interested…

**Aral**: Do you think any of that, Laura… sorry to cut you off, do you think any of that has to do with some of our learned perceptions of colour from nature? Like, you know, we see something is blue and we kind of think, OK, well, that's probably going to be far away because that's kind of the sky. And if there's something that's yellow or red, we're kind of biologically wired to seeing, oh, that might be a danger sign. That might be something I need to be aware of. So do you think maybe that's that's true? Have you looked into that or…?

**Laura**: I mean I think some designers probably use those kind of cues. I mean, people tend to use red for error messages and things like that, based along those kinds of similar ideas, that may be in a traffic light, the stop light would be red. And so you have those kinds of…

**Aral**: or poison animals…

**Laura**: colour conventions that vary, but they vary across culture as well. So it could be different depending upon your culture. I don't think it's so much that. And now I mean, this is my non-scientific impression.

**Laura**: I actually think that in the case of this particular website, the issue was the change, and the lack of distinction between different areas of the page for people who could perceive the colour on it. And I think that that's the biggest thing, that is something where you are used to navigating, using particular signposts a lot of the time. And that suddenly changes. That is going to cause you problems. If it's something that you rely on in order to be able to book your travel. That's a really big deal.

**Aral**: And it might think that some of those colours, like those colour areas they saw as landmarks, like people were going for, oh, this yellow bit over here. I know. That's where I get my job done. You know what I'm trying to do.

**Laura**: Exactly. Exactly. And so you, if you look at the page, the biggest yellowest brightest buttons are the ones that you’d probably be focusing on. They also have these sort of light blue buttons that are probably the other ones that you’d be focusing on. And then they have other grey buttons, light blue buttons. And if you make that grayscale, those buttons all appear to be exactly the same, or very similar colours.

**Aral**: Right.

**Laura**: So you can't distinguish between them anymore. You can still tell what the content is, so they're not completely inaccessible, but you reduce the accessibility of the page. There’s a really good book, if you if you're interested in colour and accessibility, by Geri Coady, that's called Color Accessibility Workflows. And it's from A Book Apart. It's a “brief”, it's available as an ebook and you can read it in less than an hour. It's a really great book if you're interested in learning a bit more about colour and accessibility, I would really recommend it.

**Aral**: And since you're not very good at blowing your own horn, I'm going to blow it for you. And of course, you have a book on accessibility, right, Laura?

**Laura**: Oh, yes I do.

**Aral**: It's more general about accessibility.

**Laura**: Yes, I can find it on that very same website if I… it's slightly older, so it's not on the home page right now. [laughs] There we go, the green one, one of the green ones. Yeah, my book is Accessibility for Everyone, and I do touch on colour contrast and colour accessibility in my book as well. I think…

**Aral**: But it wasn’t just accessibility, right? I mean, to pour salt on the wound, the way that they made the site greyscale was using a tracker, one of Google's trackers. Right? And you looked into this as well because you do the Better Blocker tracker updates. For those of you who don't know, by the way, Better Blocker is a tracker blocker that we make, it's at better.fyi. So, Laura. Yeah, I remember coming upstairs to you and saying, can you take a look at this? Because I want to know how they're doing it, why Better is blocking it?

**Aral**: And so would you like to talk a bit about that?

**Laura**: Yeah. So usually when we change the colour on a page, if we change any kind of something that is styling on a page… if it’s a font, if it's a colour, it's the text. What we would usually do is do it using CSS, cascading style sheets. And that's not usually something that's an issue when it comes to privacy. But for some reason, the… whoever changed the content on this particular website decided that they would apply this change using JavaScript.

**Laura**: I mean, they used the CSS, they used a filter that's just called “grayscale”. Probably wouldn't recommend it in many cases, certainly not to use on whole pages like they did. But what they did was they used Google Tag Manager to do this. Now, Google Tag Manager is really… it's catch-all service that Google provides where they say, “hey, you want to do this stuff on your website, we want to help you do it without having to know any code at all. All you need to do is install a tracking script onto your website and we'll provide you with all of this functionality for free.”

**Laura**: And we know because Google's business model is to be able to track people and to be able to monetise that information, in one way or another. So they provide this for free because you are essentially making your website into an extension of Google. And so one of the things…

**Aral**: Sorry to cut you off, Laura, but one of the things also that people apparently routinely do using Google Tag Manager is A/B testing. So they inject one thing into one version of a site to create one version of the site, and they, so they're testing two different versions of it. And Google Tag Manager lets you do that. So, I mean, if they're already doing that, normally, it would make sense that they thought, “OK, we're just going to have this for a couple of days. We'll just use the same system.” Yeah?

**Laura**: Yeah. And I mean, you can use, this is not certainly not an ad, and advertisement for Google Tag Manager, but you can use it for all these kinds of things, use it for inserting Google Analytics. So basically, any kind of tracking script you want to add, Google Tag Manager will help you edit easily with just one thing pasted onto your web page so you can really see what the benefit is for Google there. So with Better Blocker, what we try to focus on is blocking trackers. So what you might have heard of is ad blockers that block ads that people find annoying on the Web.

**Laura**: Well, we decided to block trackers, so that often has the effect of blocking ads, because most ads in ad networks nowadays are targeted ads. And so those are tracking you in one way or another. But we are trying to block the trackers underlying everything. So that could be analytics, that could be anything that wants to track you and use your information against you, anything that might potentially exploit your privacy. And so, of course, Google Tag Manager, we block it.

**Laura**: And actually, I have to say that most of the time we don't really get any problems from blocking Google Tag Manager. You'd think that they're providing this amazing, important functionality to websites. But I checked earlier today, and as a result of blocking Google Tag Manager, we've only had six sites where people have reported the site breaking as a result of blocking that. So that's pretty good going, I’d say.

**Aral**: But I mean, you also I mean, I know this firsthand. You also go in and when you can, if some sites break when we block a tracker… and usually it's because of this, because they're tying functionality, core functionality to third party tracking, which actually should violate GDPR [General Data Protection Regulation] in every way possible, because that makes it impossible to separate core site functionality from tracking and from third party advertising, et cetera. But one of the things that we try to do with Better Blocker, because we don't want to we don't want to just ruin everyone's web experience, because the web in general is, of course, this sewer of surveillance.

**Aral**: But… so we have to go in and you usually do this, you go in first hand, and actually try and, per site, unbreak it.

**Laura**: Yeah, it's it's a rant for another time, to talk about…

**Aral**: Yep [laughs]

**Laura**: how fragile development practices have become. Because really, if you're layering on scripts into a web page, you should really be thinking about what happens if that script fails, particularly if that script is provided to you by a third party. If you're getting it from somebody else's website, somebody else's computer, as we might also refer to it as, then you have a fundamental fragility in your website because you are not only relying on that person to always have that script up and running, but you are also giving them pretty much access to all of the people who are visiting your site.

**Aral**: But of course, this is actually a good unplanned segue. It's not even going to matter pretty soon if Google has its way, right? Because Google is working on something called what is it, FLOC…  F.L.O.C.. So, OK, let me just talk about this for two seconds. And by the way, if you want to join us in the studio and take part in the conversation, the URL, the link is on the screen right now. It's just small-tech.org/ studio if you have a webcam and headphones, so we don't get any feedback. Join us in the studio. If you want to join the stream, ask a question, join the conversation. If you have any thoughts on the topic that we're covering right now. But as Google…

**Laura**: And as I always say, if you don't have headphones or a mic, or you're just too shy to ask the question yourself, pop into the studio, ask the question in the private chat, and I'll ask it on your behalf.

**Aral**: Very cool, so I'm just going to open up my web browser here and switch to my view, and just to show you what this FLOC thing is, let's just go to this great blog post that the Plausible analytics folks…

**Laura**: Are you going to share your screen?

**Aral**: Yeah, just one second. I'm also just going to remove these display names. There's no quick way of doing that… Alright. Here we are. So the… today, at least I saw it today… Yeah. It was written today, the Plausible analytics folks, which is an alternative to Google Analytics, which is a lovely alternative that doesn't violate people's privacy. It's by to two developers, one of whom is Marko, who wrote this blog post, How to Fight Back Against Google FLOC. “Federated Learning Of Cohort” is what they're calling it. So basically what it is is, it’s your it's your browser spying on you, is basically what it is.

**Aral**: So instead of third parties spying on you, Google will be spying on you in Chrome. So… and it's going to be on by default, is how they want to have it. And if you are a… in the blog post, Marko handles this really well. It says how to opt out of FLOC as a web user or as a person who uses the web. Don't use Google Chrome. It's that simple. Do not use Google Chrome. If you're using Google Chrome at this point and you're not being forced to do it for some reason, there's very little we can do to protect you, for example. Or anyone else, because you are using a tool by a people farmer. Also, though, how to opt out of FLOC as a web developer… you can set a permissions policy. So if you're a web developer, you can put this header into all of your requests, into the responses of your requests. And that will tell Google using this web standard, that actually Google suggested, that they should not be tracking people on your site.

**Aral**: Now, this is all levels of ridiculous, of course, but there is a way to do it, at least with Site.js, which is our personal Small Web server. As of the latest release of Site.js today, it does that automatically for every site that's served with Site.js. So any web server developer can actually add this to ,their product. Folks like WordPress can actually make this a default. Now, this is ridiculous. It's ridiculous because we shouldn't have to, every time a website is hit, we shouldn't have to say to Google, “please, Google, stop tracking us. Don't track the people who are using our sites. Don't track…” This is ridiculous. This goes completely against the spirit of data protection. It goes against the spirit of GDPR. They’re apparently not testing it in places where GDPR exists right now for that reason… because it would probably fall foul of it in five different ways, and it's just ridiculous. But it's also Google showing you their true colours. I mean, you, of course, didn't need this. That's their business model. That's how they make their money. We've been going on about this for years and years and years. And still people are like, “oh, Google, they're so nice. Look at their colours, their little doodles.”

**Laura**: “They give us so many things for free out of the goodness of their hearts.”

**Aral**: “They love us. They love us…” anyway. So, yeah, FLOC, to think of it like this, Google is a people farmer and you're their flock, of sheep.

**Laura**: [laughs] Nice.

**Aral**: That's where I remember from, if you want. So we also have two people who have joined us in the studio and…

**Laura**: Three! Three people!

**Aral**: We have three? Oh, I need to scroll down. Sorry. Sorry, Billy. You're down there… I'll just zoom out on my screen that I'm looking at here.

**Laura**: [laughs] Relegated to row two…

**Aral**: You know what we're going to do? Let's do this. Let's go wild. Let's just add everyone to the stream right now. Let's see what happens. What do you think, Laura? Laura’s eyes have just gone wide…

**Laura**: Yeah, everyone thumbs up…

**Margo**: Hello!

**Laura**: if you’re OK with…

**Aral**: And here we go…

**Laura**: well I was going to say, if people actually want to be added…

**Aral**: [laughs] everyone put your thumbs up if you’re ok to be on the stream, and Aral’s just like, “click! click! click! click!”

**Laura**: What if I do this? [gestures thumbs down]

**Aral**: Hello. So we have Margo de Weerdt, and we have Mikalai, and Billy Smith. And I hope I haven't butchered any of your names. Margo, I don't think I've butchered yours, at least because we we know Margo from way back… hi!

**Margo**: Yeah.

Like almost… last year. It seems like ages. Hi, everyone.

**Margo**: It seems like ages. Hi.

**Aral**: It seems like ages… Well, I mean, since the lockdown, I've known Margo since before the lockdowns and covid… So it's like we go way back…

**Laura**: There’s been a time before the lockdown?

**Aral**: There has.

**Margo**: It’s unbelievable.

**Aral**: It is, so I’m going to ask…

**Laura**: Billy made an interesting point that StreamYard actually uses Google Tag Manager. The thing that we're using for the streaming in the studio right now, uses Google Tag Manager.

**Aral**: I am in no way surprised by that. And I hoped they wouldn't, but they are just a regular startup. I mean, their product works really well. We don't expose the people who are watching, at least to it, because you're either watching this on Vimeo right now, or something I want to talk to you about actually, and show you. You can also now watch it from our own Owncast. So we're going to… we'll talk about that in a second. But first of all, just introduce yourself. So, hi, let's start with Margo and then we'll go to Mikalai and to Billy, since that's the… so Margo, hi!

**Margo**: Hi!

**Aral**: I know you and Laura knows you, but. But who are you? Introduce yourself.

**Margo**: Well, I hope everybody can hear me. I’m micced-up.

**Aral**: We can hear you. And this is your new camera, right? You are? This is a new camera you just bought…

**Margo**: I'm using a new Fuji, but I'm slightly orange because of the light in here and I'm in my living room.

**Aral**: These things happen, yeah.

**Margo**: You know…

**Aral**: Who are you?

**Margo**: I'm a freelance illustrator from Antwerp, and also do graphic design. But since I've met Aral, I also had an interest in accessibility and in using the web safer. So that's why I'm here, to support my friends and to learn.

**Laura**: Yay!

**Aral**: And you know what? Actually, here I'm going to show you something. Take a look. This is your calendar that you made…

**Margo**: It is!

**Laura**: It’s gorgeous.

**Aral**: And I love it so much, I haven't actually had the heart to put it up and actually use it as a calendar because I don't want to ruin it.

**Laura**: Too precious!

**Aral**: But these are the gorgeous, gorgeous images. And Margo was also working with Laura well, allowed Laura to use, and allowed us to use her illustrations because Laura's working on a photoblog theme as part of her work with Site.js. And so we've got those, and hopefully one day we'll be able to kind of show you those as well. It's it's really cool. It looks great.

**Laura**: Hopefully.

**Aral**: You can actually go to…

**Margo**: Looking forward!

**Aral**: Yeah. And you can go to Margo's website as well. Margo, what's your website?

**Margo**: You can.

**Aral**: Tell people your…

**Margo**: My website is www.margodeweerdt… I can type it in the in the comments my name, because…

**Aral**: OK. Cool. You do that and I'll make a banner out of it and then people can see…

**Margo**: Thank you!

**Aral**: So… and go to your website where they can actually order that calendar as well. I'm totally going to be pimping your stuff here.

**Margo**: Thank you.

**Aral**: Alright.

**Margo**: That is so sweet. But I do have a question…

**Aral**: Did you have a question? Yes. Go, while I do that, ask your question.

**Margo**: It's about the accessibility and Google Tag Manager also. I have a Better Blocker, so it normally blocks everything, as Laura said, from this tool. But I have my own website on Wix and I know other people who have their… who are not developers, who have their sites on platforms. Can you know, as you have your site on there, if, you know, Google Tag Manager is installed? So your visitors will… yeah, it gets used on some of the visitors on your website? I don't know if…

**Laura**: That's a good question. I can probably actually have a quick look at your website and tell you. I think a lot of sites especially… so Wix is a place where you can, a platform, where you can create your own website. And it's… I think it's going a drag and drop interfaces and things like that…

**Margo**: Yeah.

**Laura**: …that makes it really easy to make your own site.

**Margo**: Yeah if you’re not a designer…

**Laura**: And a lot of the these… yeah, and it is not in the slightest bit your fault that the people who make these things don't necessarily have the same approaches as we do. And… but I will…

**Aral**: That's where you can also apply pressure because you're a customer, right? You're you're paying for that.

**Margo**: Yeah.

**Aral**: So it's also a great opportunity for you to go to them and say, “look, I've noticed that if there is Google Tag Manager, and if you're using Better on a Mac as well, you can actually open up the developer console…

**Laura**: Which is what I'm doing right now, on my screen…

**Aral**: Because I think we looked a little bit at developing sites and I think we covered the Developer Console last time we spoke. So if you open that up, Margo, you'll be able to see which trackers Better is blocking.

**Margo**: Yeah, okay.

**Aral**: Apple won’t actually let us give that information, like get that information, somehow. I put in a request for that many years ago. There's been no work on Apple's part, on the content blocker stuff ever since it was released. There was like one update, I think they added a certain feature and it's been just stagnant ever since. They've had their own stuff that they've released, which works differently, which is fine, but it works differently. So we can't even show you like which sites we’re blocking, you have to look in the Developer Console for that, sadly. And I mean, if Google gets away with what they're doing, that's also going to be kind of moot in a way, if anyone's using Google's Chrome, they're going to be tracked.

**Margo**: And…

**Aral**: So while this is happening, yes, go… I was going to…

**Margo**: Last thing and then I’ll…

**Aral**: It's all right, go.

**Margo**: OK, but if you don't have installed on this site, I don't have Google Analytics or anything installed on it, it pushes it to do it, but I don't.

**Aral**: Yeah. And you should be fine.

**Margo**: Does that mean it’s not on there?

**Laura**: Yeah, I just had a little look, I just had to look at the site and Better’s not actually blocking anything on your site. So… and I just had a look through the code on the site just to see on the homepage what it's pulling in. And it looks like Wix mostly has its own… Well, it's swings and roundabouts. It looks like it has its own first party functionality for everything. And so the negative to using a platform like Wix, and this is also a negative to using sort of big platforms like Google Analytics and things like that, is that for Wix, the benefit for them is they can they see all the activity that's going on on any Wix site. So they have an overview of any visitors on any Wix site. So that is a big win for them. So it's really, if you're comparing using maybe a big provider versus a small provider on a site creator, going for a smaller one is better than a big one, and going for something like Wix is probably better than going for something if there was a Google equivalent, because it's basically who has the most power. But…

**Margo**: OK, so. Yeah.

**Laura**: Yeah.

**Aral**: And you know…

**Laura**: So they're not giving things to Google that we can tell.

**Margo**: OK, thank you.

**Aral**: This could be a really great use case for Site.js as well. If we had the capacity to actually build a hosting solution just for Site.js, just for hosting small personal websites. But our efforts are going into building what I'm calling the Small Web, or what we call the Small Web. So that it's not just any website that you can set up very easily, and that we’ll actually put all of our effort into supporting that, but it'll be a site that can talk to other people's websites as well, peer to peer.

**Aral**: So that's why even though we have Site.js, we just don't have the capacity to put into like building a hosting service around it, which would be a Wix that you own sort of thing. Right?

**Margo**: Yeah.

**Aral**: But it’s free and open. If somebody else does have this capacity, you know, build that. And you can also build on a lot of the hosting-related stuff that we're working on, if you look at our repositories and kind of my s’updates that I have on my website on ar.al, but OK, so we have two other folks in the studio, so I'll go to Mikail first.

**Aral**: Mikalai, sorry, Mikalai, not Mikail. Hi. I'm butchering your name right, man?

**Mikalai**: No Aral, you're doing it perfectly. It's Mikalai and…

**Aral**: You're too kind.

**Mikalai**: So yeah I know all about your existence for just two weeks now, and I think I sent you an email, maybe you remember it, but like I'm doing…

**Aral**: Ooh I suck with emails. Did I reply?

**Mikalai**: You did. It's a 3…

**Laura**: That’s an achievement!

**Mikalai**: Yes. So basically, I am. And like folks that work with me, like we're doing this 3NWeb, which is like, how do we have a modern world where, as you said, like on the pyramid, you can have your wonderful, exciting things done, but in such a way that the level, this foundation, you have something that supports your privacy, security, the dignity. You know, the dignity of your life can happen.

**Aral**: Right.

**Mikalai**: And that platform is like what that 3NWeb thing is. And the 3NWeb is basically like the server sees no plain text, the server, has no unnecessary metadata. As a result, there's nothing on the server to, you know, leak and stuff. And the web, it means that, oh, it's a web-style federation.

**Aral**: Right.

**Mikalai**: It's like, for example, SMTP federation or XMPP federation… it’s like it can be broken by the monopolies. So all of a sudden in the security, like there's this thing we talk about can can somebody see us or something like that? But then there's like a denial of service attack, like, for example, what Google did it with their Hangouts. There was a nice XMPP world, and people they used to be and then they stopped federating with everybody.

**Aral**: Right. So we've seen this happen with email, right?

**Mikalai**: Yes.

**Aral**: You've seen it happen with XMPP, and with Google Hangouts, you said. And this is something like you were saying that's inherent to the federated model. So email is federated. Mastodon is federated, for example, right now.

**Mikalai**: So…

**Aral**: And sorry yeah, that's what I was saying was what can happen there is it has economies of scale. So if a large, powerful corporate with a lot of money comes in, and they become the largest node. So Google did this with Gmail, right? They became the largest node. So now if actually Google says, I don't think you should be sending email to, not just anyone using Gmail, but if they say, well, we're not going to accept your emails, we're not going to root them. They're so big that you kind of disappear from the email world in some way. So, yeah, that's a real issue. And that doesn't exist in peer to peer networks, which I believe is what you're talking about.

**Mikalai**: Yes and no.

**Aral**: Because there are no economies of scale. Yes… OK. All right.

**Mikalai**: So yes and no part comes in the following way. So there's the way of us users looking at the things. So when I mean a federated user experience, it means that I send email to you, Aral, at your website.

**Aral**: Yes.

**Mikalai**: At your actual domain.

**Aral**: Yeah.

**Mikalai**: That’s a federated user experience. Now there's a definition for federation which is like in the technical graphs for the tech guys, and it actually says that I, as a user, I think that I send a message to you. But you remember there were studies about emails when they came to actual users of the email that asked, OK, how do you think the email goes? And they say, well, the messages goes to Aral, and in reality it goes to my server, and then my server cooperates with that other server…

**Aral**: Yeah.

**Mikalai**: So the federation is always meant to be a cooperation between servers. And what I'm trying to say is that my user experience of federation can be mitigated in a different way. So it means that we can have a federation, we can have the servers, but they don't have to cooperate. And this is how the web has been already working. Right now I'm looking at the stream on StreamYard.com and another tab was your small.js, right? But your… those servers don't cooperate. In fact, if I were going through the tour, they wouldn't know that I am talking to you at the same time.

**Aral**: Right. So basically, it's about going directly.

**Mikalai**: Yes. It’s going directly. But at the same time, when we go… when we see peer to peer, what all of that people hear and what, for example, like Libre software, the Libre planet or FSF folks, they usually hear peer to peer, is that there's no servers involved. But…

**Aral**: Right. Well, basically what you're explaining… Yeah.

**Mikalai**: Yeah.

**Mikalai**: And if you don’t involve servers, you cannot have the modern convenience…

**Aral**: experience. Exactly, so the things that you can't have are availability and reliability…

**Mikalai**: Yup.

**Aral**: Because the nodes can't find each other. What you’re building, and what the Small Web is, is essentially so similar. And I think I wrote that to you as well.

**Mikalai**: Yes.

**Aral**: If I remember correctly…

**Mikalai**: They are compatible and they believe like what you are doing is like the web page is essentially a public place, right?

**Aral**: Yeah. Well public and private.

**Mikalai**: Yes. Well, here's the thing that we're making the server on the 3NWeb just doesn't know anything. So, for example, if on your server I have something, stuff that is private. My question is if it's stolen. What happens? Do they know? I mean…

**Aral**: Well, no, because anything that's private on the Small Web is end-to-end encrypted…

**Mikalai**: It has to.

**Aral**: So we never trust… So I think like what we're saying, what you're saying as well, is that you don't trust the server at all.

**Mikalai**: Yes.

**Aral**: It's just a relay node. All it does is it relays. So exactly the same thing. We should definitely chat more. But I also want to get to Billy. So it's really cool to meet you… if you want to put your URL for the 3NWeb stuff that you're working on in the chat, as well. I'll create a banner for you so people can look at that. And finally, Billy, who are you? Hi! You’ve been waiting so patiently.

**Billy**: Hi. My name’s Billy Smith. I’m an engineer. Mostly hardware. Software, I enjoy using but I don't really enjoy writing it. But it's, how to put it, different forms of process work. But mostly work on the hardware, I mostly work on open source hardware. And it's…

**Aral**: OK, cool.

**Billy**: Oh no, it's it's a fascinating field…

**Aral**: And very necessary. And we're seeing amazing stuff with, like the people at Pine 64, for example, are coming out… I don't know if you're probably familiar with their work. They're doing amazing things. We've got, I don't know… the StarLabs, folks, actually, this stream is running right now on my laptop, my everyday laptop, which is a StarLabs labtop, which the next version is going to be called StarBook, because when I got it, I was like, why didn't they call it StarBook? And then they sent me a tweet this week saying, we listened. So the next one's going to be called the StarBook, it’s pretty cool. But yeah, open hardware is so important. Did you have a question or something to add to what we were talking about specifically?

**Billy**: Well, have you seen… that and if I can get it…

**Aral**: There's a link to low tech magazine, let me just open it up, that Billy sent us in the chat, and I think I know what you're linking to… I'm not sure… this is a solar powered website, yeah?

**Billy**: Mmhm.

**Aral**: Here, let me just show you on my screen, because I just brought it up. So this is, if you haven't heard of it, this is really cool. It's solar.lowtechmagazine.com. And this is a site that's run on solar power. Sometimes it goes off if it does not have enough power. And this an article on how to build a low tech website. So that is… that's really cool. Billy, did you want to tell us very briefly about what that is?

**Billy**: Oh, low tech magazine is one that I've been following for decades. They basically look at a lot of older forms of technology, but using… and shows how they're using modern day materials, or today's forms of materials, you can get better results from… without having to have your technology and which is how do I put it? One of the problems that crops up is that, if you're running, if you're running a profit factory, you're producing a product, or a for-profit manufacturing system, you're producing products that are the most for your profit margin, not necessarily solve the customer's problems in the best way. The top example is the Edison battery. If you need a static battery system of that particular power, and that will provide power for you, the Edison battery works incredibly well. However, they can't be sold for a profit. The battery lasts too long. There was a mining disaster back in maybe 10 or 15 years ago. And the company that was using the… the mining company that had that system thing take place. The batteries… one of the reasons that the miners were able to survive so long, stuck underground, is because they were using power systems provided that were provided using Edison-style batteries. The only problem was that they were still using the original batteries that Thomas Edison jad sold them something like 70 or 80 years before.

**Laura**: Wow.

**Billy**: We cannot run a for-profit factory if you are only able to sell products to your customers once a century, however…

**Aral**: So planned obsolescence.

**Margo**: Wow.

**Billy**: However, if you need the sort of power required… if your power requirements can be fulfilled by a power storage system of that form factor, and that sort of power draw, then it's using modern CNC and systems. And then you can just make the bloody batteries for yourselves and then just use them.

**Aral**: Right.

**Billy**: And the other link I sent was which followed on from that was solarprotocol.net  where this is, it's basically… they're acting as an aggregator. So what they've got is  different sites all around the planet. Each will have links to the original. And each web server is based around different places on the planet. Each one server will be acting as storage for different range of websites. Yes! That’s it! And what they're doing is they use these… when you link to the solar protocol website, the stuff that will be served to you will be served from the website that currently has the most amount of sunlight, and the most amount.

**Laura**: Haha! That’s cool.

**Aral**: Now, that is very cool.

**Billy**: Now combine those two…

**Aral**: I can't reach it right now, though. I think it's down.

**Laura**: Not enough sun!

**Margo**: I’m on it.

**Billy**: It's up and running at the moment.

**Aral**: Is it?

**Billy**: Or at least…

**Aral**: solar…protocol.net…

**Billy**: Yes, solar…

**Laura**: Margo’s on it.

**Margo**: Yep.

**Aral**: Margo’s on it?

**Margo**: I’m on it.

**Aral**: Margo, are you using up all the solar power?

**Margo**: Possibly. [laughter]

**Aral**: That… is it you? Is that what's happening right now?

**Billy**: Anyway…

**Margo**: I’ll go off it…

**Aral**: Haha, yeah go off so I can go on!

**Margo**: It’s gone.

**Billy**: It was going… but if, for instance, you look at the way that low tech magazine is running this thing off the equivalent of a Raspberry Pi, so very little power requirements. And the… what's being delivered was actually, when you connected to their website, they can't use any of the trackers. They can't use any of…

**Aral**: No!

**Billy**: that particular thing, because those rely on good data systems, they rely on the constant always-on system, always-on tracking systems, which those can't run off a solar power system because there's constant drop. So, I mean, this is another…

**Aral**: That's so true. And I'm just going to just switch to my view here, and show you something. This was many years ago… This is an article in the… oh, god accept cookies whatever.

**Billy**: Yes.

**Aral**: In the Observer where we calculated and this was like in 2016… we found the site, a single site that actually cost people in the U.S. 4 million dollars in mobile phone charges every month just to download their trackers. So… haha, I like this quote from me. “We planted a bullshit seed and we got a bullshit tree.” [laughter] So yeah. This was literally one site, and the amount of data that was being downloaded by people in the US just on their mobile phones was costing them four million dollars. Just to download this tracking. It is absolutely ridiculous the amount of resources that are wasted on this bullshit that is surveillance capitalism. So it's… and then you see other things like right libertarian projects like Bitcoin and proof-of-work mining where we're literally at that point just saying, OK, the whole idea is that we waste energy.

**Aral**: The whole idea is that we have these machines upon machines, upon machines, and all they're doing is they're calculating hashes. They're calculating hashes. To do what? To secure a global ledger, which makes a handful of right libertarians even richer than they were originally

**Laura**: In the middle of a climate emergency.

**Aral**: In the middle of a climate emergency. Or you can sell your gif for a couple of million if you're lucky. So it's very… it's a very interesting topic, Billy. And I think, you know, I'd love to go into it more, and maybe we should actually have had one just  on these topics, really. But thank you. Thank you for that contribution and also for the anecdote as well.

**Laura**: Yes, it was great.

**Aral**: Lovely to meet you. And thank you for working on what you're working on, too. So I'm just going to say. Oh, sorry, Mikalai, did you want to say something?

**Mikalai**: Yeah, I wanted to comment… too many people ask, like, OK, so you guys allow me as a developer to make, you know, experience, like with a Chromebook, and use it on my device and I login, everything is back and it's not a luxury. How come it's not a luxury?

**Aral**: [laughs] We get used to it.

**Mikalai**: And we're caught it, we're actually caught as somebody who is trying to develop anything private or anything secure, we are caught… somebody created for us a really a tilted PR field in which we are already in that unsustainable corner. So maybe some of our customers who want to be green, eat less meat and stuff like that. They may not talk to us because they may think privacy means blockchain.

**Aral**: Yeah, it's ridiculous and it doesn't, right? So when when when I talk about the Small Web, for example, I'm not talking about having one global database that we can have a billion copies of, and then prove that every copy is the same. Right? That is a global proof. That is what proof of work gives you in a system like Bitcoin. Right? So that's important if you want to have a currency, if you want to have a currency and you want to prevent things like double spend. So I paid you that money and then I'm going to pay someone else with the same money. No, that's not… that fails as a currency. If, on the other hand, you're trying to protect personhood, you're trying to protect privacy, what you need is… If there are a billion people, you need a billion databases owned by those people. All of those databases unique to those people. And maybe you need two billion, maybe three billion, maybe four billion, because really what those databases are, are those people exploring aspects of themselves. It's not their identity because we don't have a single “identity”. It's I might be saying, OK, well, here's me online exploring this part of my identity or myself, maybe here's me exploring another one. I own all of those. So those are all aspects of me. So I think that's very important. And like I said, Mikalai, I think we are very aligned in how we see this, like, you know, from hearing what you're saying. So I'm going to be looking further into what you're doing as well. And let's definitely keep in touch.

**Aral**: Laura, you wanted to say something?

**Laura**: I was just going to say, going back to what Mikalai was saying, I think we are always going to have people trying to co-opt sort of progressive and positive movements with their own buzzwords and things like that, trying to push their own agenda. Usually the thing that makes them the most money. I think the only way… we can’t fight people trying to co-opt things all the time, it's a losing battle. What we can do is try to explain as clearly as possible what we are trying to do, the intent behind it and what we are not. And it's frustrating to have to do it over and over again, but it's the only way we are going to be able to spread the message of what we're trying to talk about, is to just remain being clear and simple and accessible as possible. And I think that's the only way we'll be able to succeed, as annoying as it is.

**Aral**: Oh, and Sophie actually has a question, Sophie, may I add you to the stream with your video? Is that OK? Thumbs up? Yeah. OK, she says thumbs up. So, hi Sophie!

**Laura**: Hi Sophie!

**Sophie**: Hello. Good evening, everyone.

**Aral**: Sophie, good evening. Could you very quickly introduce yourself in like one sentence or something of what you do? And then you have a question that you wanted to ask, so please go ahead.

**Sophie**: My name is Sophie and I work in accessibility, and obviously I'm also interested in privacy and security. And my question basically was, if you do accessibility audits, do you try to get some wiggle room to give recommendations on privacy to your clients? And if so, how do you handle that that kind of questions and advice?

**Aral**: Laura, I think this is all for you…

**Laura**: I think that’s a good question. So, I mean, we don't really do work for clients, certainly not anymore. But I would say that if I was going to do an audit, one of the things I actually wanted to mention earlier, but completely forgot because I'm tired today… was that one of the other things that we block with Better Blocker that we started blocking a few weeks ago is an accessibility overlay that's called AccessiBe. Now AccessiBe is, has become notorious in the accessibility community for being basically an overlay that doesn't work. It doesn't fix anyone's accessibility needs. It's something that's, much like a Google Tag Manager product, people install on their website with a sort of one click script kind of situation. And what it does is it provides an overlay that has a little accessibility icon or a button or something that people can select and choose whatever features that is supposed to make their browsing better on that site. The problem is it doesn't. It usually makes people's experiences far worse.

**Laura**: And what we've discovered to be potentially dangerous about it is the fact that in knowing… AccessiBe has access to, much like Google Tag Manager has access to… Google has access to any site that is using it, AccessiBe has access to the information of anyone who is using the accessibility overlay on that website. So that means, from site to site, they could track people, they could track what their accessibility needs are. Therefore they could potentially try to deduce… it doesn't even matter if it's accurate or not, they could predict whether someone is disabled, what their potential disability might be. They can create lists of those people. They can sell that information, pass it on to other people. I'm not saying that they necessarily do that right now, but the potential to invade people's privacy is huge. It's a massive privacy issue. And this is why I think that privacy is incredibly important when it comes to accessibility. I think one of the reasons why we struggle talking about privacy on the web and in the web community, is that a lot of people in the web community are very privileged and don't necessarily think of their privacy as something that they need to guard.

**Laura**: And that is often because they are not the most marginalised people. It is usually people who are most marginalised in society, who are most vulnerable to their privacy being exploited. That could be by their government discriminating against them. It could be potential employers discriminating against them. I mean, there is no end to the ways that people could be discriminated against, particularly if you're talking about disability, you're talking about race, if you're talking about gender, if you're talking about people from any particular traditionally marginalised group.

**Laura**: And so I think that is, if you have the possibility in an audit to raise potential privacy issues, I think that's really important way of trying to support the accessibility community, because I think that I mean… we're all vulnerable. Our privacy is… everyone's privacy is vulnerable in various different ways. But I think that it's a really important issue to raise for people who are particularly vulnerable to being exploited, like being discriminated against.

**Aral**: And I think there's… this is definitely going to be a debate for a different time, but I think there's also a very valid question to be asked here as to what it means to have certain spaces that are abusive spaces. For example, Google is a surveillance capitalist. We know their business model. We know that what they do, how they make their money, is through abusive practices. What does it mean to make an abusive space more accessible as well? I think is a debate that I think the accessibility community definitely needs to be having, you know. And what does it mean for… what does that mean? I think that's a very valid question that we should be asking as well and whether maybe we should be talking about these other issues. We should be taking an intersectional, look at, you know, these topics when we think about the ethics of what we're doing in our work as well. And which projects, for example, also deserve our efforts and deserve our labour to make better and which ones we should be shunning, and which ones we should be basically saying, look, this is actually there is no reforming this because it's a factory farm for human beings. It's never going to become, you know, a sanctuary. And I think that's very important because sometimes otherwise the work of very well-meaning people, people who just want to do the right thing becomes public relations for these very abusive spaces.

**Aral**: And I think that's something that we would we would have to hopefully, you know, maybe look into it, maybe another a different live stream, because we are quickly running out of time on this. And I saw that Laura’s video is actually popped out so she might be having connection issues downstairs. That is a possibility. Hopefully she will make it back before we have our exit. But before then, I did also want to say thank you all for joining… oh Laura's back. Hi there, Laura. Yay, you're back. So I was just going to say we're running out of time. So I want to thank everyone who's joined us today. So because I have not got the best memory, we'll just put our display names back on. So that was Margo, Sophie, Billy and Mikalai. I thank you all so much. Really livened up the stream. So lovely to have you and to have…

**Laura**: Thank you!

**Aral**: you as well. And for next week's stream. So maybe I should have done this earlier because this is actually quite exciting. But I'm just going to go back to me here, for a second. And so for next week's stream. Oh, and how do I do this now? Because I think my… All right here we're going to… we're going to try something here, because I think that's all right. I'm going to go back to oh, is it just us now, Laura? Has everyone left?

**Laura**: I've just removed everyone from the stream to help you. Sorry, I was going to tell everybody the chat. I've just removed them from the stream just to make this a bit easier.

**Aral**: Oh OK, well, I am…

**Laura**: That’s why I’ve been having connection issues as well.

**Aral**: Oh, why were you having a connection issues?

**Laura**: I get connection issues when there's a lot of people on the stream.

**Aral**: Oh, OK. Well, I've had an issue where my stream deck has disconnected, so I'm just restarting that…

**Laura**: Are you going to be able to…? Oh OK, so…

**Aral**: Sorry, I guess I'm just going to need to restart that so that I can… let me just see. It's just like…

**Laura**: Just while you’re doing that… Mikalai, in response to what you've just said about sorry about not being on the accessibility points, this is not an exclusively about accessibility stream. We are always across all the different areas. We like talking about accessibility. We like talking about privacy, we like talking about inclusivity. And there is there is no set we-have-to-talk-about this particular thing because all of these issues really matter. And these are all issues that are massively underrepresented in tech. So it's important that we talk about them. And I think from what… I didn't entirely hear what Aral said earlier, I got the gist because we talk about this quite often. And I think that one of the things that's very important when we're talking about people's access to platforms, whether they're platforms that we approve of or not, is to not be… is to try to make sure that we're giving people choice. We're making people have actual choice. So we're not being paternalistic and choosing what they should and shouldn't be able to access. That is completely down to them. But the key thing is that we provide alternatives so that people can decide to go with something that is better and is less likely to exploit them.

**Aral**: Yeah, and it's very important that that's accessible. All right, I'm back, my stream back is back. So my chroma key is back. And so next week, not next week! Next month! [Laura laughs] Not next week. No, no. We do this once a month, not once a week! So next month, we are going to have the primary author of Owncast, Gabe, with us. And so I just want to talk a little bit about Owncast, because Owncast is very, very cool. So right now we're using StreamYard and StreamYard is streaming to Vimeo. Now, what you don't know, because I haven't told you… probably should have told you earlier, but anyway, is that we're also streaming to our Owncast server.

**Aral**: So what is Owncast? Well, Owncast is… think of it as your own personal Twitch. So you can stream, you can set up your own instance. It's an instance of one, so it's basically a Small Web app. So it fits the definition, because the definition of a small web app is that it's single tenant. It's just for one person. It's owned by one person. So and as of yesterday, what you can do is you can set up your own Owncast production server with one command using Site.js.

**Aral**: So Site.js is our personal web server. So if you have installed Site.js, all you have to do is to say “site enable --owncast” and that will give you basically your own production server ready. So running as a service, it will survive restarts. It's being served over HTTPS and TLS. So all of these things, with just one command… and to install Site.js is just one other command. And right now, actually, this has been streaming to our Owncast server. So I just want to show you that. And in the future, we're going to be doing more things with this. So I'm just going to open up owncast.small-tech.org and hopefully. Yep, there it is. I'm going to share my screen with you. There I am looking very… there we go. So now I'm playing it, and there's a little chat here.

**Aral**: [typing into chat] “Hello, it's me.” There we go. So there you go. And then we're going to get into some sort of an Inception-style thing here, so I'm going to stop that. There we go. But so that's Owncast and that is our own instance. And it was set up with that one command, basically. And it's just… now we can just leave it and it just works. So that's very cool. Check it out. Go to Site-js.org if you want to, to check that out and how to install it.

**Aral**: You can find out more about that… so you can have your own personal Twitch basically set up. And we're going to have the the primary author of Owncast, Gabe, with us on next month's stream. So that's… oops.

**Laura**: That's a funky layout.

**Aral**: Yeah, I know, right? The darn thing is, with StreamYard, you can't actually map stuff to your stream deck. But anyway, that's a first-world problem. So sometimes the transitions are not as smooth as I'd like them to be. But yes. So here we are at the end of our Small is Beautiful… Laura.

**Laura**: I have been Laura Kalbag with the dodgy connection…

**Aral**: And I am Aral Balkon and this has been Small is Beautiful. Take care. Have a lovely week. Have a lovely month. And we will see you again next month. Bye bye.

**Laura**: Bye bye!

{{< fund-us >}}