---
title: "Small is Beautiful livestream #4"
date: 2021-01-21T18:00:19Z
speaker: "Aral and Laura"
description: "Laura and Aral discuss Live Streaming on a Budget (including creating transcripts and captions!)"
---

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/500549798" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>

## Live streaming on a budget

This video was live-streamed on the 21st of January 2021 at 5pm Irish time.

We take you through our live streaming setup. Includes Aral struggling with an iPhone/AirPlay/Apple TV (while all the Linux stuff around them were busy, ahem, just working) and Laura covering creating transcripts and captions, a crucial accessibility requirement.

## Transcript

**Aral:** Right, and we should be live.

**Aral:** Hello, Laura. What are we doing here?

**Laura:** Well, and yet again for another week… I am Laura Kalbag and, that is Oskar the dog. And I am downstairs. Aral is upstairs and the two of us together, are Small Technology Foundation.

**Aral:** And this is a weekly live stream that we've been doing since the end of last year. As in, we did the first one at the very end of last year, and we call it Small Is Beautiful. And today on today's show, show, stream,

**Aral:** we've been getting questions about the live streaming setup itself. What equipment do we use? How do we do the streaming? Which services do we use? And we thought it would be a good idea to actually, you know, do a stream about that.

**Aral:** So it's a meta stream in a sense. And I'm actually. Laura, would you… how should we start this? Maybe I'll start by taking…

**Aral:** Yeah.

**Laura:** Yeah. You start. I'll carry on afterwards. My stuff will make more sense after you've said what you're going to say.

**Aral:** That is true.

**Laura:** After you’ve shown what you're going to show.

**Aral:** That is true. But first of all, what I want to do, because Reuben was one of our friends who asked about the about this or asked for it. And I think he might be able to join us and share. Sherri might be able to join us, his partner from the US. So I'm just going to give out the studio URL, first of all, so that anyone who wants to can actually join us in the studio.

**Aral:** If you have questions, if you want to join in, join us in the private chat and tell us what you want to talk about. And we can include you in the stream. Just have a webcam and headphones. I've got one here. You can see it take you to that as well. And you can join join the stream. So I'm just going to put that in the stream.

**Laura:** What you are doing that, Aral… I'm just going to say to people, when you come on and into the room, we only have room for 10 people. And last week we had a few extra people trying to get in. So the idea is, if you come in, be ready to ask your question and say in the chat what you want to ask. But when you're done, if you wouldn't mind leaving the room, you can go back to watching the live stream on our site and it will be back to normal there. It's just the in the room itself, we've only got so much space and it's difficult if we want to let other people in to ask questions.

**Aral:** Also. OK, so let me start, because if you can see, I've got this sort of blue and red and kind of cyber punkish look going on here and quite a setup up here from which is where I'm also controlling the stream from. So let me just take you full screen to me. And if you want to join us again, the URL is down there. So if you go there [points at banner on screen] , if you go there, Laura is doing it as well. But wait.

**Laura:** [Points at banner on screen.] I was doing it even though you couldn’t see me. [Laughs]

**Laura:** Just for my own fun and satisfaction.

**Aral:** [Laughs] So let me take you through the the setup that I have up here. Now, what you can see is, of course, my my monitor here and I've got my keyboard, et cetera. And I'm going to, in order to actually show you the setup itself, I'm going to use my phone. And the way I'm going to do that is I'm going to turn on screen mirroring and I'm going to mirror the screen of my phone, which now that I'm actually doing it, is not is not showing the… let's see if you can see this… [holds iPhone to camera] like that would go screen mirroring is actually not finding, of course, my Apple TV, which is because we reset a router…

**Laura:** It’s not on the same network. I think you need to…

**Aral:** It is…

**Laura:** You need to make sure it’s on the same network Aral.

**Aral:** Yeah, it is. We restart the router. So this was all working right before we did that.

**Aral:** This is the sort of thing that could happen

**Laura:** It’s the classic case of connection issues.

**Aral:** [Laughs] Right? Ten minutes before and then restarting the router. OK, let's just see. I wonder if the Apple TV actually went into. OK, all right. Let's say let's try that one more time.

**Aral:** All right, so what I'm doing right now is just trying to connect to the Apple TV, Apple being a trillion dollar company, of course, their products just work, which is one of the great things about it. This is probably not. [Laura laughs] This is the network. But, hey, you know, all right, now it's not finding the Apple TV. I am just going to try and see why that is, let's just see if the Apple TV is in… this must be riveting for you because you can't actually see what I'm doing. All right. Apple TV, stop asking me for my Apple account.

**Aral:** I don't care about that right now. What I do care about is whether or not you're on the right network. So let's just see… this is just entirely seamless.

**Laura:** Hey, Aral? While you do that… why don't I show people my set up down here?

**Aral:** All right, cool. So Laura’s got less complicated set up where less that fewer things can go wrong. So I'm going to put you in solo. There you go. Right.

**Laura:** So I'm going to, first of all, come forward and grab the webcam that is sitting on top of my computer. And it's one of the ones that detaches. So I can fortunately do this, [holds camera in hand] like that and I'm going to try my best to show you.

**Laura:** So this is our bedroom and you can see here, I’m looking to see what I can show you. Right. And so we have the set up here today. I brought my second monitor downstairs and just realised I'm walking further away from the microphone, so I'm getting closer to the microphone. So I've got a second monitor.

**Laura:** I got my laptop, I got my microphone, I've got my notebook over there. And I've got a camera that I'm just putting on top of the monitor, because if anyone else has got a laptop with a camera in it, you know, they don't tend to be particularly good.

**Laura:** So let's try and get you nice [puts camera back on monitor] right good, and so that is my very basic setup. I'm currently running StreamYard in Chrome because it doesn't like Safari and I have also Safari running for when I share my screen with you now, Aral. Have you had any luck?

**Aral:** Er no. It's on the right network and my phone is still not connecting to screen mirroring, so would you like to continue?

**Laura:** Yeah, if I put myself full screen again, I have one more thing to show you. And it's called Oskar.

**Aral:** When it doubt, bring out the puppy.

**Laura:** I’m moving away from my microphone so I’m going to shout… But when in doubt. Here we go. Can we… where’s he gone? Oh ok. He is right.

**Aral:** [Laughs] We're having technical difficulties with the dog as well.

**Laura:** Can you see him? There we go. I think I can't see the screen now. So that my attempt to show you the dog. I hope you got a little bit of him in there. Because his bed is right behind my chair right now, which is usually where he'd be sitting, trying to squeeze in behind me, but not today. Ooh now I’m a bit wonky [realigns camera]. There we go. Of course, the problem with putting a monitor on top of a bed is just liable to a bit of sloping.

**Aral:** OK, so I'm just trying to turn on and off WiFi on my "just works" Apple iPhone here. See if that does it. Let's see. Let us see.

**Laura:** Trying to recentrally position myself… [fiddles with camera]

**Aral:** Wow, this is like the first time screen mirroring has not worked for me on an iPhone, it is actually quite amazing.

**Laura:** Well, I think it's quite important to point out to people how fragile these setups are…

**Aral:** What can go wrong as well. Exactly. You know. You’re entirely right. Anything that's that's kind of wireless could go wrong, I guess, which is why wires are good. All right. I'm going to restart the phone now.

**Aral:** Siri, go away. Go fuck off, Siri.

**Laura:** I mean, Aral, could you pick up the camera and move it? I mean, I know that you do…

**Aral:** I could but that is that's actually wired down. So, Siri, fuck off.

**Aral:** All right, let's. My goodness. All right. Now, just slide the power off.

**Aral:** This is riveting. But this is the sort of thing that could happen. And it is happening literally while trying to airplay to an Apple TV. I mean, everything else is Linux and it's working, right? It is the "just works" trillion dollar corporation’s devices that are actually not working right now.

**Aral:** So good one, pay your taxes, dude.

**Laura:** OK, Aral. How about I’ll start talking about what I wanted to talk about? It doesn't really need your setup…

**Aral:** The order doesn’t matter, does it?

**Laura:** It really doesn't matter, no.

**Laura:** We can be free and easy with it. So what I want to talk about today was I don't know if any of you follow me on social media. You might have seen that I’ve been agonising about transcripts and captions recently. So one of the things that's really important to us. And it's one of the principles that we have for Small Technology is what we create is inclusive. And a huge part of inclusivity is accessibility. And accessibility is what makes the technology that we use accessible to everyone, but in particular to disabled people.

**Laura:** This is something I know a little bit about, is a little bit of a specialism. Wrote a book about it, but I think one of the things that's really important to us is that if we're going to be producing videos and sharing videos, we want them to be accessible to people. And one of the ways of doing that is to do transcripts and captions. So let me show you what those are. I'm going to share my screen with you.

**Laura:** "Share screen." "Pick the screen" Now, you should be able to see my screen, and so here you can see the video on, the last video that we did on our website and here if you scroll down.

**Laura:** You can see there's a heading that says transcript and underneath is a transcript of the video. So this is a text form of the speech that we said, and each of the people as they speak, are labelled as well. And so what we've tried to do is make it as readable as possible so that if you just want to understand what was in the video without having to watch it, you can read it. And that also works if you're not able to watch the video. So that might be that you're blind or your deaf.

**Laura:** Or you have visual impairment or hearing impairment. And it might just be that you don't have the network connection. It might be that you don't have a device that likes video. You might just not enjoy videos. I'm not a massive fan of watching videos myself. I quite like reading. So transcripts work really well for me. And so the transcript is a really great way of having all of that information in there and you see there, I’ve tried to sort of punctuate it well, and things like that.

**Laura:** Captions, however, I'm scrolling back up to the video. So here you can see I’ve just paused a point in last week's video, and this is the little Vimeo player that we embed on our site. We embed it and we don't have any tracking in it. That's a very important point. We don't track the people who are watching your videos. And if you go to the little CC, button that they've got, you can choose captions. And we have English closed captions right now because that's the only language I speak, so I can't do them in any other language. So there, you can see when I put them on.

**Laura:** We have some captions. So it has two lines. It says that Nicolas is speaking and then it has what Nicolas was saying at that particular frame. Now, how I produce captions and transcripts. Now, back in the day before we had so much great technology. What, I was liable to do was actually produce them manually. So I would use a piece of software I would go through I would write out all of the text with corresponding timecodes. Fortunately, nowadays, technology is a little bit more available to us where we have better options and that is automated speech to text.

**Laura:** Now, very rarely can you find this kind of automation for free. I mean, arguably, you could go to YouTube, you could upload your video, you could grab their automated captions and then delete your video. That is something I have done before. But actually, the YouTube auto-captioning isn't as good as it could be. The interface isn't great. And why get Google involved if you don't have to get Google involved? So, although I suspect that a lot of automated speech-to-text libraries are actually using Google underneath it all, so  I'm going to show you now, going back to sharing my screen.

**Laura:** Thank you, Aral. I've got the magic wizard pressing buttons behind the scene. And I'm going to show you, if I've just uploaded, so I didn't want to have all of this waiting and have you have to watch me uploading a video or audio. So I've done it already, as they would say in Blue Peter, "Here's one I made earlier." And so I've got a little video that I made about accessibility last year. And I've decided, even though I've already made transcript and captions for it, I just wanted to show you, for an example, how I would do it again.

**Laura:** So this is one of the services I've been using recently. It's called Thisten and it's really good. It's particularly good for transcripts. And they actually have an offering to do live transcripts. So you could be doing a live event, a live video. We could be doing it here where Thisten will listen in on the audio. So you have to run it through a speaker and a microphone and it will live transcribe what you're saying into a little screen for people to be able to access. I think you can access it via the app or via a website. So that's really useful. So, Aral, if I could just go back to… thank you, and say if I go to view my latest transcript. So here now this is I haven't touched it at all, I've done is I've uploaded a recent video. It's just grabbed the audio out of that video and it has automatically processed the transcript for me. And so you can see it says "assign speaker", so what I could do is I can say, well, actually the speaker for all of this, it's just me speaking in the video.

**Laura:** So I’ll just say, "Laura", I will apply that Laura, to all matching segments, "add Laura". "Save". And now Laura is set for the transcript. The interface looks a little bit funky here. It's just because I've zoomed in quite far so that if you're on a tiny screen, you can read it quite easily. I care about accessibility, right? And so if I scroll down, you can see just the classic thing, that automated transcripts, they can do a huge amount of very clever things, but they're not particularly good at proper nouns.

**Laura:** So names, things like that, especially with technology, especially with non English names like Kalbag. I get all kinds of interesting variations. I mean, you should see what it comes up with for Aral! So it says here "I'm Laura Cal bag" and it is so it's put my name "Cal bag" into two words so I could just take the space out between those words. And it's spelt Calbag with a C, whereas Kalbag is spelt with a K so I can go through and edit it. So this is a thing with automated transcripts is they're really good and they can produce really quite accurate information that will save you a huge amount of typing.

**Laura:** But that is not enough. It is not accessible if you don’t bother to edit it. So it's really difficult for people to actually understand what you're saying. Now, as another example, I found Thisten really great. And what you can do is when you're done with that, you can export to different formats. So you can export it to either, here if I go to "export transcripts". Oop, you don't need to see my notifications, that's for sure. You can export it to a text file, a word document, a PDF or in .SRT, subtitle format. And so that's what you'll want for any video players use. And that's really great. So what I would usually do is export as a text file for the transcript itself, which I can then put into a markdown file or something for a website, but for captions I would export as a .srt file . The problem is, if I exported this particular transcripts as a .srt file, it would put the entirety of these long paragraphs into the SRT file so that when you were looking at the captions, you would see them as big chunks of paragraphs of text over the frames of the video.

**Laura:** And you don't really want when you're trying to watch… the example I would give is if you're not used to using captions, you might use them if you're trying to watch a film in a foreign language, you do subtitles and you see there that in subtitles they split the text up into a small amount of time. So it's easy for you to read whilst trying to keep an eye on the screen. And so here in Thisten and what I could do as a hack for making it work for captioning is I could go through and I could press enter every few, every sentence, every few words, something like that, to break it up into a reasonable chunks.

**Laura:** What I would try to do when I was using Thisten is look at the timestamp at the side of the paragraph and check that the time that the text is going to appear on the screen, so the distance between the timestamps is a distance can probably read people to be able to read a speed is comfortable for them without necessarily having to pause the video. Now, you can see going through and editing this video like this, going through and hitting enter on all of these little sentences takes a really long time. And so that last week I was starting to like spend my life editing transcripts. So I thought I got to try to find something that's a bit better for producing captions.

**Laura:** So this brought me to Simon Says. Oh, gosh, lots of Internet connection errors there .

**Laura:** Now, Simon Says is fantastic. What I should say is about both Thisten and Simon Says do cost money. But as you know, we're a tiny little non-profit, so we don't have loads of money. Thisten is a tiny amount. And they charge you, I think, per minute. And it's the same with Simon Says, you can do it per minute or you can pay a monthly fee. I think the pro package is about thirty five dollars a month for Simon Says. And so you can see here on Simon Says, you have the little video in the corner, which is quite handy when you're going through, I'm not going to play it now because it will make our Internet connection play up.

**Laura:** But you can see I've got the transcript itself, particularly for this first part is really, really accurate. It did yet again, call me Laura Callback. It doesn't like Kalbag, so I can just change that very easily in here. One of the great things about the Simon Says editor, is you can still edit while you're playing the video. So you can pretty much, if you’re quick, edit almost in real time. And nothing's the video doesn't stop when you interact with the text.

**Laura:** That's really handy. And then when I'm done, here is the feature that really was brilliant for me is that I can export it. And I can export either as a plain old text file, if I just want a transcript or I can export to subtitles now if I export to subtitles. I say "Let’s do this." Takes a second to get going. Oh, I can either export it like that. Or I can use the fancy visual subtitle editor. Now this is really useful because what it allows you to do is apparently show me a help article.

**Laura:** I'll tell you about it instead, what it allows me to do is to… go back to this. Is it allows you to export and choose a number of characters that you see on the screen at a time and export the captions automatically, and that way you can do all of the breaking up of the text into readable chunks without having to do it all manually. So you think about the amount of time it took me hitting enter after each paragraph to go through it? Or, after each sentence.

**Laura:** I cut all of that out, it cut my time enormously as well as the really good editor, so I would really recommend, Simon Says, if you want to edit both transcripts and captions and if you want to do a live transcript, I would recommend Thisten. In fact, I can throw up a banner to give you the… So Simon Says is simonsays.ai. And Thisten is Thisten like, listen, .co,

**Laura:** And I'll just put the stream banner back up there so you can see it, and, sorry Aral, I just have one more quick thing to show.

**Aral:** Go for it.

**Laura:** If you don't mind. And that is that, go back to sharing my screen. And when you're done, when you've got the captions, if you're using something like Vimeo. So Vimeo.com is particularly good because in their pro version, you can host a bunch of videos, which is quite expensive. You have a good video player that works for a lot of people and allows people to stream at a quality that works for them and no tracking in the player.

**Laura:** And then if you go to your video. You can go, they keep it in the distribution section. Under "subtitles" and that you can add in, replace whatever, choose the language and choose the type of different…

**Aral:** Laura, Could to make that a bit bigger? I'm having trouble seeing it from here.

**Laura:** Yes. That's a very good point. We can see how well their interface zooms in. [Laughs] So I’m in the subtitles section of Vimeo.

**Laura:** And you can add a new file and you choose the language and you choose whether it's captions or subtitles. Now this will be included in the interface in Vimeo, but what it will also allow you to do… So if it's a subtitle, that usually means that it is translating it into a different language. And if it's a caption, it means that it is usually the same language as the language that’s being spoken. There are some formatting differences, but a lot of people use the word subtitles and captions fairly interchangeably.

**Laura:** So if you're not sure which it is, I wouldn't worry too much about it unless you are doing something seriously professional.

**Laura:** And there we go, Aral, over to you.

**Aral:** Right, well, thank you, Laura. Restarting the phone did it, by the way, so I'm going to show you the set up.

**Laura:** Sorry… [after hiding Aral’s view by accident]

**Aral:** That's OK.

**Aral:** I'm going to show you I'm going to show you the set up and also we'll see why confusion like that happens as well. It's due to one part of the interface that we're using for the streaming. But I'm going to show you the setup that I'm using. And to do that, I'm using my phone and it's connected via airplay to an Apple TV that's connected to the mixer. So let me take you to that view. So now you are seeing what's coming out of my iPhone's camera. And let me just break it down to you w hat I have here now, first of all, this is my Linux laptop over here. It's a StarLabs laptop that I got quite recently. It's about a thousand euros or so, and that's my daily driver and I'm running Elementary OS on it. Now, what that has over here is StreamYard running in un-Googled Chromium right now. And that's why you can see this lovely effect that we have. [Camera views tiling .]

**Aral:** And that's where I'm monitoring the live stream from. And part of the issue is, even though I have a stream deck, which I'll come to in a second and I have an ATEM mini pro, but definitely the stream deck. I can't map these buttons that you see over here for changing the view.

**Aral:** Laura's waving. I can I can click on Laura to, I can click on Laura to give her a solo layout in StreamYard. There we go. I can click on myself to bring myself back, but I need to do the clicking so I can't actually map those easily to stream deck to my stream deck, which would be really nice. But I'm going to look into that. You can there are ways of doing it. It just takes a bit of work anyway.

**Aral:** So that's, that's, that's the, the main laptop that's doing the stream. Now what's coming into that with this USB connection is the ATEM mini pro.

**Aral:** Now, this is a life saver. It is one of the most amazing pieces of equipment that I've ever had. And in terms of background, in terms of a little bit of background. I actually did four years of media production, which was mostly video in my undergraduate, and actually it ended up being mostly critical media theory. And then I did a masters degree in digital media production. So back then, at the very beginning, we were using VHS cameras and, you know, analog editing suites and this and that. So and things cost tens of thousands of dollars. And right now, it's just amazing that this [ATEM mini] costs a few hundred.

**Aral:** And this is a production studio basically all in one. And now the way I have it set up, as you can see, that number four set up here, if I actually press on this, it's got an X for me not to press it by mistake, but that will show *me*, hi! The reason I have an X on it is actually because I use this [another] button instead normally to to switch switch to me and I'll talk about that in a second because that means that I can switch to it with a key.

**Aral:** And that's how you get the effect that you had at the beginning with the titles coming in. But the ATEM is amazing. You can have four sources over here. You can switch between them, you can cut or you can fade between them. So if I want to fade between… there we go. I can just fade between me and the camera input here. And it's got know basic audio controls. I could do picture in picture if I wanted to.

**Aral:** And the cool thing about the ATEM mini pro is if I wanted to, I could stream directly what you're seeing right now directly to Vimeo. So I don't need anything else. I don't need a computer, really. And you can see that if I go to three, you'll see my screen. And if I launch something here, like the Elementary store, I'm just sharing my screen and there we go. The App Center is launched and oops, it's launching the wrong. There we go. So there is the App Center, Elementary, so I can share my screen, I can go back to my video, I can go back to this [iPhone camera] . And this is all I need, really. And that is so awesome.

**Aral:** If you've messed with OBS studio, which is a way that you can do this, the same thing that the ATEM does and more in a way, on your computer, which I did, I started out doing that initially. And that's a lot of work, you know, and it also means you need to have a really beefy computer, especially to be to streaming and the encoding at the same time, which means you're going to need two computers really to do it well. And that's that's a lot of cost, that's thousands of, you know, to get a beefy enough computer or in this case, a few hundred. I mean, I don't actually remember how much this was, but I think it was about four hundred, five hundred euros.

**Aral:** I don't know how much we paid for this, Laura?

**Laura:** I think it was about 400 euro, yeah.

**Aral:** Yeah. Four hundred euros or so. And that's just amazing, you know. And I've also got the Stream Deck, like I said, and the stream deck is going into my old MacBook and you can see here with the old MacBook. This is kind of interesting. I'm running Keynote on it and that's how I did the original the titles. So remember the titles that came on? Let me actually exit out of that and show it to you.

**Aral:** In fact, I can just. Let's see if I can do this, let me show you its screen over here. This is just Keynote, and here I've just got a green that I've set as the green from my background in on the ATEM mini pro.

**Aral:** And if I just take you through it, I have this here, let me maximize this… nope it won’t let me maximize because it's a mac, it'll do whatever it wants to. It'll be like that's the right size. You should use that size.

**Aral:** You can see here that all it does is a magic move. So here, are all those little strips, I'll move it around so you can see it. Those color strips… and command-Z, put it back in place. And here all those strips are out here on the end and just does a magic move in a magic move just basically animates everything. So if I were to run this without the video, this is what it looks like. [Animation on green background.] So if I were to, however, superimpose that now to do that, I'm actually using something called Companion. And that is BitFocus Companion, I can't actually show that to you very easily. Oh, here we go. Move it over here. This is BitFocus Companion. It's it's basically a a third party app for the Stream Deck so that you can program things with it that you normally can't with the stream deck. And what I'm using that for is let me show you on the screen over here.

**Aral:** All right. Let me just launch the ATEM software. So ATEM mini, the ATEM Mini Pro comes with an ATEM controller, ATEM controller software. And that's just launching right now over here. And it's telling me… what I can't read that now that it's on screen. On the other screen, ATEM mini pro. I guess I'm just going to say "connect". Nope. Let me just move it to the other screen so I can see what I'm doing.

**Aral:** No ATEM connected. That's odd. Let's see why not. I think this is this is all down to the network issues we were having. Yeah, exactly, because that's now connected to a different Wi-Fi network. All right. I'm just connecting my Mac to the… so what happened basically was ten minutes before we got on, we thought we were having a network issue. And so we restart the router. And so all the computers have connected to different… we have a backup network and it connected to the backup network. OK, so now this is up and I can show it to you. I'm going to switch back to that screen and you can see that there's this ATEM software control.

**Aral:** So that's what and this is over here the BitFocus Companion. That's what I've authored an action for here that does a key. And so I actually set that to be my source right now. And if I play that Keynote. Let me actually just bring up the screen and take Keynote back as well, because I'm extending my screen so normally, of course, I don't touch this while there is a while, there is a program on. So if I just play that now and if I go back to my view and if I press this, that's only in preview right now. I do that again. Take it live. So normally what I would do is I'm in this view and you can see over here that Keynote is running over here, you can see my Slides View. Oh yeah.

**Aral:** And I have a monitor over here at the back and that's showing the multi view from the ATEM Mini Pro. So that's in my eyeline. So I can always see what's happening. I can see my various sources. That's my camera that's up there. We'll get to that in a second. So that's the Sony A6400, which is a beautiful camera with a Sigma 16 millimeter F1.4 lens.

**Aral:** So that's that's how I'm shooting myself. Those are my slides that are coming out of the MacBook. Over here. Over here, I've got my the screen of my main everyday driver laptop. And over there I have the airplay, which is an Apple TV, which is somewhere. Is it back there. There it is, back there, that I'm streaming to… [Laura laughs]

**Laura:** Where are all my devices?!

**Aral:** Well and part of it is that so if I if I actually just zoom out here, this is the setup. And part of what I wanted with the setup was, this is actually, the part of the reason why we have this set up is not just because I want to look cyberpunk or whatever, it's because we've got this huge bed right behind me. So I'm very limited in what I can do. Let me let me just let me just show you. So this is the room, right? That looks nothing like what you're seeing, right? That's right behind me.

**Aral:** That's the huge bed. So I only have about this much space to work with. So I can't even put a backdrop or anything because I'm too close to the backdrops. So that's why I've got this camera angle. If I go back here, that's why I've got this camera angle that kind of you see a little bit of the bed, but on the ground, you've got, let me just show you that as well.

**Aral:** These LifX LEDs on the ground, I don't know if it's going to pick it up, it's going to pick it up. There we go. These little LEDs, it's a led strip that you can buy and you can program it with different things like guitars and my little amplifier there. And that's what gives that nice little glow on the back. But mostly the main light, the key light is this FalconEyes flexible LED.

**Aral:** So I don't know if you can see it, but this is all flexible and actually the LEDs are flexible. Now, this has got a diffuser on it. We've got this grid. The grid is really important because it helps to focus the light and it reduces spillage, light spillage, from the sides. So it's a very focused light on me, which is also why you can still see the background.

**Aral:** So if I switch back here, this is why you can still see that nice background over here. So coming back here, that's the that's the FalconEyes. It's all LEDs and it's just gorgeous. It is. It's it's one of the my favorite pieces of equipment that I have.

**Aral:** And otherwise, in terms of the the recording equipment, there's a Blue microphone here. This was actually a kit with the arm, which I would recommend getting so this can be moved around. And the interesting thing about this is over here, you can see that this is actually a USB microphone, but the ATEM mini pro does not have a USB in for microphone. But it does have is an audio in. Right. So what you can do. Is you can connect your lineout here, you can connect it into your mic-in over here, but you do have to and this is important, you do have to configure your ATEM mini pro so that it knows that that input is a line and not a mic, because there's an impedance difference there. If you don't do that, you're going to get a lot of noise on that line.

**Aral:** The other thing you need to do is you need to connect it to a USB power source. Now I'm saying USB power source, but that's not actually quite accurate. I've tried connecting it to just USB power sources. It doesn't seem to work. It needs a computer. So I think it is looking for something that only a computer can provide.

**Aral:** So this is actually connected to to one of the computers over here to power it. But the audio is coming out of the analog, the lineout over here. But then you can just use it as a regular microphone and you can go into the ATEM mini pro, which is kind of cool.

**Aral:** So all that and I haven't forgotten that. I was actually in the middle of showing you how I did the green screen title effect. So basically on my Stream Deck, which I'm not using to its full potential, as you can see, all these buttons are blank. I can go. I've mapped my previous slide and my next slide to it using another external application that I believe is called Listener or something. I can look that up later. And over here is the button that, if it focuses, let's see. Can it focus? Can it? Nope, doesn't really let me tell it where to focus.

**Aral:** But there's a button here and it says Upstream Chroma key slides cam 2 over camera 1. So if I do that, then I have the green screen overlay because it's all green right now,  you're only seeing me if I say "next slide", then we get "Small is Beautiful". And then if I say and then it's actually oh sorry. That was the ending. So let me go back. It would go to the beginning. "Small is Beautiful" [animation transition] and then a couple of seconds later it just goes back out.

**Aral:** This is also how I did the, if you saw the first stream and I was doing my slides, this is also how I did the slides as well. So this is kind of pretty much, I think, the setup that we have here. I'm not sure if I don't think we've left anything out. Oh, the actual streaming.

**Aral:** So the actual streaming happens with, let me just share my screen over here.

**Aral:** With StreamYard again. I'm in un-Googled Chromium actually I believe.

**Aral:** Am I or is this actual Chromium? I don't know. Anyway, un-Googled Chrome is pretty cool. So you can see here that this is StreamYard and the reason we're using StreamYard… is it's odd actually, we don't have anyone in the studio. I wonder if they're if people are having issues connecting because we've never had that.

**Laura:** I've checked. I've checked, not heard from anyone I think they’re…

**Aral:** Oh interesting…

**Laura:** Just all baffled by the amount of information [laughs]

**Aral:** Haha, or maybe no one's watching. We might just be doing this for each other.

**Laura:** No, we do have at least a few people watching. I have checked. I was worried! [Laughs]

**Aral:** OK, OK, cool.

**Aral:** Well, anyway, so if we go back to my screen over here, you can see that this is where we can have other people, bring other people in, etc.. Again, I would love to be able to map this into the Stream Deck at some point, but we're also from here, we're broadcasting to Vimeo. So where do I have Vimeo up? I don't let me show you. Well, actually, let me not in case something goes wrong.

**Aral:** Actually, I must…

**Laura:** The network the network might not like it, Aral.

**Aral:** No, no, it's fine. I already have it up here. So this is Vimeo and it's streaming. It's actually streaming to to Vimeo. And we can see that you can see on Vimeo, if you go to stream health, you can see that we're fine. We've we've got a stable stream going and it's also thirty frames per second. So this this especially if you're in Europe. So this is important, especially if you're in Europe. So let me talk a little bit about the camera. Where's my phone? There we go.

**Aral:** All right. So especially if you are in Europe. And you have a camera like this, you can set the frame rates, you should you should set the frame rate to the frame rate of your streaming service. So Vimeo can't do 25, etc. It can do 30, so I'm actually putting out 30 frames per second from the camera itself. You can see that at the top over here. It says 30 frames per second and that means that it doesn't have to do any sort of work and you'll get the best possible quality.

**Aral:** And again, of course it's that F1.4 And that is the beauty of this lens. So if I just switch back to that, I mean, the let's let's see. Where am I am I here? Yeah, this lens is just this F1.4 Lens, which means that if I come closer to this, let me see if it'll it'll actually. Yeah. Do you see that shallow depth of field? That's what the F1.4 Gives you. And that's just gorgeous. Is a gorgeous gorgeous lens, gorgeous camera and. Yeah. So that, that’s just that's wonderful. Also if you're setting it, you know, if you're setting it at F1.4, one of the things that, you know, if you want to take this a little further, do it a little better, you can get one of these things.

**Aral:** It's a Luxi. So let me hide my face so it doesn't auto focus on my face. Is it going to sit too close? Is it too small? Let's see. There we go. This is a Luxi.

**Aral:** And you can also do things like this [autofocus] with F1.4. It's amazing.

**Aral:** And this little thing just clips on top of your phone and it makes your phone into a light meter. And a light meter is really cool because let me show you. You can get an app like, well, actually, let me share my screen this time so you can see it, you can get an app like Cinemeter 2 , which is the one I use. And Cinemeter 2 , I can take a reading so you can set your aperture to sorry, you can get your reading and you can see that when I'm taking a reading here and I'm going to just switch back here so you can see what I'm doing.

**Aral:** OK, so I'm just taking a reading. And is this on correctly? Yeah. There we go, I'll switch back to what it's seeing and you can see it's got this diffuse reading, right? If I put my hand in front of it, you can see that it's telling me, oh gosh, OK. Yeah, this is this is not the right light level right now.

**Aral:** But if I take my hand away, I can see that, OK, I'm perfectly exposed for an F1.4 aperture. And that's how you can set your lighting. So you don't have to just I mean, you can of course, you know, if you have a really good monitor, etc., you can eyeball it. But if you really want to also have the confidence that you've set your lighting right and it doesn't just look good on your monitor and not on anyone else's monitor, then what you can do is you can use a light meter and you don't have to buy a light meter because light meters cost so much.

**Aral:** I mean, I'd love to have a real light leader light meter, but I can't actually afford one right now or justify the purchase, really. But something like this, luckily it's just tens of euros and it just clips onto this [iPhone] . Now, you can buy this app as well.

**Aral:** The what is the app called again Cinemeter 2. And that's that's that's pretty cool.

**Aral:** So I'm also just going to, since I was showing you StreamYard and I'm going to just move StreamYard out into my other window so that I can bring Laura back into it as well. Hello, Laura. How was that?

**Aral:** Oh, I can't hear you…

**Laura:** Sorry, I say hello. It’s because I’d muted myself, because I was talking to the dog. [Laughs]

**Aral:** Fair enough.

**Laura:** It got to the time where he starts wanting something so you can see him on there.

**Aral:** Aw, cute thing. So, yeah. For the first time, we just we actually don't have anyone in the studio, so I guess we're not going to have questions for this stream, which is cool, although odd. Sorry, go .

**Laura:** I have a couple more things that I just forgot to mention earlier. So I'm just going to go, before I forget, go on to them and… one and talking about inclusivity and accessibility, I do apologise for our shoddy hand camera work because I'm pretty sure that if people are motion-sensitive, that probably wasn't wonderful for you. So if you watch that and got giddy, I'm really, really sorry.

**Aral:** And also, if anybody wants to send us a steady cam for free, I'm all for that as well. Please feel free to.

**Laura:** Alright. Alright. And and the other thing that I wanted to say, and I think that this stream has actually been quite a good example of it, is one of the things that we are trying to do in terms of accessibility is when we are showing you things on screens, when we are talking about something, waving a device around or something like that, what we try to do, and sometimes fail, quite often fail. But I promise you, we're trying… is to explain what we're showing so that we're not just waving a device around without saying what it is, so that if you aren't able to see it, if you are using a phone with the tiny screen so you can't really see the detail in the video, or if you are blind and you can't see the screen, then you still understand what we're talking about.

**Laura:** And that's going to be something that we're always driving towards, is trying to make everything we do as accessible as possible. We're going to slip up and as Aral said also that we don't have a huge amount of money. So we're trying to do as much as we can on a limited budget. Like things like transcripts and captions, it's a lot of people hours to make it, but it's one of those things that I've decided I wanted to take on because I thought it was important. But a lot of it is you have to decide to put the time and effort into these things.

**Laura:** That's the thing with accessibility and inclusivity is if you care, you put the time in even if you don't have loads of money.

**Aral:** And that's one of the things like when I first started trying to get a streaming setup together, especially I mean, it was the early days of the covid pandemic and it was clear that we weren't going to be doing conference talks.

**Aral:** And also, you know, why limit yourself to conferences when you could actually do a talk every day if you wanted to? So one of the things that I started out initially just trying to see what we could do with what we have. So there are a lot of apps that will let you use your phone, for example. And these [smart]phones have amazing cameras in them. Will let you use a phone as a camera, as a webcam. You can get webcams. Laura's using a webcam right now. A Logitech, what is it the C920 or something? I think… You can probably…

**Laura:** Something like that. It’s one of their basic ones.

**Aral:** Yeah. So right. And you can see that Laura’s image is actually really great as well. And Laura, are you right now using your Blue microphone as well or?

**Laura:** Yeah, I'm using a Blue Yeti microphone that's probably about 10 years old. I've had it since I started out doing, I think since I first ever did a podcast. And so it's an old thing, but it still works really well. You don't need to have the newest gear to be able to do this kind of thing.

**Aral:** Right. And I mean, if you and if you think it looks like there is a lot here, like when I was showing it to you as well. But if you actually add up the cost of the various pieces of equipment, even including a camera, the camera with the lens was under a thousand euros. If I'm not mistaken. Laura does a lot of the the accounting that we have as well…

**Laura:** Which means Laura does a lot of the saying "we can't afford this".

**Aral:** [Laughs] Indeed. So but that was under, I think, a thousand euros. And I think the whole set up, with some trial and error as well, was definitely under five thousand euros, at the end of the day. And that is that's amazing. Or it was around five thousand at most. Would that be correct? Laura, am I remembering that?

**Laura:** I actually have no idea, because while I do the accountsing, my off-the-top-of-my-head  maths isn't particularly good.

**Aral:** I think it was around that. I mean, I actually had the numbers when I when this was months and months and months ago, last year. But it was at most five thousand euros. And I know that sounds like a lot, but back in the day, that wouldn't get you a camera, you know, that would get you maybe a good camera. So it is amazing how accessible these things are. You don't need this, right? You could get a webcam instead of that camera. You can you can use your phone if that's what you have. I mean, I have a lot of tests, if you go to our video. You'll see our old tests that I did there.

**Laura:** And if you compare your upstairs sets up to my downstairs setup, you can see I'm pretty much using,  I'm entirely using things we already owned before the pandemic, before any of that.

**Aral:** Right.

**Laura:** Because we didn't need to buy the same gear for two lots of people. That would be ridiculous. We’re more often doing just one video at a time.

**Aral:** Exactly. And I mean, for something like this, it's great that we can actually improvise. And your setup is great. You know, your setup downstairs is awesome. And if we need anything more where we need to like green screen in slides etcetera, or even just have slides, have a much easier way of going between them so that it's much more seamless. You're giving a talk, let's say. This is meant to be quite casual. You know, one of our aims for this was that we don't have a lot of time, we're actually developing things. But we want to be able to do this sort of weekly updates and we don't want to have a huge amount of preparation for it.

**Aral:** So it's very casual. We kind of go into it. And this is what this set up lets you do, something like an ATEM mini pro. I don't have to mess with, like the computer and like, is there a problem with the computer? Is this is one app hogging the CPU, etc. So we don’t have to worry about that. So that kind of so I mean, if you were going to get anything for live streaming, I would say right now that, you know, I would say get the get the ATEM mini pro. The ATEM mini pro is just an amazing, amazing product.

**Aral:** You know, as someone who started again, like I said, you know, decades ago, the stuff you can do with almost next-to-nothing in terms of an investment today is amazing. Hardware, software, etc, that's not going to be the thing that stops you from from doing this if you want to. And I think that's that's probably the main takeaway from this, that there are many layered levels that you can you can have for a streaming setup. It doesn't have to be this this convoluted as what I have here, and, yeah, I hope this has been helpful.

**Aral:** It's just it's so odd not to have anyone in the studio. It's the first time, I mean, apart from us. But, you know, what of what I wanted to show off is that…

**Laura:** We also need to get better at promoting these streams…

**Aral:** Well that’s the thing…

**Laura:** And sharing it with people. And we are considering potentially having an email mailing list to let people know about upcoming streams. If that is something you would be interested in, would you let me know on social media @laurakalbag? Because then…

Are we?

**Laura:** I’ll actually know it's worth setting up, otherwise I might not…

**Aral:** Although I'm kind of not I'm not a huge fan of email lists, to be honest Laura.

**Laura:** But that's because you never check your email. But I think…

**Aral:** Exactly. I don't want email, I don't want to send people email…

**Laura:** In these days of algorithmic timelines and people trying to not be on social media as much because it's a cesspit. I think its email is actually a good way of… people don't really use RSS. RSS, I think is a good way of doing it. And I use RSS, but I still I check my email every day, I don't read my RSS feed reader every day. And with email if…

**Aral:** I don't check my email every day. But I hear you. [Aral laughs, Laura shakes her head.]

**Aral:** But OK, sure, I mean…

**Laura:** This is why we have a shared email address because one of us at least checks the email every day.

**Aral:** [Laughs] Oh gosh, it's the only way I can get work done. But anyway, I hope this has been useful, Laura. I mean, the stuff that you were showing, I think, is… the stuff I'm showing you can kind of put together stuff you're showing a lot of people don't necessarily think about. So I think that was definitely important to have. And we should probably wrap it up now because we're coming up to the one hour mark. And for the first time, we actually haven't had anyone in the studio and no questions. It's eerily quiet, but we'll do a better job of promoting it.

**Aral:** So would you like to start the outro?

**Laura:** Yes, and now you all know how it works, so I have being Laura Kalbag .

**Aral:** And I'm Aral Balkan and this has been Small is Beautiful. Join us again next week.

**Laura:** Bye!

**Aral:** Bye bye.