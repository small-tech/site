---
title: "Small Is Beautiful livestream #5"
date: 2021-01-28T18:10:34Z
speaker: "Aral and Laura"
description: "Join Sander van der Waal, research director at Waag, alongside Martine Delannoy and Karl-Filip Coenegrachts from Cities of People as we discuss what the public stack is, why it’s necessary, and how we can fund projects for the public good with public money."
---

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/503144121" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>

## The public stack and how to fund it

We need “open, democractic, and sustainable digital public spaces.” In other words, we need what [Waag](https://waag.org/en) calls “the public stack.” The [Small Web](/research-and-development) that we’re building at Small Technology Foundation aims to be one such space. So why is it privately funded, almost entirely with our own personal funds?

Join [Sander van der Waal](https://twitter.com/sandervdwaal), [research director at Waag](https://waag.org/en/sander-van-der-waal), alongside [Martine Delannoy](https://citiesofpeople.com/en/bio-martine-delannoy/), and [Karl-Filip Coenegrachts](https://citiesofpeople.com/en/bio-karl-filip-coenegrachts/) from [Cities of People](http://citiesofpeople.com/) as we discuss what the public stack is, why it’s necessary, and how we can fund it with public money.

__Hosts:__ [Laura](https://laurakalbag.com) and [Aral](https://ar.al).

## Transcript

**Aral:** All right, I am putting us live and we are live. Awesome, so hello and and welcome to another stream. Laura, do you want to take it away?

**Laura:** Yeah. Hello, I am Laura Kalbag and together, Aral and I are Small Technology Foundation, a teeny tiny little not-for-profit based here in Ireland. As with the previous weeks. I am downstairs, the dog is there and Aral is upstairs.

**Aral:** And this is a tiny weekly stream that we call Small is Beautiful.

**Aral:** And today we want to start off. In fact, you know what? We started off by having everyone in the shot. So let's maybe Laura, change up what we were going to do, which is just a very brief introduction to to have to who everyone is. And then I'll take it away with a very brief look at what I've been working on today with the Small Web. And then we'll get back to our guests.

**Aral:** What do you think, Laura?

**Laura:** Great.

**Aral:** Go for it. If you'd like to introduce our guests… Laura, can you hear me?

**Laura:** Sorry, Aral, I can't hear you right now. Would you go ahead? Sorry.

**Aral:** OK. All right. So let me just do a quick introduction. Today on our on our stream. We will have Sander van der Waal  from Waag.

**Aral:** He is the… he heads up research there. And the topic of today's stream, the public stack, is what he's working on. So we're going to hear about that. Hi, Sander. Welcome to the stream.

**Sander:** Oh, you should be able to hear me now.

**Aral:** I can hear you now, I can hear you now.

**Aral:** Great. And we also have Karl-Filip and Martine… Karl-Filip, your last name. Say it for me because I can't…

**Karl-Filip:** Coenegrachts.

**Aral:** Coenegrachts.

**Karl-Filip:** Coenegrachts.

**Aral:** And Martine Delannoy from Ghent, joining us from Ghent.

**Aral:** Martine is the Chief Foresight Officer for the City of Ghent and Karl-Filip is with the Cities of People Initiative, Organization. So and we worked together, with both of them, with the city of Ghent a few years ago on our Indienet project. So it's really lovely to have to see you both again and to have you on the stream.

**Karl-Filip:** Thanks for having us.

**Laura:** Just to correct you, Aral, Martine is the Futures Strategist at the Cities of People.

**Aral:** Oh, right. OK, on the Internet, it's still says Chief Foresight Officer. So I was going I was going by the fake news on the City of Ghent’s website. [laughter]

**Aral:** All right.

**Aral:** So I'm just going to take it away with a very brief look at what I've been working on at the Small Technology Foundation this week. Because part of why we do the stream is to give you an update. As you know, we're doing research and development into what the Small Web is and what it can be. And this week, I basically spent some time working on the authentication for the Small Web.

**Aral:** So just the little spike that I did. So let me just show that to you now. So in my terminal, I'm just starting up Place.

**Aral:** So for those of you that don't know, Place is the it's a fork of Site.Js. It's not ready for use or anything. You can see that even like the output isn't looking that great right now, it will not even run for you. But I'm just going to show you the little test that I did this week. This is a potential future Small Website. We've got different sections. These are just little tests that I was doing with Svelte components. I'll talk about that in a little bit. But the actual thing that I was working on was how we do sign in.

**Aral:** And so on the Small Web, when you go to /keys, you can actually see your encryption and signing keys, your public keys. These get created for you when you first create your site. So I basically came up with this simple, very simple authentication mechanism for authenticating you with the server. Because this isn't the main security the main security is everything you do is signed with your keys. Everything you do is encrypted. That's supposed to be private end-to-end encrypted. That's the actual security. This is how the server actually authenticates you. And I call it a Bernstein token after Daniel J. Bernstein because his algorithms are what make it possible. So what happens is basically the server sends you a random string, a 32 byte random string that's encrypted with your public key. And then when you enter your password, as I'll show you, your passphrase. So I'll just enter my passphrase here, and sign in.

**Aral:** Once I have the right passphrase, I go to my private section. So what's actually happening here is I'm making a websocket connection at that point that gets authenticated. So you can find this on source.small-tech.org. If you, let me actually.

**Aral:** Well, you don't need to sign in to go to small source, sorry to to explore the projects that are there. So if you actually go there, you'll be able to find it from our source control repository. But again, this is all very new. If you want to see what, let me just show you here at GitHub.com/small-tech/place, that's where the source code for Place is, which I'm working on right now. And again, there's this huge warning. This is not ready. Unlike Site.js, Site.js is our tool for building regular websites, the websites we have today. But for individuals. That's already for use. That's very mature, but Place isn't so… but if you want to keep track of what we're doing, take a look. So that's where I am this week. It's actually quite exciting because now that authentication is working, the next step is making all of the WebSocket connections to the various different people that you're following and who are following you. And that's that's going to be the next step in what we're working on. And again, this is what we do. We do research and development and we're kind of working on how do we create a Small Web where every individual owns their own place on the Internet and they can each connect to one another?

**Aral:** It's very similar to the project that we were working on… it's actually the project we were working on with Karl-Filip and Martine in Ghent, was the precursor to this.

**Aral:** So I am going to get back to all of us now because I've spoken enough. So onto the topic of today's discussion and we introduced our guests earlier. So, Sander, I'm going to ask you, so what is the public stack?

**Aral:** And also why do we need a public stack? Because there are some, I don't know if you've heard of them, there's some lovely companies like Google and Facebook [sarcasm], for example, and they make all the products that we could ever need for free and they give it to us for free. So I guess they're not even I don't even know if we should call them corporations? They're probably more philanthropists… because they create all these things for free and they give it to us. So why do we even need a public stack? Yeah, take it away.

**Sander:** It's a good question. Of course, we don't need we don't need new products. We… let's just go with Google and Facebook. No, exactly. That is not what we what we think we need. I'm going to share my screen, which is the easiest way for you to explain the work that we do with the public stack, let we make sure that this works.

**Sander:** And here you can see can you see my my screen now? Can I just check, Aral?

**Aral:** I can't yet. We're still looking at you as far as I can see. However, here, you know what? I'm going to add your screen there to the stream and now we can see it.

**Sander:** Ah great. OK, so I'm going to sort of explain what the public stack is by taking you through the website that we launched recently on publicstack.net. And what you can see here is an example of this piece of technology, like a smart toaster, which is kind of, the way in which things are sometimes evolving, we feels like, oh, somehow people feel like everything needs to have technology inside them. A toaster. Why do you just need a toaster? You can make it work from your app, etc..

**Sander:** So this is the kind of way in which technology seeps into everything that we do. And what we are starting from is, sort of, this this understanding that a phone is not no longer at least just a phone. There's a lot sort of behind the technology that we see when we use our apps on our phone. And the point is, this is not just this technology is not just based on trying to make it work best for us. It's practical and it does what we want it to do. But ultimately, the companies that built these these these phones, these apps, the operating system on our phones, have other interests in mind rather than just sort of serving us. Especially when you are not paying. It's an old saying nowadays, but you're not paying for the app, if you're not paying for the service, you're probably simply the product of the technology company.

**Sander:** So what we wanted to make clear is that we wanted to show what are the things that are behind the technology? And we use this sort of metaphor of a stack to explain what is underneath. And when we look at the phones, what happens is that you have yourself as a person. You can see this here on the on the right hand side, which is you mostly just pictured as a consumer. You're using or buying a product which is closed technology, which has been designed to develop through closed processes. And it's been built on market values. It's based on a commercial model in our society where the main motivation for the company is to generate maximum profit for their shareholders, which is often how companies, especially big tech companies, operate.

**Sander:** And there are alternative models available. If you look at what happens in China, for example, the people that use the technology are more like subjects. They are being monitored, observed, surveyed through the technology that they use, which is still closed. And it's been designed and developed through closed processes. And it's the states and the interests of the state which has sort of informed those processes and ultimately the technology that we use.

**Sander:** So we're trying to to come up with this model as a way of saying there is actually a third way possible. And this is what we call the public stack. There's a new way in which we can design and develop technology, which is based on common values, which is based on the on the values that we think is important for us. We're based in Amsterdam, in the Netherlands, and it's true for us. But I think on the European level, we have common values that we feel are important and we want to make sure that those values are being respected in the technology and how we develop and use them throughout our daily lives. And then ourselves, we are citizens. We are people able to exercise our democratic rights, just as we do in our normal life, with the use of technology as well.

**Sander:** So let me take you through these layers of the stack to explain a bit more about what we envision. And we have these three components, the foundation at the bottom, the design process, and then the technology stack, on top of which the citizen perspective is central. But let me start at the bottom and explain a bit about the foundation of our public stack that we have in mind.

**Sander:** What we when you design and develop technology, we want to recognize four pillars in our foundation that we think is important when considering developing technology based on public values.

**Sander:** So the first is all stakeholders are involved in the process, and it's clear why we optimize and what we optimize for. So often, the starting point when developing technologies are not clear, they're just being held secret, or the people are not involved who are ultimately, for example, using the technology and we want to change that.

**Sander:** We think that a different approach is needed when we build a product, which is, as the second pillar says, based on human rights, which are guaranteed and public values that are being respected, which is often not the case. We know from the numerous news articles that there is a lot of things going on right now with privacy not being respected, but also ways in which our democratic rights are being trampled upon through the way in which technology is deployed in society.

**Sander:** And that needs to change. So that's the second pillar of our foundation.

**Sander:** The third one is about governance…

**Aral:** And Sander just before you go, just before you get to the third one, could I possibly ask you to make your browser… just scale it up a little bit? I'm having trouble seeing the… there we go. I think that's about better so that everyone can. Yeah. So that everyone can actually see it. All right.

**Sander:** Great.

**Aral:** There we go. Thank you.

**Sander:** OK, good. So the third point is about the governance and supervision on it. What is crucial, especially when we think about the digitalisation of our societies. That is, it is not embedded within our democratic systems. If Facebook or Twitter decides to throw you off their platforms, you have no way to appeal to that.

**Sander:** And these companies are trying to come up with their own alternative. So now there's an oversight board that Facebook and Twitter installed themselves to try and make a decision whether or not Trump can get back on Twitter or not. This is not the way in which we decide a governance and supervision within a democratic society. So we, as our own societies across Europe and indeed beyond, need to be able to get a grip on these processes, on the way in which it gets developed. Which doesn't mean that we need to refer back to the state stack like we have in China. It means that the democratic norms, and indeed sort of laws and regulations that are relevant to our offline world, will be applied to the way in which we develop online technologies as well.

**Sander:** The final pillar in our foundation of the public sector is the financial economic model and the way in which we're we're striving to take human and the human boundaries and the planetary boundaries into account. So we want to make sure that those boundaries are respected.

**Sander:** For example, looking at the model that Kate Raworth is developing with the doughnut economies, we're talking with her, and see how these models might might intersect and relate to each other, because we need to move away from this understanding that the profit maximization is the way to go in order to maximize the value of technology. We need different models and we need different sustainability models. And it’d be great to talk a bit more about this in this conversation. But just to wrap this up, maybe… we have these sort of four pillars. And with these four pillars, these these are reflected in the design process, and the way in which the technology is being developed. So that we have co-created models which involve the different stakeholders, which ensures that the technology is open, is distributed, is within the human boundaries. And I think this is also a nice sort of connection with what you're doing with the Small Technology Foundation, is that we need to bring back this human perspective. We need to move away from this aspect of trying to make everything as big as possible and thinking that that is better. As Kate Raworth famously says, is this this concept of a never-ending growth that we are seeking for our GDP… If we put that perspective on our bodies, that something growth indefinitely, we call it a cancer.

**Sander:** So we need to move to a different kind of model and bring back that human perspective in the way. In which we design, and indeed develop, technology. And then ultimately, and then I'll stop there, and start what we're you can apply this model to all kinds of technologies. So for reference, we have the technology stack here, which are the different sort of layers of the stack. So this needs to apply all the way from the infrastructure level to the application layers that we're we're familiar with when we use our apps and our websites. But also what we define as context layers are relevant for this. So think about the services we use, the security of those services, the protocols and standards that need to be open, and in the way in which we deal with technology through our data and algorithms that run through all of these levels of the stack. So let me stop here and I hope this gives a sort of brief introduction and in what we mean by the public stack.

**Aral:** It it does. Sander, thank you and we're going to go into different aspects of this as we go on, but I have straight off the bat again, you know, like I asked you at the very beginning, so how is this different from what Google and Facebook do? And I mean, do we really need this when we have Google and Facebook? And here's the reason I'm asking. OK, so let me show you. So right now, I don't know if you guys are aware of it, but, I'm just going to I'm just going to share my screen here. So I'll just put myself fullscreen here and share my screen. I don't know if you're aware of it, but there is a conference called CPDP that's going on. Yeah. Have you heard about this CPDP conference?

**Sander:** Yes, with Peter Booth speaking?

**Aral:** CPDPconferences.org. Yeah. So it's this CDPR conferences. It's comes out with… it's about *privacy*. So this is very interesting. It's about… it's a privacy conference. And let me just let me just get rid of their cookie notice there. And what's very interesting about this privacy conference is if you go to their sponsors over here, like if you go to their platinum sponsors, for example, let's see who’s sponsoring them… it’s* Microsoft, Intel, *Google* and *Facebook*.

**Aral:** So, you know, I'm very confused by this because I'm thinking now, Sander, is Sander full of shit because… is Sander full of shit or is CPDP conference full of shit? Because one of those two must be the case… because if Facebook and Google are actually forces for good in the world, and they're allowed to sponsor conferences like CPDP, then you know what's so bad? Why should anyone fund *us* right? Why should anyone fund the public stack? Why should anyone fund people like us, Laura and me doing Small Technology in Small Technology Foundation? What would you say to that?

**Sander:** I think the main sort of argument that is relevant here is to look at the kind of organizations that Google and Facebook are, right? They’re very sort of traditional corporations that operate in a model where their reason for existing is to to maximize the value for their stakeholders. So ultimately, what they're trying to do is to generate as much profit as they can. And they have been very successful at that, the Big Tech companies are the biggest corporations around the world, and have not been bothered by the level of regulation that is there for other sectors in the world because regulation is behind on that.

**Sander:** And that means that us, the people using that technology, and not paying for the technology, is that we do bring revenue to these companies so they don't work in our interests because we don't get their money. They don't maximize their profits by through us paying them because we are not paying, and we use this for free. So they have a different revenue model, which is often through advertising for Facebook and Google, which means the advertisers are the real customers and they are the ones that maximize the revenue for these organizations.

**Sander:** And that is why we in the public stack up this concept of socio economic model, because that is not the model that works in the interests of the citizens, because these corporations will want to maximize their shareholder value. And that is not in our interest. So we need to work on alternatives that work in our interest. And that's the core of public stack.

**Laura:** You put it in a very *kind* way. [laughs]

**Aral:** Yeah, because, of course, Google and Facebook are surveillance capitalists. Right? We know what their business model is. It's not just advertising. It's not like an ad that's in a newspaper. It's an ad in the newspaper if the ad in a newspaper could actually see you and hear you while you're looking at it, it's based on tracking. So it's a very different model, of course. But I guess my point is, it's very difficult for us to make the to make the case that these projects in the public interest, for the public good, should be funded when these corporations, these surveillance capitalists, like Google and Facebook are being whitewashed, are being privacy-washed by the very people who are telling you, "we care about your privacy, we care about these things". Right? Because, I mean, I also like Sander. I mean, I'm a big fan of Waag and what you guys are doing.

**Aral:** But have you heard of Mozilla, for example? Mozilla is like, they "work to protect your privacy. They're really working for your human rights and everything". And they're funded almost entirely by Google. They get, they get half a billion dollars every year from Google. And so they're telling… so what that says to me is that Google's OK, Google's fine. So why should anyone fund an alternative to it? And I'm not just being facetious here. I'm not just trying to make this point out of context.

**Aral:** So today. So, I mean, we've applied for public funding several times, and I'm at the point where I just actually don't even try anymore, right. I'm just we tried one last time with a NLnet, because somebody from a NLnet actually said, "oh, there's a Next Generation Internet thing that we should apply for. It'll be great. Try it." We did it. Rejection again. We tried for things like, you know, the Shuttleworth this-or-that, whatever… reject, reject, reject over the last seven years. So we're funded by basically us selling three family homes. You know, the only homes that we had, and we don't have any homes anymore, but we're not getting any public funding.

**Aral:** So I've kind of kind of given up. But these things are related. Today somebody actually sent us a link and said, "hey, there's this new Next Generation Internet initiative called Pointer. You should try for that." I just want to show you something. OK, so and then and then we'll move off of this topic. But I think it's very, very important that we do cover it. So I'm just going to put myself full screen again and share my screen. So somebody mentioned, "hey, go to this thing called pointer.ngi.eu. It's this Next Generation Internet initiative and it's called Pointer.

**Aral:** And they're funding the "next generation ecosystem of Internet architects". Wow, that sounds amazing. So they were like, you should get some funding from this. And if you read it, it says, "we want to find ambitious architects to change the underlying fabric of the Internet and the Web by supporting promising bottom-up projects, et cetera, et cetera, et cetera." It really sounds like. Yeah, that's what we're working on until, again, you go to their partners. So who are their partners? Who are the people who are working on this? Well, we have Aarhus University.

**Aral:** That's fine. I'm totally OK with that university. That's great. Funding Box. What is Funding Box? Oh Funding Box is an accelerator. OK, well, accelerators work with startups, it’s the Silicon Valley model. Right? So let's go to Funding Box and see what they do. Oh, look, they have one hundred million euros under management. They have accelerated one hundred and ninety five startups and they have seven million euros of venture capital funding. Right? Venture capital is what built surveillance capitalism.

**Aral:** Venture capital is what every Silicon Valley startup takes. And they have to either succeed or they need to fail very quickly. Right? This is not our world, right? This is not the world of the public stack either. I don't think… unless I'm very mistaken, it's definitely not our world with Small Technology Foundation. So these people are going to fund startups. They're going to fund Silicon Valley-style startups. They're not going to fund us. So I guess the question is, who funds us? Because right now, you know, it's personally funded by by me and our own cash.

**Aral:** So it's it is related, these initiatives that tell you that they're trying to that they're building the alternatives are actually funded by the very people that created the problem. And this is an issue. So I'm just going to unmute everyone. And we can kind of anyone who wants to take this can take it. And then I'll I'll maybe we'll go to Karl-Filip and Martine as well, because we did work on a project that was the closest we ever came to getting public funding. It wasn't public funding, we were contracting for them. But maybe we can have a talk about that. But, Sander, is there anything you want to?

**Sander:** Yeah, no, I think this is exactly the point that we need to work on alternative models. And I think there is, it should involve public funding. I think that is that's the way in which we fund a lot of our society right? Through taxes. So I think… but there's definitely an educational issue here for our governments and for the way… So this public stack is also directed to the governments themselves. And we're trying to influence them and explain to them that we need a different model.

**Sander:** And that in our stack, this economic model needs to change in order to make sure that our human rights and public values are being respected. And unfortunately, in a lot of these examples, as the one you point to, that is not being picked up in a great sense, because this is why this the doughnut economy is a good example. And Melanie Rieback is doing with post-growth entrepreneurships, she’s looking at alternative models in which we can sustainably fund technology that go beyond this crazy model. We need a different model that’s for sure.

**Sander:** And philanthropy… sorry, just a quick one to add, I think philanthropy is a challenging one because, as you know, there's a lot of sort of rich Americans who are investing in technology and trying to do that, but they're influencing the agenda. They're influencing and deciding on what to gets funded or not. And that's an issue because it's also this is also about power. So it's about the fact that these people that have the power to invest the millions, like Bill Gates, investing the millions and doing great work. But that's not based on our priorities.

**Sander:** It's based on their priorities. So I also think while we do take them, I do want to be fully clear, we do take sometimes philanthropic funding. But it is an issue for us because because we don't think that that is the right model longer term. So we need different sustainability models. So it's great to be able to talk about it today.

**Aral:** Cool. Karl-Filip, Martine, what do you have to add to this? Because you have, I mean, we know you because you were working in the city of Ghent and you were the people who brought us in.

**Aral:** And when we said, you know, can we actually start this Small Web, maybe in a city, maybe give every person in the city their own website. That was our Indienet project that we were working on. And don't go to the website right now because once the funding was cut, I actually let it go. And now it's, I think, a very successful porn site. So don't go there. [laughter]

**Aral:** Somebody picked it up, but… when we were working on it, it was a project to get people, to get everyone in Ghent their own website. So we actually built a prototype of it. We presented it, it was very successful. And then and this is another issue.

**Aral:** And then the government changed. A conservative government came in and the funding was boom, gone, poof. So Karl-Filip, Martine, what are your thoughts on this subject?

**Karl-Filip:** Yeah, well, yeah, it was it was a shame that that we couldn't actually launch the pilot itself, and because to me that would have been a way to convince politicians, but also to convince the larger population of the worth of what we were trying to do, namely to create the virtual city of people.

**Karl-Filip:** I think we took a risk there. You said that you were contracted. That's true. But actually, this was public funding and it was public funding because of the fact that Ghent at the time was putting the emphasis on innovating or trying to find innovative solutions to a problem that was emerging that not too many people were aware of, namely the fact that they didn't own the Internet anymore.

**Karl-Filip:** So there were a couple of visionary politicians and civil servants in Ghent and that became aware of that fact and wanted to do something to to develop the alternative. And this was before the Decode project that had some more or less the same objective, but was funded by Horizon 2020 of the European Union. This was before all the efforts that Nesta has done in the field in the U.K. and so on and so on. So the only thing that we could do was because of the fact that this is a priority, we allocate funds to it from the perspective of local governments. And I think this is something that might offer a solution to finding alternative models for funding digital technology and technological evolutions on the local level, because that's where it's happening. And once you managed to convince people on the local level in one city or in one municipality, the others become jealous. So they also want to get on board. So this is to me still today a way to provide funding for things that are out of the ordinary in the city context. But the hardest part is, Amsterdam as a frontrunner, although I'm going to be nasty, although Amsterdam is very good in city marketing, but, Amsterdam is a frontrunner. And so is Barcelona, a couple of other cities, you should not find one of those cities to pilot something new and you should find a second tier city that really puts in the effort to get to the results and with city, I do not only mean to the local governments, there is a wealth also present in every city of stakeholders, of societal stakeholders, especially with the emergence of new commons initiatives.

**Karl-Filip:** There is in every city almost a strong basis of the commons initiatives that can put put on the pressure to local decision makers to start something to fund it, not only to fund it with, how should I say that? Grants, but also fund because they believe in it. When the stakeholders believe in it, generally there will be a politician that believes in it.

**Aral:** Well, that's interesting because also, I guess the question is, how do we make these not just projects? Because so many projects are started, right? How do we actually fund the organizations that do research and development, that work on this long term that are in it to work on this for years and years and years like we are, for example? We're not there, just do a project and to have a little checkmark on a politician's list so that they get a favorable polling in the next election cycle, because that's I mean… and don't get me wrong, Karl-Filip and Martine, both of you, you I have so much respect for what you achieved in Ghent, for what you enabled to do. It wouldn't have been… I mean, I know you said there are politicians who were involved, who supported, etc. It wouldn't have happened without you two. And I'm very clear on that, whether you admit it or not. I saw what you guys did.

**Aral:** So, you know, how do we make it so that we can break ourselves out of this short term thinking? You know, this kind of either for a politician to win votes or for this or that, how do we actually invest long term in organizations that are going to create infrastructure?

**Aral:** Because it's not necessarily going to you know… we've learned about the problem in the last seven years that we've been working on it and our solutions are evolving based on that. And now what we have today couldn't have happened without all of that work. But how do we get this sort of longer term thinking and longer term funding?

**Aral:** Nobody wants to take that one?

**Laura:** Is it because none of us really have got the solution yet? We haven't found these means to being… to long term sustainability because it is so far outside of so many people's imaginations

**Martine:** It’s a difficulty and it's…

**Aral:** Maybe so people should join us and if they have any questions?

**Aral:** Sorry, Martine, go.

**Martine:** It's a difficulty, actually. And most of our projects, also, because we also try for European funding, especially when it comes for riskful projects with a certain level of risk. And what's always hoped is that we'll, through the experiment phase, come up with a sustainable model to make sure that's able to continue after the funds stop. But that's, even in European contexts, an extremely difficult thing to do.

**Karl-Filip:** I think that there should be some kind of a revenue model and I'm using the word revenue very explicitly because you cannot rely on public funding to sustain a solution. That's impossible, even if when it is a priority in a city or a regional government or whatever, it will remain a priority for a certain period of time and then it will disappear. And now I'm not only talking about a priority for the politician, but for the the government itself.

**Karl-Filip:** It will disappear, funding will dry up. It won't be a priority anymore. So you have to find another revenue model. And it could be it could be based on, again, on the on the commons, on the strength of the commons. To give an example, in different fields, there are a lot of energy cooperatives right now actually running, and doing the exploitation of windmills, and and even the solar panel farms that were initially funded by governments to a large extent, or a lesser extent.

**Karl-Filip:** But now they are running on the community itself and on the revenue model by the community itself. So the peer-to-peer thinking can offer a solution here, but it has to start somewhere. And I think what you are trying to do, what you are doing, with Small Tech is something that should be piloted first with public funding and then moving to another revenue model.

**Aral:** Well, I guess we're doing the exact opposite, right? So it's it's personally funded right now. And I think maybe at this point we're kind of invested in that. But my hope is that we can build this pilot finally this year, before our funds run out and we're homeless, so that we can actually build this and launch it and have developers initially, because it's going to be aimed at developers, start to get their own little servers, paying maybe a couple of euros a month for it. And so hopefully start to bring in some revenue that way. And also that way, prove to people, look, it can work. Look, we have a network. It's starting. Oh, look, there's even some revenue coming in. But, you know, and we've done this all ourselves, you know. No thanks to government funding, apart from the lovely city of Ghent, maybe a little bit, which kept us going. I mean, it's amazing, really working with that was just great. Gave me more hope that it's possible.

**Aral:** But if we can actually show that it works, then maybe it will have the the effect of getting more government funding. I don't know! Or public funding, you know, but we have trusts, right? We have trust for things like environmental projects, et cetera, that  protect certain things. So maybe that's also a thing that we can think about longer term? Maybe we need to invest in a certain type of trust. I know Waag is… how is Waag itself funded, Sander?

**Sander:** We actually a lot of our funding is public funding. So it's paid through through the government on the national level, on the local level in Amsterdam. And we also participate in Horizon 2020 projects a lot. In practice, about eighty five percent of our turnover is projects. So, so individual project grants. So those being, as I said, European funding. And I think a lot of the a lot of the initiatives that we fund are already sort of early stage prototypes. Right? It's new ideas and new concepts such as the public stack, which is, you know, we worked about two years on that. And only now we sort of starting to get a better sense of what it is, and putting this into practice. I think longer term, there is always going to be a mixed revenue model. I think. In a sense, a lot of the work that we're trying to do is convince organizations, public organizations, including governments, that this is worth them investing in.

**Sander:** And with investing. I don't mean the VC-type investing, but it's investing the same way in which you invest in your other infrastructure. So I actually think part of the sustainability model for these new initiatives should be funding that comes from public purses, but also from individuals. As Karl-Filip said, in the commons type approach, different other organizations, other citizens wanting seeing the value of this and therefore being, you know, paying for it themselves, is part of what we need to achieve. But it's a very difficult uphill battle because we're still trying to convince people that that Google… that free is not free and and it's definitely a longer term trajectory that will take years and years before it gets into fruition. Yeah, right.

**Aral:** Right. Well. I've got I've got the studio URL on  screen, Laura just had to pop out because she's having audio issues. I'm sure she'll be back to join us in a second. But we have Leonardo, who's joining us in the studio, who has a question about funding, so. Hi, Leonardo.

**Leonardo:** Hi. Good afternoon.

**Aral:** Hello. You're joining us from Brazil, right?

**Leonardo:** Yes.

**Aral:** How is it like in Brazil right now?

**Leonardo:** Politically speaking, it's a mess.

**Aral:** Oh, yeah. Yeah. Tell me about it. But you're all right? How like how's everything on the ground?

**Leonardo:** It's crazy because there is covid everywhere. We don't have a plan for fighting the pandemic, so it's it's crazy. I got covid myself and it was…

**Aral:** Oh my gosh.

**Leonardo:** Yeah.

**Aral:** Wow.

**Leonardo:** It's unbelievable. It's everywhere. But I don't want to change the directions. I would like to to ask, what about the model of membership that Free Software Foundation has for funding their projects? It seems to me, I was a Free Software Foundation member last year. Uh, it it seems to me that it works. I don't know. What do you think?

**Aral:** About donations?

**Leonardo:** Yeah, because donations seems a one time thing and membership or…

**Aral:** Ah, membership patronage, OK.

**Leonardo:** Yes, it looks like a long term commitment. So I don't know if it's…

**Aral:** I mean, Laura, do you what do you want to take that? Because we actually do have patronage for Small Technology Foundation. Might be a good opportunity to talk about that.

**Laura:** Yeah, we we do have patronage. We have the option for people to give us a one-off donation or to support us on a monthly basis. And we're really, really grateful to the people that do. But there's not loads of them. And I think the amount we get in every month probably covers infrastructure costs. So it covers things like web hosting, the web hosting that we don't get for free, because we get some of it for free. And otherwise it doesn't cover a huge amount. And so I don't know whether that's a failing on our part, that we need to do more publicity around it, that we need to encourage people in a better way, that we need to talk about what we actually do with that funding. Or whether actually it's much easier for big organizations to get that level of attention and that level of support than it is for us.

**Aral:** Yeah, and of course, it's not long term sustainable, you know, this is kind of what I think Karl-Filip was also touching upon, that it does need to be sustainable. I mean, one of the hardest things, really, is for any organization or project that's trying to change the mainstream, change the character of the mainstream, and change the infrastructure of, say, the Internet. They need to, currently, be… they need to be successful both in the current system and in the system that they're trying to build. I mean, we know this. We need to be successful in terms of making revenue. And we're a not-for-profit, but we still need to bring revenue in, etc. in this current system, the capitalist system. And then we need to be successful in the system that we're trying to build, with the infrastructure that we're building. And that's that's kind of difficult as well, you know, in and of itself, because you have these two very conflicting places and you're trying to build a bridge between them, but you're also trying to survive that trip, you know, across that bridge. That's what it feels like, at least to me. Does that make sense?

**Leonardo:** Yes, but it seems that when we talk about governments. Every time you have a new politician in place, this kind of funding goes away or you need to fight again. So I don't think it's sustainable in the long term, either.

**Laura:** Maybe in some way. It's actually that we need to have diverse revenue streams. We need to have multiple sources of revenue so that we're not solely reliant on a particular stream. I think that to some degree, the nature of doing public work is that you tend to go from one source of funding to another. And perhaps we need to kind of take the "Internet generation" way of doing that and try to bridge, we'll have to carry on being across the bridge between multiple sources of income, but maybe that is the most realistic way to approach it in the current system.

**Aral:** Well, I guess there's there's one way of looking at it, which is when you say, well, can't we in essence crowdfund this stuff? It's also important, you know, crowdfunding on a regular basis, let's say, with a patronage, et cetera. Well, we already have a system where we all get together and we put a certain amount of our money into a pot for it to be used for the common good, right? We already have a crowdfunding system for the common good. It's called taxes, right? It's called public money. So maybe one of the challenges is on a conceptual level, how do we separate funding from control? Because a lot of the times, when a government funds something, for example, they also want to control it.

**Aral:** So how do we say, well, no, actually, let's separate governance, control, from the funding. Let's fund something for the public good, but the government doesn't govern. It doesn't control it. It's independent. And again, this is where maybe the concept of a trust can come in, etc., so that, you know, the mission of that organization can continue, so they can continue to make things for the common good without being influenced by the short term demands, the political demands of whatever government seems to be, you know, is in power at that time.

**Aral:** Does that make sense?

**Sander:** Yeah, it makes a lot of sense.

**Laura:** I think it makes me sort of think of the the way in which we fund culture. That often is sort of the way it happens in the Netherlands, is that there is this sort of independent board that is making recommendations of what cultural institutions get to fund. The government is not decided what is good culture, what is bad culture. Right? What cultural institutions can or cannot fund cannot be funded. So we have a different way for the society to make those calls on the priority and then ultimately use public funding in order to pay for it.

**Sander:** And that sounds a bit similar to the kind of model that you're describing here, right?

**Aral:** Well, I mean, one thing I'd love to know is Martine and Karl-Fillip, and this is for both of you individually as well. What are your thoughts, having worked in this area, having had some of the drawbacks, seen the drawbacks, seen, you know, you were in the thick of it. What are your thoughts for the future? What do you think can work? What are you personally working on in this area to to encourage these sort of projects, to encourage projects and what Sander would call the public stack? And I think that's a very good term, actually.

**Karl-Filip:** I go first again. Maybe to to talk a little bit about the projects that we are working on and the way that we are funding what we are doing, actually. We want to change governance in general.

**Karl-Filip:** That may sound very broad, but that's that's actually the mission that we have set ourselves, to change the way our society is governed. And it starts on the local level and it starts from the citizen. So that's not an easy task. It's as difficult as the work that you are doing, I think, especially to to get it funded. So I don’t want to be the typical consultants, and neither does Martine, I hope. The typical consultant that sells all kinds of facilitation workshops to cities and so on and so on, or to sell strategies or trends or whatever to cities.

**Karl-Filip:** We stick to our principles and we stick to what we believe in. And we only want to do what we believe in. So that's one. It's not easy to get assignments. It's very easy to write down our vision, but it's not easy to sell it because there's always public tendering procedures and so on. So what are we doing?

**Karl-Filip:** European projects actually at this moment, but European projects with objectives and a consortium that we truly believe in. We're never leading a consortium, but we're attaching ourselves to consortia that, with partners that we really believe in, that have the same vision. And this makes that we can pilot certain ideas that we have with our vision and in an environment with strong institutional partners that support us, but also know how to work with government levels.

**Karl-Filip:** So by doing that, and by giving examples, we think that we can convince other levels of government, other governments to follow us. And we're doing that on the European scale, not under the Belgian or Flemish scale where we live, because it has a lot more impacts. And at European level, you get a lot more space to to work with your ideas. And then it should trickle down and it should change the mindset of others at the regional level, at the local level. So that's what we are trying to do now. What was your initial question? I've lost it, sorry.

**Aral:** You muted youself.

**Laura:** Aral, you’re muted.

**Aral:** Yes. [laughs] So basically, that is what I was saying. So what are you doing? What what is your approach going forward from what you've seen, from what you've learned from your experience? So you've answered it perfectly. So, yeah, thank you.

**Martine:** But one of the things we're also doing using the future strategists idea is actually working out scenarios, working on the ideas of how the world might change and what we see as something desirable, and what we see as things that are less desirable. And those are situations in which, when we're talking about data, that we might have groups come up with the solution. While we don't want this model anywhere, we want a different model. But that's a first step. And after getting the necessary change with some governments to actually change the model, and know how to change it, and then comes solutions like you have in the picture.

**Aral:** Well, cool.

**Martine:** I think Waag society is also involved in the digital rights movement, that might be interesting to look into. Because all of all the cities that have signed up agree on these principles. And in fact, following, I imagine that there are certain types of developments that they would rather not have and others types that they would rather have, as you said.

**Sander:** No, no. Exactly. So there's this this network for cities for digital rights which Amsterdam signed up to and lots of other cities signed up to. And it is interesting because cities make a commitment to respecting human rights in the digital age.

**Sander:** So you need to hold them to that commitment and then say, OK, that means that you need to deploy technology in a much different way, that you don't want to rely on Google data to to govern your city. You need to to invest in different solutions and in alternatives. And I think you're also there's this concept of spreading instead of scaling makes a lot of sense. To work on the local level, as Karl-Filip said earlier, not the second tier, first tier cities doesn't matter. Work on the local level, in your city level.

**Sander:** Curious what happens in Brazil, for example… We can learn from each other what happens, and what we're doing, and build on what others have been working on as well. Because the way, of course, technology being sort of not live for us, we can use the open source governance model to share what we're doing with each other, learning from each other and building on what others have done already.

**Sander:** So I think that's also part of the whole equation, that you don't need to build everything from scratch. But we can build on each other's work and collaborate in ways that are much more different or…

**Aral:** Which is actually, Sander, I don't know if you if you're seeing the private chat, but Jan just joined the chat and he was actually asking exactly that, Jan from RedHat. And he was asking exactly that same question. So you just answered that as well, without having to not have it, without us having to ask it.

**Aral:** And also one thing I want to point out, because I think it's very important what you said, Sander, "human rights in the digital age" is very different to "digital rights". Right. We really need to start framing this correctly, because every time I've heard and not, Martine and Karl-Filip… not you and not people who don't speak English as a as a first language necessarily, because they can seem very similar. But for people who speak English as a first language, who have certain vested interests, "digital rights" are a very different thing to "human rights in the digital age".

**Aral:** Because when you have a separate set of digital rights, you you have an opportunity to make those rights lesser than the human rights that you already have. So we really need to be very careful and not kind of get caught by this, that we already have a set of human rights. We need to apply them to the digital age, very different to creating a separate set of digital rights. Digital things don't have rights. Humans do. So let's get that right. I think and it looks like Laura's actually just left us as well. Again, she's probably having connection difficulties.

**Aral:** One thing I want to see, we're running out of time. We have a couple of minutes left. But one thing I really want to see and for us to cover is Martine, you have a cat behind you. Is it possible to bring that cat closer that we can all see? Him or her? Who is who is that?

**Karl-Filip:** There's also a dog here, so the cat and the dog get along fine.

**Aral:** Oh, that's awesome. So what's the cat's name? Is it a he or a she?

**Martine:** She.

**Karl-Filip:** It's a she.

**Karl-Filip:** Her name is Pixel.

**Aral:** Pixel! Ok. Of course, her name is Pixel. Where’s Pixel? There's Pixel, hi Pixel. How are you?

**Aral:** I'm adding Laura back into the stream as well. She's back. Oh, Adam's here as well. You know, we're about to wrap this up.

**Aral:** We are about to wrap this up, but Adam's been trying to come on. I don't even know if he has a question to ask, but I'm just putting him live.

**Aral:** I'm not even asking if. Hi Adam!

**Adam:** Hi Aral, thanks. I hope you guys can hear me.

**Aral:** Yeah, we can. How are you?

**Adam:** I'm good, thank you. I do have a question but I think it’s maybe a bigger thing. So I…

**Aral:** Go for it.

**Adam:** Like many of us here I'm building a tool and I can see that tool augmenting human thought, I'd say extends ourselves into the machine. And so obviously I'm concerned about the extension. You touched briefly on that idea that if we understand the machines extend us as beings, that there needs to be the extension of our rights into the machine. I know Aral, you've talked about this in lots of ways and whatever…

**Aral:** Cyber rights.

**Adam:** Exactly. Exactly. So are the organizations that are pushing governments to think about this seriously? Because if we are extending ourselves into Google, we know we are losing part of our, we're losing part of our brain to Google in an inappropriate way, I think.

**Adam:** And if there are organizations that doing that, can we work with them to then tap into that public funding? I don't know if anyone knows or has thought about that? My funding is through universities and looking at research council funding and those kind of things and often it’s looking at sideways ways to sort of fit something in. And so, yeah, that was kind of my question, was just the idea that we've extending ourselves digitally, surely then there must be organizations pushing governments to think about that?

**Aral:** So, I mean, who wants to take it? Because I you know, I'm on board with that, that's that's how I see things.

**Sander:** I don’t really understand the question. Is this about sort of respecting the human rights that we already have, or are you talking about new rights that relate to cyber?

**Aral:** So I think it's about. Sorry, Adam, go.

**Adam:** No, just the idea that you extend your thinking into machines. And so if I'm using a tool for thought, which is what I build. And there's more coming now, because of the pandemic and whatever, the tools then extend ourselves. So I think collaboratively using a tool, and I want that data to be somewhere to extend my mind and my memory, to me as a machine.

**Adam:** They talked about in the 60s, human computer symbiosis and this kind of stuff. But if most of those tools are documents, whether the information is not owned by me in the first instance, then that's problematic. I don't want to think about… I don't want to think in a space where someone else decides that they're going to use that to try to sell me more red trainers or whatever. So it’s an extension.

**Sander:** Yeah.

**Aral:** Yeah. And that's that's actually, that gets back to the very definition of privacy. Because privacy very simply is the right to decide what you keep to yourself and what you share with others. The moment a thought originates, a moment something you write originates on a platform owned and controlled by someone else, you've lost that fundamental right, that initial original right to say, I don't want to share this. Right. So, yeah, exactly. So I mean, Sander, to get back to what you were saying. Yes, it is about applying human rights to the digital age.

**Aral:** It's basically understanding that if we extend ourselves using technology, then we must extend our understanding of human rights to encompass those technologies by which we extend ourselves. So it's a constitutional issue. I mean, this is like, this is not something we're talking about. This is not something we're talking about at the European Parliament. We're not talking about these things. We're still talking about very basic things. But this is a constitutional change. And I, Adam, I totally agree. We need constitutional change in order to be able to safeguard the bounds of the person in the digital age, because that's different.

**Aral:** They're no longer at your biological bounds, right? They're extended by these technologies that we use. So we must encompass those. That's a much legal… that's a much larger legal constitutional effort. I don't even know if we’re there. But we should  be talking about these things in parallel. But, you know, that's a different topic, maybe a different topic for a different show, for a different stream. Right now, I'm aware that we are out of time. Unfortunately, Laura had to remove herself again because she's having audio difficulties.

**Aral:** So I'm going to do the outro myself. Thank you so much to all of you for joining us. Sander, thank you so much for your presentation on the public stack. Maybe what we can do in the future. I was talking to Sophie Bloemen from the Commons Network as well. She couldn't make it today, but we worked with them as well.

**Aral:** And with Waag, Marlene was there, for example, and we worked on reframing away from this whole digital single market framing that the EU seems mired in, towards a human rights-based approach. So maybe we can have, in the future a stream, where we just have you and we have Sophie on. And we go into the details really of this, if that's something you'd like as well?

**Sander:** Yeah, it’d be interesting.

**Aral:** Cool. Well, thank you so much. Martine and Karl-Filip, thank you for joining us as well. And thank you for all of your support at both in the past and ongoing as well. It really means a lot. That was an injection of hope working with the city of Ghent. For me. We're still working on the same problem. We're still working. We're getting closer. So hopefully in the future will be able to do something where we'll actually have the thing ready to begin with. And that will change how we can maybe approach cities. But thank you so much for joining us. Leonardo. Adam, thank you for joining us as well. Impromptu in the studio. Hopefully we'll have more of that in the future. Thank you all to, thank you to all of you. I am going to now do our little outro so that by zooming in on Martine and Karl-Filip because why not?

**Aral:** You know, so that was that was this week's stream of Small is Beautiful.

**Aral:** And I'm just going to… ugh messed it up, you know. I've got this gorgeous, gorgeous transition, so I'm going to do it again. Thank you so much for joining us on Small is Beautiful.

{{< fund-us >}}