---
title: "Accessibility in 2020"
date: 2020-07-15T17:17:43+01:00
speaker: "Laura Kalbag"
description: "Laura gave a short talk on the importance of accessibility in 2020."
---

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/438343086" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>

{{< fund-us >}}