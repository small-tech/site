---
title: "Small Is Beautiful #10"
date: 2021-05-20T20:37:17+01:00
speaker: "Aral and Laura"
description: "Independent and ethical video and streaming with Small Tech."
---

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/537360098" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>

## Independent and ethical video and streaming with Small Tech.

Livestreamed on Thursday, May 20, 2021. [Read the transcript below](#transcript).

We were joined by [Gabe Kangas](https://gabekangas.com/) ([Owncast](https://owncast.online/)) and [Heydon Pickering](https://heydonworks.com/) ([Webbed Briefs](https://briefs.video/)). We discussed independent and ethical video and streaming with Small Tech, including using Owncast as a Small Tech alternative to Twitch, and Aral demos how he has built a quick way to install Owncast with Site.js using Basil. After briefly talking about Mastodon, and Twitter addiction, we discussed Heydon’s educational (and entertaining) Webbed Brief videos, and finished off with a chat about funding and using Open Collective.

### Links from this week’s livestream

- [Owncast - Selfhosted Livestreaming](https://owncast.online)
- [Webbed Briefs by Heydon Pickering](https://briefs.video)
- [Mastodon](https://joinmastodon.org)
- [Moa - posting from Mastodon to Twitter](https://moa.party)
- [Hell site – Aral Balkan](https://ar.al/2021/05/10/hell-site/)
- [Pixelfed - Federated Image Sharing](https://pixelfed.org)
- [Pleroma — a lightweight fediverse server](https://pleroma.social)
- [The Mutable Gallery by Heydon Pickering](https://mutable.gallery)
- [Site.js](https://sitejs.org)
- [ActivityPub](https://www.w3.org/TR/activitypub/)
- [Hypercore Protocol](https://hypercore-protocol.org)
- [Solid - Wikipedia](https://en.wikipedia.org/wiki/Solid_(web_decentralization_project))
- [Webbed Briefs - Open Collective](https://opencollective.com/webbed-briefs)
- [Owncast - Open Collective](https://opencollective.com/owncast)
- [Small Technology Foundation – Fund Us](https://small-tech.org/fund-us/)

If you like this livestream, please help support out work at Small Technology Foundation with a [patronage or donation](/fund-us), or share our videos with your friends!

### Transcript

**Aral:** Hello.

**Laura:** Hello. [both laugh]

**Aral:** Go ahead, Laura, you take it away,

**Laura:** Alright…

**Aral:** No one's going to answer…

**Laura:** Great start!

**Aral:** … that’s not how this technology works. [both laugh]

**Laura:** Hello, I'm Laura Kalbag and I am one half of Small Technology Foundation, a teeny tiny, not for profit organisation based here in Cork in Ireland. And as per usual, I am downstairs and he is upstairs.

**Aral:** And this is a monthly live stream that we call Small is Beautiful. And on today's stream, we have a couple of very awesome guests that I'm very excited to have with us. And I'll add them to the stream right now. To start with, we have Gabe Kangas from Owncast and Owncast, as you know, is kind of like your own Twitch so you can set up a Twitch without being on Twitch. Hi Gabe, thank you for joining us. How are you?

**Gabe:** I'm great, thanks for having me.

**Aral:** Awesome, and Laura, would you like to introduce our second guest?

**Laura:** I am honored to introduce Heydon Pickering, who is a pal and an all round legend, wrote the intro for my book on accessibility because he is a very knowledgeable person about all things accessability, wrote some fantastic components and gives us all a lot of great guidance on how to make the interfaces that we build more accessible. And yeah, there we go. That's Heydon’t book right there. Hi Heydon! How are you?

**Heydon:** I'm good. It's really good to see to see you after such a long time. And so I've partly agreed to do this to just to catch up. As well as talk about stuff. So…

**Aral:** It's probably why we do it. Really.

**Heydon:** Yeah. Yeah. As you say, you know it’s a relaxed thing anyway. So yeah.

**Laura:** Yeah unfortunately this time. No alcohol.

**Heydon:** True… Well you don't know. I might be… I might have started drinking already. I actually haven't… I’m drinking decaf coffee but…

**Aral:** I actually have tea with me for a change.

**Laura:** It is the evening here. But Gabe is way, way behind us in the morning.

**Heydon:** Oh!

**Gabe:** Oh.  I can't start drinking for a few hours yet.

**Aral:** So because both of you suck at self promotion, by the way. Laura, your book is called Accessibility for Everyone and is available from A Book Apart and a Heydon's book is called Inclusive Design Patterns: Coding Accessibility into Web Design. And it's from Smashing Magazine. And it is awesome. And there's also an Inclusive Component's website where you can go and you can read Heydon's posts and you can copy as code.

**Aral:** I'm going to do a demo a little later on, something I built, and you will see Heydon's tab bar component, which I ported to Svelte, featuring heavily within that interface, some might say being abused. [laughter] But at least it’s accessible!

**Heydon:** I should say this book is out of print. You can't get that one anymore. So look for the black one rather than the white one.

**Aral:** OK, there's a black one as well, which I didn’t know.

**Heydon:** Yeah. That's that's the more recent one. Yeah.

**Aral:** So folks we're here…

**Laura:** We've got that one too!

**Heydon:** OK.

**Aral:** So we're here to talk about video and streaming. How can we do it in a way that we're not just signing up to the factory farms of Big Tech? Is there a Small Tech way, a Small Web way of doing video and streaming? Something that we've all run into no doubt in the last year or so, especially with the pandemic. And the idea is just to have a conversation. But we're very lucky to have both of you, because Heydon, of course, you are working on Webbed Briefs, which I'm a big fan of.

**Aral:** I don't know how you find the time, but they're amazing. And the production quality is amazing and Gabe, of course, you're with Owncast and you've, you know, which is just amazing because it's a free and open source, basically Twitch. And that's actually doing it a disservice because it doesn't have all the bullshit that comes along with something like Twitch. So it's Twitch maybe sans the bullshit. So let's just have a conversation. Who wants to kick it off? What do you mean… yeah, you know, Heydon, do you want to talk to us about Webbed Briefs. Why Webbed Briefs? What happened?

**Heydon:** Yeah. So I started off, I've always been heavily into music production as well as visual design, and so I started getting into doing video stuff a little while ago because I wanted to… I'm very easily bored and distracted and I wanted to do something which which kind of where I could be creative in lots of different ways. And so I started doing… so with with video, and the way that I do the video, it's writing because I have to script it. And obviously I stack it as full as possible with gags, and also loads of technical information. And I try to get those two things to sit comfortably next to each other, but they often don't. And then there's like ambient sound that I engineer using random lo fi oscillators and stuff like that. So that's the sort of creative music stuff in the background that I do.

**Aral:** And then it's you know, it's the production and the performance of narration. That's kind of a challenge as well. And then obviously the animation. And then there's all the kind of like the incidental sound stuff as well, like the kind of slapstick sounds that go into it, too. So there's all of these layers and it was just something to sort of sink my teeth into, I suppose, you know, keep myself occupied. I don't have kids, so I've got to find something to do. So…

**Laura:** And the result is that you end up teaching loads of really cool things about web design and development to people in a way that's fun and silly, and not like overwhelming either. I think that's what's so great about them is they’re really digestible. And because they're humorous, you stay focused on them. So you kind of… you would say you're distracted, but actually, I think you find a way of making topics that other people might find quite dry easier for people to engage with.

**Heydon:** Yeah. So I think it's really interesting some of the feedback I've had, actually. Because the way I make them is I try to make them really dense so that you get a lot of information and a lot of fun, or just nonsense, in a short period of time. And as it turns out, loads of… lots and lots of people with ADHD have reached out to me and said this video format, I don't know what you're doing exactly, but I can actually concentrate on it. I can actually absorb this information. And I hadn't designed it for for neurodivergent folks… I'm undiagnosed, but I'm on that, sort of that way towards the spectrum. So there might be something in that that I was doing it that… The way I'm making things work because it works for me and having that kind of brain, maybe it works for other people in a similar sort of way. But yeah, no, I, I'm really glad for that kind of feedback. So to go back to the kind of the set up thing I started…

**Aral:** Heydon, wait a minute, I'm going I'm going to interrupt you for a second. For those of us with short attention spans, I want to show a bit of what you've done. Is that OK? Can I do that?

**Heydon:** Yeah, sure.

**Aral:** Is that alright? Yeah. OK, so here's one called How to Draw a Line. Let me know if you can't hear the audio or anything. So let's just start that up… [Webbed Briefs video title appears, silence]

**Heydon:** Oh, yeah, I think the audio…

**Laura:** I think we might be missing the audio.

**Heydon:** Yeah.

**Aral:** Oh, no, there's no audio. OK.

**Laura:** No.

**Heydon:** It's really minimalist without the audio.

**Aral:** [Laughs] Very, very minimalist. I should have actually rehearsed this really…

**Heydon:** Just black and white shapes…

**Aral:** So apparently there is no audio from that, unfortunately.

**Heydon:** That’s ok.

**Aral:** All right. So, OK, we’ll we’re talking about it. It’s really cool.

**Heydon:** Well, you can you can find it on Webbed… No, hang on. Briefs.video is the URL, very short URL, so you can go and check it out there. But, um, yeah. So I started making this stuff and putting it on YouTube. And there's two things that I don't like about YouTube. One is that…

**Gabe:** Only two?

**Heydon:** …I'm not comfortable with it being… [laughs] the two main things. Yeah. One is that it's owned by Google and that it's essentially algorithmically radicalising people into hating and killing each other, which is suboptimal.

**Heydon:** And also actually the experience of making videos and putting them onto YouTube is really quite tormenting as well, because the editor and the uploader thing is all sort of over-complex. And I, I have trouble with complex UXs, with lots of things going on. So I didn't like that very much. And you can't do things like edit a video after it's been uploaded. So if I… because I pack loads of technical stuff in there, sometimes I'll find that I mistyped something.

**Heydon:** And then all I can do on YouTube is like put a little sort of overlayed note in there saying, oh, actually this is wrong. Or I can write something in the… but self-hosting it, I can obviously… I can replace stuff and I can edit stuff. So there's that as well. But it's been, it got quite a lot of attention on YouTube because there is so many people there, there are people looking for stuff on YouTube and they one way or another, the algorithms, as problematic as they are in some ways, were also driving people towards my content. So that that was nice. I got lots of comments and it was lots of comments from strangers as well. It was actually bringing people into my orbit, if you like. But I still wasn't… I didn't like that. And I suppose I flattered myself in thinking that if the content is good enough, then if I take it away from YouTube, take it out of that walled garden, or whatever the expression is, that the content will speak for itself and people will come to it. But so far, it's actually quite difficult. People don't do RSS, as it turns out. So I've got an RSS feed for it because it is just like a blog. And it's interesting at this stage because it's not been there for very long. So I didn't expect it to suddenly take off overnight or anything like that. But I think it's a higher quality set of videos to what I was doing on YouTube, but with much less attention because it's outside of that space.

**Heydon:** So what I I'm interested in trying to learn now is how I can do that better and how I can drive more traffic to it without being, you know, click-baity or sensationalist or that kind of stuff. So, yeah. So it's an interesting challenge from that point of view

**Aral:** To kind of tied in to what Gabe is working on as well. How has the fediverse… because I know you're on Mastodon, you’re both on Mastodon, of course. But the greater fediverse, how has that been in terms of just what you said, in terms of being able to promote it, etc.? That, of course, is not the same sort of algorithmic timeline. But in a way that's good, because everyone gets to see every post, every one that's following you actually gets to see every post. Have you been using, I mean, the fediverse in any way in that sort of sense?

**Heydon:** Not so much. I, honestly, I don't tend to spend very much time on Mastodon. I just have a Mastodon.social account. It's nothing self-hosted or anything. I'd prefer to have a self-hosted one. So I already feel like it's a suboptimal thing. I feel like I should… I wish I'd started out on the being part of the fediverse in a different way. So I think, well, you know, I've only got six hundred followers there anyway. I might as well, I'll post about it on Twitter. So I end up on Twitter because literally…

**Laura:** Showing off with your six hundred followers! [laughter]

**Aral:** But on Twitter, you don't know if 600 people are seeing your posts…

**Heydon:** That’s the thing.

**Aral:** Zero people might be seeing it. A portion of your followers could be seeing it. And the only people who know are the people who write, you know, Twitter's algorithms. And even they probably don't know because it's not you know, it's probably adaptive in lots of different ways.

**Heydon:** Yeah. Yeah.

**Aral:** I found personally, for example, that on Mastodon, and on the fediverse, I have far more people actually interacting, actually writing thoughtful comments, actually responding. I did a… I think a poll in both at one point and, you know, I have like what, 44,000 people following me on Twitter. I'm not on Twitter anymore, in terms of I don't check it anymore, but I have maybe several thousand, I don't know how many, on Mastodon. And the amount of people responding to the poll, for example, that I did the same thing, was far, far greater percentage on the fediverse and on Twitter. So even if that's the case, I think yeah. So I think and what I do now is I use something called I think Moa Party, or something, to forward my posts from Mastodon to Twitter.

**Aral:** So I'm just using Twitter as a “if you want to keep in touch with me, here you go.” But I'm not on that hell site. In fact, I just wrote an article called Hell Site on my blog about it. I'm not on there anymore because the algorithm is an asshole. You know, the algorithm is a shit-stirring asshole. And and I'm sick of it because it's kind of like, “hey, look at this. This will really rile you up.” And then it riles me up. And I'm like, “this sucks.” And then, of course, it creates conflict and this and that. And I'm done with that. So now I…

**Heydon:** I spend all day just trying not to.

**Gabe:** Yeah, Gabe you were saying?

**Gabe:** You know, not to go on too much of a tangent, but let me tell you, it takes work to get off of Twitter and use something else. Just like anything else. And it took me a couple times leaving Mastodon and coming back because I'm like, it's not really jiving with me. Right? It took me explicitly removing, little by little, everybody that I followed on Twitter in order to get me off of Twitter, because once I took away the content, then it became less and less useful to me. And then I just kind of started replacing that content little by little with the fediverse. And it absolutely took time and effort. And it's work, right? It's work to take away something that feels comfortable and easy and useful to you and try and replace it with something else. And, you know, I put in that work and I do think it has paid off, but it still has not been a one to one mapping to what Twitter was. It is a different thing. And that's OK.

**Laura:** I've written a CSS user stylesheet for people who use Twitter that actually hides the content of all of the timelines aside from your mentions. And if you go, you can go to people's specific pages, it won't hide that… as a way of weaning myself away from Twitter. And also initially I write it for also the Aral so that, Aral  I wanted write-only Twitter. And so I made that. And then I did a version for myself that was read-some, as I call it. And that's really it's helped me because I, the muscle memory of typing in t-w-i hit enter and suddenly you're there and you're reading all this stuff. If you go there and you can't see anything, you're slowly taking yourself away. But just for balance, I have to also say that I do get as many obnoxious men correcting me on Mastodon, I do on Twitter. So it's an equal experience in some ways…

**Aral:** [wagging his finger dramatically] Actually, Laura. Actually, If I may… [laughter]

**Laura:** So, yeah, I’m not all in on Mastodon quite yet…

**Heydon:** [laughing] Unfortunately, men are still allowed on Mastodon. [laughter]

**Laura:** I think that’s maybe what I'm looking for…

**Gabe:** A right of privilege doesn’t…

**Aral:** Look at us like we're here discussing, like we're like addicts discussing how to get rid of this addiction. And that's exactly what it is designed for addiction. You know, there's actually a book by Nir Eyal called Hooked: How to Build Addictive User Experiences. And this is not a warning, right? It is an instruction manual and it's a best selling book. And in Silicon Valley, they love it. Right? They actually read through this and go, “oh, yeah, this is what we need to do.” I mean, you know, is it any surprise that that we're at this point? Which is why I love the fediverse, which is why I think we need to have, you know, a web where everyone owns and controls their own place on that web, which I call a Small Web. And Gabe, which brings me to something like Owncast. So Owncast. I thought, you know, that we would be launching the first kind of real Small Web app that that we're working on. No, you did. And you didn't even call it a Small Web app. I don't know if you if you do now at all, but that is what it is. It's a single tenant web app. That's really what a Small Web app is. So you want to tell us a little bit about Owncast? What made you do it? What made you start it and how you feel about where it is today?

**Gabe:** Yes, so Owncast is a self hosted single tenant live streaming and chat server, and the idea is that you take your existing live streaming workflow and you just point it at a server that you control instead of a server that somebody else controls and it will just work. So no additional technical overhead. No you having to use different software. You just go in your settings in OBS and change the dropdown from Twitch, which is the default, and point it at your own server and go off running. The reason this was built was like a lot of things during the pandemic… things got weird, right? And I was seeing a lot of DJs go to Twitch and YouTube and Instagram Live and things like that. And I was just hearing all these horrible stories of these people just trying to have fun playing music for people and just keep getting kicked off of their live streams just because, you know, they are playing music, which is what a DJ does. Right? So it's like, well, if they're not allowed to play music, then what are they going to do? That's that's their thing. So I just started thinking a little bit. I'm like, well, there's a self hosted version of everything, right? You know, you can self host your blog and, you know, there's a there's Pixelfed for doing an Instagram now. And you can bring up a Mastodon or Pleroma. There's no there's no Twitch, right? There's nothing like it. And and I thought, well, you know, it's probably hard because, you know, streaming video is magical. Right? There's an air when you talk about streaming video that it's somehow like greater than the sum of its parts. And it's just it's magical and mystical. And Twitch has a team of wizards like in the basement or whatever, and that's why they can do it and you can't, which is absolutely not true.

**Gabe:** Right? Video is just stuff on the Internet like anything else. Right? You email a photo to a friend, you're sending some data. Well, that's all that streaming video is. You're just doing more of it, you know, one after another. So I started experimenting. I'm like, can I run this myself? And started just like duct taping some things together and seeing that, yeah, it works. You know, you don't need a mega farm of computers to be able to pull it off.

**Gabe:** And I think people want you to believe that is true because it'll keep you going back to Twitch and YouTube. Like that they're the only ones that are technically capable of doing this for you. But you know, once I…

**Aral:** Well, but you need a server farm, Gabe, you need a server farm if you're going to farm people. Right? That's a server farm is for.

**Heydon:** Well yeah all the computational powers is for that stuff, isn't it? Yeah? It's all of the tracking and the hacks

**Aral:** It is!

**Heydon:** and all of that.

**Laura:** Yeah.

**Gabe:** Yeah. For the harvest.

**Aral:** I mean, just look, yeah. Look at how much of any web page like on a mainstream site is taken up with JavaScript, et cetera, that is solely for tracking you, solely for violating your human rights and perpetuating this toxic system that's eroding our democracy. As you alluded to earlier, Heydon.

**Heydon:** Oh, you just reminded me of something, actually. I've just I've just done a kind of like a buy your own print of the artwork you create on… I’ve got a website called Mutable.gallery. And I was talking to the people who have the API for the printing, because obviously I can't do print fullfillment stuff on my own. And it just is a serverless function, talks to them, converts the SVG image into something, etc.. But the response time on their end from pressing the magical “print this unique artwork” button was really slow, like 10 seconds slow. And I thought, how can that be? Because all it's really doing is just taking my settings and a bit of cached JavaScript, which has my, like, little shop settings in it. And I still don't know the answer to why this is. But I looked at the… you know, when you just see “waiting on such and such” and you see kind of like a load of third parties flash up and it said “Google Ad Services.” I thought all this is doing is someone on my site pressing a button to provision a product on their site. What's Google Ad services got to do in the middle? Like why is that there at all.

**Gabe:** You know exactly why it's there. [laughter]

**Heydon:** Well like, it's kind of like I get it, but also what? You know, and it's slowing the experience down as well. It's not just bad for for ethical reasons. It's bad because it's like you just put this massive wedge there between… and now I'm not selling prints because it's going to be too slow for some people.

**Laura:** It’s one of my greatest frustrations with people who talk about performance as experts, the fact that the first thing… they don't say is the first thing they should be saying is “remove the tracking junk.” They’re talking about optimising the tiniest bits of nonsense when, that make very little difference, when actually just removing that tracking junk would get rid of it.

**Heydon:** Yeah.

**Aral:** Yep.

**Heydon:** That's actually one of my favorite things. I think I'm going back to Twitter. I think it's something you said on Twitter, and I love that you said, you know, like Laura you were saying “hire me as your performance expert. All I do is to say just get rid of the third party, bang!” And that would be it. That would be the lion's share of the performance difference that you'd make it probably 80 percent faster, just like that. Just by refusing to include those things. Yeah, I thought that was really good. And some of the responses you must have got to that I don’t know… [laughter]

**Laura:** You know there's a reason why I don't tweet that much anymore, get myself in trouble.

**Aral:** So I'm going to pull this back to Owncast. And the reason I'm doing that is kind of self-serving. So, Gabe, right now. If you want to host your own Owncast, you have to set up a server and do that, which takes some technical knowledge. Right? So what's the current state of it? Because I want to talk about something I've been working on.

**Gabe:** Yeah. Yeah. So there's really a pretty wide array of options to install Owncast. We now have a single click installs with Linode and Digital Ocean. So we're seeing people who are not technical people and don't really have any interest in learning how to set up a server. Just do that method and then, you know, they'll pay the five dollars a month to Linode or Digital Ocean and then they will get a functioning Owncast server from that. So, you know, us spending the time to kind of flesh out all these different ways that people can install, I think is is paying dividends because there really is a wide array of people that want to do this. Right? I would like to see the kind of the mentality that if you are running server software, it has to be hard and you have to download the source. Right? Like kind of the mindset that started with, like Ruby on Rails and continued on with, like, NodeJS and Python, that it's like if you want to run server software, you have to know and understand how to run the source. Right? You have to know what the runtime environment is. You know, you better have all your dependencies in check. You better have your version managers. Right? Because you have to run four different versions of of Ruby at any given time. Right? So, like, that all sucks. And it treats people who want to run server software like they're less than people who want to run client software. Right? Like your mom who is installing Microsoft Excel, doesn't know what version of Visual Studio it was compiled with. Right? And that she shouldn't have to. So why on the server are people treated differently. So…

**Laura:** Or your dad… your mother might be a fantastic programmer.

**Aral:** Your mom might be a computer scientist.

**Laura:** Yeah.

**Gabe:** Yeah. So, yeah, just just having these different ways of treating server software as real software for real people, it opens up doors for people. And that's why I kind of love what what you guys are up to because that's your mindset, right? You're not not saying, OK, download this version of Node to run Site.js, and OK, you know, do npm install and oh whoops, you have a conflict with this another project?

**Aral:** Yeah, you’ve just lost…

**Gabe:** I mean, you lose me.

**Laura:** You lose me!

**Aral:** Yeah. Well, and that's the thing. Site.js, for example, is for developers. But that doesn't mean that we shouldn't make it as simple as possible, because unless you have that mindset at every level of what you're building, every layer of what you're building, then you're going to build something complicated that people can't use. So that has to start from the very beginning in terms of your approach to how you build things, and especially if we want to move people away from the silos of Facebook and Google and et cetera. So what's what's really great about Facebook? Right? And you might say nothing, but it's the onboarding process, right? At least it used to be. I don't know what it is now. I'm sure they're adding more bullshit into it. But you go to Facebook.com, and anyone can really create an account. You're on there and you're mired within that swamp. So how do we make the ethical alternatives as simple, if not simpler to use? Because we have a huge advantage. We're not here to mine people. We don't have to have exponential growth. We don't have to have, you know, people that we exploit and we don't have to have even a concept of users. So Owncast is for one person, right? The person is the owner. They control it. They own and control it. This is what I would call a Small Web app. Call it an SWA if that's going to make it take off. So, you know… they love their three letter acronyms on the web.

**Aral:** Swaaaa, that's that's how you pronounce it. No, please don't. Ever. So anyway, so how do we make it as simple? So we're working on our own sort of Small Web application where you'll be able to talk to each other privately, publicly, etc.. But the first one, so I'm building a hosting solution for this, that other people will hopefully also run and cooperate with us, not compete, cooperate with us so that we can have lots of domains hosting lots of Small Web apps and Small Websites. So I want to give you a demonstration of something that I just got working today. So it's called Basil, the host, and this is it, installing Owncast, which is one of the first things that it can install. And it can install it so easily because Owncast is single tenants, I was able to actually build the installation into Site.js. So that's how we're doing it. But I'm not going to talk about it. I want to show you. In under a minute let's install Owncast and and I'm going to stream to it. OK, so you know, if the demo gods allow me.

**Aral:** So I'm just going to make myself full screen here. Hello. [waves] All right. So let's do this. So I'm going to switch to my screen and bring up my terminal. You don't need terminal or anything, I'm actually going to just run Basil. So I'm running it locally right now. But you would just go to a website. And normally if you went on the website, you would see a site like this, only it wouldn't say this is a private instance. It might have, for example, a form there for you to create your own app. Because I haven't built the payments stuff yet into it, we're just going to go into the admin. This is so early, this is nowhere near use, ready for use, by the way. So this is the administration, this tab component that I'm abusing the hell out of over here is Heydon's accessible, inclusive, sorry, tab component that I ported. So you can actually tab into it. It's like if you select something, it has focus, and you can focus into what's in there. Heydon, great job. I mean, so good, it’s so lovely to work with. So here we go. This is how you would use it if you had like your own installation of the web host. Maybe it wasn't a public instance. Maybe you have it for your family, maybe you have it for your organisation. So I want to install Owncast with site Site.js. [selects Owncast from dropdown] I want to install it on my owncast.small-web.org [types owncast into text field]. OK, let's create that server [presses Create Server button]. Now. It's happening. So I'm going to make this full screen here so we can see it [installation progress checkboxes]. This is actually doing it now, right? So it's installing Owncast, it's going ahead. And then it's… actually what it's doing right now is running Site.js which is installing Owncast. And these [timings] actually are based on readings that I had on the server itself. So those are not actual progress that's being reported, but it's progress we're guessing at. It's getting its security certificates. And right now it's actually now trying to see if the response, if the server is up and responding, that can take a little longer, a little less, sometimes the Let's Encrypt certificate takes a little while to load. There we go. “Visit your place.”  Now, if I go there, what's loading right now is Owncast. And it's loading, if you look at the URL at owncast.small-web.org, now I'm going to go a little further and actually stream to it.

**Aral:** So I'm going to switch to my roaming camera here so I don't create a dreaded… what is it called? A drast effect? A drust something druss?

**Laura:** If it ends up getting choppy, I'll remove the rest of us from the stream.

**Aral:** No no no. We won't. It won't.

**Laura:** We’ll see.

So what I'm going to do is, I'm just going to I've already got owncast.small-web.org  pre-programmed here. So I'm just going to go on the air and it's going to broadcast it here. Now, there's going to be about a 10 second latency, which is normal for the type of streaming this is. But oh, well, here we go here. What's going on? Where are we da-da da-da? The stream’s online, where's my play button? All right, let me see, maybe it's just… do I have it? Oh, there it is! There we go.

**Heydon:** Nice.

**Aral:** And if I switch to me, oh, wait, no I can’t switch to me, you're going to get that effect regardless. Maybe if I do it like this [turns camera to selfie view], is that? No then you're just going to see me. All right? Ok.

**Aral:** You see this place, you know, seriously. But anyway, you get it. It's working. We're streaming to it! Woo!

**Heydon:** That's great. I've got to say, this is like to reiterate what you were saying before. This is something I've had discussions with people a lot because I was talking to a few people who are interested in these kinds of alternatives that Gabe and you two are doing, like genuine alternatives to things. And that really is, I think you're absolutely right, it has to be as convenient as as the Big Tech versions, because often what you're doing is you're sacrificing your ethics for convenience. That's what we're doing all the time. That's why we still eat shit out of plastic containers, you know, but it's really important to get that part right, because otherwise, unfortunately, a lot of this stuff, like the kind of the Indie Web sort of stuff and that kind of thing becomes, not deliberately so, but it gets a bit elitist because it's so technically-focused and it can be very alienating. I'm sort of technically adept in some areas, but very much not so in others. So your efforts to… with Site.js and and with the streaming to make all of that just happen for me easily so I can just go on and be creative and do the stuff that I can actually do. That's great. I mean, I've just spent a week recreating a website with a bit of JavaScript and stuff, and it's just like the DNS stuff and all of the server admin stuff I could just do without. So that’s that demo you just did where you build it and that. And there's Owncast is there bang. That's a dream. That's fantastic.

**Aral:** And right now it works for Site.js, it works for your Owncast because actually Site.js is installing Owncast and you can even do that on your Raspberry Pi, just like one line to install Site.js and then site enable --owncast. I don't have to even write it out. That's how you do it and that's why it's able to do it that way. And that's this is so early. Like I said, I just got it working today. Like an hour before this. So going forward, we're also going to have a social network that we're, not a social network in the sense of Facebook etc, but your own presence. If everyone has their own place (and it's called Place) then and we can talk to each other's places, then we don't need Facebook anymore. And if it's 30 seconds. So what you saw was really complicated. It's not going to have an interface like that, that was on the admin panel. It's just going to it maybe is going to have a lovely illustration, like an animated illustration as it's being set up, 30 seconds and then, boom, you have Facebook without Facebook, Facebook without Zuckerberg,

**Heydon:** Facebook, without Facebook is the optimal Facebook. [laughter]

**Aral:** Yeah. So yeah…

**Heydon:** It negates, itself great. Yeah.

**Aral:** Gabe, what do you think about that? Now you’ve seen it for the first time, how’s that?

**Gabe:** Yeah, that was a very cool demo, and I kind of knew what you were working on because you've been sending some posts to the fediverse and I saw when, you got it integrated into Site.js and people are already using that. Right? So within the the Owncast chatter, I heard a couple of people saying, yeah, using it with Site.js works great.

**Aral:** Oh cool, I didn’t know that.

**Gabe:** You know, we added it to our documentation and for another. Yeah, so, you know, having these options and this one where we can say, like, if you have no interest in all this other crap. You know, some people really have interests in that other crap and then they can have the more, you know, hands-on experience. But if you just want to start streaming, then, you know, give them that option. And this is a way to do that pretty effortlessly.

**Heydon:** Yeah, great job.

**Aral:** And I think I'd like to talk to you after the stream at some point about how we can maybe work together a little bit to make it so that, for example, updates are seamless. And so that it really is once you've installed that you don't have to worry at all. It manages itself. Site.js does that for itself. I'd love to make that build it for Owncast as well and for everything else that we do. And I think then we can make these things kind of almost like protocols. If you adhere to this, this, this, it’s a single tenant app, if this is how it updates etcetera, and we make that a standard of some sort, then other people can build apps that way.

**Aral:** And the cool thing about these single tenant apps is no one's going to become the next Facebook, no one's going to become the next Google with these. Because the economies of scale are not there. And that's very, that's really important. Even with something like Mastodon, the primary Mastodon instance has like, what, five hundred thousand people? That's very big. And I know Eugen’s aware of that. And I know Eugen's been trying to kind of limit that. But anything that's federated like email was, etc.. You saw how email was captured by Gmail. Like today, if Google doesn't want you to send an email, they just have to not allow that email through.

**Aral:** And for all intents and purposes, given how many people are using Gmail, you can't send an email. So with single tenant, when you bring it down to one instance for one person, you get rid of that. There are no economies of scale here. There's no incentive for capture, for corporate capture of it. So I think that's where it's like it's so exciting for me to see things like Owncast. And of course, it's completely against the mainstream, like it's lunacy if you were to ask someone who's in Silicon Valley. Like, you know, we don't want to scale, we don't want to grow if it's becoming more successful. I don't want Small Technology Foundation to become a billion dollar behemoth. You know, what would we… why? We fail.

**Heydon:** It’s genuinely subversive because it does completely invert that whole sort of doctrine.

**Aral:** The Small Web is the exact opposite of the Big Web. Yeah, the exact inverse of the Big Web.

**Aral:** Laura…

**Heydon:** I like what you're saying about protocols as well. Sorry, to… before we move on.

**Aral:** No no no, please go…

**Heydon:** This is what came up in conversation… because I was particularly interested in having like a kind of, and I know there have been attempts to do this already, usually through ActivityPub or that kind of stuff. But I really liked the idea, and I was talking to a few people about it, of doing like a social web where it didn't, it didn't use servers at all, it was all literally… the idea was around just having an agreed protocol like a format like RSS or something like that, and then just simply a community of little PWA kind of applications, or like a set of instructions to build those kinds of little applications, which you would then install yourself or create yourself or take one like you would a WordPress theme or something like that. And then you're just consuming data in that format from wherever it's posted. That kind of thing, you know.

**Aral:** And basically…

**Heydon:** And you probably thought more deeply about this, but it's something kind of new to me. But it's something that I've been thinking about.

**Aral:** But that's basically the same direction that we're going in with the Small Web with with Place. The idea is to have an extensible protocol where… so think Twitter. But on Twitter, if you could not just send tweets, but if a message could have semantics. So if a message could mean something else. So if a message could be, for example, a backgammon move in a game. Or a chess move, let's say, or something like that. And the protocol is not set by some centralised body again, but it evolves. We pave the cow paths, if you will.

**Heydon:** Yeah.

**Aral:** So if people start playing these games, then for some people they'll appear as plain text messages and they'll go, what is this? Or maybe they'll appear and they'll say, well, this is a chess move, actually, if you want to install this plugin or this protocol, just tap here and then you'll be able to see it as a chess game, for example. An extensible protocol where… and Twitter was actually going to do this back in the day before they turned full evil. They were actually… they had annotations in the pipeline and you could do this. And I was building some back then called Twitter Formats based on microformats where we could add semantics to tweets, that, of course didn't happen, it won't happen. It goes against their centralised system. But exactly what you're describing. PWAs, Apple kind of, in my view, killed PWAs. And…

**Laura:** What does PWA stand for again? Remind me everyone, because I can’t…

**Aral:** Of course…

**Heydon:** Progressive Web App.

**Aral:** Progressive…

**Laura:** Right, there we go.

**Aral:** Yeah. So basically, it's a web app that gets installed on your machine and for anyone who hasn't… it's basically an offline-first offline web app. So PWa is a marketing acronym…

**Laura:** One of those three letter acronyms!

**Aral:** Exactly. But the thing about it is Apple's kind of killed that because on Safari, if you don't, for example, log in for a week or something, all of your data gets deleted. Unless you add it to your home screen. So you can add it to your home screen in iOS but I don't know how to add it to a home screen on mac, I don't know if that works. But basically, unless you play by their rules again, they delete all the data. And they did this actually to say, oh, this is an anti-tracking thing. And I think that's a case of throwing the baby out with the bathwater.

**Heydon:** Mmm.

**Aral:** But they… because I was building it with an offline-first application, I was going to have replication on there. And when they did that, I was like…  can't do that. You know, imagine any of your apps you use today. If you don't use it for a week, all your data gets erased. That's ridiculous. So that's why I started building it the way that I'm building it right now, where with Place it's going to be a single page application. And that's important because it's like a download of an app. And then we can verify that that app that you're downloading is what you expect it to be. Otherwise you have to trust the server and you could be getting anything, right? So for that reason, it needs to be a single page app, even though there are issues, of course, with SPAs. But other than that, it's basically what you describe with protocols. But I realise that I'm just rambling on.

**Heydon:** No, no, no. It's all still kind of new to me. I just pictured it as that just not being any central place or even any kind of… yeah… central place where things were even kind of amalgamated in the cloud. You literally just have like in your little app, whether it's a progressive web app or whatever it is, some sort of thing on your desktop, a web page that you've saved or something with a bit of JavaScript, maybe. Just literally take like JSON files from different places and just run some functions together to merge them together into a timeline.

**Gabe:** And that really is kind of is what the fediverse is.

**Aral:** basically… sorry.

**Heydon:** Yeah, I guess. Yeah, it's yeah.

**Laura:** This is exactly what Forest is just saying in the chat. We want to bring him into the stream.

**Aral:** Sure. I'll just add him…

**Forest:** Hello, can you hear me ok?

**Aral:** I can, I think we might be able to hear your TV or is there someone else speaking there?

**Gabe:** Or an echo?

**Forest:** My partner is. Wait, there's an echo?

**Gabe:** Maybe it's not, it’s the TV or just something.

**Aral:** Nah, I think it's OK. Go ahead Forest. Hi!

**Forest:** OK, so hi, nice to meet everyone here.

**Heydon:** Hi.

**Forest:** I'm just so I think what Gabe said about… So let's see, Heydon was talking about what if you made an app that just pulls data from somewhere… You know, I'm imagining like an object storage system, like Backblaze, for example, or S3, and you can just pull JSON files and use that to make posts and make, you know, tweets or chess moves or whatever it is. And I think Gabe is right to say that that's kind of the same thing that ActivityPub does.

**Heydon:** OK, my misunderstanding. I guess I thought that ActivityPub required there to be like a cloud, if you like, where where things…

**Forest:** Yeah…

**Heydon:** things were sort of…

**Forest:** So it does.

**Heydon:** Ok.

**Forest:** ActivityPub is the same thing, but there has to be a process running on the cloud somewhere.

**Heydon:** Ok.

**Forest:** So what you're talking about is where the process only runs on the client. There is no cloud to process… the extent is just storage, and that's just the extent…

**Aral:** So that's peer-to-peer…

**Forest:** And that’s all that’s really cool.

**Aral:** And that's a peer to peer system. And the issue you run into with peer to peer systems is… Well, there are two things, availability and findability.

**Forest:** It's not a peer to peer system.

**Aral:** Sorry, go ahead.

**Forest:** It's using… It's not peer to peer. Peer to peer would be where there is no centralized cloud storage like objects storage, like S3 or Backblaze. There's none of that. Peer to peer is like BitTorrent where it's just sending, you know, client to client.

**Aral:** Oh OK, so wait a minute. What you're saying then, Heydon, is you… Because I thought you said you didn't envision a centralised place where the data is stored?

**Heydon:** Yeah exactly. Yeah. So I just imagine your little app on your computer is what takes, simply takes data sources. And…

**Aral:** And where are those… who hosts those data sources?

**Gabe:** It’s a chicken and egg problem.

**Heydon:** So that would be you would… I’m probably show my ignorance here…

**Aral:** No no…

**Heydon:** But you would host them *wherever* basically. So you would just have a JSON file, which you maintained somewhere. But again, this is the problem where that would require at the moment…

**Aral:** technical knowledge…

**Heydon:** some technical knowledge. Right?

**Aral:** Right.

**Heydon:** So this is resolving that, and trying to get that.

**Gabe:** And it's also…

**Aral:** Go, Gabe, go.

**Gabe:** Yeah, any time you say the phrase this will live somewhere, then that's inherently a server, right? So…

**Heydon:** Yeah.

**Gabe:** …while you're saying, oh, kind of client only, it becomes a bit of a chicken and egg problem because like, this data needs to be bootstrapped from somewhere. Right? So…

**Aral:** Well that's what I meant. If you're saying client-only, then you're talking about peer to peer. So if that JSON file actually lives on your client as well, and then I actually ping your client to get a portion of that, or to communicate with you, then we’ve built a peer-to-peer system. That's what I thought you were actually describing. And that's actually how Place is going to work with the with the caveat that that Place is actually an always-on node on the Web. So that solves the findability issue because it's at your domain name and it solves the availability issue. It's always available. Right? Which is an issue that you have with peer to peer, because initially I was looking at things like Hypercore protocol, which is amazing. They're doing great work there. But again, you have the issue of, well, how do we… how do peers find each other, etc.. This is not… this is a common problem with peer to peer. So, yeah, the Small Web stuff is kind of the the culmination of trying out all of these various things and thinking, well, what can be made sustainable? For one thing. How can an organization like us sustain ourselves in the current system by doing this? And at the same time, how can we build a system that might take us beyond capitalism, post-capitalism, where, for example, these servers might be subsidised by municipalities and given to citizens. So you could have a token based system. That's one of the first things I'm going to build into it, along with Stripe for the current system. So that we can get some money and pay the rent. But hopefully we'll be working with educational institutions, hopefully we'll be working with municipalities, et cetera, for this.

**Heydon:** That's interesting, yeah.

**Laura:** Just before we go any further… Aral we share the studio link? For anyone who…

**Aral:** l’ll share the studio link again…

**Laura:** wants to join us to ask us any questions. Oh, we've just had someone else join, fabulous.

**Aral:** Forest, thank you so much for your question as well. Is there anything else you wanted to add?

**Forest:** Sure, so. I really like what Heydon was talking about. This is something I've thought about as well, that having a process is very onerous, you know, you have to have a servers online that costs money like you talked about, you have to pay the rent. So I think it is possible in theory to make a new type of protocol that would be similar to ActivityPub, but it's based on a more object storage type layer instead.

**Heydon:** Yeah.

**Forest:** And the cool thing about that is you don't have to give Amazon or Backblazecks your data necessarily, you will give them the metadata to some extent because, you know, you're writing files on their disk and they know which IP address is writing the file. But the files themselves can be encrypted. There's tons of ways to do this, including ways that share between your followers. So maybe only your followers can decrypt this file. There are definitely ways to do that. I think that's an area of growth for technology. It's something that I'm interested in.

**Aral:** Cool.

**Forest:** You know, later on.

**Aral:** So very cool. And I think part of it is also, and we touched upon this earlier, you know, you see initiatives like Solid, for example, which of course has venture capital, etc. now. So it's not something that I'm looking into… because how are they going to make their billion dollars and exit? But there are initiatives that start from the protocol layer up and in design, we call this inside-out design. It's where you start with the design of your database and then you work for six months a year, five years, you get European Commission grants, et cetera, et cetera, et cetera. And at the end of five years, you've got your protocol stack and everything's really cool and you're so happy with it. Then you go, well, what are we building on top of it, guys? Well, you've already made so many decisions, the design of your protocols, the design of database data structure, et cetera, that you've made a lot of decisions affecting what can and can't be built on top of it without building any of those. So I think we need to really invert that as well. Protocols are very important, but I believe that protocols should be, like I was saying earlier, we should pave the cow paths, build the applications.

**Aral:** So Gabe’s built Owncast. People are using it. We're learning. So how can we generalise from something that's working then to a shared protocol. When, and I think even with, you know, Tim Berners-Lee, when he created the Web, he, first of all, created a working website and it was useful and he wanted to hyperlink documents together. He didn't think “linked data” and RDF, that came later when he'd already built the thing that was working. Everyone started using it. And then it was like, well, what's the next theoretical step? And nobody's using RDF. Nobody's using linked data, really, right now because it's too difficult. Right? So I really think we need to follow basic design rules, which is design from the outside in. Think about the people using it. What are what are you creating for them? What is the object? And then once we've done that, how do we generalise standards out of that? How do we generalise protocols out of that? But if we're not building things that people want to use to begin with or if we're being so generic, we're like “we're building the next Internet.” Well, do you even know what that means? I don't. I think it's…

**Gabe:** Actually, what the problem with ActivityPub is at its core, it was built as a standard and something that is overly-extensible and saying, all right, world, you know, fine, find a use for this. And people are finding awesome uses for it. But because of that, it's probably way more difficult to deal with than it needs to be because it's just overly broad and was built as a standard, not a solution. And I know people will probably be upset with me for saying that, but…

**Heydon:** I agree…

**Gabe:** You can build whatever you want on ActivityPub, but you can't build anything efficiently and easily.

**Heydon:** And it's a steep learning curve. Yeah.

**Gabe:** Yeah.

**Aral:** Yes, and I think anyone who's tried to implement it has run into that. I did personally as well. I was looking at ActivityPub and building it, and it's exactly that. And I think the reason why it's gotten so much traction is we did actually have a very usable client in the form of Mastodon because it was around for a long time, not ActivityPub itself, but, you know, the technology it's been based on. But they weren't seeing as much traction. What I think Eugen did really well was to, again, create that usable thing at that people actually started using. And then, of course, that impacted the standard because these are living things and and vice versa. But yeah. So I really think we need to take that sort of an approach. Build useful things in the right way, like in ways where we've got the where we know it can't scale to be a Facebook, et cetera. So we built a single tenant, for example. And as simple as possible, because that gives us a huge advantage. You know, if you build something and even like, say, Mastodon, Mastodon can work for one person or five hundred thousand people on Mastodon.social. So that is a huge amount of complexity. The more the moment you go one person more, you've just added a huge amount of complexity. If we get rid of that complexity, we can use that as our competitive advantage against these corporations that employ tens of thousands of people. So simplicity is our competitive advantage in this. We don't have much. We don't have money. We don't have tens of thousands of engineers. What we do have is simplicity. What we do have is a lack of bullshit, which I think is very important. Right?

**Aral:** They have to build two apps for every app they build. They have to build the app that people want to use and they have to build the app that their customers want to use to farm those people that are using it. So, you know, hats off to them. It's a lot of work. It's amazing that they pull it off. They pull it off because they have so much money and talent. We don't need to do that. We just need to build one app because there's no bullshit involved. Right. We don't have that ulterior motive. So we say…

**Heydon:** OK, you go ahead, go ahead.

**Gabe:** No, you go.

**Aral:** Go Gabe.

**Gabe:** When people look at big companies and I hear them say, like, what do all those people do at that company? Like, they are so big, they have so many employees. And then you start thinking of things like that. Right? It's like not everybody at Twitter is working on Twitter.app for iOS. Right? [laughter] So what are they doing? And then the answers are obvious, right? It's everything else. Right? There are far more people working on all the other shit than they are working on what your timeline looks like.

**Heydon:** Yeah.

**Aral:** Yeah, yeah.

**Aral:** And I mean, I feel so sad for people who are on Twitter's web team, because if you go on the web interface, it's the only interface I use Twitter on, and it says “Twitter is better on the app.” Can you imagine working on the web team? And the first thing that I mean, some poor sod had to code that…

**Heydon:** Someone working on the web team…

**Aral:** You know. Someone said, right, they said to someone on the web team that Twitter is better on the app, like, oh, my god, why? It's not better on the app, they can farm you better on the app. So, you know, I'd be pissed off if I was on the web team.

**Heydon:** The other thing that I think apart from reducing, you know, reducing complexity, increases performance, increases usability and everything like that. But also I just have a particular thing about, um, about brands having like an incisive, obvious thing about them. So, like, I heard this story the other day about someone who they ran a shop called Just Sandwiches, and they serve soup as well as sandwiches. And so, like, that bothers me. And I'm very much like you, Aral, where I want to take a small problem and fix it well. I don't I don't want to reinvent the Internet or whatever. I just want to do one thing really nicely. I mean, that's all I can do because I'm only one person anyway. But that's yeah, the idea is to just to just take one thing and that's your job. And if everyone takes one little job, then …

**Aral:** That’s how we reinvent the Internet.

**Heydon:** Yeah, exactly. Yeah.

**Gabe:** It's a very slippery slope. Right? So you do one thing and you do it pretty well. And then people say, why don't you add X, Y and Z? And you think, well, I did X pretty well. Why don't we add Y an Z? Why don't we make some soup. Right? We're pretty good at making sandwiches. People ask for some soup to go with their sandwiches. Why not?

**Heydon:** But change the name!

**Gabe:** It's just like and it's a very slippery slope, right. Oh, then do you need to hire, like, a couple more people to make the soup? Right? And then it's like, oh, you need to change your menus and then maybe you have to raise your prices in order to pay for the people and the the name change and, right? So it’s just like, it goes downhill real quick.

**Aral:** And this is so…

**Heydon:** Yeah.

**Aral:** …this is so true for free and open source as well, and especially where, you know, why don't we just add another setting? Why don't we just add another feature? We need to cultivate a culture of No. Where every yes has to earn its place through by going through a trial of fire. Right? No has to be your first response when someone says, hey, do you want this feature? No, tell me why. And we're going to put it through a trial by fire to see if this really needs to exist. Because, again, you have to keep in mind that simplicity is our advantage. Complexity is theirs. Both to make things seem more complex than they are, to tell people that they're stupid, they don't understand it. Right? Because then people are just like, oh, I guess there must be a reason for all this bullshit. But also they can handle complexity because they have the resources to. So simplicity is our is our competitive advantage…

**Aral:** Laura… go. You were…

**Laura:** Oh, yeah, I was going to say… and with adding extra settings and things like that, I think sometimes we have to ask when we're building these things. Who is it for? If you are building this just for yourself and you're not intending for anybody else to use it, then fine, add whatever you want. But the second you're actually building something that's for other people, you have to bear in mind that there's a lot of other stuff and responsibilities that comes with that. This is one of the things that I think about when people talk about accessibility and inclusivity. Like, if you are building it for other people to use, you better make it so other people can actually use it, and you're accommodating their needs and not just your own. And it's the same with those settings as well. And I think a lot of it is about what do we intend to do with the things that we build? And I think the same also goes for sustainability in terms of those things, like if you're building something, you want to share it, you've got a responsibility to make it sustainable. How are you going to keep that going in this capitalist hellscape that we live in?

**Heydon:** I think it is…

**Aral:** Without. Sorry, go ahead, Heydon.

**Heydon:** I was just going to say still on this sort of accessibility tip that we're now on… it's I always find it funny that teams and people I've worked with always quite excited. And and I want to cultivate that excitement about accessibility of adding like settings and ways to make things accessible, like a checkbox which suddenly makes something accessible or whatever. So sometimes you end up with all of these additional features where you could have just had sensible defaults from the start, you know, so when you have, like your your button, where you press the button for contrast, like you'd have to find that button with the low contrast [laughter] in order to access it… So that's something that I've, yeah, that I keep banging that drum. Quite a lot…

**Aral:** I mean…

**Heydon:** A lot of the extra features are things that… there's another dimension to it as well, which is that you have all of these new features that people are adding, but you still got the core features, right? So the stuff that should make it an MVP [Minimum Viable Product] like accessibility isn't there. And you're still adding features like, you know, Nazi dating or whatever it is that social media decides is going to be popular now. But yeah.

**Aral:** Yeah, I mean, a lot of guys and we can talk about this for hours, but a lot of this goes back to business models. Again, there's a reason why these things are being added. It's not just people making mistakes. You know, there is a financial reason or they’re… you know, they may or may not be able to reach their goals, but they're trying. But, yeah, I think I'm also aware that we are we've been running for about an hour and we try to keep it an hour. But Gabe, you were about to say something. And Laura, do we have any other last questions that we can take?

**Laura:** We don't have any other questions right now. So I think let’s hand it over to Gabe.

**Aral:** OK, before I do… before we do, I just want to thank Forest…

**Laura:** [laughs] Sorry, Gabe!

**Aral:** Sorry Gabe. Forest, thank you so much for joining us as well. I'm going to take you out of the stream, if that's cool?

**Gabe:** Good seeing you, Forest.

**Heydon:** Thanks Forest.

**Aral:** Thanks so much, Forest.

**Forest:** Owncast rules!

**Gabe:** Thanks buddy.

**Aral:** It does! Take care, bye bye. Right, Gabe? Just the four of us now, so go for it.

**Gabe:** Yeah, I just wanted to chime in on kind of the feature addition thing and how, you know, that it's a little bit of a different vibe when you're talking about these smaller projects and open source. I know with Owncast, I really started small because I didn't know what people would use it for, and what features they needed. And I wasn't really interested in guessing and being wrong. So I would you know, I generally kind of go for the lighter version of things and then see what people complain about. Right? Because then then I can fix that as opposed to just making the wrong decision up front and being like, well, that's kind of what I came up with. And I don't know. It’s built now. So, yeah, that's it's different when money isn't involved. Right? Because then it's just you and the people and you were trying to build the best thing possible for those people. And this approach that I've been taking, of staying light and keeping the features down until they bubble up. And we know that, you know, people want this and this is an actual use case, then then you can go, you know, go wild, adding whatever is useful to them. But yeah, when money is not involved, then it's not an advertiser asking for something or an investor…

**Heydon:** Yeah.

**Gabe:** With some ulterior motive telling you to add features, then you can move slower, you can move more deliberately, and you can just build the stuff that the people who are interested in what you are building would use…

**Heydon:** You can actually make it and you have the freedom to actually make a good product…

**Gabe:** Yeah…

**Heydon:** You don’t have like someone breathing down your neck and saying, hey, can you put this dark pattern in here so that it's more difficult to use, or more addictive or whatever?

**Gabe:** Yeah, yeah.

**Laura:** And Gabe, if you don’t mind me asking… how are you sustaining yourself while you’re working on Owncast?

**Gabe:** I have a day job. I'm a contractor. So, you know, I've been kind of working on and off throughout the pandemic. I was working full time when the pandemic hit. And then I, I left and I said, well, in a few months the pandemic is going to be over and I'll just get another full time role and… pandemic’s still going strong. And I'm still in my apartment by myself. But, you know, now I have now I have Owncast.

**Heydon:** So that's all I think that's amazing that you've done, you've created this just through the pandemic. That's a really like, you've taken it so far in such a small amount of time.

**Gabe:** Yeah. We are about to hit the first commit date I think is the 23rd. So. Yeah. And just a few days ago, the first commit, that’s the anniversary.

**Laura:** Wow.

**Aral:** Wow, well Gabe… Another thing I'd love to talk to you about after this is, if we're going to launch Basil, for example, at small-web.org, and with Basil being free and open source, anyone can start hosting these things… In the current system, I want all of us that are involved to be eventually sustainable. That goes for Small Technology Foundation. That goes for you, so you can keep working on Owncast. So let's have a chat about what that would mean going forward. So if we're hosting Owncast, I think it makes perfect sense that a percentage of what we're getting goes to you, goes to the project. So let's talk about what, how we can do that. Because, as much as I'm sure a lot of us want a world where, you know, that's not the primary concern, we have to survive in this system. And in the system that we're building. So I see it as we're building bridges. But in order to build a bridge, you need to be able to survive the trip, you know, the first half of the trip. And, you know, we’re struggling with that right now, but hopefully, with Basil, when it launches. And I like really to, you know, for us to chat about that and see what we can do to it, even a small way,to be able to contribute to what you're doing. And in the future, like I said, I don't just see people, you know, putting a tenner down and getting an app hosted for them. They can do that for less in other places as well. But I see us working with municipalities. I see us working with universities. And, you know, getting these into places where the goal is isn't necessarily just to make a profit, but where they might help in sustaining it because we're building stuff for the commons, it makes sense for it to be supported from the commons. Right? Unfortunately, a lot of our taxpayer money is going into bullshit block chain projects and this and that…

**Heydon:** Public private partnerships…

**Aral:** Aw, fucking hell, don’t even… [laughter]

**Aral:** Anyway… But yeah, on that note… yeah. I'd love to chat more, Gabe, about that. And Heydon, as always, like if there's anything we can do together going forward. Love your work. We both love your work…

**Laura:** Heydon, people can support you on your Open Collective, can’t they?

**Heydon:** Yeah.

**Laura:** That's how you sustain your videos.

**Heydon:** Yes. Actually, so there is an interesting story around that because I really wanted to… I don't actually know what the model is with Open Collective. You might be able to enlighten me, but the the original plan was I was talking to a group of people who are starting a completely co-operative model for like a co-operative socialist version of Patreon, I guess. Because Patreon is quite problematic in a lot of ways.

**Laura:** Oh yeah.

**Heydon:** And I was talking to them back and forwards quite a lot. And, I actually don't know where they're at now. I'm hoping that they're thriving. I was… there was some discussion about my blog and the funding stream for that being part of like the first round or whatever. Yeah, I haven't I haven't spoken to them for a while, but that was the idea, originally… was to to get into being involved with them. But yeah. So, so it's Open Collective. And what I've done with Open Collective is… you do get a few personal things every so often, but it's mostly stuff you might, so far probably stuff that you wouldn't expect. So like 20 minute mixes of ambient music I've been giving to people. But because I have these interlude videos which are just, because the video, the main videos are so intense, they interlude videos are just kind of like art installation-style visuals and and generative ambient music I've made. And then I create longer mixes of them, which I listen to myself when I'm working, because they just, you know, it's proper ambient music. Not like chillout music, but really just sort of sets the tone for the room you're in. I find it listening to that kind of stuff helps me focus when I'm working. And I figure the people who are watching Webbed Briefs and learning about the web work in that kind of way, too. So that's some of the stuff I've been offering. I'm going to be doing like annotated transcripts and stuff like that, too. But mostly at the moment, it's just like, please try and support me, because if you can, because these videos take fucking ages to make. [laughter]

**Heydon:** Like a week. Sorry Gabe?

**Aral:** Gabe also has…

**Gabe:** OK, go ahead.

**Aral:** Sorry Gabe, go.

**Gabe:** Oh, Owncast also has an Open Collective, and we do have some great donors on there that help us kind of pay for some infrastructure that we use for development and testing and things like that. So I think Open Collective, I think has been really kind of a godsend to a lot of open projects like yours and Owncast where, like the transparency makes you feel less dirty about collecting money. Right. Because…

**Heydon:** Yeah.

**Gabe:** I'm a lousy capitalist, I don't want anybody's money. I just want people to have a good time and use cool stuff. [laughter] Right? But the reality of the situation is like, you know, there are costs involved and…

**Aral:** Yeah, your landlord is a much better capitalist than you are.

**Gabe:** Yeah. Yeah. So while I'm not paying my rent with these donations, like, you know, the Open Collective money goes directly into the project and allows us to do… and you know, and run servers, and donate instances to people you know. I'm glad we did it, right. It took a lot for me to say, OK, fine, we'll take your money. But, you know, in retrospect, not because, like, money is great, it's because money can help you sometimes.

**Heydon:** Yeah. Absolutely.

**Gabe:** If anybody's out there and, you know, is turning away people's money. Give it some thought because you might actually find good use for it. And I mean…

**Laura:** And it’s reassuring, because I think nowadays a lot of these big free and open projects have been, are being bought out by big corporations who are sucking them up into them, into their organisations, and often making them much harder or impossible for people to continue using. And so if you know that the project is sustainable, it’s less likely that it's going to be sucked up and removed.

**Aral:** Well, and also there's a reason why they would never buy us, even if they could, because we're actually working against their interests. Right? And they probably think I'm an asshole. But apart from that… There's a reason why certain type of projects get bought. So if we're talking about open source, they were never for what we're doing anyway. They were for the corporate system. They just see open source as part of the business model. And, of course, it's compatible with a large corporation and then they can get bought up. That was always their reason for being, even if that wasn't advertised. So I think part of that is making sure that you are poisonous to big corporations, taking the steps to make sure that, you know, they they wouldn't even want to touch you. They wouldn't want to come close. Right?

**Heydon:** If they're not interested in you, then you’re probably doing something right, you know.

**Aral:** Exactly, exactly. On that note, by the way, we suck at doing this as well. So you can fund us too, the Small Technology organisation… organisation? Small Technology Foundation. Do I even know what we are? I don’t apparently.

**Heydon:** It is .org.

**Aral:** Small-tech.org. I know. That's what I read. It's like my brain is not… it doesn't catch up as quickly as it should sometimes. So /fund-us and we don't use Patreon or Open Collective, etc.. Although, Heydon, when you asked I actually looked it up… Pia, I've met several times at various… she's the one who I believe set it up. I met at several events and she's, you know, totally legitimate as far as I'm concerned, and what they’re doing is really cool.

**Heydon:** Were they Open Collective?

**Aral:** Open Collective, yeah.

**Heydon:** Oh, that's reassuring to know because I couldn't find much info.

**Aral:** So yeah. I mean, it's been a while since I've seen her, but as I remember, she's lovely and you know, they were doing it for all the right reasons.

**Heydon:** Oh fantastic.

**Aral:** And, you know, for us, we have our own funding form that we built on Site.js, actually. It's all free and open source as well if anybody wants to grab it. We actually just built support for this not very well known cryptocurrency as well, that doesn't use all of the proof of work mining, et cetera, that Bitcoin does… called Nano. So, to see if there's like, both to get some experience with it, but also to kind of show that, you know, it doesn't have to be the bullshit that Bitcoin is, etc. Not that… I don't think we're getting anything from that right now. There was a time when the Nano community picked up on it, so they gave us some nanos. I don't know what to do with them. You can build stuff with them, which is kind of cool. You can actually use it, unlike like Bitcoin, you can actually use it. And it's easy to build with…

**Heydon:** OK.

**Aral:** I built a Svelte component and it's actually kind of cool if you have a not for profit, for example, that can't… that is outside the banking system, somewhere. They could use this. I don't know how again they would cash it out, but, because then they would need to be part of the system. But again, we built that in. So that is small-tech.org/fund-us. If you do want to fund this, we're just two people. We don't take any money from surveillance capitalists. We don't take, you know, roundabout money from surveillance capitalists. We get into trouble for calling out people who do so. Yeah. And I think, is that a good place, Laura, to kind of wrap it up?

**Laura:** I think that is a fabulous place to wrap it up.

**Aral:** OK.

**Laura:** But it’s been a really great time. Thank you so much, Heydon and Gabe. You've been wonderful guests. It's been really fun.

**Heydon:** A pleasure! Pleasure.

**Aral:** Thank you so much to both of you. I'm going to take you both out of the stream so we can attempt to do our snazzy outro, unless I mess it up on the mix here again…

**Laura:** Unless I mess it up either…

**Heydon:** I'm going to strap myself, in.

**Gabe:** Bye. [waves]

**Aral:** All right. This is it. This is very exciting. It's going to be an *overlay*. Take care, guys.

**Laura:** Oops, [Gabe is put full screen, laughter] See! I just put Gabe full screen.

**Heydon:** Oh hello again.

**Gabe:** Hi everyone! [laughter]

**Aral:** Let's try to take you out of there, Gabe. But OK, let's do this again. Last 10 seconds didn't happen. Take it easy. See you later. Bye. And here we are, just the two of us, Laura. Take us out.

**Laura:** Right. So I mess up the intro, you mess up the outro. I have been Laura Kalbag.

**Aral:** And I have been Aral Balkan. Thank you so much for joining us on today’s Small is Beautiful. Take care and be well. We'll see you again next month. Bye bye.

**Laura:** Byee!

{{< fund-us >}}