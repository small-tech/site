---
title: "Towards a Small Web – Howest 2020"
date: 2020-12-03T12:00:00+01:00
speaker: "Aral Balkan"
description: "Aral’s live stream to students at Howest in Kortrijk, December 2020"
---

## Live Event

A live talk by Aral presented to students studying design/development at Howest academy in Kortrijk, Belgium on Friday, December 4th, 2020.

{{< video 
	poster="poster-aral-towards-a-small-web.jpg" 
    vimeo="https://player.vimeo.com/external/487237374.hd.mp4?s=8b61e4ef32641b3a5bfe9e9cef53021745f1793c&profile_id=175"
>}}

{{< fund-us >}}
