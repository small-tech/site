---
title: "Small Is Beautiful #16"
date: 2022-02-17T17:00:00+00:00
speaker: "Aral and Laura"
description: "NodeKit and Domain updates and reviews of our recent talks."
---

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/678826485?h=aab2dda2c7" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>

This month we presented NodeKit and Domain updates and reviews of our recent talks.

_Streamed using our own [Owncast](https://owncast.online) instance. (Hint: you can install Owncast using [Site.js](https://sitejs.org).)_

### Links from this month’s livestream

- [NodeKit](https://github.com/small-tech/nodekit)
- [Domain](https://github.com/small-tech/domain)
- [Laura’s latest web3 talk](https://vimeo.com/677162808)
- [Aral’s latest web0 talk](https://vimeo.com/678148583)
- [web0 manifesto](https://web0.small-web.org)
- [Site.js](https://sitejs.org)

If you like this livestream, please help support out work at Small Technology Foundation with a [patronage or donation](/fund-us), or share our videos with your friends!

### Transcript

To follow.

{{< fund-us >}}
