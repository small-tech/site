---
title: "Small Is Beautiful #24"
date: 2022-11-17T17:00:00+00:00
speaker: "Aral and Laura"
description: "#TwitterMigration & Small Web authentication."
---

{{<video poster="./poster.jpg" vimeo="https://player.vimeo.com/progressive_redirect/playback/772136013/rendition/1080p/file.mp4?loc=external&signature=444e1838b7f31c51527caf5e0d4d9c32c5f59d62459902d5c22db99786d1f3b6" >}}

## #TwitterMigration & Small Web authentication

**Broadcast on November 17th, 2022.**

[Laura](https://laurakalbag.com) and [Aral](https://ar.al) talk about the recent migration from Twitter to Mastodon/the fediverse, and Aral demonstrates the latest feature in Kitten: authentication. (Warning: may contain emoji.) And Laura and Aral are joined in the studio by [Erik Kemp](https://erikkemp.eu/) to chat about his experiences in the past week running a Mastodon instance in the Netherlands. 

### Links mentioned during the stream:

- [Is the fediverse about to get Fryed? (Or, “Why every toot is also a potential denial of service attack”)](https://ar.al/2022/11/09/is-the-fediverse-about-to-get-fryed-or-why-every-toot-is-also-a-potential-denial-of-service-attack/)
- [Nlnet Grant Application for Domain – First Update: Questions and Answers](https://ar.al/2022/09/28/nlnet-grant-application-for-domain-first-update-questions-and-answers/)
- [Kitten](https://codeberg.org/kitten/app)
- [Domain](https://codeberg.org/domain/app)

_Streamed using our own [Owncast](https://owncast.online) instance. (Hint: you can install Owncast using [Site.js](https://sitejs.org).)_

If you like this livestream, please help support out work at Small Technology Foundation with a [patronage or donation](/fund-us), or share our videos with your friends!

### Transcript

We’re behind on transcripts at the moment, sorry.

{{< fund-us >}}
