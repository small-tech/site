---
title: "Disruptive Design: Harmful Patterns and Bad Practice"
date: 2019-08-22T17:44:28+01:00
speaker: "Laura"
description: "Laura speaking at From Business to Buttons in Stockholm, Sweden. May 2019."
---

{{< video 
	poster="poster-disruptive-design.jpg" 
	vimeo="//player.vimeo.com/external/355368004.hd.mp4?s=eb14326be1635a81424144ee0d3946c2069f2adf&profile_id=169"  
>}}

Laura speaking at From Business to Buttons in Stockholm. May 2019.
