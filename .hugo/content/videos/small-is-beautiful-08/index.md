---
title: "Small Is Beautiful livestream #8"
date: 2021-04-08T18:00:29+01:00
speaker: "Aral and Laura"
description: "An unscheduled Small Is Beautiful episode for the Svelte Nano Donation component release."
---

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/525561186" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>

## Introducing the Svelte Nano Donation component

An unscheduled Small Is Beautiful episode for the Svelte Nano Donation component release with Laura and Aral.

We covered:

- using it with SvelteKit
- using it in plain HTML/CSS (via Vite)
- accessibility considerations, etc.

__Hosts:__ [Laura](https://laurakalbag.com) and [Aral](https://ar.al).

## Transcript

**Aral**: Hi, I'm Aral Balkan,

**Laura**: and I'm Laura Kalbag, and together we are Small Technology Foundation, a teeny tiny little not-for-profit here in Cork in Ireland, and for once we're in the same room.

**Aral**: We are in the same room. And this is a special edition of Small is Beautiful. And the reason we were chuckling when the the stream started is we were battling for focus from the camera because it's got a 1.4 f1 lens on it, which is excellent unless you want two people in focus at the same time. And it seems to like Laura better so… there we go. All right. It's going to work. It's going to work here. It's me, I'm talking right now.

**Aral**: OK, there we go.

**Aral**: So this is a special edition of the stream because normally we are on on the third Thursday. [laughs] No, it's still following you, Laura.

**Laura**: I'm trying to hide…

the third Thursday… just looks like I have like an odd body. Anyway, it's usually on the third Thursday of the month, which we didn't really realise is next week. So we thought, you know, we have this component we want to release for the nano cryptocurrency. Because like over the weekend, the nano community donated something like a 130 nanos, which is about, I don't know, about 600 euros or so because we implemented a nano donation form on our website and we thought we'd do something nice and pull that component out, make it a reusable component that anyone can use, any not for profit can include on their site to get donations and nanos, which is kind of cool.

**Laura**: Very cool.

**Aral**: Because. Well, because you don't have to sign up for like a payment process or anything. You can actually just put it up there. And within minutes you could be taking donations.

**Laura**: So you don't have to have all the banking stuff set up. You don't have to have all the business stuff set up. Although you should be aware, you need to pay taxes on that stuff.

**Aral**: Well, if it's a donation, you probably don't. But if it's not a donation, if you're giving anything back, then you would. But that the thing with the component is it's a donation component. So you can't actually tell, you know, when someone's donated, etc. you'll just see it in your account, but you won't be able to say, oh, OK, well, you know, great. And here's a t shirt to you. You can't do that. That wouldn't be a donation anyway. That's when you'd have to pay taxes.

**Laura**: And that's our small lesson in taxes.

**Aral**: Well, a lot of crowdfunding people get burned by that.

**Laura**: That is true.

**Aral**: So. All right. So let's get started. Let me just show you what it looks like. So I'm just going to switch to my screen over here, and you can see it here on the right. That is the demo that is at small-tech.org/svelte-nano-donate. So that demo, there's also the source code for it, which I'll share later and we'll look at the code and all of that today as well.

**Aral**: But just to show you how it works, this has been set up so that initially it's set for 10 US dollars. And what you do is, if you have your nano wallet on your phone, you can just scan this QR code. Or if you're on your phone, of course, you can’t scan your phone with your phone, there's a link that you can click and tap on your phone and that will actually take you to the app. So the component itself is very simple. If I reload the page, you kind of see that it's got a little pre-loader.

**Aral**: And then what that's doing is loading the currency exchange rates and then you, the person who's doing the donation, can actually… it's kind of cool… change the the amount and you see the QR code changing in real time there, Or they can change the the currency. So it could be euros. So just to show you how it works, I'll just actually take it down to one euro. And let's go back to our camera here, and [laughs] Laura's hiding, and I'll just take my… I'll open my Natrium app.

**Aral**: So Natrium is the there we go, so that's that's how much they donated over the weekend, the people in the community, thank you so much. That was really cool. We're going to keep those as nanos. We're going to start working with them. Laura’s doing a thumbs up. You can't see it. And so on the Nano app, you have these buttons you can say “receive” and “send”. So I'm just going to say “send”. OK, so then it says “send from”, “enter amount” or “scan QR code”.

**Aral**: So I'm just going to go to “scan QR code”. And just going to… there we go and it says “you are sending 0.2 nanos”, which is, I guess, one euro, to our address and “confirm” or “cancel”. So I'm just going to say “confirm.” It's got my face ID, and it sent the nanos. And I'm going to close this. And let's just see here. Was that 0.2? [Amount in account on screen increases by 0.2.] There we go. That that's the amount at the top that just arrived

**Aral**: And, yeah. Is that the? Yep. That's the one that we just sent. So that's how quick it is as well, which is just so amazing. Of course I just sent it to myself. So that's, you know… and there are no fees with Nano as well, which is very, very cool.

**Laura**: Well I thought, actually. Just, it might be worth quickly explaining why, Nano? Given that cryptocurrencies is such a big thing, and you might have heard both of us probably saying that we are not general fans of cryptocurrency for reasons both social, cultural and environmental.

**Aral**: Right, right. And actually, on that note, let's also put up the URL where you can join us in the studio. If you have a camera and if you have headphones, if you hit that link, you'll join us in the studio that we're actually presenting this from. We have space for nine other people. And if you have any questions or you want to join the conversation, if you have any thoughts, et cetera, you can join us at that URL and we can put you live and you'll be live on the stream. But just make sure you have headphones so we don't get any feedback.

**Aral**: And Laura, if I forget to take that off when I'm doing my demos, please tell me to take the lower third off. OK, so the question was, well, we have so many reservations about blockchain and cryptocurrency, so why Nano? Why add Nano our to our sign up form? And on our form. So if you actually go to small-tech.org/fund-us, you'll see where the form originated. So this is our Fund us page, and originally we just had the card, the credit card payments here.

**Aral**: You could choose a monthly or a one time donation. And this is basically blatantly ripped off of the Wikipedia donation form. And you become a patron, and the card would take you to Stripe's, if I said become a patron there, we get the Stripe sort of form there. But then we added Nano. So this is where the form originated. We've actually pulled it out. The component that we have now is much nicer because we had a couple of days to work on it this week, so we were able to really refine it. But this is where it originated.

**Aral**: So why did we add this? You know, if we have such reservations about cryptocurrency, and blockchain? Well, for one thing, they're not all the same. So what are you doing?

**Laura**: Don’t worry.

**Aral**: OK. You were watching the stream on your phone.

**Laura**: I was trying to get access to the chat so that I could see what if we had questions.

**Aral**: OK, all right.

**Laura**: I'm trying to be too clever for my own good.

**Aral**: That's fine. [laughs]

**Laura**: Sorry, I apologise for the interruption.

**Aral**: So to get back to the question, which I still haven't answered, why Nano and not these other blockchain things? So first and foremost, personally, I'm not opposed to the idea of a digital currency. What I am opposed to is proof of work mining, which is used in blockchain, which is used in, sorry, Bitcoin and in Ethereum on the Ethereum block chains and the forks and the alt coins, et cetera, that are based on those. So proof of work mining is basically in a nutshell, wasting huge amounts of energy, running the same meaningless hash algorithm over and over again until you hit the jackpot.

**Aral**: Lots and lots of computers around the world and specialised ASIC units, etc. doing this, wasting huge, huge amounts of energy, more energy than it's used by certain countries in the world. And why? Well, just so a handful, a literal, like a relative handful, of libertarians, right libertarians can get even richer. So, yeah, we're not about that at all. Nothing about that excites me. That actually disgusts me. I think that proof of work mining should be considered a crime against humanity.

**Aral**: That doesn't mean that we're against the concept of having a digital currency. And in fact, at some point, you know, we are going to move to those currencies. There are things to be worried about there as well, because as we move to what some people are calling a “cashless society” and, we kind of experienced this firsthand in Sweden a little bit…

**Laura**: Where they are big time into cashless, they won't take cash anywhere.

**Aral**: Right. But what's the alternative that's being posed there? The alternative is credit cards from banks. So let's just as a hypothetical think what happens, what kind of world do we get if there's no cash whatsoever? And the only way you can buy food and the only way you can buy something to drink is with your bank-issued card. Right? That's fine. Right? I mean, we have nothing to worry about. We trust the banks. The banks are lovely people. The governments are lovely people. Until you become someone that maybe criticises the government or criticises banks, suddenly you can't get a debit card. So.

**Aral**: But you still need to eat to live. Right? But there's no cash. What happens? I mean, that's an extreme case, but that's not a case that I you know, that that couldn't happen. You know, you look at the kind of bank blockages and sanctions that are put on people who do speak out against… who are whistleblowers, et cetera. And you can see that sort of thing happening. So we do need alternatives in such a world that are not controlled by a handful of banks and not controlled just by governments.

**Aral**: So I think it's you know, we definitely need to be investing in alternatives that are digital. But at the same time, you know, how these currencies are distributed is very important. So when you create a currency is eighty five percent of it kept by a small group of people? And some of it distributed in the…

**Laura**: essentially a digital pyramid scheme.

**Aral**: Well, yeah. And we've also created new kings at that point. Right? We've created a new monarchy and they've just minted their own coin. And those people will say, well, that's what the central banks do. Exactly. I mean, money is is a fiction, right? It’s a fiction we agree on. A central bank literally creates money out of thin air the moment they say this money exists in their ledger. So it's not that different really to a digital ledger.

**Aral**: So we do have two people in the in the studio. I'm not sure if they've… if you want to use the private chat and let us know if you want to ask a question or if you want to join the conversation, just use the private chat.

**Aral**: You should see it in the interface. That's Duncan and NanoLover. Duncan is just lurking. OK, Duncan says I'm just lurking. OK, so that's really important. So with Nano, there is no proof of work mining. There is a small amount of proof of work that's performed when you're actually making a transaction and that's performed in your own wallet. And that's a completely different thing. So I'm… from what I've seen looking into it, it was distributed in a fair manner. The people around it I mean, there's a foundation around that. The people around it like I mean, here’s my criteria, right? I'm looking into this space. And if I see a community full of people with laser eyes in their profiles, then I'm going to be out of there, you know, in a New York minute. If I see always Trumpers, et cetera, in this community, that's not for us. Right? So looking into Nano community, I saw a bunch of developers building things, people who really value that, a currency that is meant to be used.

**Aral**: So, I mean, all this to say, I mean, as a caveat, all of this. Currencies, money, et cetera, are things that I find very difficult to get excited about at the best of times, right? They are a fact of life. I don't think that blockchain or cryptocurrency should be at the centre of any decentralised effort. So what we're building with the Small Web is a world where we want to see everyone own and control their own place on the web, right at their own address, their own place where they can be private, truly private, where they only hold the keys and they can be public.

**Aral**: Now, if there's going to be a currency that's used in this sort of a system, it would have to be similarly topologically decentralised. And that's something that I've seen in Nano. So in that everyone has their own directed acyclic graph. Right. So this is a sort of system that is very compatible with a concept like the Small Web where everyone has their own node on the on the Web. That could also be, say, a node that, you know, is your nano node as well. I'm not saying we're going to build this in version one at all. No. What doesn't make sense to me is like, oh, you're building a decentralised network or a decentralised social network, you're doing it on block chain, right? No. Why? Why would I do that? That's ridiculous. Why would we do that? Because the idea is not to create a world where we have a billion copies of the same database that we can prove that they're the same and that is decentralisation. No, that's actually a very centralised system. I want to see a world where everyone has their own databases. If there are billion people, there are different billion data, a different billion databases, and they can all talk to one another. But people own these databases and they're all unique and different and they might reflect different aspects of ourselves. Anyway. So long story short, that's that's kind of why Nano was interesting to me with all of those caveats.

**Aral**: So anyway, to get back to this and get back to the component, because we said we were going to keep this one short. This is not a scheduled Small is Beautiful. Just wants to say thank you to the community and release the component. So just to show you how it works and how you would use it if you're a developer. So. Well, I show you how it works. That's all there is to it, really. Laura, do you want to actually, before we go into how to build with it, do you want to just take us through? I can demonstrate.

**Aral**: Maybe you can talk about it. The various features we have, like we've got dark mode support…

**Laura**: I’ve got me a little list here…

**Aral**: OK…

**Laura**: Put my phone on the floor and…

**Aral**: and I'll drive.

**Laura**: Oh that's great. Because, as Aral very well knows, I can't use this keyboard to save my life because I have very small hands and his keyboard is like a piano.

**Aral**: It's a lovely keyboard. It's Real Force.

**Laura**: It's awful. And you have to listen to the noise of it as well.

**Aral**: And I love it.

**Laura**: So. So one of the reasons that Aral and I work quite well together is that we both have…

**Aral**: different keyboards [laughs]

**Laura**: different keyboards and we work in different rooms, but also that we have different skills and areas of experience and knowledge that are quite complementary and crossover in certain places as well. And one of the things that I really wanted when Aral mentioned the component that he's made, I was like, well, let's have a look at some of the accessibility of it to make sure that if we're sharing something, we want to make sure that we're sharing the most accessible thing that we can possibly share. And also, I had some ideas for some CSS kind of trickery that could actually just make it a little bit easier and make the styles a bit simpler for the component as well.

**Aral**: So let's let's show it off. I'm going to switch to the screen so that folks can see maybe should we start with dark mode?

**Laura**: Go for it.

**Aral**: OK, I'll start off with dark mode then. So it has dark mode support. That's the that's the gist of it really. So if we go over here to the preferences and I search for dark in Firefox, I can turn dark mode on and then boom, you see that we have the dark mode.

**Laura**: It's remarkable that there's not an easier way of doing it in the operating system. But I think that a lot of operating systems, for example, as people will previously know that I am a Mac user and it's disappointing to all. However, that is the dark mode support is often built in and you can just change it in your general settings in macOS that you just change what theme you want, I think they call it something like that.

**Aral**: And the next version of elementary OS, which is what I'm running, it has full dark mode support as well. So you wouldn't have to do this.

**Laura**: Yes, we don’t talk badly about elementary OS in this house. If there’s a great feature, they're about to have it because they're good.

**Aral**: Well, and of course the difference is elementary OS is a free and open source operating system and it's not controlled by a trillion dollar monopoly, which is always good. So moving on. [laughs]

**Laura**: Moving on. Do you want to show your screen now?

**Aral**: What are we showing, reduce motion?

**Laura**: No, I think it would be a good point just to show the color theme bit, because that's pertaining to the dark mode…

**Aral**: As in, like, showing how you change it in the code.

**Laura**: Mmhmm.

**Aral**: OK, well, in that case, let me actually go through actually how to set up a project and to use it so that we have that code.

**Laura**: OK, we’re having a little production meeting right here.

**Aral**: OK. Cool. All right. So I am going to show you how to actually use this component. It's very simple. I'll show you how to use it. It's still on Laura [(the camera focus)].

**Laura**: Sorry!

**Aral**: It just doesn't… I'm just going to be a blur in this in this one.

**Laura**: Stop leaning back!

**Aral**: So. You can use it in Svelte projects very easily, you can use it in non-Svelte projects as well, very easily. It is a Svelte component but Svelte components compile into pure JavaScript. So I'll show you an example of both of those things. Let's start with Svelte, since it's a Svelte component and since SvelteKit is such a big thing. So let's start with that. So first of all, let me just open up a new tab here. There's Firefox with its little branding go Firefox. They get half a billion dollars from Google every year, in case you didn't know that. It's good to know.

**Aral**: So I'm just going to start a SvelteKit app. So I'll make a folder and I'll say, let's say svelte-demo and I'll go into svelte-demo and npm init. I think it's svelte@next is how you install SvelteKit. Yeah, there's a setup wizard. I'll just use the defaults which just I'm just saying just use CSS so I'll follow the instructions that it's given me.

**Aral**: I'm just doing an npm install to install the template itself and then it says run dev, I've already got my browser open so I won't do the ----open thing. But if I say npm run dev and I go to localhost:3000 where it's running, this is just the Svelte Hello World. [In bad European accent] Hello World… Hello World example from SvelteKit. [More bad European accent] Svelte, ya.

**Laura**: No, don’t. No accents.

**Aral**: No accents? OK.

**Laura**: No.

**Aral**: Right, back to the screen.

**Aral**: So this is just a regular SvelteKit app. OK, and I'm now going to open up my code editor. Whoa. That is not how that should have opened up, but that's fine. Oh, great. There we go. It's… every other time I've done it, the code editor has remembered its position that I paced painstakingly had put it into, but not this time. But there we go. So let's make that… not you, not you, there we go. Let's make that perfect. Yay! Why am I not using a tiling window manager? I don't know. All right.

**Aral**: So if we look in /source and we look in /routes, you can see that there's a general layout, very, very simple. Just loads the app, CSS . This is all SvelteKit. Right? There's a slot that your index page is going to slot into. And this is that index page. So in SvelteKit, if I was say “hello Nano folks” and save, you can see that with hot module replacement that's there.

**Aral**: So let's delete all that and instead let's import our components. Right? So I'm going to import, nano-donate I'll call it, from @small-tech/svelte-nano-donate and that's the name of the component. Now did I install it? I didn't. So let's do that. npm install @small-tech/svelte-nano-donate… so just installing the component right now, just so I can use it.

**Aral**: OK, so now I can just use it and I say “nano donate” and let's just see what happens if I just add the components on here. So for one thing I have dark mode on, so let me just turn dark mode off so it looks a little better while we're looking at it. Another thing, I'll just make this a little smaller so the whole component shows. All right. So it says initialisation error, missing property address. So if you screw up, it tells you you’ve screwed up.

**Aral**: What are we missing? The nano address if my wallet. Right? So that's the only thing that you actually need. And I don't know why, but this SvelteKit template defaults to tabs instead of spaces. So that is really messing me up. So there we go. There we go. Spaces, I can breathe easily again because it's…

**Laura**: What a hill to die on…

**Aral**: It’s an important hill do die on, I believe. I don't know. Let's get back to it. So address=, and I'm just going to put in my nano address there.

**Aral**: And if I save now, it's operational. Right? It's loading the value from… it’s doing… that's taken a long time ago. It was just slow. OK, the coin gecko API can be slow at times, which is why we have a…

**Laura**: A loading spinner!

**Aral**: …progress indicator. Yeah, exactly.

**Aral**: So there it is. It's running. And you can see that these are the default colours, for example. This is the default amount, the default currency there because we haven't overridden any of them, but we're up and running. You could just put this like live, on your site now. Just deploy your SvelteKit site and you have a means to take donations. Which again, I think is really, really cool for not for profits, etc.. Right?

**Laura**: I mean, you just… it's that quick. It's that easy. And you don't have to worry about setting anything else up. Because if you set up a Stripe account and things like that…

**Aral**: Stripe is excellent. Don't get me wrong.

**Laura**: Yeah, but you have to go through the process to verify you and all of that. What if you just want to run a short campaign for something in particular? You're trying to raise funds for something, you don't necessarily want to jump through all of those hoops.

**Aral**: I mean, it's awesome. If you can just start like that, then if you think no permission required…

**Laura**: And if you think about a lot of countries around the world have issues with these kinds of providers as well, particularly because of, I don't know there’s levels of fraud, supposed levels of fraud, or levels of fraud in the country that means that they need extra verification and have to jump through even more hoops. It's just not particularly fair. And so the idea was that this could be easy for everyone, hopefully.

**Aral**: Right. So this is all you need to do to get started with it in a Svelte project. Now you can customize it more. You want to set the amount like we had it before. Let's say you want that to be 10, boom, it's 10. If you want the currency to be something different like USD, so let's just recreate the one that we had, and then you can also do a theme so you can set the theme colour here. And that is the British spelling, yes, I know. To be contrarian. And as the colour lets just pass, I don't know, a very garish blue. And then you have that theme colour and the border colour is calculated according to it. You probably have something to say there, Laura, about the contrasts.

**Laura**: So one of the things is really important in accessibility, probably the thing that you would notice the most as a casual user of the Web is the difference in the colour between the text and the background. And this is quite an important concept in accessibility, the idea of text contrast. Because if your text is a really light colour against a white background, it makes it much harder to read. And that's not just for people who have sort of old screens or using their device in bright sunlight, but it's also for people who have impairments related to their eyesight, have difficulty seeing.

**Laura**: And so if we try to make sure that we use high contrast text, reasonably high contrast text, that way we know that we're making Web pages that are accessible to the widest possible audience.

**Aral**: And if you actually go to the source code repository for the component, which is at github.com/small-tech/svelte-nano-donate, and you look at the readme, apart from instructions on how to use it, of course. If you scroll down you'll see under theming that there is a note there saying, please make sure you check the contrast ratio with a link to Lea Verou’s online contrast ratio tool, which is very, very cool. So we could actually hear it… let me make this a bit smaller so you can see it. So we could actually I could pop the blue that we put here. That we were just using, and we can see that that blue is actually perfectly fine. That's garish, but it's accessible.

**Laura**: Yeah, it might not be the best taste and it might actually be a little bit bright, like for eyesight. And I don't know about you, but my eyes hurt a little bit looking at that on white and it would probably be even more garish against black, but it's a good start, it’s a good start.

**Aral**: There we go. So… and you can change and you can actually specify other things like the border colour, etc. if you want to overwrite them. Otherwise, they're automatically create for you. And of course, the dark mode setting as well will use those as well. It doesn't look really great right now, dark mode on there, because in our app, the :root doesn't actually have anything on it. So but if I go in there and and actually say background-color: white; for :root, and this is just in your own app, of course. And then if I do a media query, so if I say prefers-dark-scheme or, sorry, prefers-color-scheme: dark…then on :root we'll just say background-color: black; to match that, and there we go. That looks a little bit better.

**Laura**: Very nice. And one of the things that is worth mentioning that Aral did was he made the colours for the dark mode, it flips, so it also it uses your colour, but it makes it nice, a light version of it…

**Aral**: So it’s still accessible. If you have an accessible colour, it should be accessible when the dark mode.

**Laura**: And you're doing that using the invert filter…

**Aral**: the invert filter in CSS, which is very, very cool.

**Laura**: Yeah.

**Aral**: Yeah. Although actually talking about Lea Verou… if you're into this stuff, what was her…?

**Laura**: You said it was on her homepage…

**Aral**: Yeah I did. Actually, let's just go back to the website for the component, because the link is there… to her website as well. So we search for “Lea Verou”. OK, that's her website. She actually has, her latest article, I just on there today when I was linking to her site, is Dark mode in 5 minutes with inverted lightness variables, which is actually a different way of doing what we did, but actually an even nicer way. This is very cool. So she's using these percentages of a primary :root coluor here to set different colours. And so you get something like this…

**Laura**: And that's using the HSL colour units, which means you have…

**Aral**: hue, saturation…

**Laura**: saturation and lightness, which means that you can tweak the lightness separately from the hue and the saturation, which is quite good if you're just trying to change a shade of a colour.

**Aral**: Right. And so what she does in this is, for the dark mode version of it. She's actually setting up these percentages as CSS variables, and then in dark mode, she's changing those percentages and flipping them around.

**Laura**: Yeah that’s very clever!

**Aral**: So that's really clever. And so take a look at that as well. We're just using a very, very basic… so if I was to show you the… actually let me show you the source code from the component that we just downloaded…

**Laura**: Using CSS variables again, which I would really recommend looking into if you haven't tried to use them yet, because they've got pretty wide support cross-browser nowadays, and if you're familiar with the world of SASS or Stylus, you'll be familiar with the concept of using variables in CSS. But this way you can do it just in vanilla CSS. And the way I see it, nowadays, vanilla CSS pretty much has most of the features that you would have previously, or certainly I would have used, in SASS or Stylus. And so it's really great to take advantage of that and not have to have all that abstraction.

**Aral**: And so this is how we're doing the component, using the invert filter with a hue-rotate and the hue-rotate is important to keep the same hue, so that it doesn't just invert and you get that kind of traditional inverted look where you're just like where they pick this horrible colour from? You're rotating at 180 degrees. So you keep the hue, which is very cool. And I'm just re-inverting the canvas afterwards. So the QR code is being rendered into a canvas. So I'm re-inverting the canvas because I don't want the QR code could be to be inverted. Some readers are fine with inverted QR codes, but not all of them. So that's that.

**Aral**: So this is how you would use it in a Svelte application like SvelteKit, which is like very hot right now. It's not even out. It's just beta. But what about any other app, like a just a general website you want to add this on to? Right? So let me show you that as well.

**Aral**: And to do that, I'll use Vite, just so we can actually have a framework for, sorry, a scaffolding kind of for importing ES modules, and using them. And using it for importing as well as the component using it. You can also do this by just using something like Rollup or even Browserify etc, but I'm just going to use Vite for it. So I'll say… let's go over here to my site… so I'll say vite… npm init vitejs/app, I think is how you start a ViteJS app if I remember correctly. Let's see… no, that is not how you do it. npm init vitejs/app… vite app? Was it vite app?

**Aral**: Let's go to ViteJS and find out, come on. They'll tell us, they'll tell us how to do it. So where’s that Getting Started button? There we go. Tell us how to do it… @vitejs/app! That's it, @vitejs/app. And we'll call it nano-donate, and we're going to create a vanilla CSSS HTML project, OK? So I'll go into nano-donate, npm install. And open up my code editor there. Of course, it's going to remember the wrong size again because why not? Why not? It's not like I ran through this earlier, made sure it was the right size, made sure it remembered it. But, hey, growing pains… it's… this is basically so that you all can see all of the components, it's a very high resolution, that's also part of the problem. And elementary OS doesn't really necessarily like it, that I'm at such a high magnification on my…

**Laura**: You don’t need perfection.

**Aral**: It's fine. So here we go. And let's also run that. So I'm going to npm urn dev…

**Laura**: It worked!

**Aral**: [laughs] It did… common enough mistake. There we go. So if I refresh here now.

**Aral**: Oh, it's on :3001, OK, that's our previous site. Hello Vite! Yay!

**Aral**: And how does that work? So there's an index file here, and you can see that they have a `<div>`. This is all HTML. They have a `<div>` called `id="app"` and they have a script that they're loading in called “main”. If we jump to that main script, you can see that there's just a query selector, and they're setting the innerHTML to the message that we're seeing there. OK, so let's delete that. In index.html, I'm just going to call this nano-donate… you can call it whatever you want. I'm just saying that's my component. Again, I'm going to install, npm install @small-tech,/svelte-nano-donate. So I'm going to install the component just like I did in the Svelte application, and then over here I'm just going to import it just like I did previously. So I'm going to say import nano-donate from @small-tech,/svelte-nano-donate. But this time, it's not a Svelte app, right? That's fine, because nano-donate is actually just a JavaScript class and so it's… [focus] still on Laura. Let’s see if you can get it there again. Hi. Hi! So nano-donate…

**Aral**: So nano-donate… again straight to you. It loves you. It's just… there we go. Maybe… is just a class, it's just a JavaScript class. All right? So let's go back here. And so all I need to do is… instantiate it. So I'm going to say new NanoDonate() and I'm going to give it a target. And as a target, I'm going to say document.getElementByID() and we called it nano-donate? We should have called it nano-donate. I used camel case. Let's use a dash instead. And if I do that, actually, and I save this, you can see, OK… let's get out of that dark theme colour as well, so it looks a bit better. OK… so as you can see, this is exactly where we were previously. Only, of course, the component doesn't specify any fonts. It'll use the fonts that your site uses. And so our style.css doesn't have that, have any fonts that are applying to this right now because the size or scope, I guess. So here I'm going to just pass my props, my properties, as an object this time. And again, it's just the address that's missing. Right? So if I just go back and I say address over here and I save, boom. We're back to where we were. Right? And you can also do again, you can do amount, and that was 10, right. And you can do currency, and that was euro. Oops, don't forget the comma and it's working exactly as it was. And then you can even pass the theme and then you can pass the colour. And we had… what was it? `#0000ff`, and we're basically back to where we were. But this is plain HTML, CSS and JavaScript, so you can just use that component that easily in any app, really. Is there anything we haven't mentioned yet?

**Laura**: Well, we've got a few things that we want to talk about from the accessibility side of things.

**Aral**: Prefers-reduced-motion?

**Laura**: Yeah.

**Aral**: Alright.

**Laura**: First things first, and you can demonstrate this, Aral, on the screen as…

**Aral**: OK.

**Laura**: …I'm saying it. One of the key things when it comes to accessibility and forms is can people use the form? And one of the things that we so often forgets about is the idea of being able to keyboard-navigate. So if you tab through the form elements, can you see what's actually active, what's actually got the focus at the time? Because that's very important, not just for people who just use keyboard navigation, but if you think about assistive technologies like screen readers, those are using… often used… with keyboard navigation, not exclusively, but very often.

**Laura**: And so you want to make sure that all of those form eleements are essentially “visible” in all possible senses of the word. So the keyboard navigation is incredibly important. Making sure that you have focus styles on that keyboard navigation. So, Aral, if you just switch back to the screen briefly. So it's quite difficult here to see because the colour that Aral has chosen is quite similar to Firefox's default colour focus styles…

**Aral**: I can change that…

**Laura**: But now if he tabs through, you can see that there's that kind of bluish outline. And then when it goes around the link, it's a kind of a dotted outline and it's just those little styles that make sure that you can see what's active. And I think that that's such a common thing, that people will apply a CSS reset that wipes out focus styles. So it's a really good tip to just make your site more accessible, go in and make sure, and that's using the pseudo selector, the little : focus. And you can apply your styles from there. What I would say is… by default, don't mess with the browser defaults. They’re usually fine, they could be better, they could be a bit higher contrast sometimes. But if you're not… if you don't know what you're doing or you're not really sure, leave them at default. Just don't disable them. And then if you want to make it something a bit more high contrast, a bit easier to see, then you can do that. For this component, we're not going to be setting focus styles because if they're different and inconsistent with the rest of the page that they’re on, or other elements of the page, that's not going to be easy to use. You want to make sure that you've got consistency across the whole site.

**Aral**: Right. Which also just reminds me, there is also some basic error handling that we put in as well. So if, for example, let's go to the preferences. No, actually, let's go to… was that closed? Let's go to the Developer view here, Network view. So let's say, for example, that the coin geko API call failed on your site. What would happen? So let's refresh. You get a network error. If coin gecko comes back and you try again, then it works. So we've you know, this stuff actually doesn't exist in our component, on our website yet, but it'll be interesting to apply some of these things that we've implemented for this component.

**Aral**: But that's that's what's so interesting, actually, because if you think about the amount of time that goes into making something like this… and again, like this is over the weekend, that the nano donation thing happened… Because somebody donated for the first time, and I tweeted about it. And then people were like, I think the Nano folks retweeted it or they wrote a tweet about us accepting it, and then it just kind of took off from there. So whoever did the first donation, thanks so much. You just kind of sparked the whole thing that gave us all those donations. But… and I started working on it, I think it was on the Monday, or the Sunday to Monday.

**Aral**: And the component was basically “ready”, as in doing the main thing that it does, kind of on Monday, it was like it was like, you know, just a couple of hours.

**Laura**: To have the basics, yeah.

**Aral**: So to pull it out and to make it into a Svelte component. But actually, if you look at the Git log of it, because this is really actually quite interesting… Where are? Oh that's not the… I need to actually go into the actual component itself, and show you… svelte-nano-donate. OK, if we look at the log of that, you can see that this tag, the 1.0 tag, was like three commits in. And I was like, yeah, yeah, this is ready. It's doing whatever it's doing.

**Laura**: Cocky.

And then in the next few days, these are the tags that we've had as we went through, and we added the input, we just layered on the accessibility issues, the accessibility supports, the error handling, this and that, checking on different browsers. And that's where all the other commits came in. So I think that's kind of important to keep in mind when you're building anything really. Right?

**Laura**: Yes. I mean, making sure that you pay attention to the priorities as well, before you release something.

**Aral**: Yeah.

**Laura**: It can be very easy thinking oh, yeah, got it done. But actually, accessibility is one of those things that always falls by the wayside, particularly for people who don't have accessibility needs themselves. Because it's not necessarily always at the forefront of your mind. And so it's pretty good if you work on a team to hold each other to account for those kinds of things.

**Aral**: Yeah, and I mean, if you look at the days as well, so 1.0 release was two days ago. Right? And then we've got like the work, two days ago, 30 hours ago, etc.. And then this rush over here [laughs] was when I realised that the QRious component that I'm including, which is a common.js component, works perfectly well with SvelteKit, which is, again, it's still beta, SvelteKit, it is still beta. So remember that. But when you… works perfectly well if you are using the component locally, linked locally, but the moment you get it from npm, like I installed it, it just baulks. That's something I'm sure they're going to fix. But I had to inline it with about two hours to go, which was fun.

**Laura**: So one of the other elements that we worked on was…

**Aral**: Reduce-motion?

**Laura**: Yes, I’m getting to it!

**Aral**: Sorry.

**Laura**: It was a loading spinner, which you might have seen in right at the very beginning of the stream when we first loaded the component in, was that when you're trying to pull the currencies from the API and the… it sometimes takes a bit of time, particularly if you're on a slower network. And so we wanted to make sure that people knew something was happening, that things weren’t broken, and so Aral started off with a nice little loading spinner component, that I think was a component, was that right?

**Laura**: And then we worked on ways to make that more accessible, because if you think about it, by default an image swirling around, that's not going to be much good for assistive technologies, for people who can't see that on the screen, they don't know what's going on, and so one of the things that we did was we added some text, just a bit of paragraph text that explained that… I think it's something like “Currencies loading…” And then the… sorry I wasn’t sure what you were doing… and so we had that text so that…

**Aral**: was getting the loading spinner…

**Laura**: was simple to people that couldn't see the contents of the screen, and we use CSS to ensure that happens. And then one of the other things that’s important, when you're putting in things like CSS animations, or any kind of animation onto a page, was the motion. Because motion is actually a potential trigger for people with epilepsy, people who have vestibular- disorder and people who are just otherwise motion sensitive. So, for example, when I was doing CSS dev work on this component and I… so I froze it were just the loading point so I could work on positioning and things like that, I was getting giddy just looking at that thing. So you don't want to be making someone potentially ill, triggering something in someone with your animation. You don't need to. And it's really great because we have a media query called prefers-reduced-motion. And that's another thing that you can set in your operating system settings or in the browser. And I think I'm not sure… for you, where do you set that setting?

**Aral**: You… Well, on Elementary OS, there isn't a setting for that right now. I tried to find it. You can install Elementary Tweaks and then set animations off. That still doesn't set it for the browser. So that's what I was showing earlier on Firefox. What you can do is, let me just show you the screen. You can create a property called ui.prefersReducedMotion, and if you set that to 1, then you will have set that on, it needs a number property. So not ideal, but something that I'll talk to the elementary folks about.

**Laura**: Right. And so if you're using macOS, you can it's in the accessibility settings for the operating system. You can take a box that says, or check a box if you’re American, that says “Reduce motion” and the browser will have that flag for websites. And so you can just use a media query to target the situation when people prefer reduced motion. And so what we did was we used that in order to… initially we thought we'd stop the animation and just have it just an image. But then we thought, what's the point? So we hide the animation. That's what we're doing now, isn’t it? Hiding…

**Aral**: I just showed that, while you were talking…

**Laura**: showing the text alternative that we initially did for screen readers, but also that's beneficial for this situation as well. Which is just showing that text alternative to things are always a good idea.

**Aral**: There we go. And that's the component. So we do have Duncan and NanoLover in the in the chat. I'm not sure if either of you… well, neither of you seem to have a video, so we can't really add you to the stream. But thank you for joining us. If there's anyone else who wants to join, oh, someone getting a video connection. No. Hey! We can add Duncan. Duncan, can we add you?

**Laura**: Does he want to be added?

**Aral**: Can we add you?

**Laura**: Do a thumbs up.

**Aral**: Do a thumbs up if we can add you to the stream.

**Laura**: Oh, he’s got dinner. Time to go. But it's nice to see your face!

**Aral**: Oh, OK. Well take care. At least we saw you in a thumbnail. Take care. Bon appetit. All right. So I think we're good. It doesn't seem to be.

**Laura**: I mean, it’s been quite a long mini livestream…

**Aral**: We are going to have are our regularly scheduled Small is Beautiful is next week. Actually…

**Laura**: Yup.

**Aral**: I didn't realise that, because the 1st was actually a Thursday. So maybe we'll look at… we'll see if we can get someone from like the Nano Foundation or the community to join us or what do you think?

**Laura**: Yeah, maybe we'll try and do something with that.

**Aral**: Yeah. Or maybe we'll get some some people from the… we'll have a cryptocurrency-based related-thing.

**Laura**: And then we can look more at the issues around the outside, not just the technical side.

**Aral**: Yeah. Not the technical side. But yeah. So anyway I hope this is useful. If you want to see a demo of it you can go to small-tech.org/demo/svelte-nano-donate. Oh Carl's joined. OK, let's see if Carl's got a webcam. Maybe we can add Carl to the stream if he does…

**Laura**: We’re wrapping up…

**Aral**: It's so small-tech.org/demo/svelte-nano-donat and yeah. So that I hope that the some if there are not-for-profits et cetera, who were looking to maybe add to whatever donation systems they have, very, very simple component. We've kind of, you know, it's an accessible component. And I think it's about as easy to add to your projects as can be, for developers. So, you know, I hope it helps some people out there. We just want to say a little thank you to the community for their donations over the weekend and. Yeah, well, yeah, I don't think Carl's got… no camera or mic, says Carl. Well, thank you for joining us still, Carl. And thank you to those of you who are watching right now. And also thank you to those of you watching on the recording. And yeah, we'll let you know about next week's proper scheduled Small is Beautiful.

**Laura**: As soon as we have something proper scheduled.

**Aral**: Yeah, yeah. I mean, once we have some guests. Yeah.

**Laura**: Yes.

**Aral**: All right. Awesome. So I've been Aral Balkan.

**Laura**: I have been Laura Kalbag.

**Aral**: And? This has been Small is Beautiful.

**Laura**: Yeah.

**Aral**: Yay. Take care, everyone.

{{< fund-us >}}