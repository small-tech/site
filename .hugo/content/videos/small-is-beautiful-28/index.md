---
title: "Small Is Beautiful #28"
date: 2023-03-16T17:00:00+00:00
speaker: "Aral Balkan and Laura Kalbag"
description: "What’s new in Kitten this month? Markdown support, new HTML validator, and HTML renderer improvements."
---

{{<video poster="./poster.jpg" vimeo="https://player.vimeo.com/progressive_redirect/playback/808800620/rendition/1080p/file.mp4?loc=external&signature=c1053239b4787e7e104cd0b4ef9abccb0cabb62f84a2d7ec6427534e33c13778#t=26" captions-en="./auto_generated_captions_edited.vtt" >}}

Want to watch it offline? [Download the video.](https://player.vimeo.com/progressive_redirect/download/808800620/rendition/1080p/small_is_beautiful_-_mar_16,_2023_%E2%80%93_kitten_and_domain_updates%20%281080p%29.mp4?loc=external&signature=3e1199d1d053fe2b6ab75f0e0c7d07fa989779ed012226d018f381e3284ccfc4)

_Want to skip past Aral working around the Node.js web site 404ing on the Node binary Kitten uses and get to the good part where we build a faux blog with comments using Kitten? __Jump to the 19 minute mark.___ 

## What’s new in Kitten this month?

___Markdown support, new HTML validator, and HTML renderer improvements.___

**Broadcast on March 16, 2023.**
  
Join Laura and Aral for a demonstration of what’s new in Kitten since last month’s stream.

This includes comprehensive Markdown support with syntax highlighting, table of contents generation, typographical niceties, etc., and the new HTML validator.

The HTML renderer has also been improved and inlined. It’s now more forgiving, safe by default, and now renders preformatted elements properly.

Finally, we have a couple of new authoring idioms: `safelyAddHtml()` for including untrusted HTML in your pages and class-prefixed CSS to scope styles to components.

_Streamed using our own [Owncast](https://owncast.online) instance. (Hint: you can install Owncast using [Site.js](https://sitejs.org).)_

If you like this livestream, please help support out work at Small Technology Foundation with a [patronage or donation](/fund-us), or share our videos with your friends!

### Transcript

[Auto-generated transcript from the captions.](./auto_generated_transcript_edited.txt)

{{< fund-us >}}
