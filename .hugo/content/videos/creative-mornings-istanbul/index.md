---
title: "Beyond surveillance capitalism: alternatives, stopgaps, Small Web, and Site.js."
date: 2020-07-01T12:30:00+01:00
speaker: "Aral Balkan"
description: "Aral on the Small Web and Site.js at Creative Mornings Istanbul. June 2020."
---

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/433950462" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## Details

This is the talk on moving beyond surveillance capitalism with alternatives, stopgaps, and the Small Web that Aral gave at [Creative Mornings Istanbul](https://creativemornings.com/talks/aral-balkan) on 26 June, 2020.

The recording of the live stream has been edited to:

  - Fix the 3-frame audio/video sync (live and learn: all HDMI output has a delay and if you are bringing in your microphone separately, you need to delay it while streaming/recording it).

  - Reduce audio levels (like a rookie, I didn’t check my levels before starting the live stream so the audio is clipped. Can’t fix that but I lowered the audio track so at least now it’s not booming in your ears.)

  - Cut out a part of my bumbling conclusion where I forgot the switcher on my screen instead of switching to my camera and then did a second awkward conclusion to camera when I realised.

We’ve also added closed captions (made using thisten.co), chapters (so you can skim/jump to relevant sections), and links to the tools mentioned in the talk.

All in all, not terrible for a first live stream with our new live stream setup.

Music credit: Kevin MacLeod, FreePD.com (Creative Commons 0).
