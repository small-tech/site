---
title: "Small is Beautiful livestream #3"
date: 2021-01-14T17:06:32Z
speaker: "Aral and Laura"
description: "Discussing Free and Open Education with Pine64 and elementary OS with brilliant guests."
---

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/498063999" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>

## Free and Open Education with Pine64 and elementary OS

This video was live-streamed on the 14th of January 2021 at 5pm Irish time.

We were joined by Nicolas Vivant, CIO of the city of Fontaine, who oversaw the deployment of 200+ elementary OS PCs and Debian servers in schools in Fontaine. And we discussed with our panel of guests, Lukasz Erecinski, Pernille Tranberg, and Cassidy James Blaede, how Pine64’s inexpensive and ethical Chromebook alternative, combined with elementary OS, could be the perfect educational device, and the strategies for getting devices like these into schools.

## Transcript

**Aral:** Five seconds to go, I'm putting Vimeo live.

OK, I just put us live so the stream should be hitting the players in the next few seconds, Laura is just checking, just in case,

**Laura:** Refreshing and refreshing.

**Aral:** How's it looking? It looks like things are good on my end.

**Laura:** I'm not getting it yet…

**Aral:** No? All right, let's just wait a few more seconds, make sure that the stream is going to the players.

You sure?

**Laura:** Aha. No, I've got it, I can see the "Live Now", button now yeah, I’ve got me.

**Aral:** Oh, no, no, I meant, are you checking on the website?

**Laura:** Yes, OK, yeah, I can see myself we’re all good.

**Aral:** Perfect! All right.

We are live. We've probably been live and people have been watching this for the last 15 seconds. But that's entirely all right. Laura, do you want to kick it off?

**Laura:** Yeah. So first of all, I got to make sure that I not playing myself, through my mobile phone…

So hello, I am Laura Kalbag and together, Aral and I are Small Technology Foundation. We’re a teeny tiny, not-for-profit organisation, based here in Ireland, and much like the previous weeks, Aral is upstairs,

I am downstairs, and the dog is over there.

**Aral:** Yep. And today. Well, I mean, welcome first of all to this show, that, or stream, actually, that we are calling Small is Beautiful.

This is the third time that we are holding the stream. It's a weekly stream and we are very lucky today to have a host of guests, more guests than we've ever had before.

So perhaps a quick introduction before we do anything else, starting with Nicolas Vivant. Nicolas is the Chief Information Officer for the city of Fontaine, and he's joining us from Fontaine in France. Hi, Nicolas.

**Nicolas:** Hello.

**Aral:** It's lovely to have you here. And then I'll just go clockwise. We have Lukasz Erecinski. Did I say that correctly? I've been practicing.

**Lukasz:** Yes. Absolutely perfect.

**Aral:** And Lukasz is from Pine 64. Pine 64 is a an organisation that makes free and open source hardware, like these notebook computers and phones. So welcome, Lukasz.

**Lukasz:** Thank you so much.

**Aral:** And then continuing on, we have Pernille Tranberg and Pernille, we have known each other for several years now, and she is with Data Ethics EU. And I believe Pernille, you also have your own consultancy.

**Pernille:** Yeah, more like teaching and talking and not really consultancy, but…

**Aral:** OK, all right.

**Pernille:** My main work is to try and promote ethical use of data and tools also. So.

**Aral:** And you even have a book on it, right?

**Pernille:** Yes.

**Aral:** Do you want to this is your this is your cue to tell people what your book is…

**Pernille:** Well, it's four years old now and it's called Data Ethics, A New Competitive Advantage.

And I'm working on a new one now, which is part of data ethics. It's called Data Democracy, Individuals Control Their Own Data. So I'm trying to explain how can we actually build this data democracy.

**Aral:** Nice!

**Pernille:** Explaining to the ordinary people.

**Aral:** Awesome. Well, we definitely need to do that, and last but not least, we have Cassidy James Blaede. Yep, I got the last name right. OK, and Cassidy is co-founder of Elementary OS and he leads Experience Design there.

Cassidy, hello. Welcome.

**Cassidy:** Hello. Thanks for having me.

**Aral:** And thank you all for being here.

The reason we have this panel of guests is, I think a week ago or a week and a half ago, someone on Twitter mentioned that Nicolas had overseen the deployment of about two hundred Elementary OS devices and Debian servers in the city of Fontaine. So free and open source for schools. And this caught my attention because, you know, I think it's very important, especially today with covid-19 and with everything being virtual.

There is a definite speed-up in in virtual learning as well, and turning to virtual systems and getting kids laptops and getting kids tablets, et cetera. And it's very important, you know, what the characteristics of these devices that we're putting into schools and normalising are. So I thought, you know, let's get this panel together. Maybe initially, I don't know, Laura, would that be a good way to start this? Maybe initially we'll start by having Nicolas just give us some background?

**Laura:** Yeah, I think that's a fantastic idea. I think, Nicolas, it would be really great to know how you came to introduce these computers into the schools and what got you to that point. What led you to do that?

**Nicolas:** OK. Good evening, everyone. I'm I'm pretty proud that it was Daniel Foré, the other founder of Elementary OS, who mentioned Fontaine. I didn't even know that he knew that we existed. So I was pretty proud of that.

And I'd like to thank Cassidy for the excellent job he's done in designing Elementary OS , which is one of the reasons why we we choose this very distribution and none other, because it's been a political choice within within Fontaine to install, deploy and promote free software all over the city. So we we started deploying Linux within the city network, within the town hall network.

So the first to use it were the elected officials, the mayor, his cabinet and the general managers and so on, and the whole employees within the city network prior to go to schools and propose them to move to Linux. So when we started to implement Linux in the in the school, we already had about 100 desktop installed in our internal network.

The truth, we decided to put in place a strategy to make sure that what we were doing would be easily accepted by the people working there. I didn't want that, because of a change in the majority or a change in the CIO, some people would decide to go back to Microsoft, for example, because people would be angry about the way the things we're working, or not working. So I've been looking for Linux distribution that was easy to use, that would integrate perfectly within our information system and that would be beautiful.

It was very important for me so that people, you know, adopt the… I mean, it had to be more beautiful than what Microsoft was proposing at the time, which was Windows 7, I guess, and it was actually. So thank you again, Cassidy, for the excellent, excellent job. It's been a great part of the success.

Then in the schools, what we decided to propose, it was both a technical and educational choice, technical because we needed something that was beautiful. Of course, that was simple also.

But we wanted to… the objective is was that kids are used to Microsoft all day long within schools and that the reason why, when they are at home, they work with Microsoft because for them, the PC is Microsoft and Internet is Google. You know that the way they are trained when they are when they are young and the objective was to change that.

So they are existing distributions specially dedicated to kids with beautiful colours, few icons, something so simple, but that's so different to what they would use when they are back at home and we didn't want that.

We want them to have a full distribution, with all the tools that they would use at home. We were hoping that some of them would make the same, the same choice when back when back at home. And that's what occurred in some cases.

So it was it was great. So really Elementary OS was the OS of choice because it was simple, beautiful and and all the tools we needed were there.

**Aral:** Wow. Well, Nicolas, I mean, that's that's so inspiring. Cassidy, how do you feel hearing that?

**Cassidy:** That's awesome. That's I mean, that's exactly why we do what we do at Elementary. You hit on, like, all of the points. It's like, you know, simple and beautiful and easy to use. But actually full featured, like, that's that's really important to what we're designing is like, you know. Yeah, you could you can make something that's so, so simple and stripped down that, you know, it's very on rails and not a real operating system.

And honestly, in some cases, that's what people are used to with like mobile operating systems. In some cases, they're so limited in what they allow you to do and what they, how they allow you to use your computer. And so where does that we're trying to design something that's, you know, easy to use and simple, like those operating systems that people are coming from, but actually has all of the power of full desktop Linux under the hood. So you can always install whatever you want or do whatever you need to do on it. So that's really cool to hear that kind of information.

**Nicolas:** And, you know, something that's very satisfying is that at work, some people even didn’t notice that they were using Linux. And that's awesome. Yeah, you know, we are working remotely these days. And and when we take remote control on the PC to help people, before doing that, we ask them, are you using Windows or Linux? Because we still have some Windows PCs within the network. And so we always ask the question and most people, they don't know, they don’t know. So, yeah, you have to ask about the start button. You know, what was the button that you have? You have these applications when you go on the left side or do you have this web, the window at the bottom? That’s the way they know…

**Cassidy:** That's so cool because we you know, we get we get some flack within the Linux world for not being vocal about being Linux. You know, there's people who have been using desktop Linux for decades or whatever. Sometimes you're like, well, you really need to talk about the fact that it's Gnu/Linux and they explain, you know, why that's important. And we agree that being Linux-based is important because it's open source, it's auditable, but the end user doesn't care that it's "Linux". And unfortunately, "Linux" has had a stigma to a lot of end users. They're like, oh, I've heard that hackers use that. Or I you know, I have to know how to use a terminal.

And it's like, yeah, you can use a terminal in Linux and it's beautiful and it's wonderful, but you don't have to you don't have to know how to open it. So that's cool to hear that, you know, people in the real world are using Linux without knowing it. And and that's OK, I think.

**Nicolas:** Yeah, and…

**Aral:** And Pernille? Pernille had a question.

**Pernille:** Yeah, I had a question. Yeah. Two questions. One for you, Nicolas. What what are you using? Instead of Google Meet or Microsoft teams? Are you using Jitsi Me, or whatever you are using? And I want to ask you, Cassidy, are you go are you doing this for a lot of other schools or do you have kind of a commercial packet? So I can promote this in Denmark, for example? Or in Northern Europe?

**Cassidy:** Sure.

**Nicolas:** I'm sorry, go on Cassidy.

**Cassidy:** Go ahead. She asked… your question was first. [Laughter]

**Aral:** Go on, Nicolas.

**Nicolas:** The choice we made for video is Big Blue Button. So we installed our own instance of Big Blue Button internally. And then the tool we use, Jitsi is great. It's a great solution. But when you want to do a meeting with like 30 or 40 people, things can be can be a bit difficult depending on the available bandwidth.

You know, if people have poor bandwidth at home, it's going to impact the whole performance. I mean, they are going to have a bad experience during the whole meeting. So Big Blue Button works with I mean, it's centralised, the video stream, and then they send it back as one video stream to for each participant and and the test we made were much better. So. Well, what we do is we recommend Jitsi when you have a meeting for less than than five people and we use public service in this case, but for normal meetings, usually meetings we use our internal Big Blue Button.

**Aral:** Right, cool.

**Cassidy:** And as far as there's not really a commercial package or anything for Elementary OS , we have actually, I think spawned… based on these discussions that we've been having on the Internet recently, we've started to kind of document some efforts about promoting Elementary OS for use in education and the public sector, because there is a there is a real good like pairing there of, you know, using open source, making sure your data is protected, make sure it's privacy-respecting and being, you know, serving, serving the public good. So I think we don't have anything really right now.

But that's something we're aware of. And we're working on. In general are like licensing scheme, is it's completely free to use. It's completely open source. When you download the operating system, we ask for a pay-what-you-want value and you can type in zero. And that's fine if you're, you know, trialing it or whatever. We've also talked about, you know, ways we can design that flow a little bit better for encouraging specific amounts, for specific use cases.

But we also think it's important to not not put up any artificial barriers for either organizations or individuals who can't afford, necessarily, more expensive software. So it's kind of a balance that we're we're working on.

**Aral:** Which kind of brings me very naturally to Lukasz, because I think part of the conversation here has to be like, you know, of course, Microsoft, as Nicolas said, is a huge player in this area.

A lot of schools are just all Microsoft houses, but so is Google and Google is pushing ChromeBooks and whatever else devices they have. And of course, these are all surveillance devices. Right? And even if they don't surveil the kids, the if they say, OK, we're not going to, because initially they were, you know, and then the people were like, don't do this. This is really bad. And they're like, oh, OK, if it's really bad, we won't do it. But these, the real danger I see here is that we normalise surveillance capitalism for a whole new generation that, you know, a company that makes its billions by tracking everyone, profiling them and then manipulating them, using that intimate insight is just normal, is OK, is fine. Here you go at school.

So when I saw I think I saw something about this recently, someone was talking about ChromeBooks in schools, which might be what triggered my thread.

I well know we have Pinebooks, we have these devices that are like two hundred dollars or something, I believe. And now this one runs Elementary OS. This is an amazing little device. It is actually cheaper than most ChromeBooks, and the cool thing about this is, I mean like nobody buys an operating. I mean your operating system, Cassidy, of course, but you don't go to

**Cassidy:** No, I agree yeah…

**Aral:** You buy a computer because you want a thing that works, right? I mean, everyday people don't buy a phone and then change the operating system. That's like buying a car and changing the engine. Nobody does that apart from people who are enthusiasts, enthusiasts love it. You know, if you buy a car, you can't change the engine. The enthusiast is like, I'm not go… I hate this car. It's terrible. It's locked down. But at the same time, everyday people don't buy a car and change the engine. In the same way, I think this is why I want to get you guys talking to each other and so much more closely.

You know, and I think you probably completely understand this, Cassidy, coming from a design background is this thing [the device] is what people get right. And either and, if they can't do something on this, this computer sucks, right? It's not Elementary OS. It's not this or that. It’s this thing sucks. Right? Because, you know, your bike just doesn't work as it should. You're there's something wrong with it. So I guess this is an open question. Cassidy, Lukasz, how do we make it so that every school, you know, has one of these things in them? That that schools don't get flooded by these surveillance devices, these ChromeBooks? You know, why isn't it that every school has one of these? That's my question to you, really? Why, when these are cheaper than Chromebooks, when these are ethical, when they're free and open source, why doesn't every school have them? How do we change that?

**Lukasz:** So I suppose that the answer to that is, part of it is a lengthy discussion which we have had in the Linux world for a long time about, generally speaking, why is there such a low adoption of of desktop Linux in the first place? Just, you know, universally? Yeah? Another part of it is that when you buy a Chromebook, it comes equipped with all the things that especially in the educational setting, which are required for cooperative work. And so it has Google Docs, I don't know what their what their other applications are called.

And it comes with this entire suite of software, which is both easy to use, it is in terms of just end user experience, I have used it somewhat obviously myself in the past. And it is it's very functional. It's good. And my understanding is that schools get special deals and all sorts of enablement related to education. And so you're buying a package. So it's not a question of you just buying a computer or a set of computers running Linux. You're buying an entire package, which just works the moment it arrives at at the school or any other educational institution.

And this is something that is very difficult to replicate, especially in the open source setting, and by open source actors. Because I don't know how many of you have heard about this, but we, back when the world was still normal in September of 2019, we said that we will be collecting money from soft cases for our smartphone. So every case like this, which you buy from a smartphone for the Pinephone, we subsidize this and we put it towards building Pinebooks. Which we will be donating to, our initial intention was to donate it to underprivileged groups, and especially children in parts of the world who need to either make a trip to a location where they need to do their homework online and just the fact that they need to make a trip, that’s very frequently, I'm told, you know, these are the things which we don't think about in the northern hemisphere, but in other parts of the world, making that trip is dangerous for them. Yeah, just making that trip. At home, working at home. There may be just one computer. They may not have access.

There's a lot of things to consider. And, you know, we were thinking about setting up some, you know, committee because obviously I am no authority on who should be getting these Pinebooks. And I'm no authority on what operating system should be shipping on these.

And, you know, this has obviously all been postponed because of what happened in 2020. And we had other things which we had to deal with more immediately, this past year. But we've collected all of this money. We're not putting it to any other use. And when we find, be it an established charity that already dabbles in this, or some project that could actively take these laptops, put in appropriate operating system on them, and then present it in a way that would be interesting to a school or another educational setting, then we're very much happy and open to any cooperation.

And we, you know, we will build these Pinebooks for you and we will give them to you to to be implemented.

So, yeah, we've been thinking about this for a long time. I mean, part of what we developed in 2019 was this vision that, you know, if we, if there's very few of our products that make money, but the money which we make, we want to contribute back to something. Usually it is Linux development and developers who we work with. But there's also a distinct aspect of what we do, where we want to contribute more broadly to the society, to others and those who are in need.

And and if we promote Linux while we do so, the the better.

**Aral:** Sounds like there's a huge opportunity there. I mean, you know, because for exactly what you said, Lukasz, which is, you know, you just went further than when I was saying, but you're entirely right.

The whole experience, if we're talking about the experience, then the experience is hardware, software and services. Right? And like you said, Google provides all of these. Apple provides all of these. You don't just buy a phone. You buy a phone that can sync to all of your other devices, that you can talk to your computer. You're not just buying these disjointed things or you just you're not getting these disjointed things.

So if we're going to compete and I think you also said, though, it's interesting, you said, you know, the desktop Linux doesn't doesn't have the sort of adoption that we want.

Why? Well, it's clear it's because of just what we just said. Right? We are competing with a full experience. We're competing, not with this part of the experience, or that part of the experience. We compete really well with those things. Oh, the hardware is really fast. Your hardware is great. Oh, you're operating system does the gesture thing really well. That's awesome. That's amazing. Right. But what happens when you put all of that together? And I think that's where we have the biggest challenge.

This is the thing that infuriates me the most about the free and open source world, which is the part a world that I contribute to that I've been a part of for years and years. You can love something and be infuriated by it at the same time. What infuriates me is the buck stops… is we have this kind of an issue with "the buck stops here". We have an issue with you know, it's an upstream fault. It's not our fault. It's their fault. It's that person's fault. Well, you know what? Again, it comes down to whether or not the thing that a person has allows them to do what they're trying to do, anything that stops that is the fault of whoever has made that thing.

We need to start taking on these responsibilities and understanding that it's the whole experience that we're responsible for. And if we don't do that, we can't compete. There's no way we can compete with things that just work, that are everyday things that just work. Sorry, Pernille. You want to say something?

**Pernille:** I think the very the very first step is to awareness, because in Denmark now Microsoft sits on half of the education market and Google now has grabbed the other half. And I'm deeply worried about it because we will end up like in the communication world. We have to fix social networks who can actually turn off Trump, or anybody that they wanted to turn off . That's what we risk with the education as well, if we are stuck to two big companies. And so I think the first step will be to create awareness.

And that's what I want to do with this talk, and a lot of other stories I'm collecting, because I know in Spain they're also doing some of what you are doing, Nicolas, in Fontaine. You probably know that, Lukasz, as well. So we have to put together knowledge there are alternatives. Because a lot of municipalities who are buying Google and Microsoft, which is obviously much more expensive than Google, they don't know that there are alternatives.

So that's the first step we have to put this together until tell them this is a possibility. So I'm trying to put together a workshop in Denmark with all the major players, and then I need actually someone to present what is this perfect, easy, beautiful, easy-to-use solution that you have made. So we had to actually show it to them because I am also worried that Linux, oh it’s so difficult I can't use it. I'm a Mac user, you know, it's so easy. How will use Linux, how does it look? And that's what a lot of people think. It's for nerds.

**Aral:** I think Nicolas wants to pipe in?

Please feel free, by the way, you don't have to raise your hands, just pipe in.

**Nicolas:** Just so that we don't speak at the same time. I just didn't want to interrupt Pernille, and thank you for what you what you said. It's very important, Aral, what you said. And we CIO and IT managers have a responsibility in all of this. I mean, this integration, this sync between the different pieces of your information system that you mentioned, it's our responsibility to make it when it doesn't exist.

It is possible to do it. I mean, before going to Linux, we made sure that the user experience would be exactly the same as before or even better. So we made sure that, for example, the authentication system that was in place, based on open LDAP, that was in place in the online network would perfectly work. That the network share would be automatically connected when you boot, when you put up your desktop.

So we made all this world of integration. That's very important. I mean, if you want people to adopt something new, they have they have to see the added value. It cannot be something that can be a pain. You know, you have to do the effort. That's where we have a responsibility. The other thing is you have to think. First, that people don't want to change, they don't like change, you know. So you have to think of a strategy to deploy and that strategy is something that must be really… Well, you have to be smart when when doing it. What we've done in Fontaine is first, you know, in the different schools, you have different teaching teams, let's say, with different experience. Some are, some feel comfortable with computing stuff, some don't. So we first asked for volunteers who would like to try Linux in the school and the first schools where we implemented Elementary OS, where the school where people volunteered.

So the resistance to change was low because they volunteered that the first thing. Then we first provided one laptop with one notebook and a pen, and we left this laptop in the in the school for one month so that people can just try and take notes and make remarks and say this is working, this I don't know how to do. So they had a few months to tell us about their experience. That was the second step. The third step was training. We had we we installed Elementary OS on the on the PCs and then we had the first training session just afterwards, and another one one month after when they had met the first problems, the first questions, that was very important as well. And then we we did I mean. Great support. They have to feel that the IT service is there to help, in any case. And that's very, very important, because one thing, for example, that we didn't anticipate is that most teachers had bought educational software on DVD that was made for Windows. They weren't compatible. They couldn't run on Linux by default. So we worked with them and we found a way, using Wine, to to have those very important software working for them. And we did the full work. I mean, we installed when we installed the software, we installed the desktop file, you know, and and put it in the in the right folder so that it appears in the menu and then added it to to the dock so that they just have to click. And that is very important. That that's key if you want people to accept the change.

And then, those schools those schools that volunteered were our best supporters. I mean they told the others. Yes, it works. It works, no problem. It's even better the desktop boots faster. I mean, we have less… it's more stable and it's not more difficult. And they were our best, you know, vendors, I mean, they they really took part in the effort to go further in the deployment afterwards. So really think about this integration so that people don't suffer and think about the strategy to migrate so that all… well, so that you make sure that all issues, all problems, are properly managed before going to two production. That’s very important.

**Pernille:** Very well done, Nicholas. It's very, very cool what you did. So can you actually take, for example, do you know this software or this service called Kahoot!, which a lot of teachers are using? Can you take all kinds of services and adapt, adapt them to Linux and then put them into their computers so they can use it in the same way? Or do you then have to pay Kahoot! for that because, you know, most teachers are downloading so many different tools to use.

**Nicolas:** Well, there's one thing that is at least changed. It is that today most software, including educational software, are based on the web anyway. So as soon as you have Firefox running, your ability to use, you can use most of the the software that are needed. So I think 10 years ago, I think it would have been very difficult, you know, with CDs and so on. Today, most of those solutions are based on the web anyway. So.

**Pernille:** Are you using Firefox as a browser? I mean, yes, that's one of the the little issues we had with Elementary OS. I mean, the default Elementary OS set up.

**Nicolas:** With the same objective of having a strategy to deploy within the whole network. What we did first is we still use windows, but we first migrated all office and messaging and all of the software on the desktop. So we first moved from Internet Explorer to Firefox, from Outlook to Thunderbird and so on. So we first changed all the environment except the operating system so that when we move to Linux, the only thing that changed was the design and the Start button. I mean, but all the other software, Firefox, Thunderbird and so on, were there. But when Elementary OS is downloaded, you have a browser which is not known by the people, the same for the messaging client. So we had to, we have to uninstall all this, and install Firefox and build so that the change is not too big for our users.

**Pernille:** And what messaging tool are you using?

**Nicolas:** Thunderbird. Mozilla Thunderbird. That’s the client, and for the messaging we use something called the Cyrus. It's from I think it's from the MIT or something. And the…

**Pernille:** What’s it called? Say it again.

**Nicolas:** Cyrus, I think it's C.Y.R.U.S. And it's an IMAP SMTP messaging server. And as a webmail and a shared agenda user solution that's not very well known, and that's a really great, it’s Canadian. It's called SOGo. Do you know SOGo? It's great. It's great. Just like Zimbra and BlueMind and all of these, these solutions.

**Nicolas:** But it's truly open source, you know, including smartphone sync and so on. So SOGo is a great solution. If you if you're looking for a great messaging solution.

**Pernille:** How do you spell that? With a C?

**Nicolas:** S.O.G.O. SOGo. It's Canadian.

**Aral:** I think it's a good point to also point out that if you're watching right now and you want to join us, you have a webcam, you have headphones and you want to ask a question or join the chat, we have room in the studio for four more people. So I'm going to just give the URL out publicly. If you go to that URL, you'll be able to join us in the studio. If you have a question for one of our panelists, again, have your webcam ready and have your headphones on so we don't get feedback.

And there's a private chat. If you go into the private chat and tell us what you want to talk about, then we can try and get you into the stream. So I'm just going to keep that URL up there for a little bit.

**Laura:** And if you have the equipment available and or if you're camera shy, you can always just write your question in there and we'll pick that up as well.

**Aral:** Yeah, but, you know, I think what I keep thinking about with the conversation that we're having here, I mean, on the one hand, of course, listening to Nicolas, it's clear that this there is a kind of an almost enterprise-y side to this for schools where, you know, you there are things you want to customise with any solution you have, et cetera, which, of course, free and open source gives you the ability to do.

At the same time, we have this other side of it where we just need to create these things that just work, you know, and then that includes hardware, software and services. So I keep thinking, you know, like Cassidy, Lukasz, maybe we get someone from, you know, something like NextCloud or one of these services that we talk to about how can we get the three of you hardware, software, services together to build. I don't know. You know, we need to get away from. We're also hearing about how we need to get away from the baggage that Linux has, the perceptions that it has. So if this [Pinebook] is a thing then why isn't this a Pine Elementary Book? A Pine Element? You know, a certain product, a single thing that is, let's say for the educational area. I hate the word market, but for education, but which is, as well as we can do it initially, you know, the marriage of hardware, the operating system, software and services, all free and open source.

But it's its own thing. You know, let's let's combine Elementary, let's combine Pine, let's combine a third party and let's build this thing that doesn't exist before, which isn't Linux. Right. Of course it runs Linux . Who cares, right. Does anyone know that they're running Darwin? Who's on Mac? Do you know you're running Darwin? No, you don't care. That's what you're running. Right? That's that's the same as like Linux. You're not. So why not a Pine Elements book, an Element Pinebook? I don't care what you call it, but a thing.

And then you can go and say, look, instead of a Chromebook, here's what we have. It's a whole solution. But if they came to you, Nicolas, with that, what would you think? You know, we would come to you and we go… and it's got the services built in and it's got it's got the offering is there. It's two hundred dollars.

What would you think? What do you think people would think?

**Nicolas:** This question was for me, Or?

**Aral:** Yeah, yeah, I came to you and I said it's ready, we’ve built it. [Laughter]

**Nicolas:** One of the difficult things in the public sector, public area in in France is that we cannot buy whatever we want. The, I mean, for legal purposes, we the French law is very rude. And it's it's not easy to buy, to buy I mean, specific computers. We use this site that's dedicated to the the public area to buy our the hardware. So what we get is, is most of the time is, is desktops running Windows and we uninstall Windows to install Elementary OS. So we have, of course it's based on the images we use for a project to deploy quickly the image and on the desktop.

But I mean buying, for example, Pinebooks would not be easy for us. You have to, if you want to sell something to the public area, you have to get into quite a heavy process, you know, to do so.

**Aral:** It is like enterprise? So is Google doing that? Do you know? I mean, does Google actually send people out and kind of go into that whole kind of process?

**Nicolas:** Yes of course.

**Aral:** Or is Google so big that it changes the dynamic ?

**Nicolas:** Yes.  Yeah. And, you know, they go directly to the, I mean, the educational department, you know, the government and they make deals with them. And so of course their products are available for us to buy , which we don't want of course.

But they are there. For a little companies or for, yeah, it's far more difficult. I mean you you would have to do that for every country, you know, it's just impossible.

**Lukasz:** Yeah. I was about to say that, you know, just just if we have so we have a piece of hardware, a computer, we have an operating system. And even if we have, you know, the cloud services, if we have some NextCloud host who pre-installs, you know, everything is set up on the on the computer, on the operating system as it arrives, we still face the problem that whose responsibility is it to contact these educational institutions?

But presumably it's not, say, "mine" or Pine 64’s, we just delivered the hardware to whoever does this. Google has an entire infrastructure and I'm no expert on this. I'm not going to pretend like I know something that I don't. But I do know that they lobby both high and low. And by low. I mean, at individual school levels. They come to individual schools and they say, hey, look how simple this is. And it just like you just put in your password here, you connect to your Wi-Fi. And look, you can access your your drive. To duplicate that, it's not only a question of responsibility of the individual. We have four players then, somebody who delivers the hardware, somebody who does the operating system, somebody who does the cloud hosting. And then you have a fourth player who has to promote this somehow on, different countries have different structures. In Germany, I suppose Munich is one of those, at least temporary success stories, which some of you may know better than I how that worked out. No you don't?

**Pernille:** Please explain. Please explain. Explain the Munich example.

**Lukasz:** So Munich was one of those places which, famously, on the on the länder level, so that's within the administration there, who adopted Linux for a period of time, and changed to Linux at top to bottom. I can't remember what they ran. I mean, Pernille, I know because I know a little bit about Denmark as I used to live there, but there are quite a few universities, Aarhus being one of them. Also Copenhagen Business School.

These are two universities which have dabbled extensively with Linux, and Linux devices. And I don't know what happened between 2008 and 2020 when I checked in last. But they're not running Linux anymore. And I suspect the reason why is because of this integration, because somebody from Google or whatever, the company came in and said, look how simple this is. And if you have four players from an open… from from the sort of open source community, we would have to coordinate extensively.

There's probably also all sorts of money-related things to be sorted out. And, you know, it becomes complex. So…

**Aral:** Pernille, maybe this is an area you can help in. Maybe this is an area that Data Ethics EU can help in. You know, this is the kind of area where maybe the kind of civil society, the civic society institutions that we have in Brussels, and other places can actually start helping out because it's clear if anyone from there is listening, like from that section of… is listening to, maybe understand that there's a need for that, you know, because a lot of times there's, again, a huge disconnect there.

They're trying to create their own systems. They're trying to create their own things. The systems exist. The solutions exist. How do we get, you know, promoting get to promoting them?

**Pernille:** So what I think we…

**Laura:** I think we have a relevant…

**Pernille:** OK.

**Aral:** Oh, sorry, Laura. Right after Pernille, let's take some questions, but right after Pernille.

**Pernille:** I just want to say, just like you did in France, Nicolas, where you had the politicians use Linux to begin with, we could try and actually convince the EU Commission, because in all the new parliament, the new laws they are making, the Data Governance Act and Digital Service Act, there is a kind of new line where they are saying, well, we shouldn't store our data with the US, we should be more protective in the EU, and use more tools which is based in the EU and stuff like that.

So we if we could build up Linux with some of the EU tools, I think we could we could try with some of the politicians first.

**Aral:** Cool. Well Laura, I know you've been in the chat room.

**Laura:** I have… this is a relevant question to this. I'm just going to add Tanguy to the feed and so that he could ask his question.

Fabulous. Go ahead.

**Tanguy:** Can you hear me?

**Laura:** We can, yeah.

**Aral:** We can hear you. Hi, welcome. Welcome to the stream.

**Tanguy:** So, yeah, thanks a lot for the nice session. So since I'm French, I actually wanted to ask Nicolas whether he had been able to get in touch with people in the French national education system to see whether it would be possible to replicate this experiment somewhere else? I mean, I know that in France there are a bunch of other towns where free software is also promoted at least at the city hall level. So, I mean, I don't know whether you explored this in any way?

**Nicolas:** No, I didn't have any contact… In France we have, uh, how how would that translate that into English? That we have contacts with education people and there's now a digital reference. A digital… I mean, people taking care of the way digital is handled within the French schools. OK, we all have this kind of local people who are responsible for this digital thing. And these people have have seen what what we are doing. And and they say it's great.

And, uh, and I'm thinking about when the woman who's from the national education and she decided herself to move to Linux too . So that's good news. But it's very local. And I we are just a city with well, twenty three thousand people. So it's a little little town near Grenoble. We don't have those kinds of contact.

But what we do is, we are ready to help. We have helped some of the cities to move to other Linux or Libre Office, or other software. We communicate a little bit on the social networks and the social media about what we are doing, but we can't do much. I mean, we are just a little city.

**Aral:** Do you have any contacts with Framasoft? Sorry to cut you off, Pernille… Do you have any contact with Framasoft in France?

**Nicolas:** Yeah, mostly online, maybe. We've met once or twice in some events and I'm sure using some of their software, like Framadate, for example, you know, or Framacalc, which is Ethercalc. But yeah, it's a great organization. And there are quite a lot of software that we use thanks to their effort to promote them.

**Aral:** Very cool. Laura, do we have any other questions? I think we do.

**Laura:** Yeah, we do. Laurence, would you be alright to be added to the feed?

Laurence? Yes, we good?

**Laurence:** Yes.

**Aral:** And here we go…

**Laurence:** Shall I speak it?

**Aral:** Yep, go for it. You’re live.

**Laurence:** Cool, yeah. I apologise, I didn't catch the start, so I'm sorry if this has been covered, but there was talk of who our competitors are, and it was put in terms of technology existence.

And it seems to me that it's more of a people thing, that the people that the people on this call are competing with our people in the marketing department of Google and Microsoft, more than the systems of Google and Microsoft. And so that I think it's a people thing that needs to raise, we need to be at that level of political influence and dedication and… meaning, you know, full time, maybe with a team, level of dedication, this kind of thing. Plus potentially superstars that everyone knows the name of, with experience of trying to get open source tech, or widely used tech into us, many into the hands of as many people, especially children, as possible.

And that's why I mentioned Nicholas Negroponte, who is One Laptop Per Child initiative, I think most of you will know about. And the BBC Micro thing going back a lot further. But that was an initiative that very successfully got computing into the hands of many thousands, if not a millions of school children in the early 1980s in the UK. The interesting thing about, I'm actually not sure about what Negroponte thinks about the success of OLPC now, or what the general impression is. With BBC Micro, that those technology, of course, evolved into ARM, and those people are potentially extremely influential and respected as champions. I just think it needs something of that level.

**Aral:** Laurence, I think I think that's a really, really important. Yeah, that's a really important point. We did kind of briefly discuss it a little earlier. But I think, with firsthand knowledge of this, there's also the very real chance that movements like that can just get co-opted by the big players. So you mentioned BBC Micro, etc.. I was part of an initiative called Code Club from the very early days. That was a coding club in the in the UK, one of the first ones. And it really became very big. It was created by Linda Sandvik and Claire Sutcliffe. And, you know, I was kind of friends with them at the time and on their board initially. But what happened is then, you know, people started sponsoring and Google wanted to sponsor and, you know, and some of us like me, we were like, well, no, we can't have Google sponsor because then what are we saying? Right?

And so we kind of got pushed out at that point. We actually resigned because they said, no, we'll take the money. And you look at things like Raspberry Pi, for example. Excellent. Again, closed source . So I think we have an opportunity here. I hear exactly what you're saying. And you're right. You know, we need this, because this feels much more like enterprise, actually, now that we think about it. You know, when we were talking with Cassidy about creating these everyday devices, that's a very different market. That's when the person actually makes a decision to use something themselves. Whereas with enterprise software, enterprise hardware, that decision isn’t made by you. Right? Somebody takes your manager out to dinner and then they buy Microsoft after that. So I'm realising now, of course, that is what the education market does as well.

So you're right, we need that. But we should also be very careful to make sure that whatever efforts we have in that area, in the free and open source world, don't then just become enclosed by a Google, by someone with money that comes over and says, OK, let's let's make this ours now. Because it was it felt terrible to be part of Code Club and then to see, you know, Google being pushed through it.

I don't want to see that happening, for example. I don't know who else has something to say about this?

**Lukasz:** I mean, so far, we have been really talking about kind of trying to, I'm using kind of "quotes" here because obviously we can't take on Google head-on or, you know, people who who go and lobby on behalf of Google in the educational sector. But what you can do, you mentioned the Raspberry Pi. And what Raspberry Pi did was it introduced hundreds of thousands, if not millions of children to Linux. And so an alternative of approaching this is having some sort of social system which would target children more directly.

So if they become acquainted with Linux, then rather than going from the top down at the problem, you can you can introduce children to Linux, have them enjoy Linux. I suppose that the problem with that is that you would have to have a certain degree of engagement with the parents. I mean, ,y three year old already the only, you know, system she has ever seen is Linux, yeah. Because there's nothing else at home.

But and, you know, she will instantly understand and recognize and enjoy using Linux in the future. Because she hasn't seen anything else. But that's me. And that's not most parents.

**Cassidy:** I think there's a parallel…

**Aral:** Laura I think, I'm sorry, Cassidy, go. Oh, I think there's parallels to I mean, we keep saying to enterprise, you know, think 10 years ago, maybe 10, 15 years ago, it would have been unheard of to bring your iPhone into your enterprise workplace and connect it to the enterprise network and get your enterprise email on it. But Apple made a really compelling device that was really easy to use that people loved, and they demanded that they were able to bring it into the enterprise and connect it. And now there are programs set up.

**Cassidy:** You know, Apple has enterprise tools and things, but it really started at the, from the bottom up, of get the users to really kind of demand that they're able to use this software and these  devices in the workplace. So, yeah, I think that that is kind of the more upstart-y and more, you know, little-guy-punching-up kind of approaches, you get people to really fall in love with the product itself. And that's I mean, that's really been our strategy at Elementary as well. It's like, we're such a small team in general that we we can't fight with Apple and Google and Microsoft. And, you know, realistically, we don't have, you know, billions and billions of dollars of revenue. But what we can do is focus on making a great product, getting people to fall in love with it, and then it can spread to places like, you know, a small city in France where they install it on all other devices. And so I think, yeah, I don't know if there's a real compelling approach today, from the top down, but that bottom up approach is definitely an interesting route.

**Aral:** So our plan lives! We are going to make the Pine Elementarybook. It's going to be amazing and people are going to want it.

And that's why…

**Cassidy:** I don't have anything I can, like, announce, I don't think. But we've been talking about, you know, obviously we've blogged about it too . You know, we've we've made a build of Elementary OS, for the Pinebook Pro, and we've been really interested in working more closely together in the future. So I think that's that's kind of as far as Elementary is concerned. That's our our approach is, you know, let's work with these hardware companies and make these really tightly integrated hardware and software products. And then the next step is definitely talking about services and and let's make these things that that people fall in love with.

**Cassidy:** And I think it's a pretty natural step then to get it trickling into companies and education from there.

**Aral:** And Laura, I think we have another question, someone… sorry, Pernille.

**Pernille:** And then have regulators help us because I mean, we are up against so such a big commercial interests, you know. That we need regulation to push in the other direction.

**Aral:** But Pernille, how do we do this when Google is lobbying? When Facebook's lobbying in Brussels and the regulators want to work there, you know, as their next job, they want to work there. I've seen this happen.

**Pernille:** You know, if you look at the new Data Governance Act, which was proposed in the autumn here, that actually and I think it's Thierry Breton and Margrethe Vestager and that were very pushing towards this. And they're talking about we will have some new data intermediaries who will help us individual individuals control data.

And they say these data intermediaries should be established, have legal establishment in Europe. So there are new tomes coming out from from Brussels, which are very, very positive.

**Aral:** I mean, that's you know, that's that's good, but also it's, being involved in this personally, myself as well, and being, you know, kind of having quite a bit of experience in talking with the European Parliament, talking at the European Parliament with these people.

I also see that whenever there is more of a commercial aspect to it, they go for it, you know? So if it's blockchain. Oh, yeah, we could make a billion with blockchain. Let's put EU taxpayer money on blockchain. But when it comes to actually solutions that aren't there to make a billion, I find that they're much less interested. So data intermediaries I don't think is a solution at all because we can build solutions where you own and control your own data without an intermediary. Right? We build intermediaries then again, we're building silos.

**Pernille:** Well, intermediaries is some kind of like banks, you know. We are all in control of our own money, but some people don't bother, and then they need an independent third party to help them with that. You can't expect everybody to control their own data. That's why they’re talking about this.

**Aral:** But at the same time, but at the same time, we have the European Commission talking about building a data economy, building that there, that they're getting left behind. Oh, why can't we be like Silicon Valley but a better Silicon Valley? We can do so much better than that, you know, and I think maybe that's that's a conversation for another time as well. Pernille, maybe a much longer conversation that we need to have, that the alternative should really be ethical. We shouldn't just be trying to recreate Disneyland because we don't have Disneyland here.

Can we do better than Disneyland? Can we do better than plastic? Laura, I know that we have a question that's been waiting for ages. I think it's Leonardo should we take that?

**Laura:** Leonardo, I will add you to the stream if you're ok with that. I'm just adding you now.

**Leonardo:** Hi, everyone.

**Aral:** Hello.

**Leonardo:** Thanks for having me. Hello.

I'm a big fan of Laura’s and Aral’s work and Elementary OS too. And… but I would like to ask a question about how what, does it really mean to make something easy to use?

I don't know if you know the work of probono? He's working with a new OS, I think he’s based on FreeBSD, but I like the way he's trying to think about what's to make something easy for people. For example, I'm not a programmer and I would love to use Site.js, but I don't know if I can make it happen.

**Aral:** No, it's for developers right now.

**Leonardo:** Yes. And I believe all these things with Linux OS’s adoption in schools, and all those things come down to how easy I had my first MacBook and then it was like, wow. Then I got interested in technology. It was so easy, I could record things and do videos and things like that. And I believe ever since I've been looking for an experience like that in Linux. Nowadays I use Ubuntu, but I've been paying close attention to Elementary OS. So I believe when it doesn't matter if it's hardware, software or service, if we don't have a name for that, what is to make it easy? We have a name for free software, ActivityPub, protocols and things like that, but we don't have a name for what's what can we call that?

Can we agree what?

**Aral:** I think it's called design, right, Cassidy?

**Cassidy:** I mean, yeah, that's hard. Design is is such a broad term. People hear design and they think the pixels on a screen are, you know, the visual aspect only. And that's why, you know, really, I'm a designer, but I don't even like calling myself a designer because I'm not very good at pushing pixels around a screen. I'm much more involved in the experience design. So I think user experience is definitely what you're describing.

**Aral:** But I think you don't even need the user, right? I mean, because user is such a term that we kind of it's like, you know, two industries have users. It's us and drug dealers. Right. [Laughter] And I think it makes sense for Silicon Valley, because users are the people that they want to addict to their products and they want to exploit. It doesn't make sense for us. We work with people. Right? I mean, because it's it's an othering almost. That's why I really love "experience design" or just "design". You know, the design is about having a medium. If our medium is an interactive medium, then of course, it's not just pixels, it's interactions. It's the experience. Sorry, Cassidy, cut you off… go.

You know, I think part of the difficulty is, by its nature, design is…  you can't really document design in a spec, necessarily. I mean, you can we have a human interface guidelines that Elementary uses and Gnome and other projects have Human Interface Guidelines, which is like trying to document how things should be designed to be easy to use and consistent. But when it's technology, you can specify, you can say this is the spec, this is the protocol, and we can give that protocol and that spec a name.

When it's an experience, it's. It's much more like amorphous, I guess. So, yeah, I'm not sure. Other than just the really broad term of experience, design or architecture, you know, information architecture, experience architecture. There's lots of different phrases you can use, but I don't know.

**Leonardo:** But do you think we can agree, for example, if we get a specific niche like OSes, do we have something that we call "easy experience" that really mixes the…

**Nicolas:** Ergonomics. Ergonomics is the right word, I think? You don’t use that in English? Ergonomics?

**Cassidy:** Ergonomics. Yeah.

**Nicolas:** OK.

**Aral:** Yeah, ergonomics is part of it. Ergonomics is part of usability… sorry I was just trying to create a little banner while you all were talking, to a talk that I gave a while back. But you know what? Actually, I can just show you the URL. Here we go. I'll just make that large. So if you go to vimeo.com/70030549, you'll have to stop the video and find that. There's a talk that I gave ages ago called Super Heroes And Villains In Design. I'd really love it if everyone in the free and open source world watched this talk.

It's not specifically about designing an operating system or designing software. It's about experience design in general. But the core principles apply. You have the core principles to making a door handle something that's intuitive, that you use without thinking, versus something that stops you from opening that door the first time you try it, because maybe they didn't adhere to the conventions. These concepts carry over to what we're building because we're just building the new everyday things, right? Those are everyday things and we're just building the new everyday things.

And that's what we have to understand. We have a problem with this in the free and open source world where we're a bit arrogant sometimes and we say, well, our stuff is ethical, our stuff is good. So if people don't put in the effort to learn it, they don't deserve it. And that's horrendous, you know, because we are building the new everyday things. And if people are having trouble with this, it's not because they're stupid. It's not because they're dumb. It's not because they haven't put in the time.

It's because they are brain surgeons and they have brain surgery in the morning and they don't have time to learn your hard-to-use thing. Right? And I think we need to get into this mindset, the mindset that as people who build experiences, we have a profound responsibility to make those experiences as beautiful, as empowering, as possible in the things that we build.

So Leonardo, is that?

**Leonardo:** Don't you think programmers feel like special people because they build things that normal people don't understand? You know, so they like…

**Aral:** We're all normal people. That's the thing. You know, that's what we have to understand.

**Leonardo:** You know, people that are not programmers.

**Aral:** Yeah, well, I think it's also, we think about it as, we think of people in certain ways. We put them into little cubbyholes. You're a programmer, you're a normal person. You are this, these are much more like roles like I know how to program. I've been programming since I was seven years old. There's a part of me that likes to tinker with things. Right. There is an enthusiast, but that's a role that I have. At the same time, I use a device like this phone every day as an everyday thing.

I don't want to tinker with this phone. I want this phone to work because if it doesn't, I miss an appointment. If it doesn't, I can't do something I want to do, like talk to my friend. If my, if this Pinebrook on the other hand, for me it's a development machine. I'm playing with it. I'm installing betas on it. When this fails, I'm happy. I'm like, hey, I know what I'm going to do on the weekend, I'm going to fix it. Right? That's the enthusiast part of me. This is the everyday person. Using technology is an everyday thing. I might have a third role.

I might work somewhere where again, like in the school, I don't even get to pick the hardware or the software that I'm using. And that's a third role that I might have. And there I might hate it because somebody else has picked it or I might like it. But we need to start not kind of putting people into these cubbyholes and especially not using it as an excuse.

We must build these free and open, beautiful devices. I don't mean beautiful just aesthetically. I mean in terms of the usability of it, in terms of the functionality of it. That is our greatest challenge. This is how we win or lose this battle for freedom in our age. You know, whether or not we can build these everyday things to be beautiful, beautiful experience.

And that's why it's so great. Sorry, Pernille?

**Laura:** I don’t agree, it’s not the greatest challenge to build them, because they're already built. The greatest challenge…

**Aral:** But they're not.

**Pernille:** … is to get them and get them marketed.

**Aral:** Pernille, they're not. Trust us. Cassidy, are they already built? You're building them. Are you done? Can you go home now?

**Cassidy:** Still a lot of work to do. Yeah, we always have more work to do.

**Aral:** You're completely right, Pernille. You are right. We need  to market them. We need to be able to talk about them. You're entirely right. They're also not ready. I mean, we're working on them, but we need to instill this mindset in the free and open source world because it doesn't exist.

**Nicolas:** Well, it exists more and more. Today we have Elementary OS. You know, some people say they tried to copy macOS, you know, that's something that we hear, and then what’s the problem? You know, one of the best user experience you may have is macOS. So it's great. It's great. And I think, of PopOS! also, you know, we have more and more distributions that are easy to use and beautiful and that the way to go, you're right. If we want people to to adopt these these new operating systems. You’re right.

**Laura:** Now I’m… very sorry…

**Aral:** I think we're over our time, Laura?

**Laura:** I was going to say I didn't want to be the buzzkill, but I think we've kept these good people for a little while over what we said we would, originally. And I think we should possibly wrap up, but I think it would be great to maybe have more sessions in the future because we've got so many more things we can talk about.

**Aral:** And this feels great. You know, if we can bring together people who are working on different aspects of these problems who care about the same things, I think that has a lot of value. We need to be talking to each other, not taking time away from what we're working on, because we all have very limited resources, but kind of finding a way to build upon what each of us is doing and kind of elevate each other a little higher. So I don't know about you guys, but I think this has been really great.

I hope we have more of these conversations going forward. Personally, I just want to say thank you all for doing what you do. Laura, would you like to carry us into our outro?

**Laura:** Yeah. And also, I'd like to thank all of you that, all of you do such fantastic work in different areas. And it's been an honour to have you on today. And I also want to thank everyone who had questions in the feed, and those that we didn't get to this time. I promise that we will have more sessions on similar topics. So we will be able to get to those questions another time.

**Laura:** And so. I have been Laura Kalbag .

**Aral:** And I Aral Balkan, and this has been  Small is Beautiful. Thank you so much for joining us, and we'll see you again next week.

Bye bye.