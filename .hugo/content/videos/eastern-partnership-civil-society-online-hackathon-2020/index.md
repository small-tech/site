---
title: "Eastern Partnership Civil Society Online Hackathon 2020"
date: 2020-07-16T12:00:00+01:00
speaker: "Laura Kalbag and Aral Balkan"
description: "Laura Kalbag and Aral Balkan gave a keynote on Big Tech vs Small Tech at the Eastern Partnership Civil Society Online Hackathon 2020."
---

## Main talk

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/443408666" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>

## Question and answer session
<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/443420922" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>

## Opening chat
<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/443421910" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>

## Note

Please note that the poor quality of the visuals is because these videos were recorded by Zoom in 640 x 360 (for some reason). We upscaled the video files to 1280 x 720 before uploading them to our Vimeo account.

We had to use Zoom for accessibility reasons as we couldn’t find a system for the event to use in time that supported both sign language interpretation and simultaneous translation. For our own events, we use Vimeo for live streaming (embedded into our web site without trackers) and we will also be using our new self-hosted installation of Jitsi for some events in the future.

{{< fund-us >}}
