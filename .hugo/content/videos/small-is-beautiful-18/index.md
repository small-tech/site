---
title: "Small Is Beautiful #18"
date: 2022-05-19T17:00:00+00:00
speaker: "Aral and Laura"
description: "Adventures in immutability"
---

{{< video 
  poster="./poster.jpg" 
  vimeo="https://player.vimeo.com/progressive_redirect/playback/721474636/rendition/1080p/file.mp4?loc=external&signature=8fbafeaed67f7839efcffe9a73781099655a1b22d0673837785f8b8b64bf23d1" 

>}}

## Adventures in immutability

**Broadcast on May 19th, 2022.**

In this shorter-than-usual stream, Laura talks about her presentation at the Birmingham Design Festival and Aral shows off his new desktop computer running Fedora Silverblue (while battling his chroma key).

### Topics covered

- [Laura’s talk at the Birmingham Design Festival](https://birminghamdesignfestival.org.uk/whats-on/events/laura-kalbag-talk/)
- Aral shows off his new Linux desktop (a passively-cooled/entirely silent black cube)
- Switching to [Fedora Silverblue](https://silverblue.fedoraproject.org/) (an immutable operating system)
- [Distrobox](https://distrobox.privatedns.org/)
- [Toolbx](https://containertoolbx.org/)
- [Open Switcher](https://openswitcher.org) for Blackmagic Design Atem Mini, Pro, etc.

- [Helix Editor](https://helix-editor.com)
- Aral battling his chroma key.

_Streamed using our own [Owncast](https://owncast.online) instance. (Hint: you can install Owncast using [Site.js](https://sitejs.org).)_

If you like this livestream, please help support out work at Small Technology Foundation with a [patronage or donation](/fund-us), or share our videos with your friends!

### Transcript

To follow.

{{< fund-us >}}
