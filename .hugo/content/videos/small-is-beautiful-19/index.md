---
title: "Small Is Beautiful #19"
date: 2022-06-19T17:00:00+00:00
speaker: "Aral and Laura"
description: "Owncast COVID special (with special guest Gabe Kangas)"
---

{{< video 
  poster="./poster.jpg" 
  vimeo="https://player.vimeo.com/progressive_redirect/playback/721475275/rendition/1080p/file.mp4?loc=external&signature=236647842fa4c6009fa49ba2cffb396070c0ed91aa7d9b3a988fd294a4959809" 

>}}

## Owncast COVID special

**Broadcast on June 16th, 2022.**

We talk Owncast, shipping software not code, trickle-down technology, ActivityPub, and more on this COVID special where everyone had COVID.

*Aral and Laura had COVID (Laura’s not on the stream) and Gabe apparently had COVID too and found out the next day.*

### Topics covered

- [Owncast](https://owncast.online)
- [Ship software, not code](https://gabekangas.com/blog/2022/06/ship-software-not-code/)
- [Trickle-down technology and why it doesn’t work](https://ar.al/notes/trickle-down-technology/)
- [Fediverse](https://fediverse.info/)
- [ActivityPub](https://activitypub.rocks/)

_Streamed using our own [Owncast](https://owncast.online) instance. (Hint: you can install Owncast using [Site.js](https://sitejs.org).)_

If you like this livestream, please help support out work at Small Technology Foundation with a [patronage or donation](/fund-us), or share our videos with your friends!

### Transcript

To follow.

{{< fund-us >}}
