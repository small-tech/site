---
title: "Small Is Beautiful #17"
date: 2022-04-21T17:00:00+00:00
speaker: "Aral and Laura"
description: "Lessons learned working against the grain of the mainstream"
---

{{< video 
  poster="./poster.jpg" 
  vimeo="https://player.vimeo.com/progressive_redirect/playback/721473736/rendition/1080p/file.mp4?loc=external&signature=9828895c53529daf864f2ec78e01ad467b9aa612ea350c32874a6cd7748073f1" 
>}}

## Lessons learned working against the grain of the mainstream

In this informal stream, Laura and I discuss some of the lessons we learned working against the interests of surveillance capitalism for close to the past decade.

### Topics covered

- Economic challenges (funding and the challenges of remaining independent)
- Things may take longer than you thought (and that’s ok)
- The challenges of working in an environment where tools and infrastructure have been built to fit needs of large corporations and centralised systems that exploit people for profit.
- Why you might not want to use crowdfunding
- Embracing your humanity (don’t try to emulate corporate models, practices, etc.)
- Design vs. Decoration
- The advantages of iteration, sharing, and working in the open (as well as the challenges)

_Streamed using our own [Owncast](https://owncast.online) instance. (Hint: you can install Owncast using [Site.js](https://sitejs.org).)_

If you like this livestream, please help support out work at Small Technology Foundation with a [patronage or donation](/fund-us), or share our videos with your friends!

### Transcript

To follow.

{{< fund-us >}}
