---
title: "Internet As A Commons"
date: 2015-10-30T17:08:57Z
speaker: "Aral"
description: "Aral at the “Internet As A Commons” conference at the European Parliament. Brussels, Belgium. October 2015."
---

{{< video 
	poster="poster-internet-as-a-commons.jpg" 
	vimeo="//player.vimeo.com/external/141562808.hd.mp4?s=fa5aaa255046c63300b420aba12ed91a&profile_id=113"  
>}}

Aral speaking at the European Parliament conference on “Internet as a Commons: Public Space in the Digital Age” as part of “The Big Picture” panel.
You can watch the [original video on the Greens’ video archive](http://greenmediabox.eu/en/ct/107-Internet-as-a-Commons-Public-Space-in-the-Digital-Age).
Copyright &copy; Greens
