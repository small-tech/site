---
title: "R&D"
date: 2019-06-12T09:42:02+01:00
menu:
 main:
  weight: 3
summary: "Building the Small Web: a public space of individually-owned and controlled places."
---

# Research & Development

<p style='margin: 0.5em 0; font-size: 10em; text-align: center;'>💕</p>

We’re building the [Small Web](https://ar.al/2020/08/07/what-is-the-small-web/).

In a digital network, public space is not a place; it is the interconnections between individually-owned and controlled places.

The Small Web is a _public space_ comprised of _places you own and control_.

(No, it’s not web3, it’s [web0](https://web0.small-web.org).)

> 🍿 Watch the recording of [Aral’s computer science colloquium at University of Groningen](https://ar.al/2024/06/24/small-web-computer-science-colloquium-at-university-of-groningen/) to learn more about Small Web.

## Big Web vs Small Web

__The Small Web is the opposite of the Big Web__ (even though it is built using some of the same underlying technologies).

| Big Web | Small Web |
| ----------------------- | ---------------------- |
| Trust the server        | Don’t trust the server |
| Don’t trust the client  | Trust the client       |
| Owned by corporations   | Owned by individuals   |
| Call people “users”     | Call people “people”   |
| Servers have many users<br>(multi-tenant) | A server has one owner<br>(single-tenant) |

## The projects

<ol class='emphasised-numbers'>
  <li id='kitten'>
    <h3>Kitten</h3>
      <p>Kitten is a web development kit that’s small, purrs, and loves you.</p>
    <p><a href='https://kitten.small-web.org'>Learn more about Kitten</a></p>
    <p>Status: <strong>Approaching API Version 1.</strong></p>
  <li id='domain'>
    <h3>Domain</h3>
    <p>A tool to enable people and organisations to run Small Web domains where people can have their Small Web places. Currently being rewritten using Kitten.</p>
    <p><a href='https://small-tech.org/videos/small-is-beautiful-11/'>Watch a video demonstration of Domain.</a></p>
    <p><a href='https://codeberg.org/domain/app#domain'>Learn more about Domain</a></p>
    <p>Status: <strong>Approaching beta.</strong></p>
  </li>
  <li id='place'>
    <h3>Place</h3>
    <p>A peer-to-peer personal Small Web app that lets you communicate both privately and publicly with other Small Web places from your own place on the web.</p>
    <p>Written in Kitten, easily deployed using Domain.</p>
    <p><a href='https://codeberg.org/place/app#place'>Learn more about Place</a></p>
    <p>Status: <strong>In development.</strong></p>
  </li>
</ol>

{{< fund-us >}}
