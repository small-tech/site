---
title: "Dynamic Template"
display_title: "small technology"
date: 2018-01-18T16:52:40+01:00
summary: "A dynamic page"
meta_title: "Fund Small Technology Foundation"
description: "We’re a tiny team of two funded by individuals like you. Your patronage goes towards keeping a roof over our heads as we build a better future for the Internet."
---

${main}

