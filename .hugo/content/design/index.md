---
title: "Small Tech Design"
date: 2019-08-20T09:39:44+01:00
---

# Rough design system

This page should contain all the markdown/HTML elements and common components/partials used on the site. We can use these to check consistency and for quick cross-browsing checking.

- [Navigation](#navigation)
- [Footer](#footer)
- [Basic Typography](#typography) including headings, paragraphs, links, bold, italics, and lists.
- [Forms](#forms)
- [Video](#video)
- [Image](#image)
- [Event](#event)
- [News item](#news-item)
- [Boxy list](#boxy-list)
- [Timeline](#timeline)

<h2 id='typography'>Basic Typography</h2>

# Heading Level 1

A standard paragraph. It has no features. Small Technology is everyday tools built by humans, for humans. The goal is to increase human welfare, not corporate profits.

Just a second paragraph, to show how paragraphs follow one another.	We divide our time roughly 75%, 15% and 10% between research and development, technological regulation, and awareness raising, advocacy, and education.

## Heading Level 2

Paragraphs with special inline elements. A linked sentence like we’re [a tiny and independent two-person not-for-profit](https://small-tech.org) based in Ireland. Then a **sentence** with bold text, first one **word** and **then a lot of text in bold, which can be quite intimidating.** Next up, we have *italics*, which we’ll probably *use less* but should still *test the readability.*

### Heading Level 3

The beginning of this section starts with a short line.

- And is followed by a simple list.
- In markdown, we start that list with a ‘-’ dash.
- It behaves like a bullet point.
- To tell us where the next item begins.
- Sometimes we make items really long, even though text this long would probably better serve us as a standard sentence in a paragraph.

#### Heading Level 4

1. Once we’ve styled bulleted lists.
2. We should focus on numbered lists.
3. We’ll use them less often, and we have a specially-styled version, but these should still work.
4. Because who doesn’t like an ordered list.
5. Another item so we can get to double figures.
6. Another item so we can get to double figures.
7. Another item so we can get to double figures.
8. Another item so we can get to double figures.
9. Another item so we can get to double figures.
10. Another item so we can get to double figures.
11. Another item so we can get to double figures.
12. Another item so we can get to double figures.
13. Another item so we can get to double figures.
14. Another item so we can get to double figures.
00. Another item so we can get to double figures.

##### Heading Level 5

Sometimes you want a list with fancy *emphasised* numbers. You can invoke this list using `<ol class='emphasised-numbers'>`. It has to be HTML inside the markdown, because otherwise we can’t give it a class name.

<ol class='emphasised-numbers'>
	<li>
		<h3>Item number one.</h3>
		<p>You can have headings in the list, and pair them with paragraphs.</p>
	</li>
	<li>
		<p>It doesn’t have to have a heading, but it does need to be wrapped in something inside the &lt;li&gt;.</p>
  </li>
</ol>

###### Heading Level 6

> A blockquote suggests quoted content. Sometimes it’s short.

A line of text can appear between two blockquotes.

<blockquote>
	<p>Occasionally, a blockquote will have multiple paragraphs inside it.</p>
	<p>“To privacy activists Aral Balkan and Laura Kalbag, we don’t need brain implants to become cyborgs; we’re already jacked in. And we need a Universal Declaration of Cyborg Rights.”</p>
</blockquote>

## Forms

<form>
    <fieldset>
      <legend>Title</legend>
      <ul>
          <li>
            <label for="title_1">
              <input type="radio" id="title_1" name="title" value="M." >
              Mister
            </label>
          </li>
          <li>
            <label for="title_2">
              <input type="radio" id="title_2" name="title" value="Ms.">
              Miss
            </label>
          </li>
      </ul>
    </fieldset>
    <p>
      <label for="name">Name</label>
      <input type="text" id="name" name="username">
    </p>
    <p>
      <label for="mail">E-mail</label>
      <input type="email" id="mail" name="usermail">
    </p>
    <p>
      <label for="pwd">Password</label>
      <input type="password" id="pwd" name="password">
    </p>
	<p>
      <label for="card">
        <span>Card type:</span>
      </label>
      <select id="card" name="usercard">
        <option value="visa">Visa</option>
        <option value="mc">Mastercard</option>
        <option value="amex">American Express</option>
      </select>
    </p>
    <p>
      <label for="number">Card number</label>
      <input type="number" id="number" name="cardnumber">
    </p>
    <p>
      <label for="date">Expiration date
        <em>formatted as mm/yy</em>
      </label>
      <input type="date" id="date" name="expiration">
    </p>
	<p> <button type="submit">Validate the payment</button> </p>
</form>

## Specific Components

### Video {#video}

### Image {#image}

### Event {#event}

<ul class="events-list future">
<li class="h-event">
          <time datetime="2019-09-14T12:26:02+01:00" class="dt-start">14th September, 2019</time>
  <h4 class="p-name">
    <a href="https://2019.stateofthebrowser.com" class="event-link u-url">State Of The Browser 2019 in London, UK
    </a>
  </h4>
          <p class="summary p-summary">Laura is speaking.
          </p>
</li>
</ul>

### News item {#news-item}

<ul class="posts-list">
<li>
				<h2 class="p-name">
					<a href="https://www.pcmag.com/news/368457/like-it-or-not-were-already-cyborgs">Like It or Not, We're Already Cyborgs</a>
				</h2>
				<div class="entry-meta"><p>Posted: <time datetime="2019-06-19T10:27:37+01:00">
					19th June, 2019
					</time> by S.C. Stuart on <a href="https://www.pcmag.com">PCMag</a></p></div>
				<blockquote class="summary">
					<p>
					“To privacy activists Aral Balkan and Laura Kalbag, we don’t need brain implants to become cyborgs; we’re already jacked in. And we need a Universal Declaration of Cyborg Rights.”
					</p>
				</blockquote>
				<div class="entry-notes">
					<p><a href="https://www.pcmag.com/news/368457/like-it-or-not-were-already-cyborgs">Read ‘Like It or Not, We're Already Cyborgs’ on PCMag</a>.</p>
				</div>
			</li>
</ul>

### Boxy list {#boxy-list}

<ul class='box-list'>
  <li><a href='easy-to-use'>easy to use</a></li>
  <li><a href='personal'>personal</a></li>
  <li><a href='private-by-default'>private by default</a></li>
  <li><a href='copyleft-share-alike'>copyleft / share-alike</a></li>
  <li><a href='peer-to-peer'>peer-to-peer</a></li>
  <li><a href='interoperable'>interoperable</a></li>
  <li><a href='zero-knowledge'>zero-knowledge</a></li>
  <li><a href='non-commercial'>non-commercial</a></li>
  <li><a href='non-colonial'>non-colonial</a></li>
  <li><a href='accessible'>accessible</a></li>
</ul>

### Timeline {#timeline}

<ul class='timeline'>
  <li>
    <h3>2014</h3>
    <p>We set up <a href='https://ind.ie'>Ind.ie</a> as a not-for-profit in Brighton, UK</p>
  </li>
  <li>
    <h3>2015</h3>
    <p>We release the <a href='https://ind.ie/heartbeat'>Heartbeat</a> pre-alpha. Heartbeat is the spiritual precursor to Tincan. We realise, however, that our current technology stack is not fit to take the project further. In dire need of funds, we invest our remaining money to develop <a href='https://better.fyi'>Better Blocker</a>.</p>
  </li>
  <li>
    <h3>2016</h3>
    <p>The Conservatives win re-election in the UK with a manifesto that details plans to pass a draconian surveillance law (the IP Act), hold a referendum on leaving the EU, and scrap the Human Rights Act. As this is fundamentally incompatible with our mission, <a href='https://ar.al/notes/so-long-and-thanks-for-all-the-fish/'>we decide to leave the UK</a> and move to Sweden.</p>
  </li>
  <li>
    <h3>2017</h3>
    <p>We release <a href='https://better.fyi'>Better Blocker</a>, a no-nonsense tracker blocker. We also start working with the City of Ghent on project Indienet to explore how Small Technology principles can be applied at the city level.</p>
  </li>
  <li>
    <h3>2018</h3>
    <p>We leave Sweden and <a href='https://laurakalbag.com/move-to-ireland/'>move to Ireland</a>.</p>
  </li>
  <li>
    <h3>2019</h3>
    <p>We launch the Small Technology Foundation and <a href='https://sitejs.org'>Site.js</a>. We continue our research and development with project Tincan.</p>
  </li>
</ul>
