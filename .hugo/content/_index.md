---
title: "Home"
date: 2018-01-18T16:52:40+01:00
menu:
 main:
  weight: 1
---

# Hello!

__We’re a tiny and independent two-person not-for-profit based in Ireland.__

We are building the [Small Web](/research-and-development).

No, it’s not web3, it’s [web0](https://web0.small-web.org).

<!--more-->

[Learn more about us](/about#the-foundation).

## Small Technology

Small Technology is everyday tools for everyday people designed to increase human welfare, not corporate profits.

### Small Tech is…

<ul class='box-list'>
  <li><a href='/about#easy-to-use'>easy to use</a></li>
  <li><a href='/about#personal'>personal</a></li>
  <li><a href='/about#private-by-default'>private by default</a></li>
  <li><a href='/about#share-alike'>share alike</a></li>
  <li><a href='/about#peer-to-peer'>peer to peer</a></li>
  <li><a href='/about#interoperable'>interoperable</a></li>
  <li><a href='/about#zero-knowledge'>zero knowledge</a></li>
  <li><a href='/about#non-commercial'>non-commercial</a></li>
  <li><a href='/about#non-colonial'>non-colonial</a></li>
  <li><a href='/about#inclusive'>inclusive</a></li>
</ul>

<!-- What do all these terms mean and why are they important? -->

[Learn more about Small Technology.](/about#small-technology)

## Small Web

<p style='margin: 0.5em 0; font-size: 10em; text-align: center;'>💕</p>

The Small Web is a public space comprised of places you own and control.

[Learn more about the Small Web.](/research-and-development)

## What we do

Our main activity is [research and development](/research-and-development). We also spend some of our time on advocacy, awareness raising, and education.

<ol class='emphasised-numbers'>
  <li>
    <h3>Research and development.</h3>
    <p>We are building the Small Web, a public space comprised of places that are owned and controlled by individuals. We’ve been working towards this for the last seven years.</p>
    <p><a href='/research-and-development'>Learn more about our research and development.</a></p>
  </li>
  <li>
    <h3>Advocacy, raising awareness, and education.</h3>
    <p>We advocate for Small Tech as the antidote to Big Tech and surveillance capitalism.</p>
    <p>We give <a href='/events'>talks</a>, write <a href='/news'>articles</a> and <a href='https://accessibilityforeveryone.site'>books</a>, and give <a href='/videos'>media interviews</a>.</p>
  </li>
</ol>

All the technology we produce is released under free and open licenses (mainly, [AGPL version 3.0](https://en.wikipedia.org/wiki/GNU_Affero_General_Public_License)). 

Our active projects are on Codeberg – see [Small Tech (general)](https://codeberg.org/small-tech), [Kitten](https://codeberg.org/kitten), [Domain](https://codeberg.org/domain), [Place](https://codeberg.org/place) – with older projects still accessible from [our self-hosted GitLab instance](https://source.small-tech.org/explore/projects) for the time being. 

## Published work

<ul class='item-list'>
  <li id='accessibility'>
    <h3>Accessibility For Everyone</h3>
    <p>A book by Laura Kalbag about building inclusive and accessible websites. Available in paperback or ebook, or in audiobook on Audible.<p><a href='https://accessibilityforeveryone.site'>Visit the publisher’s site.</a></p>
  </li>
  <li id='web0'>
    <h3>web0 manifesto</h3>
    <p>“web0 is web3 without all the corporate right-libertarian Silicon Valley bullshit.”</p>
    <p><a href='https://web0.small-web.org'>Visit the web0 manifesto</a>, <a href='https://web0.small-web.org/#trivia'>learn some geeky trivia</a>, and <a href='https://github.com/small-tech/web0'>view its source</a>.</p>
  </li>
  <li id='site-js'>
    <h3>Site.js</h3>
    <p>The precursor to <a href='https://kitten.small-web.org'>Kitten</a>. Site.js is a personal web server for small sites. It powers this site, <a href='https://better.fyi'>the Better Blocker site</a>, and our personal sites (<a href='https://ar.al'>Aral’s site</a>, <a href='https://laurakalbag.com'>Laura’s site</a>).</p>
    <p>As of November 2024, Site.js has been deprecated. It will be maintained for as long as we still have sites that rely on it but all future development is focussed on its successor, Kitten.</p>
    <p><a href='https://sitejs.org'>Visit SiteJS.org.</a></p>
  </li>
  <li id='jsdb'>
    <h3>JavaScript Database (JSDB)</h3>
    <p>A zero-dependency, transparent, in-memory, streaming write-on-update JavaScript database for the Small Web that persists to a JavaScript transaction log. Used in Kitten.</p>
    <p><a href='https://codeberg.org/small-tech/jsdb'>Visit the JSDB repository.</a></p>
  </li>
  <li id='https'>
    <h3>@small-tech/https</h3>
    <p>A drop-in standard Node.js HTTPS module replacement with both automatic development-time (localhost) certificates via <a href='https://codeberg.org/small-tech/auto-encrypt-localhost'>Auto Encrypt Localhost</a> and automatic production certificates via <a href='https://codeberg.org/small-tech/auto-encrypt'>Auto Encrypt</a>. Used in Kitten.</p>
    <p><a href='https://codeberg.org/small-tech/https'>Visit the @small-tech/https repository.</a></p>
  </li>
  <li id='catts'>
    <h3>elementary OS task switcher</h3>
    <p>As part of our work on improving the free and open source tools we use ourselves, Aral contributed <a href='https://ar.al/2021/11/08/my-three-month-long-elementary-os-6-upgrade-adventure-in-three-parts-part-1-catts/'>the new task switcher</a> (Calmer Alt-Tab Task Switcher or Catts) in <a href='https://elementary.io'>elementary OS 6</a>.</p>
    <p><a href='https://ar.al/2021/11/08/my-three-month-long-elementary-os-6-upgrade-adventure-in-three-parts-part-1-catts/'>Read more about Catts.</a></p>
  </li>
  <li id='comet'>
    <h3>Comet</h3>
    <p>Comet is a distraction-free Git commit message editor for elementary OS with spell-check, first line character limit warnings, and emoji support.</p>
    <p><a href='https://comet.small-web.org'>Visit comet.small-web.org.</a></p>
  </li>
  <li id='better'>
    <h3>Better Blocker (2016-2021)</h3>
    <p>Better was a tracker-blocking privacy tool for Safari on iPhone, iPad, and Mac.<p><a href='https://better.fyi'>Visit Better.fyi.</a></p>
</li>
</ul>


## Rather watch a video?

Here’s a talk we gave at the ThinkAbout Conference in May, 2019 that summarises the problem we’re working to solve and introduces the solution we’re exploring.

{{< video
  poster="/videos/small-technology/poster-small-technology.jpg"
  vimeo="//player.vimeo.com/external/355368347.hd.mp4?s=3e60da74579b368cd489b5493065f354db762d1a&profile_id=175"
>}}

<a href='/videos'>Browse more of our talks</a> and learn about <a href='/events'>our upcoming events</a>.

{{< fund-us >}}
