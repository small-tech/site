---
title: "About"
date: 2019-06-12T09:26:11+01:00
menu:
 main:
  weight: 2
summary: "We’re a tiny and independent two-person not-for-profit based in Ireland. We’re working on building the Small Web."
---

# About

__We’re a tiny and independent two-person not-for-profit based in Ireland.__

We’re working on building the Small Web.

Read on to learn more about [the foundation](#the-foundation), [Small Technology](#small-technology), and the [Small Web](/research-and-development).

## The Foundation

<img src='laura-osky-aral.jpeg' alt='Laura, Osky, and Aral' style='filter: sepia(100%) hue-rotate(42deg); border: 5px solid #333;'></img>

We’re [Laura Kalbag](https://laurakalbag.com) and [Aral Balkan](https://ar.al) (and Oskar the huskamute). We live and work in Bray, Ireland.

Since 2014, we’ve been advocating for regulation of surveillance capitalism, investment in ethical alternatives, and carrying out [research and development](/research-and-development) on ethical alternatives.

After [leaving the UK](https://ar.al/notes/so-long-and-thanks-for-all-the-fish/) and [moving to Ireland](https://laurakalbag.com/hello-ireland/), we set up the Small Technology Foundation with the mission to evolve the Internet so each one of us can own and control our own place on it.

We strive to follow [the principles of Small Technology](#small-technology) in our work.

We don’t take money from surveillance capitalists and we exist thanks to [the support of individuals like you](/fund-us).

### History

Here is a short timeline of important events in our history.

<ul class='timeline'>
  <li>
    <h3>2014</h3>
    <p>We set up <a href='https://ind.ie'>Ind.ie</a> as a not-for-profit in Brighton, UK</p>
  </li>
  <li>
    <h3>2015</h3>
    <p>We release the <a href='https://ind.ie/heartbeat'>Heartbeat</a> pre-alpha. Heartbeat is the spiritual precursor to Tincan. We realise, however, that our current technology stack is not fit to take the project further.</p>
    <p>In dire need of funds, we invest our remaining money to develop <a href='https://better.fyi'>Better Blocker</a>.</p>
    <p>The Conservatives win re-election in the UK with a manifesto that details plans to pass a draconian surveillance law (the IP Act), hold a referendum on leaving the EU, and scrap the Human Rights Act. As this is fundamentally incompatible with our mission, <a href='https://ar.al/notes/so-long-and-thanks-for-all-the-fish/'>we decide to leave the UK</a>.</p>
  </li>
  <li>
    <h3>2016</h3>
    <p>We move to Sweden.</p>
    <p>We release <a href='https://better.fyi'>Better Blocker</a>, a no-nonsense tracker blocker.</p>
    <p>Laura’s book, <a href='https://accessibilityforeveryone.site'>Accessibility For Everyone</a>, is published.</p>
  </li>
  <li>
    <h3>2017</h3>
    <p>We start working with the City of Ghent on project Indienet to explore how Small Technology principles can be applied at the city level.</p>
  </li>
  <li>
    <h3>2018</h3>
    <p>We leave Sweden and <a href='https://laurakalbag.com/hello-ireland/'>move to Ireland</a>.</p>
  </li>
  <li>
    <h3>2019</h3>
    <p>We launch Small Technology Foundation and <a href='https://sitejs.org'>Site.js</a>. We continue our research and development.</p>
  </li>
  <li>
    <h3>2020</h3>
    <p>Site.js matures and now runs our own sites and we fork it to start on the next stage of our research and development: to build the <a href='https://ar.al/2020/08/07/what-is-the-small-web/'>Small Web</a>.</p>
  </li>
  <li>
    <h3>2021</h3>
    <p>We start work on <a href='https://codeberg.org/domain/app'>Domain</a> as the first step towards the Small Web. Watch <a href='https://small-tech.org/videos/small-is-beautiful-11/'>Small is Beautiful #11</a> for a preview of what it does and how it works.</p>
    <p><a href='https://laurakalbag.com/working-with-stately/'>Laura starts contracting with Stately</a> to keep us afloat financially while Aral works on developing Domain.</p>
  </li>
  <li>
    <h3>2022</h3>
    <p>We move to Kilkenny (as our lovely landlord sold our place in Cork the first moment the pandemic restrictions were eased), start work on <a href='https://kitten.small-web.org'>Kitten</a> and begin porting <a href='https://codeberg.org/domain/app'>Domain</a> to it.</p>
    <p><a href='https://ar.al/2022/07/29/nlnet-grant-application-for-domain/'>We apply for NLNet/ngi funding for Domain</a> and, <a href='https://ar.al/2022/09/28/nlnet-grant-application-for-domain-first-update-questions-and-answers/'>following an initial round of questions</a>, <a href='https://ar.al/2022/10/20/nlnet-grant-application-for-domain-rejected/'>we are rejected</a>.</p>
  </li>
  <li>
    <h3>2023</h3>
    <p>We move to Bray and buy a house (well, technically, the bank buys the house and we pay the bank rent now) as it’s nigh-on-impossible to rent in Ireland these days due to the housing crisis and we need stability to do our work without looking over our shoulders every two minutes to see if we are going to be forced to move again due to some landlord’s whim.</p>
    <p>Work continues on <a href='https://kitten.small-web.org'>Kitten</a> and <a href='https://codeberg.org/domain/app'>Domain</a>.</p>
  </li>
  <li>
    <h3>2024</h3>
    <p><a href='https://kitten.small-web.org'>Kitten</a> and <a href='https://codeberg.org/domain/app#domain'>Domain</a> continue to mature.</p>
    <p>Development begins on <a href='https://codeberg.org/place/app#place'>Place</a>.</p>
    <p>Kitten is already close to version 1 and our goal is to open Domain up slowly for initial registrations and beta testing either late this year or next year.</p>
    <p>Laura starts contracting at <a href='https://penpot.app'>Penpot</a> to keep us afloat financially while Aral continues to work on the Small Web.</p>
    <p><a href='https://ar.al/2024/06/01/small-technology-foundation-funding-application-for-nlnet-foundation-ngi-zero-core-seventh-call/'>We apply again for NLNet/ngi funding</a>, this time to support Aral’s full-time work on Small Web in general, including Kitten and Domain. We get rejected again.</p>
  </li>
</ul>

## Small Technology

Small Technology are everyday tools for everyday people designed to increase human welfare, not corporate profits.

### Small Tech is…

<ul class='box-list'>
  <li><a href='#easy-to-use'>easy to use</a></li>
  <li><a href='#personal'>personal</a></li>
  <li><a href='#private-by-default'>private by default</a></li>
  <li><a href='#share-alike'>share alike</a></li>
  <li><a href='#peer-to-peer'>peer to peer</a></li>
  <li><a href='#interoperable'>interoperable</a></li>
  <li><a href='#zero-knowledge'>zero knowledge</a></li>
  <li><a href='#non-commercial'>non-commercial</a></li>
  <li><a href='#non-colonial'>non-colonial</a></li>
  <li><a href='#inclusive'>inclusive</a></li>
</ul>

### Personal

Small Technology are everyday tools for everyday people. They are not tools for startups or enterprises.

### Easy to use

Personal technology are everyday things that people use to improve the quality of their lives. As such, in addition to being functional, secure, and reliable, they must be convenient, easy to use, and inclusive. If possible, we should aim to make them delightful.

__Related aspects:__ [inclusive](#inclusive)

### Non-colonial

Small technology is made by humans for humans. They are not built by designers and developers for users. They are not built by Western companies for people in African countries. If our tools specifically target a certain demographic, we must ensure that our development teams reflect that demographic. If not, we must ensure people from a different demographic can take what we make and specialise it for their needs.

__Related aspects:__ [share alike](#share-alike), [non-commercial](#non-commercial), [interoperable](#interoperable)

### Private by default

A tool respects your privacy only if it is private by default. Privacy is not an option. You do not opt into it. Privacy is the right to choose what you keep to yourself and what you share with others. “Private” (i.e., for you alone) is the default state of small technologies. From there, you can always choose who else you want to share things with.

__Related aspects:__ [zero knowledge](#zero-knowledge), [peer to peer](#peer-to-peer)

### Zero knowledge

Zero-knowledge tools have no knowledge of your data. They may store your data, but the people who make or host the tools cannot access your data if they wanted to.

Examples of zero-knowledge designs are end-to-end encrypted systems where only you hold the secret key, and peer-to-peer systems where the data never touches the devices of the app maker or service provider (including combinations of end-to-end encrypted and peer-to-peer systems).

__Related aspects:__ [private by default](#private-by-default), [peer to peer](#peer-to-peer)


### Peer to peer

Peer-to-peer systems enable people to connect directly with one and another without a person (or more likely a corporation or a government) in the middle. They are the opposite of client/server systems, which are centralised (the servers are the centres).

On peer to peer systems, your data – and the algorithms used to analyze and make use of your data – stay in spaces that you own and control. You do not have to beg some corporation to not abuse your data because they don’t have it to begin with.

__Related aspects:__ [zero knowledge](#zero-knowledge), [private by default](#private-by-default)


### Share alike

Most people’s eyes cloud over when technology licenses are mentioned but they’re crucial to protecting your freedom.

Small Technology is licensed under Copyleft licenses. Copyleft licenses stipulate that if you benefit from technology that has been put into the commons, you must share back (“share alike”) any improvements, changes, or additions you make. If you think about it, it’s only fair: if you take from the commons, you should give back to the commons. That’s how we cultivate a healthy commons.

__Related aspects:__ [interoperable](#interoperable), [non-colonial](#non-colonial), [non-commercial](#non-commercial)

### Interoperable

Interoperable systems can talk to one another using well-established protocols. They’re the opposite of silos. Interoperability ensures that different groups can take a technology and evolve it in ways that fit their needs while still staying compatible with other tools that implement the same protocols. Interoperability, coupled with share alike licensing, helps us to distribute power more equally as rich corporations cannot “embrace and extend” commons technology, thereby creating new silos.

Interoperability also means we don’t have to resort to colonialism in design: we can design for ourselves and support other groups who design for themselves while allowing all of us to communicate with each other within the same global network.

__Related aspects:__ [share alike](#share-alike), [non-colonial](#non-colonial)


### Non-commercial

The primary purpose for Small Technology is not to make a profit but to increase human welfare. As such, they are built by not-for-profit organisations. Eventually, we hope that small technologies will be recognised for their contribution to the common good and therefore supported from the commons (e.g., from our taxes). In the interim, some methods for monetising Small Technology include:

  - Charging for hosting and maintenance services
  - Sales on App Stores (for native apps)
  - Donations and patronage
  - Grants and awards

Equity-based / Venture Capital investment is incompatible with Small Technology as the success criterion is the sale of the organisation (either to a larger organisation or to the public at large via an IPO). Small Technology is not about startups (temporary companies designed to either fail fast or grow exponentially and get sold), it’s about _stayups_ (sustainable organisations that contribute to the common good).

__Related aspects:__ [non-colonial](#non-colonial), [share alike](#share-alike), [interoperable](#interoperable)

### Inclusive {#inclusive}

Being inclusive in technology is ensuring people have equal rights and access to the tools we build and the communities who build them, with a particular focus on including people from traditionally marginalised groups. Accessibility is the degree to which technology is usable by as many people as possible, especially disabled people

Small Technology is inclusive and accessible.

With inclusive design, we must be careful not to assume we know what’s best for others, despite us having differing needs. Doing so often results in colonial design, creating patronising and incorrect solutions.

__Related aspects:__ [easy to use](#easy-to-use), [non-colonial](#non-colonial)

{{< fund-us >}}
