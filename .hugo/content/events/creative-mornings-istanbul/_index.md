---
title: "Creative Mornings Istanbul"
date: 2020-06-26T12:00:00Z
publishdate: 2020-07-01T10:05:45Z
eventURL: "https://creativemornings.com/talks/aral-balkan"
eventCity: "Istanbul"
eventCountry: "Turkey (online)"
speaker: "Aral"
---

Aral gave a talk titled ‘[Beyond surveillance capitalism: alternatives, stopgaps, Small Web, and Site.js.](/videos/creative-mornings-istanbul/)’
