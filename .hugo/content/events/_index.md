---
title: "Events"
date: 2018-07-30T14:15:40+01:00
menu:
 main:
  weight: 6
summary: "We regularly speak at international and online events. View our past talks and contact us if you want to book Laura or Aral for your event."
---

# Events

We regularly speak at international and online events. [View our past talks](/videos) and [contact us](/contact-us) if you want to book Laura or Aral for your event.

<!--
## Small is Beautiful

[![Aral Balkan and Laura Kalbag with tin-can telephones to their ears. Multicolour striped pastel background. Title: “Small is beautiful”](/videos/small-is-beautiful-01/small-is-beautiful.jpg)](https://owncast.small-web.org)

Small Is Beautiful was a monthly live stream on small technology, small web, and conversations with others who are working on interesting projects in the area.

Episodes are streamed from our Owncast live stream server at https://owncast.small-web.org.

__Watch <a href='/videos'>past episodes</a>, with transcripts and captions, on <a href='/videos'>the videos page</a>.__ 

### Next Stream

#### Small Web: Back to the Future

{{<figure src="gwbasic_000.png" alt="Screenshot of a pixellated space ship and colourful stars" caption="What’s DOS got to do, got to do with it?" >}}

___Why we do what we do… hint: it’s not so we can go back to the days of GeoCities. It’s so we can go forward differently by taking the best of the personal computer era into the web era.___

A sneak peek at [Aral’s presentation at NewCrafts Paris](https://ncrafts.io/speaker/aralbalkan) next week and a chance for him to rehearse his live-coding setup. We’ll also be covering the latest developments in [Kitten](https://codeberg.org/kitten/app) and [Domain](https://codeberg.org/domain/app).

- __Date:__ Thursday, May 18, 2023
- __Time:__ 5PM Dublin (4PM UTC)
- __Where:__ https://owncast.small-web.org

### Get live stream reminders

To get notified about upcoming streams, [follow our Owncast server](https://owncast.small-web.org) from [the fediverse](https://fediverse.info/) (use the Follow button on our Owncast site).

<style>figcaption { text-align: center; font-weight: bold; }</style>

-->