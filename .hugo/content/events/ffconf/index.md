---
title: "ffconf"
date: 2019-11-08T12:09:42Z
publishdate: 2019-11-08T12:09:42Z
eventURL: "https://2019.ffconf.org"
eventCity: "Brighton"
eventCountry: "UK"
speaker: "Laura"
---

Laura gave a keynote titled [‘8 Unbelievable Things You Never Knew About Tracking’](https://noti.st/laurakalbag/Y4Q95l/8-unbelievable-things-you-never-knew-about-tracking).
