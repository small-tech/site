---
title: "SmashingConf Freiburg 2020"
date: 2020-09-08T13:28:27Z
publishdate: 2020-02-03T13:28:27Z
eventURL: "https://smashingconf.com/freiburg-2020/"
eventCity: "Freiburg"
eventCountry: "Germany (Online)"
speaker: "Laura"
---

Laura gave [an online keynote titled “This One Weird Trick Tells Us Everything About You”](https://noti.st/laurakalbag/z23Us9/this-one-weird-trick-tells-us-everything-about-you).
