---
title: "From Business To Buttons"
date: 2019-05-03T17:49:06+01:00
publishdate: 2019-04-12T17:49:06+01:00
eventURL: "https://frombusinesstobuttons.com"
eventCity: "Stockholm"
eventCountry: "Sweden"
speaker: "Laura"
---

Laura gave a keynote titled ‘[Disruptive Design: Harmful Patterns and Bad Practice](https://noti.st/laurakalbag/6WyKNZ/disruptive-design-harmful-patterns-and-bad-practice).
