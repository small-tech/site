---
title: "Accessibility Scotland: Accessibility and Ethics"
date: 2019-10-25T12:28:00+01:00
publishdate: 2019-08-02T12:28:00+01:00
eventURL: "https://accessibility.scot"
eventCity: "Edinburgh"
eventCountry: "Scotland"
speaker: "Laura"
---

Laura gave a talk titled ‘[Accessible Unethical Technology](https://noti.st/laurakalbag/9GG02Z/slides).’
