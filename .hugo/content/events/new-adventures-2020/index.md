---
title: "New Adventures 2020"
date: 2020-01-23T15:28:45Z
publishdate: 2020-01-02T15:28:45Z
eventURL: "https://newadventuresconf.com/2020/conference/"
eventCity: "Nottingham"
eventCountry: "UK"
speaker: "Laura"
---

Laura gave a keynote titled ‘[Defying the mainstream: building technology that respects our rights](https://noti.st/laurakalbag/firuh2/slides)’.
