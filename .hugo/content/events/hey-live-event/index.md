---
title: "Hey! Live: Using the web for social good"
date: 2020-07-02T12:39:33+01:00
publishdate: 2020-07-06T12:39:33+01:00
eventURL: "https://www.eventbrite.co.uk/e/hey-live-using-the-web-for-social-good-with-laura-kalbag-tickets-110789053040#"
eventCity: "Cork"
eventCountry: "Ireland (Online)"
speaker: "Laura"
---

Laura was interviewed about ‘Using the web for social good’ for Hey! Live.
