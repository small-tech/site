---
title: "UX Australia"
date: 2019-08-27T12:57:10+01:00
publishdate: 2019-04-17T12:57:10+01:00
eventURL: "http://www.uxaustralia.com.au/conferences/ux-australia-2019/program/presentations/"
eventCity: "Sydney"
eventCountry: "Australia"
speaker: "Aral Balkan"
---

Aral gave the opening keynote.
