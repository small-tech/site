---
title: "Loupe 2019"
date: 2019-08-15T18:03:02+01:00
publishdate: 2019-04-26T18:03:02+01:00
eventURL: "https://www.framer.com/loupe/"
eventCity: "Amsterdam"
eventCountry: "Netherlands"
speaker: "Laura"
---

Laura presented a keynote titled ‘[Accessibility For Everyone](https://noti.st/laurakalbag/DnXmgy/slides)’.
