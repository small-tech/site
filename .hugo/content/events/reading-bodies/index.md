---
title: "Reading Bodies! Cruising Corpoliteracy in Arts, Education and Everyday Life"
date: 2019-09-14T12:26:02+01:00
publishdate: 2019-09-09T12:26:02+01:00
eventURL: "https://www.hkw.de/en/programm/projekte/2019/corpoliteracy/corpoliteracy_start.php"
eventCity: "Berlin"
eventCountry: "Germany"
speaker: "Aral"
---

![Image from the event](./reading-bodies/human_rights_tattoo_imgsize_buehne.png)

Aral took part on [a panel on Human/Body/Rights](https://www.hkw.de/en/programm/projekte/veranstaltung/p_158516.php).
