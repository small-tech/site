---
title: "State Of The Browser 2019"
date: 2019-09-14T12:26:02+01:00
publishdate: 2019-08-02T12:26:02+01:00
eventURL: "https://2019.stateofthebrowser.com"
eventCity: "London"
eventCountry: "UK"
speaker: "Laura"
---

Laura gave a talk titled ‘[Accessibility For Everyone](https://noti.st/laurakalbag/K0n0Gl/slides)’.
