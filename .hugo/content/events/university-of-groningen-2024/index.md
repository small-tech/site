---
title: "Small Web at University of Groningen"
date: 2024-06-11T09:00:00Z
publishdate: 2024-11-17T12:00:00Z
eventURL: "https://ar.al/2024/06/24/small-web-computer-science-colloquium-at-university-of-groningen/"
eventCity: "Groningen"
eventCountry: "Netherlands"
---

Aral presented a [computer science colloquium on Small Web](https://ar.al/2024/06/24/small-web-computer-science-colloquium-at-university-of-groningen/).
