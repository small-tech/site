---
title: "Howest 2020"
date: 2020-12-04T14:00:00Z
publishdate: 2020-02-03T13:28:27Z
eventURL: "https://localhost/videos/towards-a-small-web-howest-2020/"
eventCity: "Cork"
eventCountry: "Ireland (Online)"
speaker: "Aral"
---

Aral gave [an online keynote titled “Towards a Small Web”](/videos/towards-a-small-web-howest-2020/).
