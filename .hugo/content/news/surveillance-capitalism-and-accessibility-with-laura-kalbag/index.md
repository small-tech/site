---
title: "Thundernerds Podcast Episode 162 – 🖥️ Surveillance Capitalism & Accessibility with Laura Kalbag"
date: 2018-10-22T12:54:19+01:00
categories: ["Updates"]
author: "Thundernerds"
image: ""
postURL: "https://www.thundernerds.io/2018/10/surveillance-capitalism-accessibility-w-laura-kalbag/"
sourceURL: "https://www.thundernerds.io"
sourceName: "the Thundernerds podcast"
---

In this episode we get to speak with Laura Kalbag: Author, Co-Founder and Designer at Indie. We chat with Laura about her residence in Ireland, and the the current European political climate with regards to Brexit. We then dive into Laura’s company Indie, and their product Better;  Better is a privacy tool for Safari on iPhone, iPad, and Mac that protects from behavioural ads and companies that track and profile on the web. We also discuss the state of surveillance capitalism as a whole, and what we can do about it.