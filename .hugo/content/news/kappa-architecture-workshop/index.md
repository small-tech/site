---
title: "Kappa Architecture Workshop"
date: 2018-12-15T18:11:54Z
categories: ["Labs"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2018/12/15/kappa-architecture-workshop/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Kappa Architecture Workshop is an excellent online resource by Stephen Whitmore (of Cabal fame), Mathias Buus (one of the cornerstones of the DAT Project), et al., that gives you an introduction to Kappa Architecture using modules from the DAT Node.js ecosystem like hypercore, multifeed, discovery-swarm and kappa-core…

You can follow along with the workshop online, view my working files as I do, and also submit any issues you may run into or improvements you might want to suggest on the workshop’s source code repository.
