---
title: "A simple Node transform stream with the new Node 10 pipeline() function"
date: 2019-12-20T15:25:33Z
categories: ["Labs"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2018/12/20/a-simple-node-transform-stream-with-the-new-node-10-pipeline-function/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

As part of my research for Hypha, I just completed the Kappa Architecture Workshop and I’m continuing to dive deeper down the stack to brush up on the fundamental concepts I need to be comfortable with going forward.

Next up is Node streams.

Streams are used everywhere in Node and while I’ve made extensive use of them in the past, there’s always been parts that seemed magical. I don’t really grok them and I’m working to change that.
