---
title: "Owning and controlling my own content"
date: 2018-07-17T13:59:13+01:00
categories: ["Updates"]
author: "Laura Kalbag"
image: ""
postURL: "https://laurakalbag.com/owning-and-controlling-my-own-content/"
sourceURL: "https://laurakalbag.com"
sourceName: "Laura’s blog"
---

One of the ultimate goals we have at Ind.ie is owning and controlling our own data. That means I want to have ownership and control over my own personal information, rather than it being in the hands of big corporations. My personal information could range from something as intensely private as my medical information, or my private messages with another person.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->