---
title: "Mentoring the Eastern Partnership Civil Society Online Hackathon"
date: 2020-07-09T09:30:50+01:00
categories: ["Updates"] # Updates or Labs
author: "Aral Balkan" # Aral Balkan or Laura Kalbag (or another writer if article etc)
image: "eastern-partnership-civil-society-online-hackathon.jpg" # file name relative to this page
postURL: "https://ar.al/2020/07/09/mentoring-the-eastern-partnership-civil-society-online-hackathon/" # URL for the post
sourceURL: "https://ar.al/" # URL for the site the post is on
sourceName: "Aral’s blog" # Name of the site the post is on
---

Laura and I will be mentoring teams developing civil society applications at the Eastern Partnership Civil Society Online Hackathon.