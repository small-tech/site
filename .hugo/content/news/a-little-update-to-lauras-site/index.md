---
title: "A little site update"
date: 2021-02-10T16:48:34Z
categories: ["Updates"] # Updates or Labs
author: "Laura Kalbag" # Aral Balkan or Laura Kalbag (or another writer if article etc)
postURL: "https://laurakalbag.com/a-little-site-update/" # URL for the post
sourceURL: "https://laurakalbag.com/" # URL for the site the post is on
sourceName: "Laura’s blog" # Name of the site the post is on
---

In our Small is Beautiful #2 livestream, I spoke about how I spent a lot of last year creating a Hugo starter theme for Site.js. We decided to move in a different direction with Site.js, but I’d built so much cool stuff into this new theme (please let me be pleased with myself for once!) it seemed a waste to not make use of it.