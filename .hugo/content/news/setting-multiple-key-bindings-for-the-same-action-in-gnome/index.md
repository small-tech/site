---
title: "Setting multiple key bindings for the same action in GNOME"
date: 2019-03-11T09:25:39Z
categories: ["Labs"]
author: "Aral Balkan"
postURL: "https://ar.al/2019/03/11/setting-multiple-key-bindings-for-the-same-action-in-gnome/"
sourceURL: "https://ar.al/"
sourceName: "Aral’s blog"
---

In GNOME, you can only set one key binding for a given action using the Settings app (under Devices → Keyboard) even though the settings data structure itself accepts an array.

You can, however, set multiple key bindings per action by installing the dconf Editor app or through the command-line using the `gsettings` command.
