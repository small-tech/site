---
title: "What does a private communicator look like?"
date: 2018-12-23T15:23:56Z
categories: ["Updates", "Labs"]
author: "Aral Balkan"
image: "https://ar.al/2018/12/23/what-does-a-private-communicator-look-like/snap-on-air-lora-communicator.jpg"
postURL: "https://ar.al/2018/12/23/what-does-a-private-communicator-look-like/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Hypha is not about building a single product. It’s about exploring the possibilities and problem domain of private communication and what it means to have technology that enables privacy (and therefore personhood). At the same time, it isn’t about designing from the inside out. It’s not about building the protocols and waiting for the tools to happen. It’s about experimenting with both. And, in the process, hopefully sparking one or more everyday things that enable people to communicate with privacy.

So what could some of those everyday things be?
