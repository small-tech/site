---
title: "The quest to design an ethical social media platform"
date: 2018-11-03T16:50:27Z
categories: ["Updates"]
author: "Jennifer Johnson"
image: ""
postURL: "https://theweek.com/articles/799415/quest-design-ethical-social-media-platform"
sourceURL: "https://theweek.com"
sourceName: "The Week"
---

Ads are the traditional funding source for social platforms; they take users' personal data and serve it to advertisers who want their ads to reach a specific audience. This virtually ensures a fundamentally exploitative business model based on surveillance, says Laura Kalbag, a designer and the co-founder of digital justice not-for-profit Ind.ie.