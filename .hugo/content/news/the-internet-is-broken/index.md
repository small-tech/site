---
title: "The internet is broken"
date: 2017-12-19T14:15:48+01:00
categories: ["Updates"]
author: "David Baker"
image: ""
postURL: "https://www.wired.co.uk/article/is-the-internet-broken-how-to-fix-it"
sourceURL: "https://wired.co.uk"
sourceName: "Wired"
---

Speaking over Skype from a coffee shop in Malmö, Sweden, Aral Balkan is advocating a grassroots revolution against the dominance of such supernodes. The founder of ind.ie, a social enterprise dedicated to building a new internet, he sees the future internet as one in which individuals retain control over their data and how they benefit from it.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->