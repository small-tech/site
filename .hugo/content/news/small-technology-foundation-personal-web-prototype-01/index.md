---
title: "Small Technology Foundation Personal Web Prototype-01: a mobile personal web server"
date: 2019-11-14T09:49:36Z
categories: ["Labs"]
author: "Aral Balkan"
image: "prototype-01-front.jpg"
postURL: "https://ar.al/2019/11/11/small-technology-foundation-personal-web-prototype-01-a-mobile-portable-personal-web-server/"
sourceURL: "https://ar.al/"
sourceName: "Aral’s blog"
---

Imagine holding your personal web site in the palm of your hand. Imagine carrying the digital aspects of your self in your pocket instead of having them on some abstract cloud under the watchful eye of some faceless multinational corporation.
