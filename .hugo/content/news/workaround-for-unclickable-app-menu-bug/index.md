---
title: "Workaround for unclickable app menu bug with window.makeKeyAndOrderFront and NSApp.activate on macOS"
date: 2018-09-17T18:14:04+01:00
categories: ["Labs"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2018/09/17/workaround-for-unclickable-app-menu-bug-with-window.makekeyandorderfront-and-nsapp.activate-on-macos/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

To open a window and make it appear on top of other windows on macOS, you can do the following:

`myWindow.makeKeyAndOrderFront(nil)
NSApp.activate(ignoringOtherApps: true)`

This works to show the window, make it topmost and also display the app menu. The problem is that the app menu is not clickable until you tab away from the application and back again.