---
title: "The Do’s and Don’ts of Tech Regulation"
date: 2019-05-12T11:26:55+01:00
categories: ["Updates"]
author: "Aral Balkan"
postURL: "https://ar.al/2019/05/11/the-dos-and-donts-of-tech-regulation/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Yesterday, I was interviewed on Al Jazeera news about the Macron-Zuckerberg meeting (Warning: the linked page on Al Jazeera includes privacy-eroding trackers, including Facebook.)1 in which they apparently decided on a framework of “co-regulation.”

This is not what I mean when I talk about the need to regulate surveillance capitalists.
