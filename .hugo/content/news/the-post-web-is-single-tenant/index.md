---
title: "The post-Web is single tenant"
date: 2019-01-09T15:01:11Z
categories: ["Updates", "Labs"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2019/01/09/the-post-web-is-single-tenant/"
sourceURL: "https://ar.al/"
sourceName: "Aral’s blog"
---

Hypha is an exploration of what personal technology means in the digital/networked age. The goal is to create a bridge from the Mainframe 2.0 era to the Peer Computing (PC 2.0) era1. When we talk about scale in peer computing, our focus is on creating systems that are human-scale.
