---
title: "Have you heard about Silicon Valley’s unpaid research and development department? It’s called the EU."
date: 2019-06-22T16:58:56+01:00
categories: ["Updates"]
author: "Aral Balkan"
image: "https://ar.al/2019/06/22/have-you-heard-about-silicon-valleys-unpaid-research-and-development-department-its-called-the-eu/spiderman-pointing-meme.jpg"
postURL: "https://ar.al/2018/10/04/setting-up-hiawatha-web-server-on-ubuntu-16.04/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Who should you thank for Facebook’s Libra?

- “One of the UK’s leading privacy researchers”
- University College London
- The DECODE project

And, if you’re an EU citizen who pays their taxes,

-You.

Surprised? Don’t be.
