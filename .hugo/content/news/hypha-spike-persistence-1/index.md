---
title: "Hypha Spike: Persistence 1"
date: 2019-02-12T13:50:27Z
categories: ["Labs"]
author: "Aral Balkan"
postURL: "https://ar.al/2019/02/12/hypha-spike-persistence-1/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Following on from Hypha Spike: Multiwriter 2 this spike aims to:

- Use persistent storage
- Evolve the sign up / sign in processes accordingly
