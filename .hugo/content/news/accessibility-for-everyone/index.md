---
title: "Accessibility for Everyone"
date: 2017-09-26T13:49:49+01:00
categories: ["Updates"]
author: "Aral Balkan"
image: ""
postURL: "https://2018.ar.al/notes/accessibility-for-everyone"
sourceURL: "https://2018.ar.al/notes/accessibility-for-everyone"
sourceName: "Aral’s blog"
---

For the past three years, Laura Kalbag has been writing a book on accessibility. It’s called Accessibility for Everyone. You should read it.
