---
title: "Remember directory from last session in Tilix with zsh"
date: 2018-12-15T18:10:27Z
categories: ["Labs"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2018/12/15/remember-directory-from-last-session-in-tilix-with-zsh/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

If you’re using the excellent Tilix terminal with the wonderful zsh shell (in my case, via oh-my-zsh), you might notice that opening a new session (e.g., splitting your current session vertically or horizontally or opening a new window), doesn’t start you at the directory you were in in the previous one as you would expect but rather returns you to your home directory.

I’ve been getting increasingly fed up with this but not so much that I actually felt bothered enough to do something about it. Until today.
