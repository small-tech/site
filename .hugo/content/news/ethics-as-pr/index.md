---
title: "Ethics as PR (or the ‘Some Very Good People Work There!’ Fallacy)"
date: 2021-05-08T16:38:45+01:00
categories: ["Updates"] # Updates or Labs
author: "Aral Balkan" # Aral Balkan or Laura Kalbag (or another writer if article etc)
postURL: "https://ar.al/2021/05/08/ethics-as-pr-or-the-some-very-good-people-work-there-fallacy/" # URL for the post
sourceURL: "https://ar.al/" # URL for the site the post is on
sourceName: "Aral’s blog" # Name of the site the post is on
---

There’s been a lot of research into “Ethics in AI” recently and it’s being sponsored and led by… *checks notes*… surveillance capitalists like DeepMind.

Similarly, lots of people are working on “Ethics in Health” at Philip Morris and “Ethics in Environmentalism” at ExxonMobil.

OK, I made the last two up.

Did you notice?

(I guess it was pretty blatant.)

Because, clearly, that would be ethicswashing.

So how come we accept the first?