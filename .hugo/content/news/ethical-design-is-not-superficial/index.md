---
title: "Ethical design is not superficial"
date: 2019-01-22T14:47:14Z
categories: ["Updates"]
author: "Laura Kalbag"
image: "https://laurakalbag.com/ethical-design-is-not-superficial/panel_hu5c5ff45e1fa8d53304cfbf780f7ac0e9_1217573_1500x0_resize_q100_gaussian.jpg"
postURL: "https://laurakalbag.com/ethical-design-is-not-superficial/"
sourceURL: "https://laurakalbag.com"
sourceName: "Laura’s blog"
---

We are seeing more and more organisations starting to talk about ethical design. Unfortunately I suspect this has less to do with caring about the impact of unethical design on society, democracy and the environment, and more to do with organisations attempting to distance themselves from similar businesses who are finding it impossible to continue to disguise their toxicity.
