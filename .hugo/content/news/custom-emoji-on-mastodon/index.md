---
title: "Custom Emoji on Mastodon"
date: 2018-09-04T11:50:21+01:00
categories: ["Labs"]
author: "Laura Kalbag"
image: "https://laurakalbag.com/images/2018/09/toot-on-dark.png"
postURL: "https://laurakalbag.com/custom-emoji-on-mastodon/"
sourceURL: "https://laurakalbag.com"
sourceName: "Laura’s blog"
---

One of the many cool features of Mastodon is that you can create custom emoji for your instances which can be duplicated and adopted by other instances. This morning I made an :indieHeart: custom emoji for Mastodon. Here are some things I learned.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->