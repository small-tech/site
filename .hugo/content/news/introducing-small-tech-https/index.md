---
title: "Introducing @small-tech/https"
date: 2019-11-08T12:16:34Z
categories: ["Labs"]
author: "Aral Balkan"
image: "image.jpg"
postURL: "https://ar.al/2019/11/08/introducing-small-tech-https-a-batteries-included-drop-in-replacement-for-the-node.js-https-module/" # URL for the post
sourceURL: "https://ar.al" # URL for the site the post is on
sourceName: "Aral’s blog" # Name of the site the post is on
---

Introducing @small-tech/https, a batteries-included drop-in replacement for the Node.js https module.
