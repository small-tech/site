---
title: "“Apple Says ‘No!’” and what that means for the future of Better Blocker following our move to Ireland"
date: 2020-01-13T12:14:30Z
categories: ["Updates"]
author: "Aral Balkan"
image: "important-message.png"
postURL: "https://ar.al/2020/01/13/apple-says-no-and-what-that-means-for-the-future-of-better-blocker-following-our-move-to-ireland/"
sourceURL: "https://ar.al/"
sourceName: "Aral’s blog"
---

In my previous blog post, I asked Apple for a little help with a problem we had with Better Blocker after moving to Ireland and setting up a new not-for-profit here…

Last week, we got a definite “computer says no” from Apple’s “developer support.”
