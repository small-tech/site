---
title: "Can Facebook be forced to comply with privacy laws?"
date: 2018-11-29T16:53:00Z
categories: ["Updates"]
author: "Al Jazeera Inside Story"
image: ""
postURL: "https://www.aljazeera.com/programmes/insidestory/2018/11/facebook-forced-comply-privacy-laws-181128171228326.html"
sourceURL: "https://www.aljazeera.com/programmes/insidestory"
sourceName: "Al Jazeera’s Inside Story"
---

‘Facebook is accused of undermining democratic institutions, but its CEO fails to face up to MPs at a hearing in London.’

Al Jazeera Inside Story featuring @aral.