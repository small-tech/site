---
title: "Indie Web Server 7.1.0: Launch a live secure static site with a single command"
date: 2019-04-01T18:48:35+01:00
categories: ["Labs"]
author: "Aral Balkan"
image: "https://ar.al/2019/04/01/indie-web-server-7.1.0-launch-a-live-secure-static-site-with-a-single-command/indie-web-server-live.png"
postURL: "https://ar.al/2019/04/01/indie-web-server-7.1.0-launch-a-live-secure-static-site-with-a-single-command/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

You have: a VPS with a domain name pointing to it and Node.js installed.

You want: to deploy a secure, live static site.

You do: `npm i -g @ind.ie/web-server && web-server --live`

Hit your domain name in a browser.
