---
title: "Where I’m speaking in Summer/Autumn 2019"
date: 2019-08-07T11:38:02+02:00
categories: ["Updates"]
author: "Laura Kalbag"
postURL: "https://laurakalbag.com/speaking-in-summer-autumn-2019/"
sourceURL: "https://laurakalbag.com"
sourceName: "Laura’s blog"
---

This summer/autumn, I’m speaking at conferences in Amsterdam, London and Edinburgh. This year I’ve had mixed experiences with speaking, so I’m being picky about future talks. So I’m looking forward to the following…
