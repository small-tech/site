---
title: "What is the Small Web?"
date: 2020-08-07T17:11:14+01:00
categories: ["Updates"] # Updates or Labs
author: "Aral Balkan" # Aral Balkan or Laura Kalbag (or another writer if article etc)
image: "" # file name relative to this page
postURL: "https://ar.al/2020/08/07/what-is-the-small-web/" # URL for the post
sourceURL: "https://ar.al/" # URL for the site the post is on
sourceName: "Aral’s blog" # Name of the site the post is on
---

Today, I want to introduce you to a concept – and a vision for the future of our species in the digital and networked age – that I’ve spoken about for a while but never specifically written about:

The Small Web.

To understand what the Small Web is, let’s compare it to the Big Web. In other words, to the centralised Web we have today.