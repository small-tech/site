---
title: "Cross-posting using meta information"
date: 2018-07-25T14:03:14+01:00
categories: ["Labs"]
author: "Laura Kalbag"
image: ""
postURL: "https://laurakalbag.com/crossposting-using-meta/"
sourceURL: "https://laurakalbag.com"
sourceName: "Laura’s blog"
---

Writing a few blog posts last week, and cross-posting them in various locations, got me thinking about how meta information can be reused. I started looking into the meta information in the `<head>` of each page, and how best to format it so that social networks (Twitter, Facebook, messaging apps…) would pick up on titles, descriptions and images. That way, when I share a URL from my site, if the site/app has the functionality, they can expand that URL into a preview including a nicely-formatted title, summary, and image.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->