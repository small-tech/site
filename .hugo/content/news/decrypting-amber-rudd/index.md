---
title: "Decrypting Amber Rudd"
date: 2017-08-01T13:48:31+01:00
categories: ["Updates"]
author: "Aral Balkan"
image: ""
postURL: "https://2018.ar.al/notes/decrypting-amber-rudd"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Facebook, Microsoft, Twitter, and YouTube (Google/Alphabet, Inc) have formed the Global Internet Forum to Counter Terrorism and Amber Rudd is asking them to quietly drop end-to-end encryption from their products. You should not believe a single word any of those companies tells you about end-to-end encryption or privacy on their platforms ever again. PS. WhatsApp is owned by Facebook.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->