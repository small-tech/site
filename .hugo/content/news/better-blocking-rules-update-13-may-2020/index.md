---
title: "Better Blocking Rules Update 13th May 2020"
date: 2020-05-13T12:44:51+01:00
categories: ["Updates"] # Updates or Labs
author: "Laura Kalbag" # Aral Balkan or Laura Kalbag (or another writer if article etc)
postURL: "https://better.fyi/news/"
sourceURL: "https://better.fyi"
sourceName: "the Better site"
---

New blocking rules! On 13th May 2020, Laura blocked one blocker blockers, fixed five sites and blocked two new trackers.

The rules will auto-update if the app is running. If it’s not, you can get the new rules by choosing ‘Update rules’ or ‘Check again now’ from Better’s menu.