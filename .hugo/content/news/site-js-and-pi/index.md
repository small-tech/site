---
title: "Site.js and Pi"
date: 2019-10-18T14:29:46Z
categories: ["Labs"]
author: "Aral Balkan"
image: "image.jpg"
postURL: "https://ar.al/2019/10/18/site.js-and-pi/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Yesterday, I released Site.js 12.8.0 which brings initial ARM support for Linux.

What that means is that it’s now easier than ever to get a static or dynamic (Node.js) web server up and running on a Raspberry Pi.
