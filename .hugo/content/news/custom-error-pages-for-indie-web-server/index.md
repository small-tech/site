---
title: "Custom error pages for Indie Web Server"
date: 2019-03-30T18:46:58+01:00
categories: ["Labs"]
author: "Aral Balkan"
image: "https://ar.al/2019/03/30/custom-error-pages-for-indie-web-server/custom-404.png"
postURL: "https://ar.al/2019/03/30/custom-error-pages-for-indie-web-server/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

I just released Indie Web Server version 6.3.0 with new default 404 and 500 error pages and support for custom ones.
