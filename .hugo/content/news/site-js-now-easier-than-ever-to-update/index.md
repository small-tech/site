---
title: "Site.js: now easier than ever to update"
date: 2019-10-21T14:30:11Z
categories: ["Labs"]
author: "Aral Balkan"
postURL: "https://ar.al/2019/10/21/site.js-now-easier-than-ever-to-update/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

I just released Site.js version 12.9.5. This version brings with it some new commands, the most important of which is the `update` command.
