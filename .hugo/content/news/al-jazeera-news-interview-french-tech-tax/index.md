---
title: "Al Jazeera News interview: French “tech tax”"
date: 2019-01-01T15:16:24Z
categories: ["Updates"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2019/01/01/al-jazeera-news-interview-french-tech-tax/"
sourceURL: "https://ar.al/"
sourceName: "Aral’s blog"
---

I did a live interview on Al Jazeera News today on France pushing forward alone with a new tax on big tech companies.
