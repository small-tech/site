---
title: "Gnomit Flatpak bundle"
date: 2018-08-20T13:01:22+01:00
categories: ["Labs"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2018/08/15/introducing-gnomit-a-simple-git-commit-message-editor-for-gnome/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Gnomit, my simple Git commit message editor for Gnome, is now available as a Flatpak bundle.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->