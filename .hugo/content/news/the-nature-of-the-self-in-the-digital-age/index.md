---
title: "The Nature of the Self in the Digital Age"
date: 2016-03-03T13:28:31+01:00
categories: ["Updates"]
author: "Aral Balkan"
image: ""
postURL: "https://2018.ar.al/notes/the-nature-of-the-self-in-the-digital-age"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

This is the original English version of an op-ed I wrote for Zeit Online.

<!-- KEY TO FRONT MATTER:
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->