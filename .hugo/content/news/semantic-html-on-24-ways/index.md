---
title: "Semantic HTML on 24 Ways"
date: 2017-12-22T14:05:28+01:00
categories: ["Labs"]
author: "Laura Kalbag"
image: ""
postURL: "https://laurakalbag.com/semantic-html-on-24-ways/"
sourceURL: "https://laurakalbag.com"
sourceName: "Laura’s blog"
---

It’s been a few years since I wrote something for 24ways, but this year I’m back, making the case for semantic HTML. As I mention in the article, I see a lot of redundant and unsemantic HTML while I’m working on Better. HTML is often seen as easy, and therefore not worth learning in detail. It’s frustrating because HTML is one of the few completely unavoidable technologies of the web.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->