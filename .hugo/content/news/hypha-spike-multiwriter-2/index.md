---
title: "Hypha Spike: Multiwriter 2"
date: 2019-02-01T10:18:27Z
categories: ["Labs"]
author: "Aral Balkan"
image: "https://ar.al/2019/02/01/hypha-spike-multiwriter-2/authorisation-request.jpeg"
postURL: "https://ar.al/2019/02/01/hypha-spike-multiwriter-2/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Following on from Hypha Spike: Multiwriter 1 this spike aims to:

- Simplify the node authorisation flow to a simple authorisation alert on already-authorised nodes.
