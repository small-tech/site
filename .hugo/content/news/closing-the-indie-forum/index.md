---
title: "Closing the Ind.ie forum"
date: 2020-01-13T12:17:52Z
categories: ["Updates"]
author: "Laura Kalbag"
postURL: "https://laurakalbag.com/closing-the-indie-forum/"
sourceURL: "https://laurakalbag.com"
sourceName: "Laura’s blog"
---

Over the last few days, we closed the Ind.ie forum. It had been a fun experiment for 4.5 years, but didn’t really fit our aims to create decentralised technology. We didn’t want to shun other big platforms in favour of creating our own big platform, it would defeat the point!
