---
title: "Apple is trying to redefine what it means to violate your privacy. We must not let it."
date: 2021-08-08T16:48:48+01:00
categories: ["Updates"] # Updates or Labs
author: "Aral Balkan"
postURL: "https://ar.al/2021/08/08/apple-is-trying-to-redefine-what-it-means-to-violate-your-privacy-we-must-not-let-it/" # URL for the post
sourceURL: "https://ar.al/" # URL for the site the post is on
sourceName: "Aral’s blog" # Name of the site the post is on
---

If Apple goes ahead with its plans to have your devices violate your trust and work against your interests, I will not write another line of code for their platforms ever again…