---
title: "New Kitten features: Interactive Shell (REPL), Multi-page Settings, and backup and restore (data portability)"
date: 2024-05-23T19:11:00
categories: ["Updates"] # Updates or Labs
author: "Aral Balkan"
postURL: "https://ar.al/2024/05/23/new-kitten-features-interactive-shell-repl-multi-page-settings-and-backup-and-restore-data-portability/" # URL for the post
sourceURL: "https://ar.al/2024/05/23/new-kitten-features-interactive-shell-repl-multi-page-settings-and-backup-and-restore-data-portability/" # URL for the site the post is on
sourceName: "Aral’s blog" # Name of the site the post is on
---

In this video, Aral demonstrates the new features added to Kitten in May: including the new Interactive Shell (REPL), Multi-page Settings, and backup and restore (data portability).
