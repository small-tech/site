---
title: "On the General Architecture of the Peer Web (and the placement of the PC 2.0 era within the timeline of general computing and the greater socioeconomic context)"
date: 2019-02-13T15:59:21Z
categories: ["Updates", "Labs"]
author: "Aral Balkan"
image: "https://ar.al/2019/02/13/on-the-general-architecture-of-the-peer-web/eras-of-computing.jpeg"
postURL: "https://ar.al/2019/02/13/on-the-general-architecture-of-the-peer-web/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

The first era of general computing was the mainframe era and it was centralised. The second, the personal computing (PC 1.0) era, was decentralised. The third is the Web era (let’s call it Mainframe 2.0). And it is, once again, centralised. Today, we find ourselves at the end of the Mainframe 2.0 era and on the precipice of a new chapter in the history of general computing: the Peer Web. Let’s call it the Personal Computing 2.0 (PC 2.0) era.
