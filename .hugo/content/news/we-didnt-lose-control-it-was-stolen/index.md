---
title: "We didn’t lose control – it was stolen"
date: 2017-03-12T13:46:49+01:00
categories: ["Updates"]
author: "Aral Balkan"
image: ""
postURL: "https://2018.ar.al/notes/we-didnt-lose-control-it-was-stolen"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

The Web we have is not broken for Google and Facebook. People farmers are reaping the rewards of their violations into our lives to the tune of tens of billions in revenue every year. How can they possibly be our allies?

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->