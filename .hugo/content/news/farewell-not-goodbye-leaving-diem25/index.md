---
title: "Farewell, not goodbye: leaving DiEM25 (or “We need to talk about democracy, transparency, feminism, and Assange.”)"
date: 2017-12-29T13:51:12+01:00
categories: ["Updates"]
author: "Aral Balkan"
image: ""
postURL: "https://2018.ar.al/notes/farewell-not-goodbye"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

This is not a post I ever wanted to write but it’s time to tie up loose ends before starting the new year. I’ve left DiEM25 and I’d like to explain to you why that is and what I hope DiEM will do differently in the future if it is to realise its tremendous potential to be a force for good in the world.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->