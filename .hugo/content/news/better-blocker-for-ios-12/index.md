---
title: "Better Blocker for iOS 12"
date: 2018-09-24T18:10:09+01:00
categories: ["Updates"]
author: "Aral Balkan"
image: ""
postURL: ""
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Better Blocker for iOS 12 is now available on the App Store.

Better Blocker is our content blocker for iOS and macOS that protects you from privacy-eroding surveillance-based behavioural advertising (adtech) and other trackers. It also removes ads that negatively impact your Web experience.

This release is a major redesign. We’ve worked hard to radically simplify the app.