---
title: "Hypha Spike: WebRTC 1"
date: 2019-01-20T14:49:40Z
categories: ["Labs"]
author: "Aral Balkan"
image: "https://ar.al/2019/01/20/hypha-spike-webrtc-1/webrtc-browser.jpeg"
postURL: "https://ar.al/2019/01/20/hypha-spike-webrtc-1/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Following on from (and in conjunction with) the Hypha Spike: DAT 1, this spike aims to:

- Replicate a hypercore from browser-to-browser using WebRTC.
