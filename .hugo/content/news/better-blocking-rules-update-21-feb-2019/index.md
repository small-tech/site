---
title: "Better Blocking Rules Update 21st February 2019"
date: 2019-02-21T15:51:56Z
categories: ["Updates"]
author: "Laura Kalbag"
image: ""
postURL: "https://better.fyi/news/"
sourceURL: "https://better.fyi"
sourceName: "the Better site"
---

New blocking rules! On 21st February 2019, Laura blocked one blocker blocker, fixed one site, and blocked four new trackers.