---
title: "Episode 4 of the Elastic Brand Podcast with Laura Kalbag"
date: 2019-02-05T13:52:39Z
categories: ["Updates"]
author: "Liz Elcoate"
postURL: "http://theelasticbrand.com/episode/ep-4-laura-kalbag/"
sourceURL: "http://theelasticbrand.com"
sourceName: "The Elastic Brand podcast’s website"
---

In this episode Laura and Liz discuss

- Ethical brand design
- Toxic technology and dark patterns
- The need for tech industry diversity.
- Accessibility and inclusivity.
- How we can be more ethical as designers.
- How we need to make sure we are working with ethical companies
