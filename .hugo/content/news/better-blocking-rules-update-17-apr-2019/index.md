---
title: "Better Blocking Rules Update 17th April 2019"
date: 2019-04-17T11:45:38+01:00
categories: ["Updates"]
author: "Laura Kalbag"
postURL: "https://better.fyi/news/"
sourceURL: "https://better.fyi"
sourceName: "the Better site"
---

New blocking rules! On 17th April 2019, Laura blocked first-party adtech on one site and blocked nine new trackers.

To get the new rules, open the app on iOS, or choose ‘Update rules’ from the menu on macOS.
