---
title: "Dear Apple, a little help here? How hard can it be to move our developer account to our new not-for-profit?"
date: 2020-01-02T12:11:41Z
categories: ["Updates"]
author: "Aral Balkan"
image: "computer-says-no.jpg"
postURL: "https://ar.al/2020/01/02/dear-apple-a-little-help-here-how-hard-can-it-be-to-move-our-developer-account-to-our-new-not-for-profit/"
sourceURL: "https://ar.al/"
sourceName: "Aral’s blog"
---

Dear Apple,

We’re a tiny two-person not-for-profit. We used to be based in the UK, where we were known as Ind.ie (and incorporated as a not-for-profit called Article 12). We left the UK (for reasons) and now we have a not-for-profit here in Ireland called Small Technology Foundation. It’s still just Laura and me (and our huskamute, Oskar).

We have an app called Better Blocker that’s on sale on the iOS and macOS app stores. Some months, the revenue we get from it helps us cover the rent. Better is listed on your Get Started: Safari Extensions page and has three years of history on the App Store.

The problem is that we are now faced with having to take it off the App Store as we don’t seem to have a way to migrate our developer account to our new not-for-profit. We also tried setting up a new developer account for Small Technology Foundation and moving Better over to it but we cannot since Better uses iCloud and apps that use iCloud cannot be moved between developer accounts.
