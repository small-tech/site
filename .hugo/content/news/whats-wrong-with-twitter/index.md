---
title: "What’s wrong with Twitter?"
date: 2018-09-05T17:04:13+01:00
categories: ["Updates"]
author: "Laura Kalbag"
image: ""
postURL: "https://laurakalbag.com/what-is-wrong-with-twitter/"
sourceURL: "https://laurakalbag.com"
sourceName: "Laura’s blog"
---

A couple of weeks ago I posted a quick note mentioning I now have my own Mastodon instance. But first things first: why?

My aim is to use Mastodon as an alternative to Twitter. While Mastodon is not equivalent to Twitter, many of its features are similar. And I’m looking for an alternative to Twitter because Twitter is not good for me.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->