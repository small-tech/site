---
title: "Surveillance capitalism has led us into a dystopia"
date: 2018-10-18T10:00:08+01:00
categories: ["Updates"]
author: "Aral Balkan"
image: "https://ind.ie/images/bbcideas.jpg"
postURL: "https://www.bbc.com/ideas/videos/surveillance-capitalism-has-led-us-into-a-dystopia/p06p0tdy"
sourceURL: "https://www.bbc.com"
sourceName: "BBC Ideas"
---

In this opinion piece, activist Aral Balkan says we're living in a world where data companies have become factory farms for human beings.