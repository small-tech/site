---
title: "Hypha Spike: Aspect Setup 1"
date: 2019-01-10T14:56:40Z
categories: ["Labs"]
author: "Aral Balkan"
image: "https://ar.al/2019/01/10/hypha-spike-aspect-setup-1/hypha-aspect-setup.jpeg"
postURL: "https://ar.al/2019/01/10/hypha-spike-aspect-setup-1/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Your identity – your self – is a sharded aggregate of information. For an organism to have integrity it must have ownership and control over the aggregate of these various elemental shards that, combined, constitute its being.

In Hypha, I will call these shards aspects.
