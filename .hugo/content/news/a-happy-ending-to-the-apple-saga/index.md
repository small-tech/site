---
title: "A happy ending to the Better Blocker saga"
date: 2020-01-20T10:38:14Z
categories: ["Updates"]
author: "Aral Balkan"
image: "little-britain-complaint-form.jpg"
postURL: "https://ar.al/2020/01/20/a-happy-ending-to-the-better-blocker-saga/"
sourceURL: "https://ar.al/"
sourceName: "Aral’s blog"
---

TL; DR: Apple have been in touch and offered us a way to migrate to our new not-for-profit without impacting the experience of existing Better Blocker customers.
