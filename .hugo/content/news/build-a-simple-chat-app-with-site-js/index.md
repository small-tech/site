---
title: "Build a simple chat app with Site.js"
date: 2019-10-11T14:29:30Z
categories: ["Labs"]
author: "Aral Balkan"
postURL: "https://ar.al/2019/10/11/build-a-simple-chat-app-with-site.js/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

This weekend, I released Site.js version 12.7.0 with improvements to its WebSocket interface. Today, I want to take you step-by-step through building and running a basic chat app using Site.js.

It’s much easier than you think, so fire up a terminal window, grab your code editor, and let’s get started!
