---
title: "Like It or Not, We're Already Cyborgs"
date: 2019-06-19T10:27:37+01:00
categories: ["Updates"]
author: "S.C. Stuart"
postURL: "https://www.pcmag.com/news/368457/like-it-or-not-were-already-cyborgs"
sourceURL: "https://www.pcmag.com"
sourceName: "PCMag"
---

To privacy activists Aral Balkan and Laura Kalbag, we don't need brain implants to become cyborgs; we're already jacked in. And we need a Universal Declaration of Cyborg Rights.
