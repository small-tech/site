---
title: "Indie Web Server 8.1.1: Reverse proxy (local mode)"
date: 2019-04-18T17:35:58+01:00
categories: ["Labs"]
author: "Aral Balkan"
image: "https://ar.al/2019/04/18/indie-web-server-8.1.1-reverse-proxy-local-mode/reverse-proxy.jpeg"
postURL: "https://ar.al/2019/04/18/indie-web-server-8.1.1-reverse-proxy-local-mode/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

I just released Indie Web Server version 8.1.1 which introduces a reverse proxy feature.
