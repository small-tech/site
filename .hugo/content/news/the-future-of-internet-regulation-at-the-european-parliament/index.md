---
title: "The Future of Internet Regulation at the European Parliament"
date: 2019-11-29T15:24:31Z
categories: ["Updates"]
author: "Aral Balkan"
postURL: "https://ar.al/2019/11/29/the-future-of-internet-regulation-at-the-european-parliament/"
sourceURL: "https://ar.al/"
sourceName: "Aral’s blog"
---

Last week, I gave three talks in Belgium, starting with one titled “Dear regulators, please don’t throw the baby out with the bathwater” at an event on The Future of Internet Regulation organised by the Greens and Pirates at the European Parliament on Tuesday.

You can watch it via the Peertube embed, above [on Aral’s site], thanks to the lovely folks at La Quadrature du Net who took the initiative to rip the recording of the live stream and host it on their own instance. They also edited together a version with my slides, captions, and translations.
