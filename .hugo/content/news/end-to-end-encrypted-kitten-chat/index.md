---
title: "End-to-end-encrypted Kitten Chat"
date: 2023-02-20T09:11:00
categories: ["Updates"] # Updates or Labs
author: "Aral Balkan"
postURL: "https://ar.al/2023/02/20/end-to-end-encrypted-kitten-chat/" # URL for the post
sourceURL: "https://ar.al/2023/02/20/end-to-end-encrypted-kitten-chat/" # URL for the site the post is on
sourceName: "Aral’s blog" # Name of the site the post is on
---

In this hour-and-a-half long [Small is Beautiful](https://small-tech.org/events/) live stream recording, Aral shows you how [WebSockets](https://kitten.small-web.org/tutorials/htmx-the-htmx-web-socket-extension-and-socket-routes), [authenticated routes](https://kitten.small-web.org/tutorials/authentication/), and [public-key encryption](https://kitten.small-web.org/reference/#cryptographical-properties) work in [Kitten](https://kitten.small-web.org) and uses them to create a centralised WebSocket chat application that he then evolves into [an end-to-end-encrypted peer-to-peer Small Web chat application](https://kitten.small-web.org/tutorials/end-to-end-encrypted-peer-to-peer-small-web-apps/).
