---
title: "Baby Steps"
date: 2018-12-07T17:36:40Z
categories: ["Updates"]
author: "Aral Balkan"
image: "https://ar.al/2018/12/07/baby-steps/first-apple-computer-vs-ipad-pro-1280-720.jpeg"
postURL: "https://ar.al/2018/12/07/baby-steps"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Five years ago, when I decided to devote myself to tackling the problem of surveillance capitalism, it was clear what we needed: convenient and beautiful ethical everyday things that provide seamless experiences on fully free-as-in-freedom stacks.