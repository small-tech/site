---
title: "Multi-writer Dat could power the next Web"
date: 2018-08-04T12:55:06+01:00
categories: ["Labs"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2018/08/04/multiwriter-dat-could-power-the-next-web/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Dat is an exciting new technology that enables you to synchronise data privately and in a peer-to-peer fashion. It uses the same underlying concepts as blockchain1 sans global consensus, and it’s run by the not-for-profit Code for Science & Society. The community has top-notch people like Mathias Buus, Tara Vancil, Karissa McKelvey, and Jim Pick.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->