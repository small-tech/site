---
title: "The future of Better Blocker"
date: 2020-01-13T12:19:06Z
categories: ["Updates"]
author: "Laura Kalbag"
postURL: "https://laurakalbag.com/the-future-of-better-blocker/"
sourceURL: "https://laurakalbag.com"
sourceName: "Laura’s blog"
---

Aral has written a detailed blog post about the logistics of moving our app, Better Blocker, to our Irish organisation.

In brief: we have to re-publish the app under Small Technology Foundation, which means anyone who wants to get future updates to the app will have to buy it again. (Though the previous version of the app will still be able to receive updates to the blocking rules.) We will lose years of favourable reviews, high rankings in our category, and a feature on the Safari App Extensions page. It’s immensely frustrating, and we’ve been trying to find a way around it for the last few months. But we can’t. Because we have to play by Apple’s rules.
