---
title: "Better Blocking Rules Update 26th February 2021"
date: 2021-02-26T15:43:47Z
categories: ["Updates"] # Updates or Labs
author: "Laura Kalbag" # Aral Balkan or Laura Kalbag (or another writer if article etc)
postURL: "https://better.fyi/news/"
sourceURL: "https://better.fyi"
sourceName: "the Better site"
---

New blocking rules! On 26th February 2021, Laura blocked three blocker blockers, fixed six sites and blocked six new trackers..

The rules will auto-update if the app is running. If it’s not, you can get the new rules by opening the app and choosing ‘Update rules’ or ‘Check again now’ from Better’s menu.