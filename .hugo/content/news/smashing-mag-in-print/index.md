---
title: "This One Weird Trick Tells Us Everything About You"
date: 2019-07-12T11:36:02+02:00
categories: ["Updates"]
author: "Laura Kalbag"
postURL: "https://laurakalbag.com/this-one-weird-trick-tells-us-everything-about-you/"
sourceURL: "https://laurakalbag.com"
sourceName: "Laura’s blog"
---

I wrote a little essay for Smashing Print #1: Ethics & Privacy titled ‘This One Weird Trick Tells Us Everything About You.’

It covers different types of tracking and how it came to exist, as well as regulation, ethics, and why we need better business models.
