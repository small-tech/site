---
title: "Gnomit 1.0.4"
date: 2018-10-19T17:56:49Z
categories: ["Labs"]
author: "Aral Balkan"
image: "https://ar.al/2018/10/19/gnomit-1.0.4/gnomit.png"
postURL: "https://ar.al/2018/10/19/gnomit-1.0.4/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

There’s a new version of Gnomit, my little commit message editor for Linux, thanks to the initiative of Sergey Bugaev who sent me a patch a few weeks ago.