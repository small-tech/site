---
title: "Get Unicode-aware length of string in JavaScript"
date: 2018-08-26T16:57:09+01:00
categories: ["Labs"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2018/08/26/get-unicode-aware-length-of-string-in-javascript/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

`stringInstance.length` in JavaScript is not Unicode aware.

That means, for example, that the following code:

`"🤓".length`

Will return 2, not 1.

If you want a Unicode-aware string length, use this function:

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->