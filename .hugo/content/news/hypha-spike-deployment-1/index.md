---
title: "Hypha Spike: Deployment 1"
date: 2019-01-05T15:14:03Z
categories: ["Updates", "Labs"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2019/01/05/hypha-spike-deployment-1/"
sourceURL: "https://ar.al/"
sourceName: "Aral’s blog"
---

Wait, what, we’re deploying Hypha – but we haven’t even built it yet?!

Exactly.
