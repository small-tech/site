---
title: "Better Blocker: two year review and thoughts on the future"
date: 2018-08-27T14:50:58+01:00
categories: ["Updates"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2018/08/27/better-blocker-two-year-review-and-thoughts-on-the-future/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

I’m proud of what we’ve achieved with Better so far. However, I’m unhappy with the reach Better Blocker has and we must figure out a way of increasing that reach while not cutting off a source of revenue without which we could not afford to pay the rent.

Here are some of my thoughts about the future, on how we can optimise for the right things to address these concerns…

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->