---
title: "How to install the Vala Reference Manual into Devhelp using the apt package manager"
date: 2018-08-13T10:59:12+01:00
categories: ["Labs"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2018/08/13/how-to-add-the-vala-reference-manual-to-devhelp-using-the-apt-package-manager/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

The Vala Documentation states that the Vala Reference Manual is available in HTML and PDF versions and “is also available for your installed version of Vala as DevHelp from your distribution, such as Fedora or Ubuntu.”

Sadly, though, it doesn’t tell you the package names or link to the packages.

For Debian’s apt-based systems like Pop!_OS and Ubuntu, the package you’re looking for is called vala-0.40-doc.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->