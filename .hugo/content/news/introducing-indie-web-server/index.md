---
title: "Introducing Indie Web Server (video)"
date: 2019-03-14T17:29:02Z
categories: ["Labs"]
author: "Aral Balkan"
postURL: "https://ar.al/2019/03/14/introducing-indie-web-server-video/"
sourceURL: "https://ar.al/"
sourceName: "Aral’s blog"
---

I just recorded a short video demonstrating just how simple and seamless Indie Web Server really is.
