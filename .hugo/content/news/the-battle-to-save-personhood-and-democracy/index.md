---
title: "In 2020 and beyond, the battle to save personhood and democracy requires a radical overhaul of mainstream technology"
date: 2020-01-02T15:18:33Z
categories: ["Updates"]
author: "Aral Balkan"
image: "australia-fires.jpg"
postURL: "https://ar.al/2020/01/01/in-2020-and-beyond-the-battle-to-save-personhood-and-democracy-requires-a-radical-overhaul-of-mainstream-technology/"
sourceURL: "https://ar.al/"
sourceName: "Aral’s blog"
---

As we enter a new decade, humankind faces several existential emergencies:

1. The climate emergency
2. The democracy emergency
3. The personhood emergency

In no small part thanks to Greta Thunberg, we’re definitely talking about the first. Whether we’re actually doing anything about it, of course, is very much up for debate.

Similarly, thanks to the rise of the far right around the globe in the shape of (among others) Trump in the US, Johnson in the UK, Bolsonaro in Brazil, Orban in Hungary, and Erdoğan in Turkey, we are also talking about the second, including the role of propaganda (so-called “fake news”) and social media in perpetrating it.

What we seem entirely clueless and ambivalent about is the third even though all the others stem from it and are symptoms of it. It is the emergency without a name. Well, until now, that is.
