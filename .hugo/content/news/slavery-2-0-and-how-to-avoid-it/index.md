---
title: "Slavery 2.0 and how to avoid it: a practical guide for cyborgs"
date: 2019-05-02T11:32:33+01:00
categories: ["Updates"]
author: "Aral Balkan"
image: "https://ar.al/2019/05/02/slavery-2.0-and-how-to-avoid-it-a-practical-guide-for-cyborgs/magazine-cover.jpeg"
postURL: "https://ar.al/2019/05/02/slavery-2.0-and-how-to-avoid-it-a-practical-guide-for-cyborgs/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

This is the original English version of an article that I wrote for Issue 32 of the Kulturstiftung des Bundes (The German Federal Cultural Foundation) magazine. You can also read the German version.
