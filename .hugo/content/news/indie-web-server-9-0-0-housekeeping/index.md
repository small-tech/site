---
title: "Indie Web Server 9.0.0: Housekeeping"
date: 2019-04-29T11:30:52+01:00
categories: ["Labs"]
author: "Aral Balkan"
postURL: "https://ar.al/2019/04/29/indie-web-server-9.0.0-housekeeping/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

I just released Indie Web Server version 9.0.0.

This is mostly a housekeeping release and nearly all the changes are under the hood.
