---
title: "How to enable the Browse Files setting in GSConnect"
date: 2018-08-03T12:58:28+01:00
categories: ["Labs"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2018/08/03/how-to-enable-the-browse-files-setting-in-gsconnect/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

GSConnect is a beautiful shell extension for Gnome Shell that integrates mobile devices that can run KDE Connect (like phones and tablets that run LineageOS) with desktop computers that run Gnome on GNU/Linux (like my notebook running Pop!_OS).

One of the features that you might have trouble with is Browse Files as it is broken by default. It fails unless you have a separate app installed.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->