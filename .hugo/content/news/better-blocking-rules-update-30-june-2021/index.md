---
title: "Better Blocking Rules Update 30th June 2021"
date: 2021-07-01T09:25:54+01:00
categories: ["Updates"] # Updates or Labs
author: "Laura Kalbag" # Aral Balkan or Laura Kalbag (or another writer if article etc)
postURL: "https://better.fyi/news/"
sourceURL: "https://better.fyi"
sourceName: "the Better site"
---

[New blocking rules](https://better.fyi/news/#update-2021-05-25)! On 30th June 2021, Laura blocked one blocker blocker, fixed three sites and blocked five new trackers.

The rules will auto-update if the app is running. If it’s not, you can get the new rules by opening the app and choosing ‘Update rules’ or ‘Check again now’ from Better’s menu.