---
title: "Better Blocking Rules Update 29th April 2021"
date: 2021-04-29T13:28:00+01:00
categories: ["Updates"] # Updates or Labs
author: "Laura Kalbag" # Aral Balkan or Laura Kalbag (or another writer if article etc)
postURL: "https://better.fyi/news/"
sourceURL: "https://better.fyi"
sourceName: "the Better site"
---

[New blocking rules!](https://better.fyi/news/#update-2021-04-29) On 29th April 2021, Laura blocked a blocker blocker, fixed eight sites and blocked four new trackers.

The rules will auto-update if the app is running. If it’s not, you can get the new rules by opening the app and choosing ‘Update rules’ or ‘Check again now’ from Better’s menu.