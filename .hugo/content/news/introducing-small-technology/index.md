---
title: "Introducing Small Technology Foundation, Site.js, and Tincan"
date: 2019-08-27T13:47:13+01:00
categories: ["Updates"]
author: "Aral Balkan"
image: "https://ar.al/2019/08/26/introducing-small-technology-foundation/small-technology-foundation-header.jpeg"
postURL: "https://ar.al/2019/08/26/introducing-small-technology-foundation/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Today, Laura and I want to introduce you to Small Technology Foundation, where we will be continuing the work we started at Ind.ie five years ago.
