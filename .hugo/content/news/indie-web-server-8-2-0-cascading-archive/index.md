---
title: "Indie Web Server 8.2.0: Cascading archives for an evergreen web"
date: 2019-04-20T16:42:35+01:00
categories: ["Labs"]
author: "Aral Balkan"
image: "https://ar.al/2019/04/20/indie-web-server-8.2.0-cascading-archives-for-an-evergreen-web/evergreen-web.jpeg"
postURL: "https://ar.al/2019/04/20/indie-web-server-8.2.0-cascading-archives-for-an-evergreen-web/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

I just released version 8.2.0 of Indie Web Server. This version brings with it a cascading archives feature to make it easier than ever for you to support an evergreen web and not break existing links as you evolve your sites.
