---
title: "Site.js: now with live reload"
date: 2019-10-29T14:30:59Z
categories: ["Labs"]
author: "Aral Balkan"
postURL: "https://ar.al/2019/10/29/site.js-now-with-live-reload/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

I just released version 12.9.6 of Site.js with live reload support for static pages.

There’s also a fix for the update command, so please update to this version so that you can keep updating using the update command when we move to 12.10.x and beyond.
