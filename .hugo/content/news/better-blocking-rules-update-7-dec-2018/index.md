---
title: "Better Blocking Rules Update 7th December 2018"
date: 2018-12-07T17:44:31Z
categories: ["Updates"]
author: "Laura Kalbag"
image: ""
postURL: "https://forum.ind.ie/t/better-blocking-rules-update-20181207/2522"
sourceURL: "https://forum.ind.ie"
sourceName: "the Ind.ie forum"
---

New blocking rules! Laura fixed one broken site, blocked one blocker blocker, and blocked twenty-seven new trackers.

To get the latest rules, select ‘Update rules’ from the menu in the Better app.