---
title: "Apple App Review: resistance is futile!"
date: 2020-01-14T19:13:59Z
categories: ["Updates"]
author: "Aral Balkan"
image: "computer-says-blub-blub-blub.jpg"
postURL: "https://ar.al/2020/01/14/apple-app-review-resistance-is-futile/"
sourceURL: "https://ar.al/"
sourceName: "Aral’s blog"
---

Last time on Dances With Drones, Apple had accepted the iOS version of Better Blocker but rejected the macOS one because of invalid app metadata even though both apps had the same metadata.
