---
title: "Hypha Spike: Diceware"
date: 2019-01-15T14:49:40Z
categories: ["Labs"]
author: "Aral Balkan"
image: "https://ar.al/2019/01/15/hypha-spike-diceware/spike-screenshot.jpeg"
postURL: "https://ar.al/2019/01/15/hypha-spike-diceware/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Pulled out from Hypha Spike: DAT 1.

- Use Diceware for passphrase generation to ensure a high-entropy process.

- Use budo in the spikes to enable lightweight use of bundling/modules/etc.
