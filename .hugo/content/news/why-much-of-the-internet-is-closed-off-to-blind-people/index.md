---
title: "Why much of the Internet is closed off to blind people"
date: 2019-09-30T17:13:25+01:00
categories: ["Updates"] # Updates or Labs
author: "Laura Kalbag" # Aral or Laura (or another writer if article etc)
postURL: "https://laurakalbag.com/why-much-of-the-internet-is-closed-off-to-blind-people/" # URL for the post
sourceURL: "https://laurakalbag.com" # URL for the site the post is on
sourceName: "Laura’s blog" # Name of the site the post is on
---

A few weeks ago, I chatted to James Jeffrey for a BBC article on Why much of the internet is closed off to blind people. In particular, I spoke to how easy it can be to make a website accessible, and why it should be part of our everyday practice as designers and developers.
