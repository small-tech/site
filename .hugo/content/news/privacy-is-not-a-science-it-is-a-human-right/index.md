---
title: "Privacy is not a science, it is a human right"
date: 2019-02-14T16:01:17Z
categories: ["Updates", "Labs"]
author: "Aral Balkan"
image: "https://ar.al/2019/02/14/privacy-is-not-a-science-it-is-a-human-right/wolf-in-sheeps-clothing.jpeg"
postURL: "https://ar.al/2019/02/14/privacy-is-not-a-science-it-is-a-human-right/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Given the levels of institutional corruption in academia and in the regulatory bodies and advocacy institutions that should be protecting our privacy, very few things shock me these days. So hats off to Bart van der Sloot for managing the impossible and finding a new low by framing institutional corruption as scientific neutrality in his article Dubbele petten in de privacywetenschap…
