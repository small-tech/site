---
title: "The punk rock internet – how DIY ​​rebels ​are working to ​replace the tech giants"
date: 2018-02-01T14:07:37+01:00
categories: ["Updates"]
author: "John Harris"
image: ""
postURL: "https://www.theguardian.com/technology/2018/feb/01/punk-rock-internet-diy-rebels-working-replace-tech-giants-snoopers-charter"
sourceURL: "https://theguardian.com"
sourceName: "The Guardian"
---

The office planner on the wall features two reminders: “Technosocialism” and “Indienet institute”. A huge husky named Oskar lies near the door, while the two people who live and work here – a plain apartment block on the west side of Malmö, Sweden – go about their daily business.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->