---
title: "Upcoming talks: April"
date: 2019-03-29T18:43:54+01:00
categories: ["Updates"]
author: "Aral Balkan"
postURL: "https://ar.al/2019/03/29/upcoming-talks-april/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

In April, I will be speaking at two events, starting with The Straits Times Education Forum debate on entrepreneurship to be held at Singapore Management University on April 6th, 2019.
