---
title: "A belated introduction to Small Technology Foundation"
date: 2019-10-03T16:14:22+01:00
categories: ["Updates"] # Updates or Labs
author: "Laura Kalbag" # Aral Balkan or Laura Kalbag (or another writer if article etc)
postURL: "https://laurakalbag.com/a-belated-introduction-to-small-technology-foundation/" # URL for the post
sourceURL: "https://laurakalbag.com" # URL for the site the post is on
sourceName: "Laura’s blog" # Name of the site the post is on
---

About a month ago, Aral and I launched Small Technology Foundation. In the frenzied rush to get the site up, and general life chaos that has overwhelmed me since, I’d not managed to write about it until now.
