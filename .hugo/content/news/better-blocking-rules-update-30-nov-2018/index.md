---
title: "Better Blocking Rules Update 30th November 2018"
date: 2018-11-30T16:13:25Z
categories: ["Updates"]
author: "Laura Kalbag"
image: ""
postURL: "https://forum.ind.ie/t/better-blocking-rules-update-20181130-001/2515"
sourceURL: "https://forum.ind.ie"
sourceName: "the Ind.ie forum"
---

New blocking rules! On 30th November 2018 Laura fixed three broken sites, blocked first-party targeted ads on three sites, and blocked fourteen new trackers.