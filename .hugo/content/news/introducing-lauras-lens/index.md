---
title: "Introducing Laura’s Lens"
date: 2019-10-03T16:12:30+01:00
categories: ["Updates"] # Updates or Labs
author: "Laura Kalbag" # Aral Balkan or Laura Kalbag (or another writer if article etc)
postURL: "https://laurakalbag.com/introducing-lauras-lens/" # URL for the post
sourceURL: "https://laurakalbag.com" # URL for the site the post is on
sourceName: "Laura’s blog" # Name of the site the post is on
---

Over the last five years or so, I’ve been sharing links to insightful articles with a critical view of technology. It started as Ind.ie’s weekly roundup email, but I found writing a long summary was taking me too long every week (I’m no journalist!) So the roundups evolved into Ind.ie’s daily Radar, where I’d post daily links, quoting some of the best bits, which I’d also post to @indie on Mastodon and @indie on Twitter…
