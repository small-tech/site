---
title: "Better Blocking Rules Update 5th July 2019"
date: 2019-07-05T16:56:28+01:00
categories: ["Updates"]
author: "Laura Kalbag"
postURL: "https://better.fyi/news/"
sourceURL: "https://better.fyi"
sourceName: "the Better site"
---

New blocking rules! On 5th July 2019, Laura blocked adtech on one site, fixed one site and blocked two new trackers.

The rules will auto-update if the app is running. If it’s not, you can get the new rules by choosing ‘Update rules’ or ‘Check again now’ from Better’s menu.
