---
title: "Evolution"
date: 2017-09-01T14:23:45+01:00
categories: ["Updates"]
author: "Aral Balkan"
image: ""
postURL: "https://forum.ind.ie/t/evolution/1842"
sourceURL: "https://forum.ind.ie"
sourceName: "the Ind.ie forum"
---

For the past four years, and with very limited resources, Laura and I – along with the help of some wonderful people – have been trying to make the Web, the Internet, and technology in general a safer and kinder place. In this we are thankful to have wonderful allies, and the shoulders of giants to stand on, some of whom have been working on solutions for far longer than we have even been aware of the problems.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->