---
title: "Better Blocker: thank you for our best week yet!"
date: 2018-09-25T19:55:31+01:00
categories: ["Updates"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2018/09/25/better-blocker-thank-you-for-our-best-week-yet/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Laura and I want to thank those of you who purchased Better Blocker last week. Thanks to you, we had our best week ever and sold over 2,500 units. That’s quite something given that we have sold just over 20,000 units in total over the last two years.