---
title: "HTTPS Server"
date: 2019-03-07T13:47:52Z
categories: ["Labs"]
author: "Aral Balkan"
postURL: "https://ar.al/2019/03/07/https-server/"
sourceURL: "https://ar.al/"
sourceName: "Aral’s blog"
---

HTTPS Server is a development server that uses nodecert to automatically provision and use locally-trusted TLS certificates.
