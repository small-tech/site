---
title: "How to send an email"
date: 2021-12-20T15:52:00
categories: ["Updates"] # Updates or Labs
author: "Aral Balkan"
postURL: "https://ar.al/2021/12/20/how-to-send-an-email/" # URL for the post
sourceURL: "https://ar.al/" # URL for the site the post is on
sourceName: "Aral’s blog" # Name of the site the post is on
---

How to send an email by speaking SMTP interactively to an email server. (This is how email works under the hood and it’s simpler than you might think. Play the video and follow along.)