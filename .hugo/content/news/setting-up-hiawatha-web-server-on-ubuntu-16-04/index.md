---
title: "Setting up Hiawatha web server on Ubuntu 16.04"
date: 2018-10-04T10:13:08+01:00
categories: ["Labs"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2018/10/04/setting-up-hiawatha-web-server-on-ubuntu-16.04/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

The unfortunately-named Hiawatha web server with its problematic logo is an independent, non-commercial, free and open web server built by Dutchman Hugo Leisink as a hobby project for the last 15 years or so. It’s primarily focused on security and the author appears to have a no-nonsense approach to its development.