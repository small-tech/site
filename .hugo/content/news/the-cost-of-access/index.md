---
title: "The cost of access"
date: 2018-07-07T13:53:54+01:00
categories: ["Updates"]
author: "Laura Kalbag"
image: ""
postURL: "https://laurakalbag.com/the-cost-of-access/"
sourceURL: "https://laurakalbag.com"
sourceName: "Laura’s blog"
---

This was my opening introduction for Futurefest’s “Who Is the Internet For?” debate today in London. I didn’t hit all the points as smoothly as this, but I covered most of it throughout the debate. In the description for this session, it says “Without the inputs from internet users with physical impairments we wouldn’t have the voice technology underpinning Siri or Alexa.” But what is the cost of using Alexa for people who would find it difficult or impossible to access the web or technology without voice recognition?

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->