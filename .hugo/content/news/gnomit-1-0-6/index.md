---
title: "Gnomit 1.0.6"
date: 2018-11-08T15:18:12Z
categories: ["Labs"]
author: "Aral Balkan"
image: ""
postURL: ""
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

The pushed the 1.0.6 release of Gnomit, my little git commit message editor for Linux, to the Flathub GitHub repo about two weeks ago but I’m only writing about it now as there was a delay with the update appearing on Flathub.