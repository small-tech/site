---
title: "Better Blocking Rules Update 25th May 2021"
date: 2021-05-25T17:15:21+01:00
categories: ["Updates"] # Updates or Labs
author: "Laura Kalbag" # Aral Balkan or Laura Kalbag (or another writer if article etc)
postURL: "https://better.fyi/news/#update-2021-05-25" # URL for the post
sourceURL: "https://better.fyi"
sourceName: "the Better site"
---

New blocking rules! On 25th May 2021, Laura blocked three blocker blockers, fixed five sites and blocked four new trackers.

The rules will auto-update if the app is running. If it’s not, you can get the new rules by opening the app and choosing ‘Update rules’ or ‘Check again now’ from Better’s menu.

Thank you to all our donors and patrons who help us continue our work! 💙

If you want to support us, you can tell your friends about our work, leave us a review for Better, gift the app to someone else, or even [become a donor or monthly patron](https://small-tech.org/fund-us/).