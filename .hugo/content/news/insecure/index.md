---
title: "Insecure"
date: 2018-07-19T14:02:12+01:00
categories: ["Updates"]
author: "Laura Kalbag"
image: ""
postURL: "https://laurakalbag.com/insecure/"
sourceURL: "https://laurakalbag.com"
sourceName: "Laura’s blog"
---

The state of sharing on the web is broken. After the Snowden revelations, it became clear that my sharing “content” willy-nilly was a potential danger to me. I was previously oblivious to the risk of sharing everything about myself on social media. The tiniest most harmless piece of information about me could be derived in a data set somewhere to become something very meaningful (even if it’s wrong) and potentially dangerous.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->