---
title: "This site [ar.al] now runs on Indie Web Server"
date: 2019-04-17T17:35:36+01:00
categories: ["Labs"]
author: "Aral Balkan"
postURL: "https://ar.al/2019/04/17/this-site-now-runs-on-indie-web-server/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

In the interests of eating my own hamster food1, I just switched this site from nginx to Indie Web Server.

The only complication in the process was that I had to update the hostname of the server to match the domain name.
