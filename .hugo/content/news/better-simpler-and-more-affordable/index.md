---
title: "Better, simpler, and more affordable"
date: 2018-09-14T18:14:36+01:00
categories: ["Updates"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2018/09/14/better-simpler-and-more-affordable/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Two weeks ago, in Better Blocker: two year review and thoughts on the future, I wrote that I was going to “radically simplify” the Better apps.

Today, I submitted a radical redesign of Better Blocker for iOS to the App Store for approval in time for the launch of iOS 12.

The app has a minimal new design and a beautiful new icon that Laura and I designed together.