---
title: "Upcoming Talks March"
date: 2019-03-07T13:48:04Z
categories: ["Updates"]
author: "Aral Balkan"
image: "https://ar.al/2019/03/07/upcoming-talks-march/who-runs-the-world.jpeg"
postURL: "https://ar.al/2019/03/07/upcoming-talks-march/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

I’m giving two talks this month; at Le Grand Barouf Numérique in Lille, France on the 20th and at the Art, Technology, and Change conference at CPH:DOX, the Copenhagen International Documentary Film Festival in Copenhagen, Denmark on the 26th.
