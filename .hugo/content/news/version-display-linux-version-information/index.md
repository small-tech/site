---
title: "version: display Linux version information"
date: 2018-10-26T17:58:56Z
categories: ["Labs"]
author: "Aral Balkan"
image: ""
postURL: "/2018/10/26/version-display-linux-version-information/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

One thing I find myself always having to look up is how to display version information in Linux. That’s because the commands for displaying version information are in no way intuitive or memorable.