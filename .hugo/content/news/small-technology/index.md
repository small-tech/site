---
title: "Small Technology"
date: 2019-03-04T13:47:38Z
categories: ["Updates"]
author: "Aral Balkan"
image: "https://ar.al/2019/03/04/small-technology/2954872088_57405cb369_o.jpg"
postURL: "https://ar.al/2019/03/04/small-technology/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

The antidote to Big Tech is Small Tech.

Big Tech, with its billion-dollar unicorns, has robbed us of the potential of the Internet. Fueled by the extreme shortsightedness and greed of venture capital and startups, the utopic vision of a decentralised and democratic commons has morphed into the dystopic autocracy of Silicon Valley panopticons that we call surveillance capitalism. This status quo threatens not just our democracies but the very integrity of our personhood in the digital and networked age.
