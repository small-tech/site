---
title: "Export/import issues with GitLab CE"
date: 2019-06-08T10:41:18+01:00
categories: ["Labs"]
author: "Aral Balkan"
postURL: "https://ar.al/2019/06/08/export-import-issues-with-gitlab-ce/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

GitLab CE (the free/open source version of GitLab) has an import issues feature but doesn’t have an export issues feature (because, not enterprise, apparently).

So if you fork a project and want to transfer the issues also, you’re out of luck. Unless you use the API, that is.
