---
title: "Tilingnome"
date: 2018-12-29T15:21:33Z
categories: ["Labs"]
author: "Aral Balkan"
image: "https://ar.al/2018/12/29/tilingnome/tilingnome.jpeg"
postURL: "https://ar.al/2018/12/29/tilingnome/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

I like the idea of a tiling window manager like i3 (or i3-gaps, or sway) not necessarily because of their lightweight nature when compared to a fully-fledged desktop environment like GNOME but because of their potential organisational and navigational value.
