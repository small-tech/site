---
title: "Ind.ie, Better, and Laura on Mastodon"
date: 2018-08-06T14:26:24+01:00
categories: ["Updates"]
author: "Laura Kalbag"
image: ""
postURL: "https://forum.ind.ie/t/ind-ie-better-and-laura-on-mastodon/2268"
sourceURL: "https://forum.ind.ie"
sourceName: "the Ind.ie forum"
---

If you’re a person who is on Mastodon, you may be interested to know that we now have our own instances for Ind.ie, and I have my own instance for me.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->