---
title: "I was wrong about Google and Facebook: there’s nothing wrong with them (so say we all)"
date: 2019-01-11T14:58:22Z
categories: ["Updates"]
author: "Aral Balkan"
image: "https://ar.al/2019/01/11/i-was-wrong-about-google-and-facebook-theres-nothing-wrong-with-them-so-say-we-all/google-microsoft-and-fsf-sponsor-copyleftconf.jpeg"
postURL: "https://ar.al/2019/01/11/i-was-wrong-about-google-and-facebook-theres-nothing-wrong-with-them-so-say-we-all/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

It’s always difficult admitting you’re wrong. But sometimes, it’s exactly what you have to do in the face of overwhelming evidence to the contrary. So, today, I admit that I was wrong about Google, Facebook, and surveillance capitalism in general being toxic for our human rights and democracy.

You see, it simply cannot be true given how they are endorsed by some of the most well-respected groups and organisations in the world. All the evidence points to Google and Facebook being good actors who are not a threats to our privacy.
