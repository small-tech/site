---
title: "Sometimes you have to stick a screwdriver in it (or how to liberate a Chromebook in ten easy steps)"
date: 2018-12-31T15:19:23Z
categories: ["Updates", "Labs"]
author: "Aral Balkan"
image: "https://ar.al/2018/12/31/sometimes-you-have-to-stick-a-screwdriver-in-it/screwdriver.jpeg"
postURL: "https://ar.al/2018/12/31/sometimes-you-have-to-stick-a-screwdriver-in-it/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Over the holidays, I found a Chromebook that Samsung had given me to evaluate about six years ago and which had been gathering dust ever since. Coincidentally, Laura’s sister Annie had just told me that she needed a laptop. Hmm… Well, there was no way I was going to give her a Google spy device, so I decided to liberate the Chromebook from Google’s surveillance-based operating system (ChromeOS) and gift it to her.
