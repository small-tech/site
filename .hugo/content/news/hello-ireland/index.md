---
title: "Hello Ireland"
date: 2018-05-28T15:09:40+01:00
categories: ["Updates"]
author: "Laura Kalbag"
image: ""
postURL: "https://laurakalbag.com/move-to-ireland/"
sourceURL: "https://laurakalbag.com"
sourceName: "Laura’s blog"
---

On Saturday, we celebrated the victory for Irish people as they repealed the anti-abortion laws. We were celebrating not just because we care about the rights of people with uteruses, but also because we have now been resident in Ireland for two months. And we love it here. Why move? If you know Aral and me, you’ll know we’ve been trying to find the right home for the last three or four years.

<!-- KEY TO FRONT MATTER:
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    description = Summary/description of original post
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog
-->