---
title: "Crafting a continuous-client desktop/mobile experience on Linux with GSConnect"
date: 2018-08-02T12:56:33+01:00
categories: ["Labs"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2018/08/02/crafting-a-continuous-client-desktop-mobile-experience-on-linux-with-gsconnect/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

A cornerstone of Apple’s approach to seamless design is reflected in a feature they call Continuity. Continuity aims to provide a “seamless experience” between your various devices. This is essentially what Joshua Topolsky called the continuous client back in 2010 and a concept that I wrote at length about in my chapter titled Mobile Considerations In User Experience Design: “Web or Native?” in Smashing Magazine’s Redesign The Web book back in 2012.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->