---
title: "News"
date: 2018-07-30T14:15:40+01:00
menu:
 main:
  weight: 5
summary: "Read the latest news and articles from Small Technology Foundation, as found on our personal sites and in other media coverage."
---

# News

Read the latest news and articles from Small Technology Foundation, as found on our personal sites and in other media coverage.

[Subscribe to this News feed with RSS](/news/index.xml).
