---
title: "The web0 manifesto"
date: 2022-01-01T18:45:00
categories: ["Updates"] # Updates or Labs
author: "Aral Balkan"
postURL: "https://web0.small-web.org" # URL for the post
sourceURL: "https://web0.small-web.org" # URL for the site the post is on
sourceName: "web0.small-web.org" # Name of the site the post is on
---

web0 is web3 without all the corporate right-libertarian Silicon Valley bullshit.
