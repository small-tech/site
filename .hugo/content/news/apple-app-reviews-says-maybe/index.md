---
title: "Apple App Review says “maybe”: the whims of trillion-dollar gatekeepers"
date: 2020-01-14T11:48:42Z
categories: ["Updates"]
author: "Aral Balkan"
image: "computer-says-maybe.jpg"
postURL: "https://ar.al/2020/01/14/apple-app-review-says-maybe-the-whims-of-trillion-dollar-gatekeepers/"
sourceURL: "https://ar.al/"
sourceName: "Aral’s blog"
---

Yesterday, I wrote about how Apple’s refusal to update a couple of fields in their database has impacted the future of Better Blocker, the tracker blocker that Laura and I build at our tiny two-person not-for-profit, Small Technology Foundation.

I also shared our plan for dealing with this situation.

Yesterday, we were at Step 3 of our plan. We’d submitted the version 2020.1 updates to Better for macOS and iOS from our old developer account and we were waiting for Apple to approve them.

Today, we are half-at step 4 because Apple has approved the macOS app and rejected the iOS app.
