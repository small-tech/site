---
title: "Set up a live static personal web site in seconds with Indie Web Server 8.0.0"
date: 2019-04-16T17:35:06+01:00
categories: ["Labs"]
author: "Aral Balkan"
postURL: "https://ar.al/2019/04/16/set-up-a-live-static-personal-web-site-in-seconds-with-indie-web-server-8.0.0/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

On April 1st (no joke), Indie Web Server 7.1.0 introduced the ability to set up a live static web site in seconds on any server that had Node.js installed.
