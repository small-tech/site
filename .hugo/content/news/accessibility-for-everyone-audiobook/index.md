---
title: "Accessibility for Everyone Audiobook"
date: 2018-08-14T17:14:44+01:00
categories: ["Updates"]
author: "Laura Kalbag"
image: ""
postURL: "https://laurakalbag.com/accessibility-for-everyone-audiobook/"
sourceURL: "https://laurakalbag.com"
sourceName: "Laura’s blog"
---

I am very happy to tell you that the audiobook of Accessibility For Everyone is now available to buy on Audible.

It’s thanks to the hard work of my brother, Sam Kalbag, who single-handedly edited, engineered and produced the audiobook, helped me do the recording properly, and gave me the benefit of his extensive knowledge of audiobooks.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->