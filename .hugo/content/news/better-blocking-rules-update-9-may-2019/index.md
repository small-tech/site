---
title: "Better Blocking Rules Update 9th May 2019"
date: 2019-05-09T15:53:10+01:00
categories: ["Updates"]
author: "Laura Kalbag"
postURL: "https://better.fyi/news/"
sourceURL: "https://better.fyi"
sourceName: "the Better site"
---

New blocking rules! On 9th May 2019, Laura blocked one blocker blocker, fixed two sites, blocked cookies for one tracker, and blocked seven new trackers.

To get the new rules, choose ‘Update rules’ or ‘Check again now’ from Better’s menu. If you keep the macOS app open, the rules should auto-update.
