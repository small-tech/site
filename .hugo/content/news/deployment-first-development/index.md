---
title: "Deployment-first development"
date: 2019-01-09T15:01:11Z
categories: ["Updates", "Labs"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2019/01/09/deployment-first-development/"
sourceURL: "https://ar.al/"
sourceName: "Aral’s blog"
---

Independent technology – ethical technology – must be as accessible as possible for its intended audience at every step of the process. That doesn’t mean it must be accessible as possible to everyone at every stage in its development but rather it should be accessible as possible for the people that are working on it or with it at any given point.

Hypha is currently at the start of its development stage and thus must be as accessible as possible to developers who want to follow along with its development, run it themselves, and possibly fork it off and try new things with it.
