---
title: "Better Blocking Rules Update 22nd October 2018"
date: 2018-10-22T12:57:31+01:00
categories: ["Updates"]
author: "Laura Kalbag"
image: ""
postURL: "https://forum.ind.ie/t/better-blocking-rules-update-20181022-001/2443"
sourceURL: "https://forum.ind.ie"
sourceName: "the Ind.ie forum"
---

New blocking rules! On 22nd October 2018, Laura fixed four broken sites, blocked six blocker blockers, and blocked six new trackers.