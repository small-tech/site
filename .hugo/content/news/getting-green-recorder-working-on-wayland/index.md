---
title: "Getting Green Recorder running on Wayland under Gnome on Ubuntu 18.10-based systems"
date: 2019-01-01T15:17:42Z
categories: ["Labs"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2019/01/01/getting-green-recorder-running-on-wayland-under-gnome-on-ubuntu-18.10-based-systems/"
sourceURL: "https://ar.al/"
sourceName: "Aral’s blog"
---

Green Recorder is an app for recording your screen on Linux which, as far as I know, is the only such app at the moment that works with Wayland. It’s what I used to capture the video of my segment on Al Jazeera News today.
