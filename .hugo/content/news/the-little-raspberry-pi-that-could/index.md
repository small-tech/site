---
title: "The little Raspberry Pi that could (serve a web site)"
date: 2019-10-22T14:30:29Z
categories: ["Labs"]
author: "Aral Balkan"
postURL: "https://ar.al/2019/10/22/the-little-raspberry-pi-that-could-serve-a-web-site/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Yesterday, I asked folks following me on my Mastodon1, if they’d help me blow up my Raspberry Pi Zero W…
