---
title: "Better for iOS 12 is now available on the App Store"
date: 2018-09-18T18:16:08+01:00
categories: ["Updates"]
author: "Laura Kalbag"
image: ""
postURL: "https://forum.ind.ie/t/better-for-ios-12-is-now-available-on-the-app-store/2377"
sourceURL: "https://forum.ind.ie"
sourceName: "the Ind.ie forum"
---

We’ve made the app simpler, easier to use, and just as effective as before.

We’ve also made the app more affordable at $0.99, £0.99, €1.09—we want more folks to be protected on the web.