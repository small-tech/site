---
title: "Site.js: now with auto updates in production"
date: 2019-11-03T12:20:44Z
categories: ["Labs"] # Updates or Labs
author: "Aral Balkan" # Aral Balkan or Laura Kalbag (or another writer if article etc)
postURL: "https://ar.al/2019/11/03/site.js-now-with-auto-updates-in-production/" # URL for the post
sourceURL: "https://ar.al/" # URL for the site the post is on
sourceName: "Aral’s blog" # Name of the site the post is on
---

Site.js version 12.10.2 introduces automatic updates in production.
