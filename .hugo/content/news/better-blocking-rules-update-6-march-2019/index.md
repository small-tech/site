---
title: "Better Blocking Rules Update 6th March 2019"
date: 2019-03-06T16:11:49Z
categories: ["Updates"]
author: "Laura Kalbag"
image: ""
postURL: "https://better.fyi/news/"
sourceURL: "https://better.fyi"
sourceName: "the Better site"
---

New blocking rules! On 6th March 2019, Laura blocked two blocker blockers, fixed one site, and blocked seven new trackers.

To get the new rules, choose ‘Update rules’ or ‘Check again now’ from Better’s menu.
