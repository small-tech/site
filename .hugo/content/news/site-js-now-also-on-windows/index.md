---
title: "Site.js, now also on Windows 10"
date: 2019-10-03T16:16:23+01:00
categories: ["Labs"] # Updates or Labs
author: "Aral Balkan" # Aral Balkan or Laura Kalbag (or another writer if article etc)
image: "" # file name relative to this page
postURL: "https://ar.al/2019/10/03/site.js-now-also-on-windows-10/" # URL for the post
sourceURL: "https://ar.al" # URL for the site the post is on
sourceName: "Aral’s blog" # Name of the site the post is on
---

Last week, I bought a refurbished 7-year-old ThinkPad 440p so I could test Site.js under Windows.

Long story short, Windows is still shit. Plus, it’s now also a cesspit of surveillance. And, over the weekend, I ended up adding native Windows 10 support to Site.js.
