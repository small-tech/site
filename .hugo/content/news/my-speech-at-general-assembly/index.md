---
title: "My speech at General Assembly in Berlin, November 2017"
date: 2018-05-23T15:06:25+01:00
categories: ["Updates"]
author: "Aral Balkan"
image: ""
postURL: "https://2018.ar.al/notes/my-speech-at-general-assembly-in-berlin-november-2017"
sourceURL: "https://2018.ar.al"
sourceName: "Aral’s blog"
---

This is the full video and text of my opening intervention at the General Assembly event in Berlin from November of 2017.

<!-- KEY TO FRONT MATTER:
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    description = Summary/description of original post
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog
-->