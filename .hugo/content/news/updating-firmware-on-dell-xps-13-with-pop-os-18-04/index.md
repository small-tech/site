---
title: "Updating firmware on Dell XPS 13 With Pop!_OS 18.04"
date: 2018-10-16T10:10:31+01:00
categories: ["Labs"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2018/10/16/updating-firmware-on-dell-xps-13-with-popos-18.04/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Firmware upgrades were failing for me on my Dell XPS 13… A little online research led to me to a page on Debugging UEFI Capsule updates, which in turn suggested that I try the latest fwupdate from master. My inability to RTFM, led me to open an issue on the fwpdate issue tracker when compilation failed.