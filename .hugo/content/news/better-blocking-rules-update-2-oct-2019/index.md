---
title: "Better Blocking Rules Update 2nd October 2019"
date: 2019-10-02T13:47:08+01:00
categories: ["Updates"] # Updates or Labs
author: "Laura Kalbag" # Aral Balkan or Laura Kalbag (or another writer if article etc)
postURL: "https://better.fyi/news/"
sourceURL: "https://better.fyi"
sourceName: "the Better site"
---

New blocking rules! On 2nd October 2019, Laura blocked adtech on one site, and blocked five new trackers. The rules will auto-update if the app is running. If it’s not, you can get the new rules by choosing ‘Update rules’ or ‘Check again now’ from Better’s menu.
