---
title: "Better Blocking Rules Update 1st April 2019"
date: 2019-04-01T18:31:37+01:00
categories: ["Updates"]
author: "Laura Kalbag"
postURL: "https://better.fyi/news/"
sourceURL: "https://better.fyi"
sourceName: "the Better site"
---

New blocking rules! On 1st April 2019, Laura blocked three blocker blockers, fixed one site, and blocked four new trackers.

To get the new rules, open the app on iOS, or choose ‘Update rules’ from the menu on macOS.
