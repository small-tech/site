---
title: "Live Stream: A web site on your phone with Site.js"
date: 2020-07-12T17:08:21+01:00
categories: ["Labs"]
author: "Aral Balkan"
image: "" # file name relative to this page
postURL: "https://ar.al/2020/07/12/live-stream-a-web-site-on-your-phone-with-site.js/" # URL for the post
sourceURL: "https://ar.al/" # URL for the site the post is on
sourceName: "Aral’s blog" # Name of the site the post is on
---

A sneak peek at hosting a web site on a PinePhone using Site.js 14.2.0 Alpha.