---
title: "Digital dystopia: tech slavery and the death of privacy – podcast"
date: 2018-01-12T14:20:44+01:00
categories: ["Updates"]
author: "Jordan Erica Webber"
image: ""
postURL: "https://www.theguardian.com/technology/audio/2018/jan/12/digital-dystopia-end-of-privacy-tech-podcast"
sourceURL: "https://www.theguardian.com"
sourceName: "The Guardian"
---

To kick off, we explore whether our privacy has been compromised by the tech giants whose business models depend on harvesting and monetising our data. We speak to cyborg rights activist Aral Balkan; the executive director of UK charity Privacy International Gus Hosein; and to Kevin Kelly, founding executive editor of Wired magazine.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->