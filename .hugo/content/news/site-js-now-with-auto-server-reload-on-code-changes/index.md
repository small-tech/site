---
title: "Site.js: now with auto server reload on source code changes"
date: 2019-10-30T12:22:26Z
categories: ["Updates"] # Updates or Labs
author: "Aral Balkan" # Aral Balkan or Laura Kalbag (or another writer if article etc)
postURL: "https://ar.al/2019/10/30/site.js-now-with-auto-server-reload-on-source-code-changes/" # URL for the post
sourceURL: "https://ar.al/" # URL for the site the post is on
sourceName: "Aral’s blog" # Name of the site the post is on
---

Site.js version 12.9.7 brings the second developer experience improvement to Site.js in as many days with an integrated auto reload feature that responds to source code changes on dynamic sites.
