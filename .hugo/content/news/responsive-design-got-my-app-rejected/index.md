---
title: "Responsive design got my app rejected"
date: 2018-09-16T18:14:22+01:00
categories: ["Labs"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2018/09/16/responsive-design-got-my-app-rejected/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

The iOS 12 version of Better is currently in review with a status of Metadata Rejected and I blame responsive design.

Let me explain.