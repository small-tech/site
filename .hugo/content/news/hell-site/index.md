---
title: "Hell Site"
date: 2021-05-10T16:01:07+01:00
categories: ["Updates"] # Updates or Labs
author: "Aral Balkan" # Aral Balkan or Laura Kalbag (or another writer if article etc)
postURL: "https://ar.al/2021/05/10/hell-site/" # URL for the post
sourceURL: "https://ar.al/" # URL for the site the post is on
sourceName: "Aral’s blog" # Name of the site the post is on
---

They have a word for Twitter on the fediverse.

They call it ‘hell site’.

It’s very apt.

When I first joined about 15 years ago, at the end of 2006, it was a very different place. A small, non-algorithmically curated space where you could have group chats with your friends.

What I didn’t know back then was that Twitter, Inc., was a venture-capital-funded startup.