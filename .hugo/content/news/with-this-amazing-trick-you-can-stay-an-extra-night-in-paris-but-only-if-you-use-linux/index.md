---
title: "With this amazing trick, you can stay an extra night in Paris but only if you use Linux!"
date: 2018-10-26T18:02:08Z
categories: ["Labs"]
author: "Aral Balkan"
image: "https://ar.al/2018/10/26/with-this-amazing-trick-you-can-stay-an-extra-night-in-paris-but-only-if-you-use-linux/time-4-25-5-25-pm.jpg"
postURL: "https://ar.al/2018/10/26/with-this-amazing-trick-you-can-stay-an-extra-night-in-paris-but-only-if-you-use-linux/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

I was supposed to be back in Cork right now but instead I’m staying an extra night in Paris and it’s all because I was using my Linux machine instead of my Mac at the airport…