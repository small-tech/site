---
title: "Comet"
date: 2021-12-11T11:16:00
categories: ["Updates"] # Updates or Labs
author: "Aral Balkan"
postURL: "https://ar.al/2021/12/11/comet/" # URL for the post
sourceURL: "https://ar.al/" # URL for the site the post is on
sourceName: "Aral’s blog" # Name of the site the post is on
---

Comet is a distraction-free Git commit message editor with spell-check, first line character limit warnings, and emoji support.

Comet version 1.0 is now available to install on elementary OS 6.
