---
title: "Success criteria for the PC 2.0 era"
date: 2019-01-09T15:01:11Z
categories: ["Updates", "Labs"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2019/01/09/success-criteria-for-the-pc-2.0-era/"
sourceURL: "https://ar.al/"
sourceName: "Aral’s blog"
---

1. Own and control your own identifiers.
2. No privileged nodes.
3. Effortlessly own and control your own always-on node on the Internet.