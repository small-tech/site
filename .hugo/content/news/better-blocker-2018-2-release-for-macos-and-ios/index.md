---
title: "Better Blocker 2018.2 release for macOS and iOS"
date: 2018-11-05T15:16:48Z
categories: ["Updates"]
author: "Aral Balkan"
image: "https://ar.al/2018/11/05/better-blocker-2018.2-release-for-macos-and-ios/better-2018.2-ios-macos.jpg"
postURL: "https://ar.al/2018/11/05/better-blocker-2018.2-release-for-macos-and-ios/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Laura and I are happy to announce that Better Blocker version 2018.2 is now available for purchase for iPhone, iPad, and Mac on the App Store and Mac App Store.