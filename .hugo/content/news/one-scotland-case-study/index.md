---
title: "Case Study – Small Technology Foundation"
date: 2021-05-11T10:01:26+01:00
categories: ["Updates"] # Updates or Labs
author: "Laura Kalbag" # Aral Balkan or Laura Kalbag (or another writer if article etc)
image: "" # file name relative to this page
postURL: "https://onescotland.org/nacwg-news/case-study-small-technology-foundation/" # URL for the post
sourceURL: "https://onescotland.org/nacwg-news/" # URL for the site the post is on
sourceName: "Scotland’s First Minister’s National Advisory Council on Women and Girls" # Name of the site the post is on
---

The organisation was co-founded by me, Laura Kalbag, and Aral Balkan. We started our work in 2014, aiming to create ethical and sustainable alternatives to the dominant “Big Tech” whose funding models usually rely on profiling people and monetising their personal data.