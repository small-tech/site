---
title: "Encouraging Individual Sovereignty and a Healthy Commons"
date: 2017-02-18T13:32:04+01:00
categories: ["Updates"]
author: "Aral Balkan"
image: ""
postURL: "https://2018.ar.al/notes/encouraging-individual-sovereignty-and-a-healthy-commons"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Mark Zuckerberg’s manifesto outlines his vision for a centralised global colony ruled by the Silicon Valley oligarchy. I say we must do the exact opposite and create a world with individual sovereignty and a healthy commons.

<!-- KEY TO FRONT MATTER:
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->