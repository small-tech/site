---
title: "Saying goodbye to Better Blocker"
date: 2021-12-15T19:56:00
categories: ["Updates"] # Updates or Labs
author: "Aral Balkan"
postURL: "https://better.fyi" # URL for the post
sourceURL: "https://better.fyi" # URL for the site the post is on
sourceName: "Better Blocker" # Name of the site the post is on
---

Better was a privacy tool for Safari on iPhone, iPad, and Mac. It protected you from behavioural ads and companies that track and profile you on the web.

Better was loved by over 40,000 people on iOS and 15,000 people on Mac. It passed away following a tragic bout of fruit poisoning.

Better is survived by two screeching voices of the minority, Laura Kalbag and Aral Balkan, working to create a better web in its memory.
