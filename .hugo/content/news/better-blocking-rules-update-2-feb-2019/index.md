---
title: "Better Blocking Rules Update 2nd February 2019"
date: 2019-02-05T10:39:23Z
categories: ["Updates"]
author: "Laura Kalbag"
image: ""
postURL: "https://better.fyi/news/"
sourceURL: "https://better.fyi"
sourceName: "the Better site"
---

New blocking rules! On 2nd February 2019, Laura fixed one broken site, blocked a blocker blocker on another site, and adtech on a third site.

To get the new rules, open the app on iOS, or choose ‘Update rules’ from the menu on macOS.
