---
title: "Better Blocking Rules Update 5th May 2019"
date: 2019-06-05T17:30:33+01:00
categories: ["Updates"]
author: "Laura Kalbag"
postURL: "https://better.fyi/news/"
sourceURL: "https://better.fyi"
sourceName: "the Better site"
---

New blocking rules! On 15th May 2019, Laura fixed two sites, and blocked four new trackers.

The rules will auto-update if the app is running. If it’s not, you can get the new rules by choosing ‘Update rules’ or ‘Check again now’ from Better’s menu.
