---
title: "Streaming HTML"
date: 2024-03-08T09:15:20
categories: ["Updates"] # Updates or Labs
author: "Aral Balkan"
postURL: "https://ar.al/2024/03/08/streaming-html/" # URL for the post
sourceURL: "https://ar.al/2024/03/08/streaming-html/" # URL for the site the post is on
sourceName: "Aral’s blog" # Name of the site the post is on
---

[Kitten](https://codeberg.org/kitten/app) has a new experimental workflow for creating web apps called Streaming HTML that Aral introduces you in this video and accompanying article.

Kitten, uniquely, enables you to build [Small Web](https://ar.al/2020/08/07/what-is-the-small-web/) apps ([peer-to-peer web apps](https://ar.al/2023/02/20/end-to-end-encrypted-kitten-chat/)). But it also aims to make creating any type of web app as easy as possible. The new Streaming HTML workflow is a big step in realising this goal.

