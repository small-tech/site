---
title: "Better Blocker for macOS Mojave"
date: 2018-09-24T18:10:33+01:00
categories: ["Updates"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2018/09/24/better-blocker-for-macos-mojave/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Today, we complete the roll out of the new design with the launch of the new Better Blocker for macOS. In line with the iOS app, we have also dropped the price to the lowest tier for the Mac app.

Even though the new apps are radical redesigns, we decided not to release a new app. If you already bought Better Blocker for iOS and/or Better Blocker for macOS, you can simply upgrade to the new apps via the App Store. Again, this is because we want to see Better in as many hands as possible. So we’d appreciate it if you could spread the word and tell your friends who don’t have Better yet.