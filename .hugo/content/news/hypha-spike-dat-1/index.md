---
title: "Hypha Spike: DAT 1"
date: 2019-01-14T14:54:28Z
categories: ["Labs"]
author: "Aral Balkan"
image: "https://ar.al/2019/01/14/hypha-spike-dat-1/replication.jpeg"
postURL: "https://ar.al/2019/01/14/hypha-spike-dat-1/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Following on from Hypha Spike: Diceware, this spike aims to explore:

- Creating an in-browser DAT data store using the keys generated in the previous spike
- Replicating that datastore over a web socket connection with the always-on node and making it available over UTP
