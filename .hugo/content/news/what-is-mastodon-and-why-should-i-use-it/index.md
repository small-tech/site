---
title: "What is Mastodon and why should I use it?"
date: 2018-09-05T17:04:26+01:00
categories: ["Updates", "Labs"]
author: "Laura Kalbag"
image: ""
postURL: "https://laurakalbag.com/what-is-mastodon-and-why-should-i-use-it/"
sourceURL: "https://laurakalbag.com"
sourceName: "Laura’s blog"
---

This post is a follow-up to What’s wrong with Twitter?.

Mastodon started out as a microblogging platform similar to Twitter, but has evolved with more features that show an ethical, progressive and inclusive focus. Instead of tweets, your posts on Mastodon are called toots.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->