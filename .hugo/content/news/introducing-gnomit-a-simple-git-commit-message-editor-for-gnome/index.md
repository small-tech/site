---
title: "Introducing Gnomit: a simple Git commit message editor for Gnome"
date: 2018-08-15T12:59:59+01:00
categories: ["Labs"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2018/08/15/introducing-gnomit-a-simple-git-commit-message-editor-for-gnome/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Gnomit is a simple Git commit message editor for Gnome, inspired by the excellent Komet app by Mayur Pawashe that I was using on macOS.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->