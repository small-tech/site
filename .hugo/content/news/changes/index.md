---
title: "Changes"
date: 2018-07-16T14:46:06+01:00
categories: ["Labs"]
author: "Aral Balkan"
image: "https://www.ar.al/2018/07/16/changes/current-setup.jpg"
sourceURL: "https://ar.al"
postURL: "https://www.ar.al/2018/07/16/changes/"
sourceName: "Aral’s blog"
---

For the last 12 years, my main development machine has been a Mac. As of last week, it’s a Dell XPS 13 running Pop!_OS 18.04.