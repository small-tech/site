---
title: "Surveillance Capitalism at the BBC"
date: 2018-12-14T15:49:28Z
categories: ["Updates"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2018/12/10/surveillance-capitalism-at-the-bbc/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Recently, I recorded a video on surveillance capitalism with BBC Ideas.

You can watch the video on the BBC Ideas web site and you can also embed it on your own site, as shown above…

But what happens when you add that code to your site?

Do you just get the video, as you would expect, or do you also get something you didn’t bargain for?