---
title: "Nodecert"
date: 2019-03-07T13:47:45Z
categories: ["Labs"]
author: "Aral Balkan"
postURL: "https://ar.al/2019/03/07/nodecert/"
sourceURL: "https://ar.al/"
sourceName: "Aral’s blog"
---

Nodecert is a Node.js module and command-line tool that automatically provisions locally-trusted TLS certificates for your development environment using mkcert.
