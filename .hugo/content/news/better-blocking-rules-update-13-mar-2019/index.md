---
title: "Better Blocking Rules Update 13th March 2019"
date: 2019-03-14T09:53:26Z
categories: ["Updates"]
author: "Laura Kalbag"
postURL: "https://better.fyi/news/"
sourceURL: "https://better.fyi"
sourceName: "the Better site"
---

New blocking rules! On 13th March 2019, Laura blocked one blocker blocker, and fixed two sites.

To get the new rules, choose ‘Update rules’ or ‘Check again now’ from Better’s menu.
