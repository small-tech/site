---
title: "Beyond my means"
date: 2018-07-17T14:00:45+01:00
categories: ["Updates"]
author: "Laura Kalbag"
image: ""
postURL: "https://laurakalbag.com/beyond-my-means/"
sourceURL: "https://laurakalbag.com"
sourceName: "Laura’s blog"
---

When I wrote about owning and controlling my own content, I talked about trying to keep my “content” in its canonical location on my site, and then syndicating it to social networks and other sites. Doing this involves cross-posting, something that can be done manually (literally copying and pasting titles, descriptions, links etc) or through automation. Either way, it’s a real faff. Posting to my site alone is a faff.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->