---
title: "Extended Codice Interview With Rai 1"
date: 2018-08-31T16:56:42+01:00
categories: ["Updates"]
author: "Aral Balkan"
image: ""
sourceURL: "https://ar.al"
postURL: "https://ar.al/2018/08/29/extended-codice-interview-with-rai-1/"
sourceName: "Aral’s blog"
---

I was recently featured in an episode1 of a technology show called Codice on Italian national television channel Rai 1. I just stumbled on an extended cut they released on surveillance-based video site YouTube. You can watch the segment here without being tracked.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->