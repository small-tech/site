---
title: "Better Blocking Rules Update 3rd November 2018"
date: 2018-11-03T16:23:05Z
categories: ["Updates"]
author: "Laura Kalbag"
image: ""
postURL: "https://forum.ind.ie/t/better-blocking-rules-update-20181103-001/2463"
sourceURL: "https://forum.ind.ie"
sourceName: "the Ind.ie forum"
---

New blocking rules! On 3rd November 2018, Laura fixed seven broken sites, blocked four blocker blockers, and blocked twenty-six new trackers.

To get the new rules, open the app on iOS, or choose ‘Update rules’ from the menu on macOS.