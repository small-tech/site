---
title: "Indie Web Server"
date: 2019-03-11T08:55:46Z
categories: ["Labs"]
author: "Aral Balkan"
image: "https://ar.al/2019/03/10/indie-web-server/indie-web-server.jpeg"
postURL: "https://ar.al/2019/03/10/indie-web-server/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Indie Web Server is a secure and seamless Small Tech personal web server.
