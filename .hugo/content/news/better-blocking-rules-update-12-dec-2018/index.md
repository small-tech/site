---
title: "Better Blocking Rules Update 12th December 2018"
date: 2018-12-12T15:47:50Z
categories: ["Updates"]
author: "Laura Kalbag"
image: ""
postURL: "https://forum.ind.ie/t/better-blocking-rules-update-20181212/2532"
sourceURL: "https://forum.ind.ie"
sourceName: "the Ind.ie forum"
---

New blocking rules! On 12th December 2018, Laura fixed two broken sites, blocked anti-tracking adtech on two sites, and blocked six new trackers.