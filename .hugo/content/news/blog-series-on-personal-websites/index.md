---
title: "Blog series on personal websites"
date: 2018-06-20T15:11:26+01:00
categories: ["Updates"]
author: "Laura Kalbag"
image: ""
postURL: "https://forum.ind.ie/t/blog-series-on-personal-websites/2230"
sourceURL: "https://forum.ind.ie"
sourceName: "the Ind.ie forum"
---

I’ve been getting more into blogging again, starting with a series on personal websites, my relationship with my own, and how they are valuable in a world of surveillance capitalism…

<!-- KEY TO FRONT MATTER:
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog
-->