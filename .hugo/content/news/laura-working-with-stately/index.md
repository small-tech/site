---
title: "Working with Stately"
date: 2021-07-06T08:06:33+01:00
categories: ["Updates"] # Updates or Labs
author: "Laura Kalbag" # Aral Balkan or Laura Kalbag (or another writer if article etc)
image: "" # file name relative to this page
postURL: "https://laurakalbag.com/working-with-stately/" # URL for the post
sourceURL: "https://laurakalbag.com" # URL for the site the post is on
sourceName: "Laura’s blog" # Name of the site the post is on
---

From today, in addition to my work at Small Technology Foundation, I’ll be working with Stately on developer and designer relations as a contractor.