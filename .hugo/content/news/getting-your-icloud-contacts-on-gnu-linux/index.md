---
title: "Getting your iCloud contacts on GNU/Linux"
date: 2018-08-05T12:53:38+01:00
categories: ["Labs"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2018/08/05/getting-your-icloud-contacts-on-gnu-linux/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Just like you can use iCloud calendars on GNU/Linux, you can also synchronise your contacts as iCloud uses an open standard called CardDAV.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->