---
title: "We forced 9 privacy experts to say “I told you so” about Facebook – hopefully we’ll listen next time"
date: 2018-04-11T14:17:36+01:00
categories: ["Updates"]
author: "Alejandro Tauber"
image: ""
postURL: "https://thenextweb.com/facebook/2018/04/11/forced-9-privacy-experts-say-told-facebook-hopefully-well-listen-next-time/"
sourceURL: "https://thenextweb.com"
sourceName: "The Next Web"
---

Aral Balkan, digital activist: Yes, I’ve been warning about the dangers of surveillance capitalism and data collection by the likes of Google, Facebook and the entire adtech industry for years. In fact, here, “have a cookie” because you’re being tracked by those same companies as you read this very article on The Next Web. (Unless you have a tracker blocker active, that is.) No, there’s no pleasure in being right about this. Especially when this toxic business model is so ubiquitous that you cannot even read about it without being exposed to it.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->