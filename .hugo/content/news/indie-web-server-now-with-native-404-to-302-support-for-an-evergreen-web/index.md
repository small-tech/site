---
title: "Indie Web Server: now with native 404 to 302 support for an evergreen web"
date: 2019-03-31T18:45:34+01:00
categories: ["Labs"]
author: "Aral Balkan"
image: "https://ar.al/2019/03/31/indie-web-server-now-with-native-404-to-302-support-for-an-evergreen-web/4042302.png"
postURL: "https://ar.al/2019/03/31/indie-web-server-now-with-native-404-to-302-support-for-an-evergreen-web/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

What if links never died? What if we never broke the Web? What if it didn’t involve any extra work? It’s possible. And easy. Just make your 404s into 302s.

Indie Web Server now has native support for 404 to 302.
