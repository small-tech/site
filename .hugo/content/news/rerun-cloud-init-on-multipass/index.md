---
title: "Rerun cloud-init on multipass"
date: 2019-06-15T10:40:45+01:00
categories: ["Labs"]
author: "Aral Balkan"
postURL: "https://ar.al/2019/06/15/rerun-cloud-init-on-multipass/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Today, I had the need to experiment with rerunning cloud-init on a virtual machine created with multipass. You can use cloud-init with multipass by specifying a cloud-init.yaml file when creating your instance. e.g,

`multipass launch --name my-instance --cloud-init ./cloud-init.yaml`
This is all well and good and works as you would expect. However, today, I wanted to experiment with running cloud-init on an already-provisioned instance.
