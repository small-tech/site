---
title: "New RSS Feeds on the Ind.ie Site"
date: 2018-08-29T11:25:47+01:00
categories: ["Updates", "Labs"]
author: "Laura Kalbag"
image: ""
postURL: "https://forum.ind.ie/t/new-rss-feeds-on-the-ind-ie-site/2321"
sourceURL: "https://forum.ind.ie/"
sourceName: "the Ind.ie forum"
---

TLDR;

We now have usable RSS feeds!

- RSS feed for Ind.ie News (everything: updates and labs)
- RSS feed for Ind.ie Updates (posts about what we’re doing and why)
- RSS feed for Ind.ie Labs (more “technical” posts)
- RSS feed for the Ind.ie Radar (news items relevant to Ind.ie interests)

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->