---
title: "Interview With Shannon Fisher About Accessibility for Everyone"
date: 2018-08-14T12:42:46+01:00
categories: ["Updates"]
author: "Laura Kalbag"
image: ""
postURL: "https://laurakalbag.com/interview-with-shannon-fisher/"
sourceURL: "https://laurakalbag.com"
sourceName: "Laura’s blog"
---

A while back I had a lovely evening chatting with the fabulous Shannon Fisher about Accessibility For Everyone. Shannon asks really thoughtful questions, and it was fun chatting to someone who already cares so much about inclusivity.

<!-- KEY TO FRONT MATTER:
    category = either Updates or Labs so people can choose whether they want technical posts
    author = who wrote the original post
    image = URL of feature image from original post (for use on social media etc)
    postURL = URL of original post
    sourceURL = URL of original site
    sourceName = Name of site of original post. E.g. Aral’s blog, the Ind.ie forum, Laura’s blog
-->