---
title: "Hypha Spike: Multiwriter 1"
date: 2019-01-22T14:49:40Z
categories: ["Labs"]
author: "Aral Balkan"
image: ""
postURL: "https://ar.al/2019/01/22/hypha-spike-multiwriter-1/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

Following on from Hypha Spike: WebRTC 1 and Hypha Spike: DAT 1, this spike aims to explore:

- Explore the current state of multiwriter in the DAT world.
