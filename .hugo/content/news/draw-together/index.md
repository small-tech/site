---
title: "Draw Together"
date: 2024-03-26T09:19:25
categories: ["Updates"] # Updates or Labs
author: "Aral Balkan"
postURL: "https://ar.al/2024/03/26/draw-together/" # URL for the post
sourceURL: "https://ar.al/2024/03/26/draw-together/" # URL for the site the post is on
sourceName: "Aral’s blog" # Name of the site the post is on
---

[Draw Together](https://draw-together.small-web.org) is a little collaborative drawing toy made with [Kitten](https://codeberg.org/kitten/app) on a Saturday night.

You can play with it on the page and watch a video where Aral takes you through the making of Draw Together, showcasing Kitten’s Streaming HTML workflow and native WebSocket features. 
