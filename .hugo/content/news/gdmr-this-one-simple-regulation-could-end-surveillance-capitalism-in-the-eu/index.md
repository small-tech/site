---
title: "GDMR: this one simple regulation could end surveillance capitalism in the EU"
date: 2018-12-05T17:25:22Z
categories: ["Updates"]
author: "Aral Balkan"
image: "https://ar.al/2018/11/29/gdmr-this-one-simple-regulation-could-end-surveillance-capitalism-in-the-eu/gdmr.png"
postURL: "https://ar.al/2018/11/29/gdmr-this-one-simple-regulation-could-end-surveillance-capitalism-in-the-eu/"
sourceURL: "https://ar.al"
sourceName: "Aral’s blog"
---

No, you didn’t misread it and, no, it’s not a typo. GDMR – the General Data Minimisation Regulation – can end surveillance capitalism in the EU.

The problem is that no such regulation exists.

So, let’s change that, starting now.