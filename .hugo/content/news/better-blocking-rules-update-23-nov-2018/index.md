---
title: "Better Blocking Rules Update 23rd November 2018"
date: 2018-11-23T17:26:00Z
categories: ["Updates"]
author: "Laura Kalbag"
image: ""
postURL: "https://forum.ind.ie/t/better-blocking-rules-update-20181123-001/2504"
sourceURL: "https://forum.ind.ie"
sourceName: "the Ind.ie forum"
---

New blocking rules! On 23rd November 2018 Laura fixed seven broken sites, blocked three blocker blockers, and blocked twenty-three new trackers.

To get the new rules, open the app on iOS, or choose ‘Update rules’ from the menu on macOS.