---
title: "Legal and Privacy"
date: 2018-09-13T14:15:40+01:00
summary: "Version 2021.1, as published on 17th June, 2021. We exist to protect your privacy"
---

# Privacy

Version 2021.1, as published on 17th June, 2021.

**We exist to protect your privacy.**

Our privacy policy is simple (and will stay that way). As this web site is open source (and all of its content released under a Creative Commons Attribution License), you can also [see a history of all changes](https://source.ind.ie/small-tech.org/site/tree/master/content/privacy).<!--more-->

## In general

We do not track you. We try our best to [minimise](https://ar.al/2018/11/29/gdmr-this-one-simple-regulation-could-end-surveillance-capitalism-in-the-eu/) the data we gather. We use self-hosted free and open tools whenever possible and we do not share your data with third parties.

Privacy is a fundamental human right that your other rights depend on. We exist to create small technology that protects your human rights. So, in a nutshell, our privacy policy is that we exist to protect your privacy.

## Videos

MP4 and Flash formatted videos are hosted on Vimeo using our own custom video player. We do not include Vimeo’s analytics in our video player.

## Analytics

Our site runs on [Site.js](https://sitejs.org). The only statistics Site.js gathers are ephemeral aggregate counts of pages visited on the site. We glance at this information on occasion to see if there are any broken pages and to get a feeling for which pages are most popular.

## Stripe

We use Stripe to accept payments. We do not store any of your payment-related information, including your email address. If you become a patron, you get your own patronage page at a unique address where you can update, pause, and cancel your patronage all without us storing any of your data ourselves. The data held by Stripe to process your donations and patronages is covered by [Stripe’s Privacy Policy](https://stripe.com/privacy).

## New: Email newsletter

We use Buttondown to handle the Small is Beautiful email reminders. Your email address is stored with Buttondown, who has a [privacy-first approach](https://buttondown.email/features/privacy). We do not use any tracking in the emails, including link tracking. Every month we’ll send you one reminder for the next episode of Small is Beautiful, the Small Technology Foundation livestream, so you can watch the stream live. That’s all we’ll send you! No spam or any other nonsense, and we will never use your email address for anything else.

## Trademarks

Small Technology Foundation, the Small Technology Foundation logo, Site.js, the Site.js logo, Tincan, and the Tincan logo, and Better and the Better logo are trademarks of Small Technology Foundation and copyright of Small Technology Foundation, all rights reserved. Please do not use our trademarks without asking permission first.

## Licenses

Everything we create is free and open source.

All content on the Small Technology Foundation web site, excluding our trademarks and unless otherwise stated, is licensed under [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).
