---
title: "Contact Us"
date: 2019-06-12T09:39:44+01:00
menu:
 main:
  weight: 8
summary: "We’re available for speaking engagements, media interviews about our work, and to chat to VPS hosts about working together."
---

# Contact Us

We’re available for speaking engagements and media interviews about our work.

## How to reach us

### Via Email

Email us at [hello@small-tech.org](mailto:hello@small-tech.org).

### Fediverse

  - [Laura’s Mastodon](https://mastodon.laurakalbag.com/@laura)
  - [Aral’s Mastodon](https://mastodon.ar.al/@aral)

## Follow us

  - [Small Technology Foundation News (RSS)](/index.xml)
  - [Laura’s blog (RSS)](https://laurakalbag.com/index.xml)
  - [Aral’s blog (RSS)](https://ar.al/index.xml)

{{< fund-us >}}
