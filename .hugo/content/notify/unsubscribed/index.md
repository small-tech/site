---
title: "Unsubscribed"
---

<!-- this page is where someone is sent when they unsubscribe from the buttondown Small is Beautiful notification emails -->

# You’ve been unsubscribed

You’ve been unsubscribed from Small is Beautiful notification emails.

You won’t get any further emails from us.

### Did you unsubscribe by accident? Sign up again

{{< notify >}}

{{< fund-us >}}