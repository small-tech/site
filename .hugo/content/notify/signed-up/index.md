---
title: "Signed Up"
---

<!-- this page is where someone is sent when they sign up for the buttondown Small is Beautiful notification emails (but may not have yet confirmed their email) -->

# You're almost subscribed!

<div class='description'>
  <p>You're almost subscribed to the Small is Beautiful email notifications</p>

  <p>You’ve been sent an email to confirm your address.</p>

  <p><strong>Click the confirmation link inside and you’ll be all signed up</strong>.</p>

  <h2>Signed up by accident?</h2>

  <p>Just don’t click the confirmation link and you won’t be signed up for emails.</p>

</div>

{{< fund-us >}}

<script>
  // if the url has /?confirmed=true, then change the content accordingly
  var h1 = document.querySelector('h1');
  var summary = document.querySelector('.description');
  var urlSearch = window.location.search;
  if (urlSearch == '?confirmed=true') {
    h1.innerText = 'Subscribed!';
    summary.innerHTML = '<p>Every month we’ll send you a reminder for the next episode of Small is Beautiful, the Small Technology Foundation livestream, so you can watch the stream live.</p>';
  }
</script>